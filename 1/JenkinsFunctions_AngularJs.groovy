// Below methods are used to build and test the AngularJs code 
def buildMethod() {
    println('AngularJSBuildMethod enter');
	sh 'npm install';
    //sh 'npm install -g @angular/cli@7.3.8';
 	sh 'npm install -g @angular/cli@11.0.6';
    println('AngularJsBuildMethod exit');
}
def testMethod() {
    println('AngularTestMethod enter');
	sh 'npm i -D puppeteer karma-chrome-launcher';
	//sh 'process.env.CHROME_BIN = require('puppeteer').executablePath()'
    sh '''
  	npm install -g webdriver-manager
	npm i chrome-launcher
	#node_modules/protractor/bin/webdriver-manager update
	npm install -g @angular/cli@7.3.9
	#apk add chromium
	#apt-get upgrade
	#apt-get install -y chromium
	#apk add chromium-chromedriver
	#export CHROME_BIN='/usr/bin/chromium-browser'
	#ng test --watch=false
    '''
    //sh 'ng test' ;
    println('AngularJsTestMethod exit');
}

def sonarMethod() {
	println('Sonar Method enter');
    def scannerHome = tool 'Sonar Scanner';
    sh "${scannerHome}/bin/sonar-scanner -Dsonar.login=$USERNAME -Dsonar.password=$PASSWORD";
	println('Sonar Method exit');
	
}

return this // this is important to return
