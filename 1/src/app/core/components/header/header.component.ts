import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IsAuthenticatedService } from 'src/app/services/isAuthenticated.service';
import {SessionUtil} from "../../util/sessionUtil";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLogin?: boolean;

  constructor(private router: Router,
    private loginService: IsAuthenticatedService) { }

  ngOnInit(): void {

  }

  Singpass() {
    SessionUtil.clearSession()
    this.router.navigate(['/singpass-login']);
  }

  hasRoute(route: string) {
    return this.router.url.includes(route);
  }
}
