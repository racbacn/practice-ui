import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FillRoutingModule } from './fill-routing.module';
import { FillComponent } from './fill.component';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
@NgModule({
  declarations: [
    FillComponent
  ],
  imports: [
    CommonModule,
    FillRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    WidgetsModule
  ],
  exports: [
    FillComponent,
  ],
})
export class FillModule { }
