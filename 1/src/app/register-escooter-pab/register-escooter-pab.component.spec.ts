import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterEscooterPabComponent } from './register-escooter-pab.component';

describe('RegisterEscooterPabComponent', () => {
  let component: RegisterEscooterPabComponent;
  let fixture: ComponentFixture<RegisterEscooterPabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterEscooterPabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterEscooterPabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
