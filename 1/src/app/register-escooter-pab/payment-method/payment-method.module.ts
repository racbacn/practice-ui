import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentMethodComponent } from './payment-method.component';
import { PaymentMethodRoutingModule } from './payment-method-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';


@NgModule({
  declarations: [
    PaymentMethodComponent
  ],
  imports: [
    CommonModule,
    PaymentMethodRoutingModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    WidgetsModule
  ],
  exports: [
    PaymentMethodComponent,
    FlexLayoutModule]
})
export class PaymentMethodModule { }
