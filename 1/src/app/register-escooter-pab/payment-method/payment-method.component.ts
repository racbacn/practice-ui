import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';
import { HttpServicesService } from 'src/app/services/http-services.service';
import { EscooterRegistration } from 'src/app/interface/escooter-registration';
import { serverUrl } from 'src/app/constants/sharedValue';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss'],
})
export class PaymentMethodComponent implements OnInit {

  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();

  cancelButtonConfig = {
    buttonText: 'Cancel',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };

  panelItems: any[] = [
    {
      "title": "Item",
      "info": "Reg of Veh 20190808100445732637"
    },
    {
      "title": "Amount Before GST",
      "info": "S$20.00"
    },
    {
      "title": "GST Amount",
      "info": "S$0.00"
    },
    {
      "title": "Amount After GST",
      "info": "S$20.00"
    }
  ];
  paymentMethod: any = {};

  escooterRegForm!: EscooterRegistration;
  apiUrl: string = serverUrl;
  constructor(private sharedService: SharedService, private httpServices: HttpServicesService) { }

  ngOnInit(): void {
    this.sharedService.getEscooterRegForm().subscribe(res => {
      this.escooterRegForm = res;
    })
  }

  clickCancel() {
    this.onPreviousStepper.emit();
  }

  clickPay() {
    let datePipe = new DatePipe('en-US');
    let paymentPayload = {
      "requestHeader": {
        "requestAppId": "VRLS_Make_Payment",
        "requestDateTime": datePipe.transform(new Date(), 'yyyy-MM-ddThh:mm:SS'),
        "userId": this.escooterRegForm?.ownerDetails?.ownerId
      },
      "transactionId": "",
      "transactionRefNum": "",
      "paymentType": "Registration_PMD",
      "paymentAmt": 20,
      "paymentMethod": this.paymentMethod
    }

    console.log(this.paymentMethod)
    if (this.escooterRegForm) {
      this.httpServices.httpPost(this.apiUrl + "/manage-pmd-service/registerPMD", this.escooterRegForm).subscribe(res => {
        this.sharedService.sendEscooterRegResponse(res)
        if(res['transactionId']) {
          paymentPayload.transactionId= res['transactionId'];
        }
        if(res['transactionRefNum']) {
          paymentPayload.transactionRefNum= res['transactionRefNum'];
        }

        console.log(paymentPayload)
        this.httpServices.httpPost(this.apiUrl + "/manage-payment-service/makePayment", paymentPayload).subscribe(res => {
          this.sharedService.sendEscooterRegPaymentResp(res)
          this.clickNext();
        }, err => {
          console.log(err);
        })
      }, err => {
        console.log(err);
      })
    } else {
      console.log("empty ", this.escooterRegForm)
    }
  }

  clickNext() {
    this.onNextStepper.emit(true);
  }
}
