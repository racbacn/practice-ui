import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewPaymentComponent } from './review-payment.component';
const routes: Routes = [
  { path: '', component: ReviewPaymentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewPaymentRoutingModule { }
