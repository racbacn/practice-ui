import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewPaymentComponent } from './review-payment.component';
import { ReviewPaymentRoutingModule } from './review-payment-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
@NgModule({
  declarations: [
    ReviewPaymentComponent
  ],
  imports: [
    CommonModule,
    ReviewPaymentRoutingModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    WidgetsModule
  ],
  exports: [
    ReviewPaymentComponent,
  ],
})
export class ReviewPaymentModule { }
