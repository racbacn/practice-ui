import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { EscooterRegistration } from 'src/app/interface/escooter-registration';
import { SharedService } from 'src/app/services/shared.service';
@Component({
  selector: 'app-review-payment',
  templateUrl: './review-payment.component.html',
  styleUrls: ['./review-payment.component.scss']
})
export class ReviewPaymentComponent implements OnInit, OnDestroy {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();
  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };
  reviewDeclarationInfo: string = "Please read the declaration details here.";
  reviewReadAgreeInfo: string = "I/We declare that I/We have read and understood the information in the declaration, and understake to abide by it.";
  escooterDeclared : string = "You have declared that the E-scooter meets the following criteria:"
  escooterCriteriaList = [
    "The document format should be in PDF or image file format (e.g jpg)",
    "The filename contains only letters, numbers and underscore, without spaces or other special characters",
    "The filename should have at most 33 characters"];
  submitCheckBox: boolean = false;

  //Create the label for the Review Page
  labelExpansionDetails = [
    { headerName: 'YOUR DETAILS', labelDetails: [''], labelInfo: ['ID Type', 'ID', 'Name', 'Birth Date', 'Mobile No', 'Email Address', 'Address', 'Unit No.', 'Postal Code'], },
    { headerName: 'E-SCOOTER PARTICULARS', labelDetails: [''], labelInfo: ['Make', 'Model', '*No. of Wheels', '*Colour', 'Seat', 'Safety Standard Serial No.', 'E-scooter Safety Standard Sticker', 'Photo of E-scooter'], },
    { headerName: 'PAYMENT SUMMARY', labelDetails: [], labelInfo: ['E-scooter Registration Fee', 'Total Amount Payable'], },

  ];

  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "YOUR DETAILS": "ownerDetails",
    "E-SCOOTER PARTICULARS": "pmdDetails",
    "PAYMENT SUMMARY": "paymentDetails",
    "ID Type": "ownerIdType",
    "ID": "ownerId",
    "Name": "ownerName",
    "Birth Date": "ownerDOB",
    "Mobile No": "ownerContactNum",
    "Email Address": "ownerEmailAddress",
    "Postal Code": "ownerPostalCode",
    "Address": "ownerAddress",
    "Unit No.": "ownerAddressUnitNo",
    "Make": "pmdMake",
    "Model": "pmdModel",
    "*No. of Wheels": "pmdWheelsNum",
    "*Colour": "pmdColour",
    "Seat": "hasSeat",
    "Safety Standard Serial No.": "safetyStandardSerialNum",
    "E-scooter Safety Standard Sticker": "safetyStandardStickerImage",
    "Photo of E-scooter": "pmdImage",
    "E-scooter Registration Fee": "eScooterRegFee",
    "Total Amount Payable": "eScooterRegTotalFee",
  };
  regEscooterSub: Subscription = new Subscription;

  constructor(private sharedService: SharedService) { }

  ngOnInit(): void {
    this.regEscooterSub = this.sharedService.getEscooterRegForm().subscribe(res => {
      // this.loadItems(this.escootRegistration)
      this.loadItems(res)

    })
  }

  ngOnDestroy() {
    this.regEscooterSub.unsubscribe();
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
            //case to replace the documentID with the filename
            if(labelinfo == "E-scooter Safety Standard Sticker" ) {
              this.labelExpansionDetails[i].labelDetails.push(inputData['pmdRegistrationDocuments'][0]['fileName']);

            } else if (labelinfo == "Photo of E-scooter") {
              this.labelExpansionDetails[i].labelDetails.push(inputData['pmdRegistrationDocuments'][1]['fileName']);

            }else {
              this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);

            }
          }  else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
    this.labelExpansionDetails[this.labelExpansionDetails.length-1].labelDetails=['S$20','S$20',]
  }


  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }
  clickNext() {
    this.onNextStepper.emit(true);
  }
}
