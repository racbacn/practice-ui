import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterEscooterPabRoutingModule } from './register-escooter-pab-routing.module';
import { RegisterEscooterPabComponent } from './register-escooter-pab.component';
import { MaterialModule } from '../material.module';
import { FillModule } from './fill/fill.module';
import { ReviewPaymentModule } from './review-payment/review-payment.module';
import { PaymentMethodModule } from './payment-method/payment-method.module';
import { PaymentAcknowledgeModule } from './payment-acknowledge/payment-acknowledge.module';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    RegisterEscooterPabComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RegisterEscooterPabRoutingModule,
    FillModule,
    ReviewPaymentModule,
    PaymentMethodModule,
    PaymentAcknowledgeModule,
    FlexLayoutModule,
  ],
  exports: [
    FlexLayoutModule,
    MaterialModule,
  ],
})
export class RegisterEscooterPabModule { }
