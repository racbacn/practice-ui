import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-register-escooter-pab',
  templateUrl: './register-escooter-pab.component.html',
  styleUrls: ['./register-escooter-pab.component.scss'],
})
export class RegisterEscooterPabComponent implements OnInit {
  @ViewChild('stepper')
  stepper!: MatStepper;

  isLinear = false;
  registerHeading : string = " Register e-scooter/PAB"
  constructor() { }

  ngOnInit(): void {
  }

  allowToProceed(a: any) {
    if (a) {
      this.stepper.next();
    } else {
      console.log('wrong opp');
    }
  }

  onBack() {
    this.stepper.previous();
  }
}
