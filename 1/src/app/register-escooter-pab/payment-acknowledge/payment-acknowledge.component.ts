import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/services/shared.service';
@Component({
  selector: 'app-payment-acknowledge',
  templateUrl: './payment-acknowledge.component.html',
  styleUrls: ['./payment-acknowledge.component.scss'],
})
export class PaymentAcknowledgeComponent implements OnInit {
  mock = {
    "responseHeader": {
      "responseAppId": "Payment_Success",
      "responseDateTime": "2021-09-14T19:16:42"
    },
    "paymentReceipt": "VRTRX-Receipt-20210914191642",
    "transactionId": 1,
    "paymentType": "Registration_CAR",
    "paymentAmt": 81801.0,
    "paymentSuccess": true
  }

  saveButtonConfig = {
    buttonText: 'Save as PDF',
    buttonClassName: 'Button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };
  returnButtonConfig = {
    buttonText: 'Return to Home',
    buttonClassName: 'Button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };
  escooterDeclared: string = "You have declared that the E-scooter meets the following criteria:"
  escooterCriteriaList = [
    "The document format should be in PDF or image file format (e.g jpg)",
    "The filename contains only letters, numbers and underscore, without spaces or other special characters",
    "The filename should have at most 33 characters"];
  //Create the label for the Review Page
  labelExpansionDetails = [
    { headerName: 'YOUR DETAILS', labelDetails: [''], labelInfo: ['ID Type', 'ID', 'Name'], },
    { headerName: 'E-SCOOTER PARTICULARS', labelDetails: [''], labelInfo: ['E-scooter no.', 'Make', 'Model', 'Type', 'No. of Wheels', 'Colour', 'Seat', 'Safety Standard Serial No.'], },
    { headerName: 'TRANSACTION DETAILS', labelDetails: [], labelInfo: ['Transaction Ref. No.', 'Transaction Date', 'Transaction Time'], },
    { headerName: 'Message', labelDetails: [], labelInfo: [], },

  ];

  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "YOUR DETAILS": "ownerDetails",
    "E-SCOOTER PARTICULARS": "pmdDetails",
    "TRANSACTION DETAILS": "transactionDetails",
    "ID Type": "ownerIdType",
    "ID": "ownerId",
    "Name": "ownerName",
    "E-scooter no.": "escooterNum",
    "Make": "pmdMake",
    "Model": "pmdModel",
    "No. of Wheels": "pmdWheelsNum",
    "Colour": "pmdColour",
    "Seat": "hasSeat",
    "Safety Standard Serial No.": "safetyStandardSerialNum",
    "Transaction Ref. No.": "transactionRefNum",
    "Transaction Date": "transactionDate",
    "Transaction Time": "transactionTime",

  };

  ownID: string = "";
  scootNo: string = "";
  transactionDate: string = "";
  threeDays: any = "";
  fourteenDays: any = "";

  regEscooterSub: Subscription = new Subscription;

  regEscooterForm: any = {}
  regEscooterResp: any = {}
  paymentResp: any = {}
  finalObject: any = {
    "ownerDetails": {},
    "pmdDetails": {},
    "transactionDetails": {},
  }
  constructor(private sharedService: SharedService, private router: Router) {
    this.regEscooterSub = this.sharedService.getEscooterRegForm().subscribe(res => {
      this.regEscooterForm = res;
      this.ownID = this.regEscooterForm?.ownerDetails?.ownerId
    });

    let eScooterRegRespSub = this.sharedService.getEscooterRegResponse().subscribe(res => {
      this.regEscooterResp = res;
      this.scootNo = this.regEscooterResp?.escooterNum;
      this.transactionDate = this.regEscooterResp?.transactionDate;
      if (this.transactionDate) {
        let dateConvert= new DatePipe('en-US');

        let tempDate = new Date(this.transactionDate)
        this.threeDays = dateConvert.transform(tempDate.setDate(tempDate.getDate() + 3), 'dd MMM yyyy');
        this.fourteenDays = dateConvert.transform(tempDate.setDate(tempDate.getDate() + 14), 'dd MMM yyyy');
      }

    });

    let payResSub = this.sharedService.getEscooterRegPaymentResp().subscribe(res => {
      this.paymentResp = res;
      this.aggregateRegFormRespPayData();
      console.log(this.finalObject)
    })

    this.regEscooterSub.add(eScooterRegRespSub);
    this.regEscooterSub.add(payResSub);
  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this.regEscooterSub.unsubscribe();
  }

  aggregateRegFormRespPayData() {

    this.finalObject.ownerDetails = this.regEscooterForm["ownerDetails"];

    this.finalObject.pmdDetails = this.regEscooterForm["pmdDetails"];
    if (this.finalObject.pmdDetails) {
      this.finalObject.pmdDetails.escooterNum = this.regEscooterResp.escooterNum;
    }

    this.finalObject.transactionDetails = this.regEscooterResp

    this.loadItems(this.finalObject);
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
            this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
          } else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
  }

  goHome() {
    //Go to Home Page
    this.router.navigate(['/owner-dashboard']);
  }
}
