import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentAcknowledgeComponent } from './payment-acknowledge.component';
import { PaymentAcknowledgeRoutingModule } from './payment-acknowledge-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { PaymentMessageComponent } from './payment-message/payment-message.component';


@NgModule({
  declarations: [
    PaymentAcknowledgeComponent,
    PaymentMessageComponent
  ],
  imports: [
    CommonModule,
    PaymentAcknowledgeRoutingModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    WidgetsModule
  ],
  exports: [
    PaymentAcknowledgeComponent,
    FlexLayoutModule,
    PaymentMessageComponent
  ],
})
export class PaymentAcknowledgeModule { }
