import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentAcknowledgeComponent } from './payment-acknowledge.component';

const routes: Routes = [
  { path:'', component: PaymentAcknowledgeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentAcknowledgeRoutingModule { }
