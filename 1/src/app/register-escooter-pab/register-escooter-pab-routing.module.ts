import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterEscooterPabComponent } from './register-escooter-pab.component';

const routes: Routes = [{
  path: '', component: RegisterEscooterPabComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterEscooterPabRoutingModule { }
