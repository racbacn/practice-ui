import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SelectRenewalComponent } from './select-renewal.component';

const routes: Routes = [{
  path: '', component: SelectRenewalComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SelectRenewalRoutingModule { }
