import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectRenewalRoutingModule } from './select-renewal-routing.module';
import { SelectRenewalComponent } from './select-renewal.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    SelectRenewalComponent
  ],
  imports: [
    CommonModule,
    SelectRenewalRoutingModule,
    WidgetsModule,
    FormsModule
  ],
  exports: [
    SelectRenewalComponent,
  ]
})
export class SelectRenewalModule { }
