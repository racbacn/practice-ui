import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectRenewalComponent } from './select-renewal.component';

describe('SelectRenewalComponent', () => {
  let component: SelectRenewalComponent;
  let fixture: ComponentFixture<SelectRenewalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectRenewalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectRenewalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
