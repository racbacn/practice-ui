import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {unwatchFile} from 'fs';
import {SharedService} from 'src/app/services/shared.service';
import {VehicleRoadTaxRenewal} from "../../interface/vehicle-road-tax-renewal";
import * as moment from "moment";

@Component({
  selector: 'app-select-renewal',
  templateUrl: './select-renewal.component.html',
  styleUrls: ['./select-renewal.component.scss']
})
export class SelectRenewalComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',
  };

  vehicleRoadTaxRenewal!: VehicleRoadTaxRenewal;
  extendedDate!: string;

  roadTaxMonthList: Array<string> = ['6 months renewal', '12 months renewal']

  bTrafficFine: boolean = true;
  bVehicleInsurance: boolean = true;
  bVehicleInspectAIC: boolean = true;

  constructor(private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.onInitVehicleRoadTaxRenewal();
  }

  onInitVehicleRoadTaxRenewal() {
    this.sharedService.getRenewalRoadTax().subscribe(data => {
      let vehicleRoadTaxRenewal: VehicleRoadTaxRenewal = data;

      if (vehicleRoadTaxRenewal) {
        this.vehicleRoadTaxRenewal = vehicleRoadTaxRenewal;
      }
    });
  }

  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }

  onSelectRenewalMonths(index: number) {
    let monthsAdded: number = 0;

    switch (index) {
      case this.roadTaxMonthList.indexOf('6 months renewal'): {
        monthsAdded = 6;
        break;
      }
      case this.roadTaxMonthList.indexOf('12 months renewal'): {
        monthsAdded = 12;
        break;
      }
      default: {
        break;
      }
    }
    this.extendedDate = moment(this.vehicleRoadTaxRenewal.expiryDate, 'DD-MM-YYYY').add(1, "day").add(monthsAdded, "months").format('DD-MM-YYYY').toString();
  }

  clickNext() {
    if (this.vehicleRoadTaxRenewal && this.extendedDate) {
      let data = {vehicleRoadTaxRenewal: this.vehicleRoadTaxRenewal, extendedDate: this.extendedDate}
      this.sharedService.sendRenewalRoadTaxWithNewExpiry(data);
      this.onNextStepper.emit(true);
    }
  }
}
