import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RenewRoadTaxRoutingModule } from './renew-road-tax-routing.module';
import { RenewRoadTaxComponent } from './renew-road-tax.component';
import { FillDetailsModule } from './fill-details/fill-details.module';
import { SelectRenewalModule } from './select-renewal/select-renewal.module';
import { ReviewModule } from './review/review.module';
import { MakePaymentModule } from './make-payment/make-payment.module';
import { AcknowledgeModule } from './acknowledge/acknowledge.module';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from '../widgets/widgets.module';

@NgModule({
  declarations: [
    RenewRoadTaxComponent
  ],
  imports: [
    CommonModule,
    RenewRoadTaxRoutingModule,
    FillDetailsModule,
    SelectRenewalModule,
    ReviewModule,
    MakePaymentModule,
    AcknowledgeModule,
    MaterialModule,
    FlexLayoutModule,
    WidgetsModule,
  ]
})
export class RenewRoadTaxModule { }
