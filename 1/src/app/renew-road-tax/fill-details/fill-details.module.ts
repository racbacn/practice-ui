import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { FillDetailsRoutingModule } from './fill-details-routing.module';
import { FillDetailsComponent } from './fill-details.component';
import { WidgetsModule } from '../../widgets/widgets.module';

@NgModule({
  declarations: [
    FillDetailsComponent
  ],
  imports: [
    CommonModule,
    FillDetailsRoutingModule,
    WidgetsModule,
    MaterialModule,
    FormsModule
  ],
  exports:[
    FillDetailsComponent
  ]
})
export class FillDetailsModule { }
