import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SharedService} from 'src/app/services/shared.service';
import {Location} from '@angular/common';
import {ManageVehicleService} from "../../services/manage-vehicle.service";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {VehicleRoadTaxRenewalRequest} from "../../interface/vehicle-road-tax-renewal-request";
import {VehicleRoadTaxRenewal} from "../../interface/vehicle-road-tax-renewal";
import {VehicleRoadTaxRenewalResponse} from "../../interface/vehicle-road-tax-renewal-response";

@Component({
  selector: 'app-fill-details',
  templateUrl: './fill-details.component.html',
  styleUrls: ['./fill-details.component.scss']
})
export class FillDetailsComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

  vehicleRoadTaxRenewalList!: Array<VehicleRoadTaxRenewal>;
  selectedVehicleIndex!: number;

  agreeCheckBox = false;

  constructor(private sharedService: SharedService, private location: Location, private manageVehicleService: ManageVehicleService) {
  }

  ngOnInit(): void {
    this.initVehicleRoadTaxRenewaList();
  }

  initVehicleRoadTaxRenewaList() {
    let userResponse: UserResponse | null = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    if (userResponse) {
      let vehicleRoadTaxRenewalRequest: VehicleRoadTaxRenewalRequest = {
        ownerUserId: userResponse.userId
      }

      this.manageVehicleService.vehiclesForTaxRenewals(vehicleRoadTaxRenewalRequest).subscribe(data => {

        let vehicleRoadTaxRenewalResponse: VehicleRoadTaxRenewalResponse = data;
        if (vehicleRoadTaxRenewalResponse) {
          this.vehicleRoadTaxRenewalList = vehicleRoadTaxRenewalResponse.vehicleRoadTaxRenewalList;
        }
      })
    }
  }

  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }

  renewalPeriod() {
    this.sharedService.sendRenewalRoadTax(this.vehicleRoadTaxRenewalList[this.selectedVehicleIndex])
    this.onNextStepper.emit(true);
  }

  goBackDashboard() {
    this.location.back();
  }

}
