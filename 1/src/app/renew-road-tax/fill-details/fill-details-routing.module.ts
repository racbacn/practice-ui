import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FillDetailsComponent } from './fill-details.component';

const routes: Routes = [{
  path: '', component: FillDetailsComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FillDetailsRoutingModule { }
