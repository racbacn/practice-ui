import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Location} from '@angular/common';
import {SharedService} from "../../services/shared.service";

@Component({
  selector: 'app-acknowledge',
  templateUrl: './acknowledge.component.html',
  styleUrls: ['./acknowledge.component.scss']
})
export class AcknowledgeComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',
  };

  paymentDetails!: any;
  dateTime!: any;

  registerHeading: string = "Renew Road Tax"

  paymentDescriptions!: Array<any>;
  displayedColumns: string[] = ['#', 'description', 'amount'];

  constructor(private location: Location, private sharedService: SharedService) {
  }

  ngOnInit(): void {
    if (history.state.data) {
      // this.paymentDetails.emailAddress = history.state.data.emailAddress;
      // this.paymentDetails.paymentMethod = history.state.data.paymentMethod;
      this.dateTime = history.state.data.dateTime;
    }

    this.sharedService.getCalculateRoadtax().subscribe(data => {
      let paymentDetails: any = data.paymentDetails;
      let paymentDescriptions: Array<any> = data.paymentDescriptions;

      if (paymentDetails && paymentDescriptions && paymentDescriptions.length > 0) {
        this.paymentDetails = paymentDetails;
        this.paymentDescriptions = paymentDescriptions;
      }
    })
  }


  clickBackPrevious() {
    this.location.back();
  }

  clickNext() {
    this.onNextStepper.emit(true);
  }

}
