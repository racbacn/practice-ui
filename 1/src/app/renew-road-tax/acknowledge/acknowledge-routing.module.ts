import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcknowledgeComponent } from 'src/app/resubmit-escooter-pab/acknowledge/acknowledge.component';
const routes: Routes = [
  { path: '', component: AcknowledgeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcknowledgeRoutingModule { }
