import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RenewRoadTaxComponent } from './renew-road-tax.component';
import { AcknowledgeComponent } from './acknowledge/acknowledge.component';

const routes: Routes = [
  { path: '', component: RenewRoadTaxComponent,},
  { path: 'acknowledge', component: AcknowledgeComponent,},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RenewRoadTaxRoutingModule { }
