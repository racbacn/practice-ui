import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MakePaymentRoutingModule } from './make-payment-routing.module';
import { MakePaymentComponent } from './make-payment.component';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WidgetsModule } from '../../widgets/widgets.module';
@NgModule({
  declarations: [
    MakePaymentComponent
  ],
  imports: [
    CommonModule,
    MakePaymentRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetsModule
  ],
  exports: [
    MakePaymentComponent,
  ]
})
export class MakePaymentModule { }
