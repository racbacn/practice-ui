import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpServicesService } from 'src/app/services/http-services.service';
import { SharedService } from 'src/app/services/shared.service';
@Component({
  selector: 'app-make-payment',
  templateUrl: './make-payment.component.html',
  styleUrls: ['./make-payment.component.scss']
})
export class MakePaymentComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',
  };

  emailAddress : string ="";
  paymentMethod: any = "";

  paymentDetails: any = {
    emailAddress : "",
    paymentMethod: "",
  }
  email = new FormControl('', [Validators.email]);

  constructor(private httpServices: HttpServicesService, private router: Router, private sharedService: SharedService) { }

  ngOnInit(): void { }
  clickPay() {
    let datePipe = new DatePipe('en-US');
    let paymentPayload = {
      "requestHeader": {
        "requestAppId": "VRLS_Make_Payment",
        "requestDateTime": datePipe.transform(new Date(), 'yyyy-MM-ddThh:mm:SS'),
        "userId": Number(localStorage.getItem("userId"))
      },
      "transactionId": "",
      "transactionRefNum": "",
      "paymentType": "Registration_PMD",
      "paymentAmt": 20,
      "paymentMethod": this.paymentMethod
    }

    console.log(this.paymentMethod)
    // if (this.paymentPayload) {
    //   this.httpServices.httpPost(this.apiUrl + "/manage-pmd-service/registerPMD", this.escooterRegForm).subscribe(res => {
    //     this.sharedService.sendEscooterRegResponse(res)
    //     if(res['transactionId']) {
    //       paymentPayload.transactionId= res['transactionId'];
    //     }
    //     if(res['transactionRefNum']) {
    //       paymentPayload.transactionRefNum= res['transactionRefNum'];
    //     }

    //     console.log(paymentPayload)
    //     this.httpServices.httpPost(this.apiUrl + "/manage-payment-service/makePayment", paymentPayload).subscribe(res => {
    //       this.sharedService.sendEscooterRegPaymentResp(res)
    //       this.clickNext();
    //     }, err => {
    //       console.log(err);
    //     })
    //   }, err => {
    //     console.log(err);
    //   })
    // } else {
    //   console.log("empty ", this.escooterRegForm)
    // }
    this.paymentDetails.emailAddress = this.emailAddress;
    this.paymentDetails.paymentMethod = this.paymentMethod;
    this.paymentDetails.dateTime = datePipe.transform(new Date(), 'yyyy-MM-ddThh:mm:SS'),
    this.sharedService.sendPaymentRenewalRoadTax(this.paymentDetails)
    console.log(this.paymentDetails)

    this.router.navigate(['/renew-road-tax/acknowledge'],  { state: { data: this.paymentDetails } });
  }

  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Please enter a valid email' : '';
  }
}
