import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { Location } from '@angular/common';

@Component({
  selector: 'app-renew-road-tax',
  templateUrl: './renew-road-tax.component.html',
  styleUrls: ['./renew-road-tax.component.scss']
})
export class RenewRoadTaxComponent implements OnInit {

  @ViewChild('stepper')
  stepper!: MatStepper;

  isLinear = false;
  registerHeading : string = "Renew Road Tax"
  constructor(private location: Location) { }

  ngOnInit(): void {
  }

  allowToProceed(a: any) {
    if (a) {
      this.stepper.next();
    } else {
      console.log('wrong opp');
    }
  }

  onBack() {
    this.stepper.previous();
  }
}
