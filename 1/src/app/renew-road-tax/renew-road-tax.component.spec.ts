import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewRoadTaxComponent } from './renew-road-tax.component';

describe('RenewRoadTaxComponent', () => {
  let component: RenewRoadTaxComponent;
  let fixture: ComponentFixture<RenewRoadTaxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenewRoadTaxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewRoadTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
