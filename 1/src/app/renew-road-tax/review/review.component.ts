import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SharedService} from 'src/app/services/shared.service';
import {VehicleRoadTaxRenewal} from "../../interface/vehicle-road-tax-renewal";
import {ManagerRoadtaxService} from "../../services/manager-roadtax.service";
import {CalculateRoadtaxResponse} from "../../interface/calculate-roadtax-response";
import {CalculateRoadtaxRequest} from "../../interface/calculate-roadtax-request";
import * as moment from "moment";
import {RoadTaxPayment} from "../../interface/road-tax-payment";

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',
  };

  vehicleRoadTaxRenewal!: VehicleRoadTaxRenewal;
  extendedDate!: string;
  roadTaxPayment!: RoadTaxPayment;
  calculateRoadtaxRequest!: CalculateRoadtaxRequest;

  constructor(private sharedService: SharedService, private manageRoadtaxService: ManagerRoadtaxService) {
  }

  ngOnInit(): void {
    this.sharedService.getRenewalRoadTaxWithNewExpiry().subscribe(data => {
      let renewalRoadTaxWithNewExpiry: any = data;

      if (renewalRoadTaxWithNewExpiry) {
        this.vehicleRoadTaxRenewal = renewalRoadTaxWithNewExpiry.vehicleRoadTaxRenewal;
        this.extendedDate = renewalRoadTaxWithNewExpiry.extendedDate;

        let licensingStartDateTemp;

        if (moment(this.vehicleRoadTaxRenewal.expiryDate).isBefore(this.vehicleRoadTaxRenewal.coeExpiryDate)) {
          licensingStartDateTemp = moment(this.vehicleRoadTaxRenewal.expiryDate, 'DD-MM-YYYY').add(1, "day").format('DD-MM-YYYY').toString();
        } else {
          licensingStartDateTemp = moment(this.vehicleRoadTaxRenewal.coeExpiryDate, 'DD-MM-YYYY').add(1, "day").format('DD-MM-YYYY').toString()
        }

        this.calculateRoadtaxRequest = {
          vehicleRefNum: this.vehicleRoadTaxRenewal.vehicleNumber,
          vehiclePropellantTypeKey: this.vehicleRoadTaxRenewal.propellantType,
          licensingStartDate: licensingStartDateTemp,
          licensingEndDate: this.extendedDate,
          vehicleRegistrationDate: moment(this.vehicleRoadTaxRenewal.coeExpiryDate, 'DD-MM-YYYY').subtract(10, "years").format('DD-MM-YYYY').toString()
        }

        console.log(this.calculateRoadtaxRequest)

        this.manageRoadtaxService.calculateRoadTax(this.calculateRoadtaxRequest).subscribe(data => {
          let calculateRoadtaxResponse: CalculateRoadtaxResponse = data;

          if (calculateRoadtaxResponse) {
            this.roadTaxPayment = calculateRoadtaxResponse.roadTaxPayments[0];
          }
        });
      }
    });
  }

  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }

  clickNext() {
    if (this.calculateRoadtaxRequest && this.roadTaxPayment) {
      let paymentDescriptions: Array<any> = [];

      let paymentDescription = {
        description:
          "Road Tax Renewal - "
          + this.calculateRoadtaxRequest.vehicleRefNum + " "
          + "Road Tax ("
          + moment(this.calculateRoadtaxRequest.licensingStartDate, "DD-MM-YYYY").format("DD MMM YYYY")
          + " - "
          + moment(this.calculateRoadtaxRequest.licensingEndDate, "DD-MM-YYYY").format("DD MMM YYYY")
          + ") "
          + moment().format("DDMMYYYY") + this.calculateRoadtaxRequest.vehicleRefNum,
        amount: this.roadTaxPayment.finalRoadTaxAmount
      }

      paymentDescriptions.push(paymentDescription)

      this.roadTaxPayment.baseRoadTaxAmountBreakdown.forEach(data => {
        let paymentDescription = {
          description:
            "Road Tax Renewal - "
            + this.calculateRoadtaxRequest.vehicleRefNum + " "
            + "Road Tax "
            + data.Name
            + " "
            + moment().format("DDMMMYYYY") + this.calculateRoadtaxRequest.vehicleRefNum,
          amount: data?.Amount ? data?.Amount : '0.00'
        }

        paymentDescriptions.push(paymentDescription);
      });

      let paymentDetails = {
        subTotal: this.roadTaxPayment.finalRoadTaxAmount,
        totalAmountPayable: this.roadTaxPayment.finalRoadTaxAmount
      }

      if (paymentDetails && paymentDescriptions && paymentDescriptions.length > 0) {

        this.sharedService.sendCalculateRoadtax({
          paymentDetails: paymentDetails,
          paymentDescriptions: paymentDescriptions
        });
        this.onNextStepper.emit(true);
      }
    }
  }
}
