import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './core/components/header/header.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { FlexLayoutModule, CoreModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { WidgetsModule } from './widgets/widgets.module';
import {ApiErrorInterceptor} from "./core/http-interceptors/api-error.interceptor";
import {ApiInterceptor} from "./core/http-interceptors/api.interceptor";
import {JwtModule, JwtModuleOptions} from "@auth0/angular-jwt";
import {SessionUtil} from "./core/util/sessionUtil";

const JWT_Module_Options: JwtModuleOptions = {
  config: {
    tokenGetter: () => (SessionUtil.getSessionStorage(SessionUtil.TOKEN))
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
    WidgetsModule,
    JwtModule.forRoot(JWT_Module_Options)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
