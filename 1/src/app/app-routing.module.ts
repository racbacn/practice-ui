import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TokenResolver} from "./services/token-resolver.service";
import {AuthGuard} from "./services/auth.guard";
import {ErrorComponent} from "./widgets/error/error.component";

const routes: Routes = [
  { path: '', redirectTo: "home", pathMatch: "full"},
  { path: 'callback', redirectTo: 'home' },
  {path: "error", component: ErrorComponent},
  // { path: '', loadChildren: () => import('./owner-dashboard/owner-dashboard.module').then(m => m.OwnerDashboardModule) },
  { path: 'owner-dashboard', loadChildren: () => import('./owner-dashboard/owner-dashboard.module').then(m => m.OwnerDashboardModule) , canActivate: [AuthGuard]},
  { path: 'review-vehicle-detail', loadChildren: () => import('./review-vehicle-detail/review-vehicle-detail.module').then(m => m.ReviewVehicleDetailModule) , canActivate: [AuthGuard]},
  { path: 'register-escooter-pab', loadChildren: () => import('./register-escooter-pab/register-escooter-pab.module').then(m => m.RegisterEscooterPabModule) , canActivate: [AuthGuard]},
  { path: 'singpass-login', loadChildren: () => import('./singpass-login/singpass-login.module').then(m => m.SingpassLoginModule)},
  { path: 'home', loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule), resolve: {access: TokenResolver }},
  { path: 'resubmit-escooter-pab', loadChildren: () => import('./resubmit-escooter-pab/resubmit-escooter-pab.module').then(m => m.ResubmitEscooterPabModule), canActivate: [AuthGuard]},
  { path: 'inbox', loadChildren: () => import('./inbox/inbox.module').then(m => m.InboxModule) , canActivate: [AuthGuard]},
  { path: 'renew-road-tax', loadChildren: () => import('./renew-road-tax/renew-road-tax.module').then(m => m.RenewRoadTaxModule)},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
