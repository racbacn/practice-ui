import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { notifications } from '../interface/notifications';
import { PollNotificationService } from '../services/poll-notification.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  notificationPoll$!: Observable<notifications>;
  notificationSub : Subscription  = new Subscription();
  modes : any = [];
  notificationData!: notifications;
  displayedColumns: string[] = [ 'subject'];
  dataSource: any;
  date: any;
  noData!: boolean;
  hasData!: boolean;

  constructor(private router: Router, private pollNotification: PollNotificationService) {
    this.notificationPoll$ = pollNotification.getNotifications();
    this.notificationSub = this.notificationPoll$.subscribe(res=> {
      //Clear the notication array
      this.modes.length=0;
      this.notificationData = res;
      this.dataSource = this.notificationData.notifications;
      console.log(this.dataSource.length)
      if(this.dataSource.length === 0){
        this.noData = true;
      }else {
        this.hasData = true;
      }
  })

  }

  ngOnInit(): void {
    this.date = new Date().toDateString();
    console.log("inbox")
  }

  back(){
    this.router.navigate(['owner-dashboard']);
  }

}
