import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewVehicleDetailRoutingModule } from './review-vehicle-detail-routing.module';
import { ReviewVehicleDetailComponent } from './review-vehicle-detail.component';
import { MaterialModule } from '../material.module';
import { WidgetsModule } from '../widgets/widgets.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ReviewVehicleDetailComponent
  ],
  imports: [
    CommonModule,
    ReviewVehicleDetailRoutingModule,
    MaterialModule,
    FormsModule,
    WidgetsModule


  ]
})
export class ReviewVehicleDetailModule { }
