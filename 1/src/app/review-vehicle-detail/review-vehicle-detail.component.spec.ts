import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVehicleDetailComponent } from './review-vehicle-detail.component';

describe('ReviewVehicleDetailComponent', () => {
  let component: ReviewVehicleDetailComponent;
  let fixture: ComponentFixture<ReviewVehicleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewVehicleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVehicleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
