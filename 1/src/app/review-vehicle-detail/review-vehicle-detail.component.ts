import {Component, OnInit, Output, EventEmitter, OnDestroy,} from '@angular/core';
import {Location, DatePipe} from '@angular/common';
import {OwnerSingPassDetails} from '../interface/owner-SingpassDetails';
import {TransactionID} from '../interface/transactionID';
import {HttpServicesService} from '../services/http-services.service';
import {Subscription} from 'rxjs';
import {SharedService} from '../services/shared.service';
import {serverUrl} from '../constants/sharedValue';
import {Router} from '@angular/router';
import {Attachment} from "../interface/attachment";

export const VEHICLE_ATTACHMENT_LIST: Array<Attachment> = [
  {
    id: 1,
    selection: 1,
    name: 'No Attachment'
  },
  {
    id: 2,
    selection: 2,
    name: 'With Wheelchair Lift'
  },
  {
    id: 3,
    selection: 3,
    name: 'Disabled'
  },
  {
    id: 4,
    selection: 4,
    name: 'With Sun Roof'
  },
  {
    id: 5,
    selection: 5,
    name: 'With Roof Rack',
  },
  {
    id: 6,
    selection: 6,
    name: 'Armoured/Bullet Proof'
  }
];

@Component({
  selector: 'app-review-vehicle-detail',
  templateUrl: './review-vehicle-detail.component.html',
  styleUrls: ['./review-vehicle-detail.component.scss'],
})
export class ReviewVehicleDetailComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();
  subscription: Subscription = new Subscription();
  singPassTxtFasterForm: string = "Faster form filling with Singpass.";
  singPassTxtSingpassEnable: string = "Singpass enables you to retrieve your personal data from participating Government agencies. With your consent, we can auto-fill this form, making your application more convenient.";
  singPassTxtButton: string = "Retrieve Myinfo with Singpass";

  reviewReadAgreeInfo: string = "I have read and agree to the information presented in this form.";
  reviewDeclarationInfo: string = "By Registered Owner: \n\n  \
                                  I hereby authorise \n\n \
                                  PERFORMANCE MOTORS LTD.\n\n \
                                  as my agent (if applicable) to submit this application and furnish all relevant particulars or documents to LTA on my behalf. \n \
                                  I hereby agree to be bound by such terms and conditions applicable to the use of user account and user password to access LTA’s digital services as LTA may prescribe from time to time. \n\
                                  I hereby agree to be bound by such terms and conditions applicable to the disbursement of the Commercial Vehicle Emissions Scheme (CVES) incentive (if applicable) set out on https://go.gov.sg/cves. \n \
                                  I hereby give consent for LTA to offset any CVES incentive (if applicable) against the total vehicle taxes/fees (excluding admin fees) payable at registration. \n \
                                  I am aware that any excess CVES incentive that is not offset will be refunded to me. \n \
                                  I understand that the vehicle must be insured at all times against at least third-party risks before it can be used on the roads. I believe the particulars of the vehicle as provided by the Authorised Representative furnished above are correct and I agree to the registration of the said vehicle in my name. \n \
                                  I have read and understood the instructions given below and overleaf and I consent to the collection, use or disclosure of my personal data to a third party as per paragraph 9 given below.";
  reviewFootNote: string = "Please do not use your browser’s Back or Forward buttons as this may result in information loss";

  apiUrl: string = serverUrl;
  submitCheckBox: boolean = false;
  finalDataToSend!: OwnerSingPassDetails;
  diplomaScheme: Boolean = false;
  disableSingpassInput = false;
  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',
  };

  getDataFrmHomePage!: TransactionID;   //Get the tranactionID and ownerID from home page
  getVehicleDetails: any;
  getSalesAgreementId: string = '';
  //Create the label for the Review Page
  labelExpansionDetails = [
    {
      headerName: 'OWNER PARTICULARS',
      labelDetails: [''],
      labelInfo: ['Name', 'NRIC No. / FIN / ACRA No. / UEN', 'Date of Birth (For individual)', "Contact Number", "E-mail Address", 'Address', 'Unit NO.', 'Postal Code'],
    },
    {
      headerName: 'VEHICLE PARTICULARS',
      labelDetails: [''],
      labelInfo: ['Vitas Approval Code', 'Vehicle Type', 'Vehicle Scheme', 'Vehicle Attachment 1', 'Vehicle Attachment 2', 'Vehicle Attachment 3', 'Vehicle Make', 'Vehicle Model', 'Propellant', 'Passenger Capacity', 'Engine Capacity', 'Power Rating', 'Maximum Power Output', 'No. Of Axles', 'Front/Rear Type Information', 'Unladen Weight', 'Maximum Unladen Weight', 'Emission Standard Code', 'CO2 Emission', 'CO Emission', 'HC Emission', 'NOx Emission', 'PM Emission', 'Primary Colour', 'Secondary Colour', 'Manufacturing Year', 'First Registration Date', 'Original Registration Date', 'Chasis No', 'Engine No', 'Motor No', 'Trailer Chassis No',],
    },
    {
      headerName: 'NEW REGISTRATION & ROAD TAX FEES',
      labelDetails: [''],
      labelInfo: ['TCOE No.', 'PARF/COE Rebate No.', 'Insurance Company', 'Insurance Cover Note No', 'Vehicle No Available', 'Open Market Value', 'Road Tax Months',],
    },
    {headerName: 'DECLARATIONS', labelDetails: [''], labelInfo: ['']},
  ];

  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "OWNER PARTICULARS": "ownerDetails",
    "VEHICLE PARTICULARS": "vehicleDetails",
    "NEW REGISTRATION & ROAD TAX FEES": "roadTaxDetails",
    "Name": "ownerName",
    "NRIC No. / FIN / ACRA No. / UEN": "ownerId",
    "Date of Birth (For individual)": "ownerDOB",
    "Contact Number": "ownerContactNum",
    "E-mail Address": "ownerEmailAddress",
    "Address": "ownerAddress",
    "Unit NO.": "ownerAddressUnitNo",
    "Postal Code": "ownerPostalCode",
    "Vitas Approval Code": "vitasApprovalCode",
    "Vehicle Type": "vehicleType",
    "Vehicle Scheme": "vehicleScheme",
    "Vehicle Attachment 1": "attachmentOne",
    "Vehicle Attachment 2": "attachmentTwo",
    "Vehicle Attachment 3": "attachmentThree",
    "Vehicle Make": "vehicleMake",
    "Vehicle Model": "vehicleModel",
    "Propellant": "propellant",
    "Passenger Capacity": "passengerCapacity",
    "Engine Capacity": "engineCapacity",
    "Power Rating": "powerRating",
    "Maximum Power Output": "maximumPowerOutput",
    "No. Of Axles": "numberOfAxles",
    "Front/Rear Type Information": "tyreInformation",
    "Unladen Weight": "unladenWeight",
    "Maximum Unladen Weight": "maximumUnladenWeight",
    "Emission Standard Code": "emissionStandardCode",
    "CO2 Emission": "carbonDioxideEmission",
    "CO Emission": "carbonMonoxideEmission",
    "HC Emission": "hydroCarbonEmission",
    "NOx Emission": "nitrosOxideEmission",
    "PM Emission": "pmEmission",
    "Primary Colour": "primaryColour",
    "Secondary Colour": "secondaryColour",
    "Manufacturing Year": "manufacturingYear",
    "First Registration Date": "firstRegistrationDate",
    "Original Registration Date": "originalRegistrationDate",
    "Chasis No": "chassisNumber",
    "Engine No": "engineNumber",
    "Motor No": "motorNumber",
    "Trailer Chassis No": "trailerChassisNumber",
    "TCOE No.": "tcoeNumber",
    "PARF/COE Rebate No.": "coeRebateNumber",
    "Insurance Company": "insuranceCompany",
    "Insurance Cover Note No": "insuranceCoverNoteNumber",
    "Vehicle No Available": "vehicleNumberAvailable",
    "Open Market Value": "openMarketValue",
    "Road Tax Months": "roadTaxMonths",
  };

  constructor(private httpServices: HttpServicesService, private sharedService: SharedService, private location: Location, private router: Router) {
  }

  ngOnInit(): void {
    //Get the data from the review button in the homepage
    this.getDataFrmHomePage = history.state.data;
    this.loadVehicle(this.getDataFrmHomePage.transactionId);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }

  retrieveMyInfo() {
    let ownerId = ""
    if (this.getVehicleDetails?.ownerDetails?.ownerId) {
      ownerId = this.getVehicleDetails.ownerDetails.ownerId
    }
    let queryID = {
      ownerId: ownerId
    };
    console.log(queryID)
    //Get the singpass API
    this.httpServices.httpPost(
      this.apiUrl + "/myinfo/owner-details", queryID).subscribe(res => {
      console.log(res)
      //Lock the singpass Input field if click retrieve
      this.disableSingpassInput = true;
      //Clear the information store in the labelDetails else it will keep pushing in data
      //Use array 0 for owner Details
      this.labelExpansionDetails[0].labelDetails = [];
      this.labelExpansionDetails[0].labelInfo.forEach((labelinfo) => {

        //Check if there is data
        if (res[this.labelMapIncomingDataHTML[labelinfo]]) {
          this.labelExpansionDetails[0].labelDetails.push(res[this.labelMapIncomingDataHTML[labelinfo]]);
        } else {
          //Set as - for no data
          this.labelExpansionDetails[0].labelDetails.push('-' as never);
        }
      });
    })

  }

  loadVehicle(transactID: string) {
    this.httpServices.httpGet(this.apiUrl + "/manage-vehicle-service/vehicleDetails/" + transactID).subscribe(res => {
      console.log(res)
      if (res) {
        if (res?.vehicleDetails?.vehicleScheme == "Diplomatic") {
          this.diplomaScheme = true;
        } else {
          this.diplomaScheme = false;
        }
        this.getSalesAgreementId = res.ownerDetails?.salesAgreementId
        this.loadItems(res);
        this.getVehicleDetails = res;
        // this.getDataFrmHomePage.ownerId=res?.ownerDetails?.ownerId
      }
    })
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {

        console.log(incomingDataFilter)

        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {

            if (labelinfo.includes("Vehicle Attachment")) {
              let vehAttachName = VEHICLE_ATTACHMENT_LIST.filter(vehAtt => vehAtt.id == incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]])
              this.labelExpansionDetails[i].labelDetails.push(vehAttachName[0]["name"] as never);
            } else {
              this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]] as never);
            }
          } else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
    this.finalDataToSend = JSON.parse(JSON.stringify(inputData));
  }

  approveBtn() {
    let finalObj = {} as any;
    let datePipe = new DatePipe('en-US');
    let timeNow = datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");

    finalObj = {
      "requestHeader": {
        "requestAppId": "VRLS_Vehicle",
        "requestDateTime": timeNow,
        "userId": Number(localStorage.getItem("userId")),
        "userTypeKey": Number(localStorage.getItem("userTypeKey"))
      }
    }
    if (this.diplomaScheme) {
      finalObj["ownerDetails"] = {};
      finalObj["approvingLTAOfficerUserId"] = "abc123"
      finalObj["isApprovedWithScheme"] = true;
      for (let info of this.labelExpansionDetails[0].labelInfo.entries()) {
        finalObj["ownerDetails"][this.labelMapIncomingDataHTML[info[1]]] = this.labelExpansionDetails[0].labelDetails[info[0]]
      }
    } else {
      //Declare the final object to be sent to the api approve
      finalObj["ownerDetails"] = {};
      finalObj["isApprovedWithScheme"] = false;
      //Acess the array index with the object name, e.g [1, "NRIC No. / FIN / ACRA No. / UEN"]
      for (let info of this.labelExpansionDetails[0].labelInfo.entries()) {
        finalObj["ownerDetails"][this.labelMapIncomingDataHTML[info[1]]] = this.labelExpansionDetails[0].labelDetails[info[0]]
      }
    }
    //Added for the approve ownerDetails
    finalObj["ownerDetails"]["ownerUserId"] = Number(localStorage.getItem("userId")); //hardcode
    finalObj["ownerDetails"]["ownerAddressLine1"] = finalObj["ownerDetails"]["ownerAddress"];
    finalObj["ownerDetails"]["ownerAddressLine2"] = finalObj["ownerDetails"]["ownerAddressUnitNo"];
    delete finalObj["ownerDetails"]["ownerAddress"];
    delete finalObj["ownerDetails"]["ownerAddressUnitNo"];

    let inputDOB = finalObj["ownerDetails"]["ownerDOB"];
    let convertedDate = Date.parse(inputDOB)
    //Check if valid date
    if (convertedDate) {
      let parsedOwnerDOB = datePipe.transform(inputDOB, "YYYY-MM-dd");
      finalObj["ownerDetails"]["parsedOwnerDOB"] = parsedOwnerDOB;
    }

    finalObj["ownerDetails"]["salesAgreementId"] = this.getSalesAgreementId;
    //Send the transaction ID
    finalObj["transactionId"] = this.getDataFrmHomePage.transactionId;

    console.log(finalObj)

    this.httpServices.httpPut(this.apiUrl + "/manage-vehicle-service/approveVehicleRegistration", finalObj).subscribe(res => {
      if (res) {
        let tempTransaction = {
          "transactionId": res.transactionId
        };

        this.sharedService.sendApproveClick(tempTransaction);
      }
    });
  }

  rejectBtn() {
    console.log(this.labelExpansionDetails[0].labelDetails)
    let tempTransaction = {
      "transactionId": this.getDataFrmHomePage.transactionId
    };
    console.log(tempTransaction)
    this.sharedService.sendRejectClick(tempTransaction);
  }

  goBackDashboard() {
    this.location.back();
    //this.router.navigate(['/owner-dashboard']);
  }
}
