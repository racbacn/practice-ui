import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewVehicleDetailComponent } from './review-vehicle-detail.component';

const routes: Routes = [{ path: '', component: ReviewVehicleDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewVehicleDetailRoutingModule { }
