import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-resubmit-escooter-pab',
  templateUrl: './resubmit-escooter-pab.component.html',
  styleUrls: ['./resubmit-escooter-pab.component.scss']
})
export class ResubmitEscooterPabComponent implements OnInit {

  @ViewChild('stepper')
  stepper!: MatStepper;

  isLinear = false;
  registerHeading : string = " Register e-scooter/PAB"
  constructor() { }

  ngOnInit(): void {
  }

  allowToProceed(a: any) {
    if (a) {
      this.stepper.next();
    } else {
      console.log('wrong opp');
    }
  }

  onBack() {
    this.stepper.previous();
  }
}
