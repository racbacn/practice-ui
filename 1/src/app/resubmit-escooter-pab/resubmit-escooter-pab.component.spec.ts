import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResubmitEscooterPabComponent } from './resubmit-escooter-pab.component';

describe('ResubmitEscooterPabComponent', () => {
  let component: ResubmitEscooterPabComponent;
  let fixture: ComponentFixture<ResubmitEscooterPabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResubmitEscooterPabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResubmitEscooterPabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
