import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { serverUrl } from 'src/app/constants/sharedValue';
import { EscooterRegistration } from 'src/app/interface/escooter-registration';
import { HttpServicesService } from 'src/app/services/http-services.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();
  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };
  reviewDeclarationInfo: string = "Please read the declaration details here.";
  reviewReadAgreeInfo: string = "I/We declare that I/We have read and understood the information in the declaration, and understake to abide by it.";
  escooterDeclared : string = "You have declared that the E-scooter meets the following criteria:"
  escooterCriteriaList = [
    "The document format should be in PDF or image file format (e.g jpg)",
    "The filename contains only letters, numbers and underscore, without spaces or other special characters",
    "The filename should have at most 33 characters"];
  submitCheckBox: boolean = false;
  escooterRegForm!: EscooterRegistration
  //Create the label for the Review Page
  labelExpansionDetails = [
    { headerName: 'YOUR DETAILS', labelDetails: [''], labelInfo: ['ID Type', 'ID', 'Name', 'Birth Date', 'Mobile No', 'Email Address', 'Address', 'Unit No.', 'Postal Code'], },
    { headerName: 'E-SCOOTER PARTICULARS', labelDetails: [''], labelInfo: ['Make', 'Model', 'Type', '*No. of Wheels', '*Colour', 'Seat', 'Safety Standard Serial No.', 'E-scooter Safety Standard Sticker', 'Photo of E-scooter'], },

  ];

  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "YOUR DETAILS": "ownerDetails",
    "E-SCOOTER PARTICULARS": "pmdDetails",
    "ID Type": "ownerIdType",
    "ID": "ownerId",
    "Name": "ownerName",
    "Birth Date": "ownerDOB",
    "Mobile No": "ownerContactNum",
    "Email Address": "ownerEmailAddress",
    "Postal Code": "ownerPostalCode",
    "Address": "ownerAddress",
    "Unit No.": "ownerAddressUnitNo",
    "Make": "pmdMake",
    "Model": "pmdModel",
    "Type": "pmdType",
    "*No. of Wheels": "pmdWheelsNum",
    "*Colour": "pmdColour",
    "Seat": "hasSeat",
    "Safety Standard Serial No.": "safetyStandardSerialNum",
    "E-scooter Safety Standard Sticker": "safetyStandardStickerImage",
    "Photo of E-scooter": "pmdImage",

  };
  regEscooterSub: Subscription = new Subscription;
  apiUrl: string = serverUrl;

  constructor(private sharedService: SharedService, private httpServices: HttpServicesService) { }

  ngOnInit(): void {
    this.regEscooterSub = this.sharedService.getEscooterRegForm().subscribe(res => {
      // this.loadItems(this.escootRegistration)
      this.escooterRegForm = res;
      this.loadItems(res)

    })
  }

  ngOnDestroy() {
    this.regEscooterSub.unsubscribe();
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
            //case to replace the documentID with the filename
            if(labelinfo == "E-scooter Safety Standard Sticker" ) {
              this.labelExpansionDetails[i].labelDetails.push(inputData['pmdRegistrationDocuments'][0]['fileName']);

            } else if (labelinfo == "Photo of E-scooter") {
              this.labelExpansionDetails[i].labelDetails.push(inputData['pmdRegistrationDocuments'][1]['fileName']);

            }else {
              this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);

            }
          }  else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
  }
  clickResubmit() {
    if (this.escooterRegForm) {
      console.log(this.escooterRegForm)
      this.httpServices.httpPost(this.apiUrl + "/manage-pmd-service/registerPMD", this.escooterRegForm).subscribe(res => {
        this.sharedService.sendEscooterReSubmitResponse(res)
        console.log(res)
      }, err => {
        console.log(err);
      })
    } else {
      console.log("empty ", this.escooterRegForm)
    }
  }


  clickBackPrevious() {
    this.onPreviousStepper.emit();
  }
  clickNext() {
    this.clickResubmit();
    this.onNextStepper.emit(true);
  }
}
