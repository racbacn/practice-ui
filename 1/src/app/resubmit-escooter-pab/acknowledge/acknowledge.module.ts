import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcknowledgeRoutingModule } from './acknowledge-routing.module';
import { AcknowledgeComponent } from './acknowledge.component';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';

@NgModule({
  declarations: [
    AcknowledgeComponent
  ],
  imports: [
    CommonModule,
    AcknowledgeRoutingModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    WidgetsModule
  ],
  exports: [
    AcknowledgeComponent
  ]
})
export class AcknowledgeModule { }
