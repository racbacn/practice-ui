import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-acknowledge',
  templateUrl: './acknowledge.component.html',
  styleUrls: ['./acknowledge.component.scss']
})
export class AcknowledgeComponent implements OnInit {

  returnButtonConfig = {
    buttonText: 'Return to Home',
    buttonClassName: 'Button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };
  //Create the label for the Review Page
  labelExpansionDetails : any = [
    { headerName: 'TRANSACTION DETAILS', labelDetails: [], labelInfo: ['Transaction Ref. No.', 'Transaction Date', 'Transaction Time'], },

  ];

  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "TRANSACTION DETAILS": "transactionDetails",
    "Transaction Ref. No.": "transactionRefNum",
    "Transaction Date": "transactionDate",
    "Transaction Time": "transactionTime",

  };


  regEscooterSub: Subscription = new Subscription;

  regEscooterResp: any = {
    transactionDetails : {}
  }

  constructor(private sharedService: SharedService, private router: Router) {
    // this.regEscooterSub = this.sharedService.getEscooterRegForm().subscribe(res => {
    //   this.regEscooterForm = res;
    //   this.ownID = this.regEscooterForm?.ownerDetails?.ownerId
    // });

    this.regEscooterSub = this.sharedService.getEscooterReSubmitResponse().subscribe(res => {
      this.regEscooterResp.transactionDetails = res;
      console.log(this.regEscooterResp)
      this.loadItems( this.regEscooterResp);

    });

  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this.regEscooterSub.unsubscribe();
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo: string | number) => {
        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
            this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
          } else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
  }

  goHome() {
    //Go to Home Page
    this.router.navigate(['/owner-dashboard']);
  }
}
