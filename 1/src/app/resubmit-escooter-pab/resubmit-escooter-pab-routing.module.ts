import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResubmitEscooterPabComponent } from './resubmit-escooter-pab.component';

const routes: Routes = [{
  path: '', component: ResubmitEscooterPabComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResubmitEscooterPabRoutingModule { }
