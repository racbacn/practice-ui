import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResubmitEscooterPabRoutingModule } from './resubmit-escooter-pab-routing.module';
import { ResubmitEscooterPabComponent } from './resubmit-escooter-pab.component';
import { ParticularDetailsModule } from './particular-details/particular-details.module';
import { ReviewModule } from './review/review.module';
import { AcknowledgeModule } from './acknowledge/acknowledge.module';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    ResubmitEscooterPabComponent,
  ],
  imports: [
    CommonModule,
    ResubmitEscooterPabRoutingModule,
    ParticularDetailsModule,
    ReviewModule,
    AcknowledgeModule,
    MaterialModule,
    FlexLayoutModule,
  ],
})
export class ResubmitEscooterPabModule { }
