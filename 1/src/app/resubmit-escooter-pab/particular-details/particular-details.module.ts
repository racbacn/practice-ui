import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParticularDetailsComponent } from './particular-details.component';
import { ParticularDetailsRoutingModule } from './particular-details-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';

@NgModule({
  declarations: [
    ParticularDetailsComponent
  ],
  imports: [
    CommonModule,
    ParticularDetailsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    WidgetsModule,
  ],
  exports: [
    ParticularDetailsComponent
  ],
})
export class ParticularDetailsModule { }
