import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TransactionID } from 'src/app/interface/transactionID';
import { HttpServicesService } from 'src/app/services/http-services.service';
import { serverUrl } from '../../constants/sharedValue';
import { Location, DatePipe } from '@angular/common';
import { VehicleRegistrationDocumentRequest } from "src/app/interface/vehicle-registration-document-request";
import { VehicleRegistrationDocumentResponse } from "src/app//interface/vehicle-registration-document-response";
import { VehicleRegistrationDocument } from "src/app/interface/vehicle-registration-document";
import { DocumentService } from "src/app/services/document.service";
import { UploadService } from "src/app/services/upload.service";
import { UploadRequest } from "src/app/interface/upload-request";
import { UploadResponse } from "src/app/interface/upload-response";
import { environment } from "src/environments/environment";
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { notify } from 'src/app/interface/notify';

export const FILE_SIZE_MAX: number = 28400000;
export const FILE_TYPE_PDF: string = 'application/pdf';
export const FILE_TYPE_JPG: string = 'image/jpeg';
export const FILE_NAME_LENGTH: number = 33;
export const LEGAL_AGE: number = 16;
export const ESCOOTER_DETAILS_NUM: number = 1;
export const OWNER_DETAILS_NUM: number = 2;


@Component({
  selector: 'app-particular-details',
  templateUrl: './particular-details.component.html',
  styleUrls: ['./particular-details.component.scss']
})
export class ParticularDetailsComponent implements OnInit {
  @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
  backButtonConfig = {
    buttonText: 'Back',
    buttonClassName: 'grey-button',
    buttonFont: 'button-font-16',
    hasIcon: false,
    buttonWidth: 'button-70',

  };

  //Create the label for the Review Page
  labelExpansionDetails = [
    { headerName: 'OWNER\'S FEEDBACK', labelDetails: [''], labelInfo: ['Incorrect information'], },
    { headerName: 'E-SCOOTER PARTICULARS', labelDetails: [''], labelInfo: ['Make', 'Model', '*No. of Wheels', '*Colour', 'Seat', 'chk_weightDeclaration', 'chk_speedDeclaration', 'chk_widthDeclaration', 'chk_certifiedSafety', 'Safety Standard Serial No.', 'E-scooter Safety Standard Sticker', 'Photo of E-scooter'], },
    { headerName: 'YOUR DETAILS', labelDetails: ['', '', '', '', '', '', '', '', ''], labelInfo: ['*Owner ID Type', 'NRIC No. / FIN / ACRA No. / UEN', 'Name', 'Date of Birth (For individual)', "Contact No", 'Address', 'E-mail Address', 'Unit No.', 'Postal Code'], },
  ];
  //Mapping data to match incoming Data key with the element ID in html
  labelMapIncomingDataHTML: any = {
    "OWNER\'S FEEDBACK": "ownerFeedBack",
    "E-SCOOTER PARTICULARS": "pmdDetails",
    "YOUR DETAILS": "ownerDetails",
    'Incorrect information': 'transactionComment',
    "Make": "pmdMake",
    "Model": "pmdModel",
    "*No. of Wheels": "pmdWheelsNum",
    "*Colour": "pmdColour",
    "Seat": "hasSeat",
    "chk_weightDeclaration": "weightDeclaration",
    "chk_speedDeclaration": "speedDeclaration",
    "chk_widthDeclaration": "widthDeclaration",
    "chk_certifiedSafety": "certifiedSafety",
    "Safety Standard Serial No.": "safetyStandardSerialNum",
    "E-scooter Safety Standard Sticker": "safetyStandardStickerImage",
    "Photo of E-scooter": "pmdImage",
    "Name": "ownerName",
    "*Owner ID Type": "ownerIdType",
    "NRIC No. / FIN / ACRA No. / UEN": "ownerId",
    "Date of Birth (For individual)": "ownerDOB",
    "Contact No": "ownerContactNum",
    "E-mail Address": "ownerEmailAddress",
    "Address": "ownerAddress",
    "Unit No.": "ownerAddressUnitNo",
    "Postal Code": "ownerPostalCode",
  };

  excludeInputTxt = ["Seat", "chk_weightDeclaration", "chk_speedDeclaration", "chk_widthDeclaration", "chk_certifiedSafety"];
  chkBoxContent: any = {
    "chk_weightDeclaration": "Weight does not exceed 20kg \n(Weight <= 20kg)",
    "chk_speedDeclaration": "Speed does not exceed 25kph \n(Speed <= 25kph)",
    "chk_widthDeclaration": "Width does not exceed 700mm \n(Width <= 700mm)",
    "chk_certifiedSafety": "Certified to safety standard"
  }
  escooterEnsureUploadFile: string = "Please ensure that your uploaded file meets the following requirements:"
  escooterUploadFileCriteria: string[] = [
    "The document format should be in PDF or image file format (e.g jpg)",
    "The filename contains only letters, numbers and underscore, without spaces or other special characters",
    "The filename should have at most 33 characters"
  ]
  singPassTxtSingpassEnable: string = "Singpass enables you to retrieve your personal data from participating Government agencies. With your consent, we can auto-fill this form, making your application more convenient.";
  apiUrl: string = serverUrl;
  getDataFrmHomePage!: notify;   //Get the tranactionID and ownerID from home page
  formFillInData: {} | any;
  pmdRegistrationDoc: any = [];

  //File upload variables
  isSizeErrorHidden: boolean = true;
  isTypeErrorHidden: boolean = true;
  isLoadingOneHidden: boolean = true;
  isLoadingTwoHidden: boolean = true;
  isSafetyStickerFileNameLengthErrorHidden: boolean = true
  isEScooterPhotoFileNameLengthErrorHidden: boolean = true
  isSafetyStickerletterNumUnderScoreErrorHidden: boolean = true;
  isEScooterPhotoletterNumUnderScoreErrorHidden: boolean = true;
  fileSizeMax: number = FILE_SIZE_MAX;
  fileTypePdf: string = FILE_TYPE_PDF;
  fileTypeJpg: string = FILE_TYPE_JPG;
  vehicleId!: string;
  responseDataOne!: UploadResponse;
  safetyFile: any = {
    'Safety Standard Serial No.': '',
    'E-scooter Safety Standard Sticker': ''
  };
  registerScooter!: FormGroup;
  disableSingpassInput: boolean = true;     //set to disabled so that ownerDetails will not be changed. Will have prob at backend if owner details changed
  nextButtonValidFile: boolean = false;
  nextButtonValidForInput: boolean = false;
  isLegalAgeErrorHidden = true;
  legalAge: number = LEGAL_AGE;
  setPmdID: string ='';

  constructor(
    private httpServices: HttpServicesService,
    private location: Location,
    private uploadService: UploadService,
    private documentService: DocumentService,
    private router: Router,
    private sharedService: SharedService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.getDataFrmHomePage = history.state.data;
    //Set those * with vaidator
    let formGroupList: any = {}
    this.labelExpansionDetails.forEach(headerName => {
      headerName.labelInfo.forEach(_info => {
        if (_info.indexOf("*") != -1) {
          formGroupList[_info] = ['', Validators.required];
        } else if (_info.indexOf("NRIC No. / FIN / ACRA No. / UEN") != -1) {
          formGroupList[_info] = ['', [Validators.pattern(/[STFG]\d{7}[A-Z]/g)]];
        } else if (_info.indexOf("Date of Birth") != -1) {
          formGroupList[_info] = ['', [Validators.pattern(/^(([0-9])|([0-2][0-9])|([3][0-1]))\ (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\ \d{4}$/g)]];
        } else {
          formGroupList[_info] = [''];

        }
      })
    })
    this.registerScooter = this.fb.group(formGroupList);
    // Set the comments for reject in the 'OWNER\'S FEEDBACK
    this.labelExpansionDetails[0].labelDetails = [];
    this.labelExpansionDetails[0].labelDetails.push(this.getDataFrmHomePage.transactionComment);
    //remove this
    // this.loadVehicle("2");
    //Check if form is valid
    this.registerScooter.statusChanges.subscribe(val => {
      if (val == "VALID") {
        this.nextButtonValidForInput = true;
      } else {
        this.nextButtonValidForInput = false;

      }
    })
    this.loadVehicle(this.getDataFrmHomePage.transactionId);
  }

  retrieveMyInfo() {
    let queryID = {
      ownerId: this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails[1]
    };
    //Get the singpass API
    this.httpServices.httpPost(
      this.apiUrl + "/myinfo/owner-details", queryID).subscribe(res => {
        //lock the input form
        this.disableSingpassInput = true;
        //Clear the information store in the labelDetails else it will keep pushing in data
        //Use array 0 for owner Details
        this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails = [];
        this.labelExpansionDetails[OWNER_DETAILS_NUM].labelInfo.forEach((labelinfo) => {

          //Check if there is data
          if (res[this.labelMapIncomingDataHTML[labelinfo]]) {

            if (labelinfo.indexOf('Date of Birth') != -1) {
              this.chkLegalAgeString(res[this.labelMapIncomingDataHTML[labelinfo]]);
              let datePipe = new DatePipe('en-US');
              let transformDate = datePipe.transform(new Date(res[this.labelMapIncomingDataHTML[labelinfo]]), 'dd MMM yyyy')
              if (transformDate) {
                this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails.push(transformDate);
                this.registerScooter.controls[labelinfo].setValue(transformDate)
              } else {
                this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails.push(res[this.labelMapIncomingDataHTML[labelinfo]]);
                this.registerScooter.controls[labelinfo].setValue(res[this.labelMapIncomingDataHTML[labelinfo]])

              }

            } else {
              this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails.push(res[this.labelMapIncomingDataHTML[labelinfo]]);
              this.registerScooter.controls[labelinfo].setValue(res[this.labelMapIncomingDataHTML[labelinfo]])

            }

          } else {
            //Set as - for no data
            this.labelExpansionDetails[OWNER_DETAILS_NUM].labelDetails.push('-' as never);

          }

          //Fix the validation after reloading the singpass value
          this.registerScooter.controls[labelinfo].updateValueAndValidity();
          //Reset the errors after click retrieve
          this.registerScooter.controls[labelinfo].setErrors(null);

        });
      })
  }

  goBackDashboard() {
    this.location.back();
    //this.router.navigate(['/owner-dashboard']);
  }

  onUploadFile(event: any, label: string) {
    const file: File = event.target.files[0];
    this.isSafetyStickerFileNameLengthErrorHidden = true;
    this.isEScooterPhotoFileNameLengthErrorHidden = true;
    this.isSafetyStickerletterNumUnderScoreErrorHidden = true;
    this.isEScooterPhotoletterNumUnderScoreErrorHidden = true;

    this.isTypeErrorHidden = true;
    this.safetyFile[label] = file.name;
    let regCharNumNospace = new RegExp(/^[A-Za-z0-9_]*[A-Za-z0-9][A-Za-z0-9_]*\.{1}[a-z]*/);

    console.log(this.registerScooter.controls)
    console.log(label)
    if (file.size >= this.fileSizeMax) {
      this.isSizeErrorHidden = false;
      return;
    }

    //Validation for the file name length
    if (file.name.length >= FILE_NAME_LENGTH) {
      if (label.indexOf("Safety Standard") != -1) {
        this.isSafetyStickerFileNameLengthErrorHidden = false;
      } else {
        this.isEScooterPhotoFileNameLengthErrorHidden = false;
      }
      this.safetyFile[label] = '';
      return;
    }

    //Validation for special char and spacing, will reject the file
    if (!regCharNumNospace.test(file.name)) {
      if (label.indexOf("Safety Standard") != -1) {
        this.isSafetyStickerletterNumUnderScoreErrorHidden = false;
      } else {
        this.isEScooterPhotoletterNumUnderScoreErrorHidden = false;
      }
      this.safetyFile[label] = '';

      return
    }
    if (file.type.toString() === this.fileTypeJpg || file.type.toString() === this.fileTypePdf) {
      this.isSizeErrorHidden = true;
      this.isTypeErrorHidden = true;
      let datePipe = new DatePipe('en-US');

      let vehicleRegistrationDocumentRequest: VehicleRegistrationDocumentRequest = {
        requestHeader: {
          requestAppId: "VRLS_DOCUMENT_REFNUM",
          requestDateTime: datePipe.transform(new Date(), 'yyyy-MM-ddThh:mm:SS') as string,
          userId: localStorage.getItem("userId") as string,
        },
        documentTypeKey: 2
      }
      this.isLoadingOneHidden = false;

      this.documentService.createDocumentId(vehicleRegistrationDocumentRequest)
        .subscribe((data: VehicleRegistrationDocumentResponse) => {

          let vehicleRegistrationDocument: VehicleRegistrationDocument = {
            documentId: data.documentId,
            documentRefNum: data.documentRefNum,
            fileName: ''
          }

          let uploadRequest: UploadRequest = {
            Bucket: environment.aws.s3.bucket,
            Key: data.documentRefNum + '_' + file.name,
            Body: file,
            ACL: 'private',
            ContentType: file.type
          }
          if (file) {
            this.uploadService.uploadFile(uploadRequest).promise().then((data: any) => {
              if (data) {
                vehicleRegistrationDocument.fileName = file.name
                // this.vehicleRequest.vehicleRegistrationDocuments.push(vehicleRegistrationDocument);
                //Store image upload
                this.pmdRegistrationDoc.push(vehicleRegistrationDocument)
                this.isLoadingOneHidden = true;
                //Check to enable / disable the next button
                this.checkValidation();

                this.responseDataOne = {
                  isSuccessful: true,
                  message: "Upload is successful"
                }
              }
            }).catch((err) => {
              if (err) {
                this.responseDataOne = {
                  isSuccessful: true,
                  message: err.message
                }
              }
              console.log(err)

            })

          }
        })
    } else {
      this.isTypeErrorHidden = false;
    }
  }

  clickBackHomePage() {
    //Go to homepage
    this.router.navigate(['']);
  }
  clickNext() {
    this.eScooterRegistrationform();
    this.onNextStepper.emit(true);
  }

  //Create the eScooter form structure with the input
  eScooterRegistrationform() {
    // Clear the formData to be sent
    this.formFillInData = {}
    for (let i in this.labelExpansionDetails) {

      let headerName = this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName];
      //Remove the ownerFeedback for escooter Registration form
      if (headerName === ("ownerFeedBack")) {
        continue;
      }
      console.log(headerName)
      this.formFillInData[headerName] = {};
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo, index) => {
        if (this.registerScooter.controls[labelinfo].value) {
          this.formFillInData[headerName][this.labelMapIncomingDataHTML[labelinfo]] = this.registerScooter.controls[labelinfo].value;
        } else if (this.labelExpansionDetails[i].labelDetails[index]) {
          this.formFillInData[headerName][this.labelMapIncomingDataHTML[labelinfo]] = this.labelExpansionDetails[i].labelDetails[index];
        } else {
          this.formFillInData[headerName][this.labelMapIncomingDataHTML[labelinfo]] = "-"

        }
      });
    }
    let singpassUserId = Number(localStorage.getItem("userId"));
    let singpassUserTypeKey = Number(localStorage.getItem("userTypeKey"));

    let datePipe = new DatePipe('en-US');
    this.formFillInData["requestHeader"] = {
      "requestAppId": "VRLS_PMD_Register",
      "requestDateTime": datePipe.transform(new Date(), 'yyyy-MM-ddThh:mm:SS'),
      "userId": singpassUserId,
      "userTypeKey": singpassUserTypeKey
    }
    //Set the pmdId for resubmit
    if(this.setPmdID) {
      this.formFillInData.pmdDetails.pmdId = Number(this.setPmdID);
    }
    //Set the ownerUserId
    this.formFillInData["ownerDetails"]["ownerUserId"] = Number(localStorage.getItem("userId"));

    //set the document ID
    if (this.pmdRegistrationDoc[0]) {
      this.formFillInData.pmdDetails.safetyStandardStickerImage = this.pmdRegistrationDoc[0].documentId;
    }
    if (this.pmdRegistrationDoc[1]) {
      this.formFillInData.pmdDetails.pmdImage = this.pmdRegistrationDoc[1].documentId;
    }

    //Load the registration Document array
    this.formFillInData["pmdRegistrationDocuments"] = this.pmdRegistrationDoc;

    //Add the ownerIdType as Singpass does not cater
    // this.formFillInData["ownerDetails"]["ownerIdType"] = "Singapore NRIC";
    //Change the date format from 09 Jun 1999 to 09/06/1999
    // this.formFillInData["ownerDetails"]["ownerDOB"]=datePipe.transform(this.formFillInData["ownerDetails"]["ownerDOB"], "dd/MM/yyyy");
    this.sharedService.sendEscooterRegForm(this.formFillInData);
    console.log(this.formFillInData)
  }

  loadVehicle(transactionId: string) {
    // console.log("Transaction ID", this.transactionId)

    this.httpServices.httpGet('/manage-pmd-service/pmdDetails/' + transactionId).subscribe((res) => {
      console.log(res)
      if (res) {
        this.loadItems(res);
        if (res.pmdDetails?.pmdId) {
          this.setPmdID = res.pmdDetails.pmdId;
        }
      }
    })
  }

  //Load the incoming data with the label and details
  loadItems(inputData: any): void {
    // labelExpansionDetails
    for (let i = 1; i < this.labelExpansionDetails.length; i++) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
        if (typeof incomingDataFilter !== "undefined") {
          //Check if there is data
          if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
            //Convert the incoming date to the correct format
            if (labelinfo.indexOf('Date of Birth') != -1) {
              let datePipe = new DatePipe('en-US');
              let transformDate = datePipe.transform(new Date(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]), 'dd MMM yyyy')
              if (!transformDate) {
                this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
                this.registerScooter.controls[labelinfo].patchValue(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]])
              } else {
                this.chkLegalAgeString(transformDate)
                this.labelExpansionDetails[i].labelDetails.push(transformDate);
                this.registerScooter.controls[labelinfo].patchValue(transformDate)
              }
            } else {
              this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
              this.registerScooter.controls[labelinfo].patchValue(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]])

            }

          } else {
            //Set as - for no data
            this.labelExpansionDetails[i].labelDetails.push('-' as never);
          }
        }
      });
    }
  }

  checkValidation() {
    this.nextButtonValidFile = false;
    console.log(this.pmdRegistrationDoc.length)
    console.log(this.pmdRegistrationDoc)
    //if did not upload 2 photo or pdf
    if (this.pmdRegistrationDoc.length >= 2) {
      this.nextButtonValidFile = true;
    } else {
      this.nextButtonValidFile = false;

    }
  }

  chkLegalAge(event: any) {

    let datePipe = new DatePipe('en-US');
    try {
      let birthDate = datePipe.transform(event.target.value, "yyyy-MM-dd")


      console.log(birthDate)
      if (birthDate) {
        let timeDiff = Math.abs(Date.now() - new Date(birthDate).getTime());
        let age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
        console.log(age)
        if (age >= this.legalAge) {
          this.isLegalAgeErrorHidden = false;
        } else {
          this.isLegalAgeErrorHidden = true;

        }
      }
    } catch (error) {
      //if error then don't show the legal age error
      this.isLegalAgeErrorHidden = false;
    }

  }

  chkLegalAgeString(inputDOB: string) {
    let datePipe = new DatePipe('en-US');
    let birthDate = datePipe.transform(inputDOB, "yyyy-MM-dd")
    if (birthDate) {
      let timeDiff = Math.abs(Date.now() - new Date(birthDate).getTime());
      let age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      console.log(age)
      if (age >= this.legalAge) {
        this.isLegalAgeErrorHidden = false;
      } else {
        this.isLegalAgeErrorHidden = true;

      }
    }
  }
}
