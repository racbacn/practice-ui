import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { HomepageComponent } from './homepage.component';
import { HomepageRoutingModule } from './homepage-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import { WidgetsModule } from '../widgets/widgets.module';
import {MatMenuModule} from '@angular/material/menu';
@NgModule({
  declarations: [
    HomepageComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HomepageRoutingModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatIconModule,
    WidgetsModule,
    MatMenuModule
  ]
})
export class HomepageModule { }
