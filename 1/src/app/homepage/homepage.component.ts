import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransactionID } from '../interface/transactionID';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

    viewButtonConfig = {
    buttonText: 'View more',
    buttonClassName: 'dashboardButton',
    buttonFont: 'button-font-16',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonWidth: 'button-70',
};
nologinButtonConfig = {
  buttonText: 'No Login Required',
  buttonClassName: 'nologinButton',
  buttonFont: 'button-font-16',
  hasIcon: false,
  buttonWidth: 'button-70',
};
  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  registerEScooter() {
    console.log("click")
    // let tmpTransID : TransactionID = JSON.parse(JSON.stringify(this.reviewNowData));
    let tmpTransID : TransactionID = {
      transactionId: "1",
    } ;

    this.router.navigate(['/register-escooter-pab'], { state: { data: tmpTransID } });
  }

  ownerDashboard() {
    this.router.navigate(['/owner-dashboard']);
  }

  singpass(btnClick : string) {
    this.router.navigate(['/singpass-login'], { state: { data: btnClick } });
  }

  renewRoadTax() {
    this.router.navigate(['renew-road-tax'])
  }
}
