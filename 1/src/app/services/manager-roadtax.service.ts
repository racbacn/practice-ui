import {Injectable} from '@angular/core';
import {CalculateRoadtaxRequest} from "../interface/calculate-roadtax-request";
import {HttpClient} from "@angular/common/http";
import {CalculateRoadtaxResponse} from "../interface/calculate-roadtax-response";

@Injectable({
  providedIn: 'root'
})
export class ManagerRoadtaxService {
  private CALCULATE_ROADTAX_URL = "/manage-roadtax-service/calculateRoadTax"

  constructor(private httpClient: HttpClient) {
  }

  calculateRoadTax(calculateRoadTaxRequest: CalculateRoadtaxRequest) {
    let params: any = calculateRoadTaxRequest;

    return this.httpClient.get<CalculateRoadtaxResponse>(this.CALCULATE_ROADTAX_URL, {params})
  }
}
