import { Injectable, OnDestroy } from '@angular/core';
import { HttpServicesService } from '../services/http-services.service';
import { notifications } from '../interface/notifications';
import { Observable, timer, Subject } from 'rxjs';
import { switchMap, tap, share, retry, takeUntil, retryWhen, delayWhen } from 'rxjs/operators';
import { serverUrl } from '../constants/sharedValue';

@Injectable({
  providedIn: 'root'
})
export class PollNotificationService implements OnDestroy {
  private pollNotification$!: Observable<notifications>;
  private stopPolling = new Subject();

  constructor(private httpServices: HttpServicesService) {
    //Retry every 3seconds
    this.pollNotification$ = timer(1, 3000).pipe(
      switchMap(() => this.httpServices.httpGet(serverUrl + "/manage-notification-service/userNotifications/1")),
      // retry(),
      retryWhen(err =>
        err.pipe(
          delayWhen(() => timer(6000))
      )),
      share(),
      takeUntil(this.stopPolling)
   );
  }

  getNotifications(): Observable<notifications> {
    return this.pollNotification$.pipe(
      tap(() => console.log('data sent to subscriber'))
    );
  }

  ngOnDestroy() {
    this.stopPolling.next();
}
}
