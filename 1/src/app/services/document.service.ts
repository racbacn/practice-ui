import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {VehicleRegistrationDocumentResponse} from "../interface/vehicle-registration-document-response";
import {VehicleRegistrationDocumentRequest} from "../interface/vehicle-registration-document-request";
// import {ConfidenceScoreRequesst} from "../interface/confidence-score-request";

@Injectable({
    providedIn: 'root'
})
export class DocumentService {
    CREATE_DOCUMENT_URL: string = '/manage-document-service/createDocument'
    // GENERATE_CONDFIDENCE_SCORE: string = '/manage-document-service-textract/generateConfidenceScores'

    constructor(private http: HttpClient) {
    }

    createDocumentId(vehicleRegistrationDocumentRequest: VehicleRegistrationDocumentRequest) {
        return this.http.post<VehicleRegistrationDocumentResponse>(this.CREATE_DOCUMENT_URL, vehicleRegistrationDocumentRequest)
    }

    // generateConfidenceScore(confidenceScoreRequest: ConfidenceScoreRequest) {
    //     return this.http.post(this.GENERATE_CONDFIDENCE_SCORE, confidenceScoreRequest)
    // }
}
