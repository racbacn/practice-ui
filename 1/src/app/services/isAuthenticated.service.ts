import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedService {

  loggedIn?: boolean;

constructor() { }

sendLoginStatus(isLoggedin: boolean) {
    this.loggedIn = isLoggedin;
}

getLoginStatus() {
  return this.loggedIn;
}

}
