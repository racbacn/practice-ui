import {Injectable} from '@angular/core';
import {UserResponse} from "../interface/user-response";
import {HttpClient} from "@angular/common/http";
import {DeregisterVehicleRequest} from "../interface/deregister-vehicle-request";
import {DeregisterVehicleResponse} from "../interface/deregister-vehicle-response";
import {VehicleRoadTaxRenewalRequest} from "../interface/vehicle-road-tax-renewal-request";
import {VehicleRoadTaxRenewalResponse} from "../interface/vehicle-road-tax-renewal-response";

@Injectable({
  providedIn: 'root'
})
export class ManageVehicleService {
  private DEREGISTER_VEHICLE_URL = "/manage-vehicle-service/deregisterVehicle";
  private VEHICLE_ROADTAX_RENEWAL_URL = "/manage-vehicle-service/get/VehiclesForTaxRenewal";


  constructor(private httpClient: HttpClient) {
  }

  deregisterVehicle(deregisterVehicleRequest: DeregisterVehicleRequest) {
    return this.httpClient.post<DeregisterVehicleResponse>(this.DEREGISTER_VEHICLE_URL, deregisterVehicleRequest);
  }

  vehiclesForTaxRenewals(vehicleRoadTaxRenewalRequest: VehicleRoadTaxRenewalRequest) {
    let params: any = vehicleRoadTaxRenewalRequest;

    return this.httpClient.get<VehicleRoadTaxRenewalResponse>(this.VEHICLE_ROADTAX_RENEWAL_URL, {params});
  }

}
