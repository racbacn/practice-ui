import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AwsCognitoService} from "./aws-cognito.service";
import {Location} from '@angular/common';
import {catchError, finalize, switchMap} from 'rxjs/operators';
import {SessionUtil} from "../core/util/sessionUtil";
import {serverUrl} from "../constants/sharedValue";
import {HttpServicesService} from "./http-services.service";


@Injectable({
  providedIn: 'root'
})
export class TokenResolver implements Resolve<any> {
  getDataFrmHomePage: any;

  constructor(private location: Location,
              private awsCognitoService: AwsCognitoService,
              private router: Router,
              private httpservice: HttpServicesService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
    const code: string | null = urlParams.get('code');

    if (!code) {
      return of(null);
    }

    return this.getTokenDetailsFromCognito(code).pipe(
      finalize(() => {
        this.location.replaceState(window.location.pathname);
      })
    );
  }

  getTokenDetailsFromCognito(code: string): Observable<any | null> {
    return this.awsCognitoService.getTokenDetailsFromCognito(code).pipe(
      switchMap((response: any) => {
        console.log('Response: ', response);

        SessionUtil.setSessionStorage(SessionUtil.TOKEN, response.access_token)

        if (response) {
          this.httpservice.httpGet(serverUrl + '/manage-user-service/user/' + 'defaultUserVO').subscribe(res => {
            if (res) {
              if (this.getDataFrmHomePage == 'ownerDashboard') {
                this.router.navigate(['/owner-dashboard'], {state: {data: res}});

              } else if (this.getDataFrmHomePage == 'regEScooter') {
                this.router.navigate(['/register-escooter-pab'], {state: {data: res}});

              } else {
                //default route
                this.router.navigate(['/owner-dashboard'], {state: {data: res}});
              }
              //set the userId and userTypeKey
              localStorage.setItem("userId", String(res.userId));
              localStorage.setItem("userTypeKey", String(res.userTypeKey));
            } else {
              console.log("Login Failed")
            }

          })
        }

        return of(response);
      }),
      catchError((error) => {
        return error;
      })
    );
  }
}
