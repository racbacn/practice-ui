import { TestBed } from '@angular/core/testing';

import { CertifyRefundService } from './certify-refund.service';

describe('CertifyRefundService', () => {
  let service: CertifyRefundService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CertifyRefundService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
