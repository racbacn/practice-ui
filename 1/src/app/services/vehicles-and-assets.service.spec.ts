import { TestBed } from '@angular/core/testing';

import { VehiclesAndAssetsService } from './vehicles-and-assets.service';

describe('VehiclesAndAssetsService', () => {
  let service: VehiclesAndAssetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehiclesAndAssetsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
