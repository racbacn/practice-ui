import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CertifyRefundRequest} from "../interface/certify-refund-request";
import {RoadTaxRefundResponse} from "../interface/road-tax-refund-response";

@Injectable({
  providedIn: 'root'
})
export class CertifyRefundService {
  private CERTIFY_REFUND_URL = "/manage-roadtax-service/certifyRefund";
  private ROAD_TAX_REFUND_URL = "/manage-roadtax-service/getRoadTaxRefund"
  private USER_TYPE_KEY_URL = "?userTypeKey"
  private VEHICLE_NUMBER_URL = "&vehicleNumber"

  constructor(private httpClient: HttpClient) {
  }

  certifyRefund(certifyRefundRequest: CertifyRefundRequest) {
    return this.httpClient.post(this.CERTIFY_REFUND_URL, certifyRefundRequest);
  }

  getRoadTaxRefund(userTypeKey: number, vehicleNumber: string) {
    return this.httpClient.get<RoadTaxRefundResponse>(this.ROAD_TAX_REFUND_URL + this.USER_TYPE_KEY_URL + "=" + userTypeKey + this.VEHICLE_NUMBER_URL + "=" + vehicleNumber)
  }
}
