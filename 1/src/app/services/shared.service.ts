import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {EscooterRegistration} from '../interface/escooter-registration';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() {
  }

  private subjectApprove = new Subject<object>();
  private subjectReject = new Subject<object>();
  private subjectRejectAPIPut = new Subject<object>();
  private escooterRegForm = new Subject<object>();
  private escooterRegResponse = new Subject<object>();
  private escooterRegPaymentResponse = new Subject<object>();
  private escooterReSubmitResponse = new Subject<object>();
  private vehicleRoadTaxRenew = new Subject<object>();
  private vehicleRoadTaxRenewWithNewExpiry = new Subject<object>();
  private paymentRoadTaxRenew = new Subject<object>();
  private certifyRefundResponse = new Subject<object>();
  private calculateRoadtax = new BehaviorSubject<object>({});


  sendApproveClick(dataInput: object) {
    this.subjectApprove.next(dataInput);
  }

  getApproveClick(): Observable<any> {
    return this.subjectApprove;
  }

  sendRejectClick(dataInput: object) {
    this.subjectReject.next(dataInput);
  }

  getRejectClick(): Observable<any> {
    return this.subjectReject;
  }

  sendRejectAPIPut(dataInput: object) {
    this.subjectRejectAPIPut.next(dataInput);
  }

  getRejectAPIPut(): Observable<any> {
    return this.subjectRejectAPIPut;
  }

  sendEscooterRegForm(dataInput: EscooterRegistration) {
    this.escooterRegForm.next(dataInput);
  }

  getEscooterRegForm(): Observable<any> {
    return this.escooterRegForm;
  }

  sendEscooterRegResponse(dataInput: object) {
    this.escooterRegResponse.next(dataInput);
  }

  getEscooterRegResponse(): Observable<any> {
    return this.escooterRegResponse;
  }

  sendEscooterRegPaymentResp(dataInput: object) {
    this.escooterRegPaymentResponse.next(dataInput);
  }

  getEscooterRegPaymentResp(): Observable<any> {
    return this.escooterRegPaymentResponse;
  }

  sendEscooterReSubmitResponse(dataInput: object) {
    this.escooterReSubmitResponse.next(dataInput);
  }

  getEscooterReSubmitResponse(): Observable<any> {
    return this.escooterReSubmitResponse;
  }

  sendRenewalRoadTax(dataInput: object) {
    this.vehicleRoadTaxRenewWithNewExpiry.next(dataInput);
  }

  getRenewalRoadTax(): Observable<any> {
    return this.vehicleRoadTaxRenewWithNewExpiry;
  }

  sendRenewalRoadTaxWithNewExpiry(dataInput: object) {
    this.vehicleRoadTaxRenew.next(dataInput);
  }

  getRenewalRoadTaxWithNewExpiry(): Observable<any> {
    return this.vehicleRoadTaxRenew;
  }

  sendPaymentRenewalRoadTax(dataInput: object) {
    this.paymentRoadTaxRenew.next(dataInput);
  }

  getPaymentRenewalRoadTax(): Observable<any> {
    return this.paymentRoadTaxRenew;
  }

  sendCertifyRefundResponse(dataInput: object) {
    this.certifyRefundResponse.next(dataInput);
  }

  getCertifyRefundResponse(): Observable<any> {
    return this.certifyRefundResponse;
  }

  sendCalculateRoadtax(dataInput: object) {
    this.calculateRoadtax.next(dataInput);
  }

  getCalculateRoadtax(): Observable<any> {
    return this.calculateRoadtax;
  }

}
