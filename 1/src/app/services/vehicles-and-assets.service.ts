import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from 'src/environments/environment.prod';

interface NormalVehicle {
  roadTaxId: number;
  roadTaxValidTo: string;
  vehicleNumber: number;
  vehicleMakeModel: string;
  vehicleType: string;
}

interface TransactionsListResponse {
  transactions: NormalVehicle[];
}

interface PmdVehicle {
  pmdColour: string;
  pmdMake: string;
  pmdModel: string;
  safetyStandardSerialNum: string;
  vehicleType: string;
}

interface PmdTransactionsListResponse {
  pmdDetails: PmdVehicle[];
}

@Injectable({
  providedIn: 'root',
})
export class VehicleAndAssetsService {
  SERVER_URL = environment.serverUrl;
  API_ENDPOINT = '/manage-vehicle-service';
  private isUpdated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  getDetails(id: number): Observable<any> {
    const outerEndpoint = '/vehicleDetails/';
    const apiUrl = this.SERVER_URL + this.API_ENDPOINT + outerEndpoint + id;
    return this.http.get<any>(apiUrl);
  }

  setNormalVehicles(): Observable<NormalVehicle[]> {
    let params = new HttpParams();
    params = params.append('userId', 1);
    params = params.append('userTypeKey', 1);
    const outerEndpoint = '/get/vehicleDetails';
    const apiUrl = this.SERVER_URL + this.API_ENDPOINT + outerEndpoint;
    return this.http
      .get<TransactionsListResponse>(apiUrl, {params})
      .pipe(map((response) => response.transactions));
  }

  getNormalVehicles(): Promise<NormalVehicle[]> {
    return new Promise((resolve, reject) => {
      this.setNormalVehicles().subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  setPmdVehicles(): Observable<PmdVehicle[]> {
    const apiEndpoint = '/manage-pmd-service/owner/get/vehicleDetails';
    let params = new HttpParams();
    params = params.append('userId', 1);
    params = params.append('userTypeKey', 1);
    return this.http
      .get<PmdTransactionsListResponse>(apiEndpoint, {params})
      .pipe(map((response) => response.pmdDetails));
  }

  getPmdVehicles(): Promise<PmdVehicle[]> {
    return new Promise((resolve, reject) => {
      this.setPmdVehicles().subscribe(
        (response) => resolve(response),
        (error) => reject(error)
      );
    });
  }

  setIsVehicleUpdated(isUpdated: boolean) {
    this.isUpdated.next(isUpdated)
  }

  getIsVehicleUpdated(): Observable<boolean> {
    return this.isUpdated;
  }
}
