import { TestBed } from '@angular/core/testing';

import { ManagerRoadtaxService } from './manager-roadtax.service';

describe('ManagerRoadtaxService', () => {
  let service: ManagerRoadtaxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagerRoadtaxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
