import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private transactionId$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  setTransactionId(transactionId: string) {
    this.transactionId$.next(transactionId);
  }

  getTransactionId(): BehaviorSubject<string> {
    return this.transactionId$;
  }
}
