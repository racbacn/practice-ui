import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserResponse} from "../interface/user-response";

@Injectable({
    providedIn: 'root'
})
export class ManageUserService {
    private USER_URL = '/manage-user-service/user';
    private IDENTIFICATION_VALUE_URL = "/identification"
    private IDENTIFICATION_TYPE_URL = "?identificationType"

    constructor(private httpClient: HttpClient) {
    }

    getUserId(user: string) {
        return this.httpClient.get<UserResponse>(this.USER_URL + '/' + user);
    }

    getUserIdByExternalIdentification(identificationValue: string, identificationType: number) {
        return this.httpClient.get<UserResponse>(this.USER_URL + this.IDENTIFICATION_VALUE_URL + '/'
            + identificationValue + this.IDENTIFICATION_TYPE_URL + '=' + identificationType);
    }
}
