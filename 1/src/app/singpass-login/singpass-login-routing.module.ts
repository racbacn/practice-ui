import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingpassLoginComponent } from './singpass-login.component';
const routes: Routes = [{ path: '', component: SingpassLoginComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SingpassLoginRoutingModule { }
