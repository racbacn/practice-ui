import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingpassLoginComponent } from './singpass-login.component';

describe('SingpassLoginComponent', () => {
  let component: SingpassLoginComponent;
  let fixture: ComponentFixture<SingpassLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingpassLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingpassLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
