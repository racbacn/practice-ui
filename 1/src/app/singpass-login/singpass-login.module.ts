import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { WidgetsModule } from '../widgets/widgets.module';
import { SingpassLoginRoutingModule } from './singpass-login-routing.module';
import { SingpassLoginComponent } from './singpass-login.component';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SingpassLoginComponent
  ],
  imports: [
    CommonModule,
    SingpassLoginRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatIconModule,
    WidgetsModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SingpassLoginModule { }
