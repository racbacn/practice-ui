import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IsAuthenticatedService} from '../services/isAuthenticated.service';
import {SharedService} from '../services/shared.service';
import {HttpServicesService} from '../services/http-services.service';
import {serverUrl} from '../constants/sharedValue';
import {state} from '@angular/animations';
import {LoginAuthenticationService} from "../core/services/login-authentication.service";
import {SessionUtil} from "../core/util/sessionUtil";
import {UserResponse} from "../interface/user-response";

@Component({
  selector: 'app-singpass-login',
  templateUrl: './singpass-login.component.html',
  styleUrls: ['./singpass-login.component.scss'],

})
export class SingpassLoginComponent implements OnInit {

  usernameQR: string = "defaultUserVO"
  inputForm: any;
  username: string = '';
  password: string = '';
  mockPassword: string = 'kenneth'

  constructor(private router: Router,
              private loginService: IsAuthenticatedService,
              private httpservice: HttpServicesService,
              private loginAuthenticationService: LoginAuthenticationService) {
  }

  getDataFrmHomePage: any;

  ngOnInit(): void {
    this.getDataFrmHomePage = history.state.data;
    console.log(this.getDataFrmHomePage)
  }

  Dashboard() {
    this.username = 'defaultUserVO';
    this.password = 'P@ssw0rd2021';

    this.loginAuthenticationService.login(this.username, this.password).then((data: any) => {
      let responseData: any = data;

      if (responseData) {
        //Put back when the api is up
        this.httpservice.httpGet(serverUrl + '/manage-user-service/user/' + responseData.username).subscribe(res => {
          // let res = {
          //   "responseHeader": {
          //     "responseAppId": "VRLS_USER_BY_USERNAME",
          //     "responseDateTime": "2021-09-29T18:21:35"
          //   },
          //   "userId": 19,
          //   "userTypeKey": 1,
          //   "username": "defaultUserVO"
          // }
          if (res) {
            if (this.getDataFrmHomePage == 'ownerDashboard') {
              this.router.navigate(['/owner-dashboard'], {state: {data: res}});

            } else if (this.getDataFrmHomePage == 'regEScooter') {
              this.router.navigate(['/register-escooter-pab'], {state: {data: res}});

            } else {
              //default route
              this.router.navigate(['/owner-dashboard'], {state: {data: res}});
            }
            //set the userId and userTypeKey
            SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(res));
            localStorage.setItem("userId", String(res.userId));
            localStorage.setItem("userTypeKey", String(res.userTypeKey));
          } else {
            console.log("Login Failed")
          }

        })
      }

    });


  }

  login() {
    console.log(this.username, this.password)

    this.loginAuthenticationService.login(this.username, this.password).then((data: any) => {
      let responseData: any = data;

      if (responseData) {
        this.httpservice.httpGet(serverUrl + '/manage-user-service/user/' + responseData.username).subscribe(res => {
          console.log(res)

          if (res) {
            if (this.getDataFrmHomePage == 'ownerDashboard') {
              this.router.navigate(['/owner-dashboard'], {state: {data: res}});

            } else if (this.getDataFrmHomePage == 'regEScooter') {
              this.router.navigate(['/register-escooter-pab'], {state: {data: res}});

            } else {
              //default route
              this.router.navigate(['/owner-dashboard'], {state: {data: res}});
            }
            //set the userId and userTypeKey
            SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(res));
            localStorage.setItem("userId", String(res.userId));
            localStorage.setItem("userTypeKey", String(res.userTypeKey));
          } else {
            console.log("Login Failed")
          }

        })
        // this.router.navigate(['/owner-dashboard']);
      } else {
        alert("Invalid Password")
      }

    })
  }

  loginSSO() {
    this.loginAuthenticationService.loginSSO()
  }

}
