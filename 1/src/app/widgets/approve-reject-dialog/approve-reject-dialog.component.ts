import {Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {VehicleAndAssetsService} from "../../services/vehicles-and-assets.service";

@Component({
  selector: 'app-approve-reject-dialog',
  templateUrl: './approve-reject-dialog.component.html',
  styleUrls: ['./approve-reject-dialog.component.scss'],
})
export class ApproveRejectDialogComponent implements OnInit {

  dashboardButtonConfig = {
    buttonText: 'Back to Dashboard',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonClassName: 'dashboardButton',
    buttonFont: 'button-font-16',
  };

  constructor(
    private router: Router,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      icon: string;
      tick: string;
      title: string;
      details: string;
    }, public dialog: MatDialogRef<ApproveRejectDialogComponent>,
    private vehicleAndAssetsService: VehicleAndAssetsService
  ) {
    dialog.disableClose = true;
  }

  ngOnInit(): void {
  }

  onClose() {
    console.error('test')
    this.vehicleAndAssetsService.setIsVehicleUpdated(true);
    this.router.navigate(['/owner-dashboard']);
    this.dialog.close();
  }

  goDashboard() {
    console.error('test')
    this.vehicleAndAssetsService.setIsVehicleUpdated(true);
    this.router.navigate(['/owner-dashboard']);
    this.dialog.close();
  }
}
