import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { DialogComponent } from './dialog/dialog.component';
import { ButtonComponent } from './button/button.component';
import { ApproveRejectDialogComponent } from './approve-reject-dialog/approve-reject-dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FooterComponent } from './footer/footer.component';
import {ErrorComponent} from "./error/error.component";


@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatButtonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
  ],
  exports: [
    ModalComponent,
    FlexLayoutModule,
    MaterialModule,
    ButtonComponent,
    FooterComponent
  ],

  declarations: [
    ModalComponent,
    ButtonComponent,
    DialogComponent,
    ApproveRejectDialogComponent,
    FooterComponent,
    ErrorComponent
  ]
})
export class WidgetsModule { }
