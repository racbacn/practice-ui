import {Component, OnInit, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from '../dialog/dialog.component';
import {ApproveRejectDialogComponent} from 'src/app/widgets/approve-reject-dialog/approve-reject-dialog.component';
import {SharedService} from 'src/app/services/shared.service';
import {HttpServicesService} from 'src/app/services/http-services.service';
import {Subscription} from 'rxjs';
import {serverUrl} from 'src/app/constants/sharedValue';
import {rejectRegistration} from 'src/app/interface/rejectRegistration';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit, OnDestroy {
  clickApproveRejectsubscription: Subscription = new Subscription;
  apiUrl: string = serverUrl;
  datePipe: DatePipe = new DatePipe("en-US");

  constructor(public dialog: MatDialog, private sharedService: SharedService, private httpServices: HttpServicesService) {
    this.clickApproveRejectsubscription = this.sharedService.getApproveClick().subscribe((dataIn) => {
      this.openApproveDialog(dataIn["transactionId"]);
    })
    const rejectSubscription = this.sharedService.getRejectClick().subscribe((dataIn) => {
      this.openDialog(dataIn["transactionId"]);
    })
    this.clickApproveRejectsubscription.add(rejectSubscription);

    const certifyRefundResponse: Subscription = this.sharedService.getCertifyRefundResponse().subscribe((dataIn) => {
      if (dataIn) {
        this.openCertifyDialog();
      }
    })

    this.clickApproveRejectsubscription.add(certifyRefundResponse)
  }

  openDialog(transID: string) {
    let dialogReject = this.dialog.open(DialogComponent, {
      minHeight: '600px',
      width: '80%',
      data: {
        details: "Transaction No.: " + transID,

      }
    });
    dialogReject.afterClosed().subscribe(res => {
      //Prevent press the closed X button and send the api
      if (res !== null) {
        let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
        console.log(timeNow)
        if (!timeNow) {
          timeNow = "";
        }
        let rejectContent = {
          "requestHeader": {
            "requestAppId": "VRLS_Reject_Vehicle_Registration",
            "requestDateTime": timeNow,
            "userId": Number(localStorage.getItem("userId")),
            "userTypeKey": Number(localStorage.getItem("userTypeKey")),

          },
          transactionId: transID,
          comment: res,

        };
        console.log(rejectContent)

        this.sendRejectAPI(rejectContent)
      }
    })
  }

  openCertifyDialog() {
    this.dialog.open(ApproveRejectDialogComponent, {
      minHeight: '480px',
      width: '80%',
      data: {
        icon: "../../../assets/images/approve-icon.svg",
        tick: "../../../assets/images/tick-icon.svg",
        title: "Vehicle has been successfully Deregistered",
        details: "Your Remaining Road Tax amount will be refunded to you via GIRO in 5 - 10 working days\n" +
          "\n" +
          "\n" +
          "(This is a mock feature for PoC purposes)",
      }
    });
  }

  openApproveDialog(transID: string) {
    this.dialog.open(ApproveRejectDialogComponent, {
      minHeight: '480px',
      width: '80%',
      data: {
        icon: "../../../assets/images/approve-icon.svg",
        tick: "../../../assets/images/tick-icon.svg",
        title: "APPLICATION HAS BEEN APPROVED",
        details: "Transaction No.: " + transID,
      }
    });
  }

  openRejectDialog(transID: string) {
    this.dialog.open(ApproveRejectDialogComponent, {
      minHeight: '480px',
      width: '80%',
      data: {
        icon: "../../../assets/images/reject-icon.svg",
        tick: "../../../assets/images/x_reject.svg",
        title: "APPLICATION HAS BEEN REJECTED",
        details: "Transaction No.: " + transID,
      }
    });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.clickApproveRejectsubscription.unsubscribe();
  }

  sendRejectAPI(dataPut: rejectRegistration) {
    this.httpServices.httpPut(this.apiUrl + "/manage-vehicle-service/rejectVehicleRegistration", dataPut).subscribe(res => {
      console.log(res)
      this.openRejectDialog(dataPut.transactionId)

    });
  }

}
