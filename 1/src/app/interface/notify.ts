export interface notify {
  notifId: string,
  subject: string,
  transactionId: string,
  transactionRefNum: string,
  transactionComment: string,
  transactionTypeKey: string
}
