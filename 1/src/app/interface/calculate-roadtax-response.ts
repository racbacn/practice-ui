import {RoadTaxPayment} from "./road-tax-payment";
import {RequestHeaderUserType} from "./request-header-user-type";

export interface CalculateRoadtaxResponse {
  requestHeader: RequestHeaderUserType;
  roadTaxPayments: Array<RoadTaxPayment>
}
