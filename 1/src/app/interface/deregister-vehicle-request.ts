import {RequestHeaderUserType} from "./request-header-user-type";

export interface DeregisterVehicleRequest {
  requestHeader: RequestHeaderUserType;
  vehicleNumber: string;
}
