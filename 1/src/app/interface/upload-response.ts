export interface UploadResponse {
    isSuccessful: boolean;
    message: string;
}
