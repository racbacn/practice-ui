import {VehicleRoadTaxRenewal} from "./vehicle-road-tax-renewal";

export interface VehicleRoadTaxRenewalResponse {
  vehicleRoadTaxRenewalList: Array<VehicleRoadTaxRenewal>;
}
