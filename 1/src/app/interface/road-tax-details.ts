export interface RoadTaxDetails {
  insuranceCompany: string;
  insuranceCoverNoteNumber: string;
  vehicleNumberAvailable: string;
  openMarketValue: string;
  roadTaxMonths: string;
  refundCoeTo: string;
  refundAmount: string
}
