export interface OwnerDetails {
  ownerIdType: string;
  ownerIdTypeDesc: string,
  ownerId: string;
  ownerName: string;
  ownerPostalCode: string;
  salesAgreementId: string;
  ownerAddressLine1: string;
  ownerAddressLine2: string;
  ownerContactNum: string;
  ownerDOB: string;
  ownerEmailAddress: string,
}
