export interface RequestHeaderUserType {
    requestAppId: string;
    requestDateTime: string;
    userId: number;
    userTypeKey: number;
}
