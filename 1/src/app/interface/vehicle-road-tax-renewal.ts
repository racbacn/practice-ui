export interface VehicleRoadTaxRenewal {
  vehicleNumber: string;
  expiryDate: string;
  coeExpiryDate: string;
  propellantType: number;
}
