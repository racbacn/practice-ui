import { RequestHeaderUserType } from './request-header-user-type'

export interface rejectRegistration {
  requestHeader : RequestHeaderUserType;
  transactionId: string;
  comment: string;
}
