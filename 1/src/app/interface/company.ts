export interface Company {
    serialNo: number;
    companyCode: string;
    companyName: string
}
