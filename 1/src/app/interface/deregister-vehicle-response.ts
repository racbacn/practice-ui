import {OwnerDetails} from "./owner-details";

export interface DeregisterVehicleResponse {
  transactionRefNum: string;
  deregistrationTransactionId: number;
  deregistrationDateTime: string;
  ownerDetails: OwnerDetails;
}
