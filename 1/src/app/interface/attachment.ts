export interface Attachment {
    id: number;
    selection: number;
    name: string;
}
