import { RequestHeader } from "./request-header";
import { OwnerDetails } from "./owner-details";
import { PmdDetails } from "./pmd-details";

export interface EscooterRegistration {
  requestHeader: RequestHeader;
  ownerDetails: OwnerDetails;
  pmdDetails: PmdDetails;
}
