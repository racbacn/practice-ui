export interface CalculateRoadtaxRequest {
  vehicleRefNum: string;
  vehiclePropellantTypeKey: number;
  licensingStartDate: string;
  licensingEndDate: string;
  vehicleRegistrationDate: string;
}
