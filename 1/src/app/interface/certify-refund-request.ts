import {OwnerDetails} from "./owner-details";

export interface CertifyRefundRequest {
  assignedOfficerId: number;
  createdBy: string;
  deregistrationTransactionId: number;
  deregistrationDateTime: string;
  ownerDetails: OwnerDetails;
  refundAmountAfterGST: number;
  refundAmountBeforeGST: number;
  refundGSTAmount: number;
  refundMethodTypeKey: number;
  refundRemarks: string;
  vehicleNumber: string
}
