import { OwnerDetails } from "./owner-details"
import { VehicleDetails } from "./vehicle-details";
import { RoadTaxDetails } from "./road-tax-details";
export interface VehicleRequest {
  ownerDetails: OwnerDetails;
  vehicleDetails: VehicleDetails;
  roadTaxDetails: RoadTaxDetails;
}
