export interface UsernameDetails {
responseHeader: {
    responseAppId: string,
    responseDateTime: string
},
userId: number,
userTypeKey: number,
username: string

}
