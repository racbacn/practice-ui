import {ResponseHeader} from "./response-header";

export interface RoadTaxRefundResponse {
  responseHeader: ResponseHeader;
  roadTaxRefundAmount: string;
}
