import { RequestHeader } from "./request-header";
import { notify } from "./notify";
export interface notifications {
  responseHeader: RequestHeader,
  userid: string,
  notifications: Array <notify>
}
