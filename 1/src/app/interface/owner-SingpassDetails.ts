export interface OwnerSingPassDetails {
  ownerId: string;
  ownerName: string;
  ownerDOB: string;
  ownerAddressLine1: string;
  ownerAddressLine2: string;
  ownerPostalCode: string;
  ownerContactNum: string;
}
