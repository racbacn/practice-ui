import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OwnerDashboardRoutingModule } from './owner-dashboard-routing.module';
import { OwnerDashboardComponent } from './owner-dashboard.component';
import { WidgetsModule } from '../widgets/widgets.module';
import { InboxModule } from '../inbox/inbox.module';
import { VehiclesAndAssetsModule } from './vehicles-and-assets/vehicles-and-assets.module';
import { ProfileModule } from './profile/profile.module';

@NgModule({
  declarations: [
    OwnerDashboardComponent,
    // InboxComponent
  ],
  imports: [
    CommonModule,
    OwnerDashboardRoutingModule,
    WidgetsModule,
    InboxModule,
    VehiclesAndAssetsModule,
    ProfileModule,
  ],
})
export class OwnerDashboardModule {}
