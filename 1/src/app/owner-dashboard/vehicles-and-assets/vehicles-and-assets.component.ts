import {Component, OnInit} from '@angular/core';
import {VehicleAndAssetsService} from 'src/app/services/vehicles-and-assets.service';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-vehicles-and-assets',
  templateUrl: './vehicles-and-assets.component.html',
  styleUrls: ['./vehicles-and-assets.component.scss'],
})
export class VehiclesAndAssetsComponent implements OnInit {
  vehicles: any[] = [];
  vehicleIds: string[] = [];
  vehicle = {};
  hasLoaded = false;

  constructor(private vehicleAndAssetsService: VehicleAndAssetsService) {
  }

  ngOnInit(): void {
    this.getVehicles();

    this.vehicleAndAssetsService.getIsVehicleUpdated().subscribe(data => {
      if (data) {
        this.getVehicles();
        this.vehicleAndAssetsService.setIsVehicleUpdated(false);
      }
    })
  }

  getVehicleIds() {
    this.vehicleIds = this.vehicles.map(
      (vehicle) => vehicle?.vehicleNumber || vehicle?.registrationMarkId
    );
  }

  setDisplayedVehicle(vehicleNumber: string) {
    this.vehicle = this.vehicles.find(
      (vehicle) =>
        vehicle?.vehicleNumber === vehicleNumber ||
        vehicle?.registrationMarkId === vehicleNumber
    );
  }

  vehicleIdChange(id: string) {
    this.setDisplayedVehicle(id);
  }

  async getVehicles() {
    try {
      this.vehicles = await this.vehicleAndAssetsService.getNormalVehicles();
      if (!this.vehicles.length) {
        console.warn('NO NORMAL VEHICLES AVAILABLE...');
      }
      this.getVehicleIds();
      const initialVehicleId = this.vehicleIds[0];
      this.setDisplayedVehicle(initialVehicleId);
      this.hasLoaded = true;
    } catch (error) {
      console.error('CANNOT GET NORMAL VEHICLES...');
    }
    try {
      const pmdVehicles = await this.vehicleAndAssetsService.getPmdVehicles();
      if (!pmdVehicles.length) {
        console.warn('NO NORMAL PMD AVAILABLE...');
      }
      this.vehicles = [...this.vehicles, ...pmdVehicles];
      this.getVehicleIds();
      const initialVehicleId = this.vehicleIds[0];
      this.setDisplayedVehicle(initialVehicleId);
      this.hasLoaded = true;
    } catch (error) {
      console.error('CANNOT GET PMD VEHICLES...');
    }
  }
}
