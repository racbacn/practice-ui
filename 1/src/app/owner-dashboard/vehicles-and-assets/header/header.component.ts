import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() vehicleIds: string[] = [];
  @Output() vehicleIdChange: EventEmitter<string> = new EventEmitter<string>();
  vehicleIdForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initializeVehicleIdForm();
  }

  initializeVehicleIdForm() {
    this.vehicleIdForm = this.formBuilder.group({
      vehicleId: this.formBuilder.control(this.vehicleIds[0]),
    });
  }

  onVehicleIdChange() {
    const { vehicleId } = this.vehicleIdForm.value;
    this.vehicleIdChange.next(vehicleId);
  }
}
