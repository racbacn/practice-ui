import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CertifyRefundService} from "../../../services/certify-refund.service";
import {CertifyRefundRequest} from "../../../interface/certify-refund-request";
import {SharedService} from "../../../services/shared.service";
import {SessionUtil} from "../../../core/util/sessionUtil";
import {ResponseHeader} from "../../../interface/response-header";
import {UserResponse} from "../../../interface/user-response";
import {ManageVehicleService} from "../../../services/manage-vehicle.service";
import {DeregisterVehicleRequest} from "../../../interface/deregister-vehicle-request";
import {DeregisterVehicleResponse} from "../../../interface/deregister-vehicle-response";
import {RoadTaxRefundResponse} from "../../../interface/road-tax-refund-response";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() vehicle: any;
  datePipe: DatePipe = new DatePipe("en-US");

  constructor(private router: Router, private certifyRefundService: CertifyRefundService, private manageVehicleService: ManageVehicleService, private sharedService: SharedService) {
  }

  ngOnInit(): void {
  }

  setBlueButtonColor(type: string) {
    return type === 'PMD' ? 'blue small-margin' : 'blue';
  }

  renewRoadTax() {
    this.router.navigate(['renew-road-tax'])
  }

  onDeregisterVehicle() {
    let userResponse: UserResponse | null = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER));

    if (userResponse) {
      let dateTime = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss")

      if (dateTime) {
        let deregisterVehicleRequest: DeregisterVehicleRequest = {
          requestHeader: {
            requestAppId: "string",
            requestDateTime: dateTime,
            userId: userResponse.userId,
            userTypeKey: userResponse.userTypeKey
          },
          vehicleNumber: this.vehicle.vehicleNumber
        }

        console.log(deregisterVehicleRequest)

        this.manageVehicleService.deregisterVehicle(deregisterVehicleRequest).subscribe(data => {
          let deregisterVehicleResponse: DeregisterVehicleResponse = data;

          console.log(deregisterVehicleResponse)

          if (userResponse) {
            this.certifyRefundService.getRoadTaxRefund(userResponse.userTypeKey, this.vehicle.vehicleNumber).subscribe(data => {
              let roadTaxRefundResponse: RoadTaxRefundResponse = data

              console.log(roadTaxRefundResponse)

              if (roadTaxRefundResponse) {
                let refundAmountBeforeGSTResponse: number = parseInt(roadTaxRefundResponse.roadTaxRefundAmount);
                let refundGSTAmountResponse: number = (refundAmountBeforeGSTResponse * 70) / 100;
                let refundAmountAfterGSTResponse: number = refundAmountBeforeGSTResponse + refundGSTAmountResponse;

                let certifyRefundRequest: CertifyRefundRequest;

                if (userResponse) {
                  certifyRefundRequest = {
                    assignedOfficerId: 5,
                    createdBy: userResponse.userId.toString(),
                    deregistrationTransactionId: deregisterVehicleResponse.deregistrationTransactionId,
                    deregistrationDateTime: deregisterVehicleResponse.deregistrationDateTime,
                    ownerDetails: deregisterVehicleResponse.ownerDetails,
                    refundAmountAfterGST: refundAmountAfterGSTResponse,
                    refundAmountBeforeGST: refundAmountBeforeGSTResponse,
                    refundGSTAmount: refundGSTAmountResponse,
                    refundMethodTypeKey: 1,
                    refundRemarks: "",
                    vehicleNumber: this.vehicle.vehicleNumber
                  }

                  this.certifyRefundService.certifyRefund(certifyRefundRequest).subscribe(data => {
                    if (data) {
                      this.sharedService.sendCertifyRefundResponse(data);
                    }
                  });
                }
              }
            });
          }
        });
      }
    }
  }
}
