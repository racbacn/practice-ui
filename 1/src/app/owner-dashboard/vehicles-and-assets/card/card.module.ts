import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from '../../widgets/widgets.module';

@NgModule({
  declarations: [CardComponent],
  imports: [CommonModule, MaterialModule, FlexLayoutModule, WidgetsModule],
  exports: [CardComponent],
})
export class CardModule {}
