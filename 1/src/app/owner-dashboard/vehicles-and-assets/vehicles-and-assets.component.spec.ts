import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesAndAssetsComponent } from './vehicles-and-assets.component';

describe('VehiclesAndAssetsComponent', () => {
  let component: VehiclesAndAssetsComponent;
  let fixture: ComponentFixture<VehiclesAndAssetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehiclesAndAssetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesAndAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
