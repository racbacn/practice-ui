import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehiclesAndAssetsComponent } from './vehicles-and-assets.component';
import { HeaderModule } from './header/header.module';
import { CardModule } from './card/card.module';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  declarations: [VehiclesAndAssetsComponent],
  imports: [CommonModule, MaterialModule, HeaderModule, CardModule],
  exports: [VehiclesAndAssetsComponent],
})
export class VehiclesAndAssetsModule {}
