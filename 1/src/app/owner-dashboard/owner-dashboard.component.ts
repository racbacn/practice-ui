import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransactionID } from '../interface/transactionID';
import { PollNotificationService } from '../services/poll-notification.service';
import { notifications } from '../interface/notifications';
import { Observable, Subscription } from 'rxjs';
import { IsAuthenticatedService } from '../services/isAuthenticated.service';

@Component({
  selector: 'app-owner-dashboard',
  templateUrl: './owner-dashboard.component.html',
  styleUrls: ['./owner-dashboard.component.scss']
})
export class OwnerDashboardComponent implements OnInit, OnDestroy {
  //Hard code the singpass for the ownerID;
  // reviewNowData: TransactionID = {
  //   "ownerId": "",
  //   "transactionId": "1"
  // }
  notificationData!: notifications;

  PendingReview: string = "New Vehicle Registration Application Pending Review  "
  notificationPoll$!: Observable<notifications>;
  notificationSub: Subscription = new Subscription();
  notifnum: number = 0;
  modes: any = [];
  getDataFrmHomePage : any

  constructor(private router: Router, private pollNotification: PollNotificationService, private loginService: IsAuthenticatedService) {
    this.notificationPoll$ = pollNotification.getNotifications();
    this.notificationSub = this.notificationPoll$.subscribe(res => {
      //Clear the notication array
      this.modes.length = 0;
      this.notificationData = res;
      console.log(res)
      for (let noti of res.notifications) {
        let tmpNotification: any = {};
        tmpNotification.msg = noti["subject"];
        tmpNotification.transactionId = noti["transactionId"];
        tmpNotification.transactionRefNum = noti["transactionRefNum"];
        //Push in new notification
        this.modes.push(tmpNotification)
      }
      this.notifnum = res.notifications.length;
    })

  }

  ngOnInit(): void {
    this.getDataFrmHomePage = history.state.data;
  }
  goHomePage() {
    this.router.navigate(['/home'])
  }
  reviewNow() {
    let lenOfNotication = this.notificationData.notifications.length
    if (lenOfNotication > 0) {
      let tmpTransID: TransactionID = {
        transactionId: ''
      };
      //Get the latest notification at the end of the notice
      tmpTransID.transactionId = this.notificationData.notifications[lenOfNotication - 1].transactionId;
      //Set logic if transaction comment then route to resubmit
      //Need to reverse
      if (this.notificationData.notifications[lenOfNotication - 1].transactionComment) {
        this.router.navigate(['resubmit-escooter-pab'], { state: { data: this.notificationData.notifications[lenOfNotication - 1] } });
      } else {
        this.router.navigate(['review-vehicle-detail'], { state: { data: tmpTransID } });
      }
    }
  }

  openNotification(notifNo: number) {
    if (this.notificationData.notifications[notifNo].subject !== 'PMD Registration Completed') {
      let tmpTransID: TransactionID = {
        transactionId: ''
      };
      tmpTransID.transactionId = this.notificationData.notifications[notifNo].transactionId;
      //Set logic if transaction comment then route to resubmit
      if (this.notificationData.notifications[notifNo].transactionComment) {
        this.router.navigate(['resubmit-escooter-pab'], { state: { data: this.notificationData.notifications[notifNo] } });
      } else {
        this.router.navigate(['review-vehicle-detail'], { state: { data: tmpTransID } });
      }
    }

  }

  goToInbox() {
    this.router.navigate(['/inbox'])
  }

  ngOnDestroy() {
    this.notificationSub.unsubscribe();
  }
}
