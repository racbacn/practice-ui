import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-text-group',
  templateUrl: './text-group.component.html',
  styleUrls: ['./text-group.component.scss'],
})
export class TextGroupComponent implements OnInit {
  @Input() label = '';
  @Input() value = '';
  @Input() pipe = '';

  constructor() {}

  ngOnInit(): void {}
}
