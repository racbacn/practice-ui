import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleComponent } from './title/title.component';
import { ViewComponent } from './view/view.component';
import { TextGroupComponent } from './text-group/text-group.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [TitleComponent, ViewComponent, TextGroupComponent],
  imports: [CommonModule, MaterialModule, FlexLayoutModule],
  exports: [TitleComponent, ViewComponent, TextGroupComponent],
})
export class WidgetsModule {}
