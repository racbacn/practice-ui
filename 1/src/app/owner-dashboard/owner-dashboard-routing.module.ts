import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InboxComponent } from '../inbox/inbox.component';
import { OwnerDashboardComponent } from './owner-dashboard.component';

const routes: Routes = 
[
  { path: '', component: OwnerDashboardComponent },
  { path: '/inbox', component: InboxComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerDashboardRoutingModule { }
