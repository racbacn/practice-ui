import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { WidgetsModule } from '../widgets/widgets.module';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, WidgetsModule, MaterialModule, FlexLayoutModule],
  exports: [ProfileComponent],
})
export class ProfileModule {}
