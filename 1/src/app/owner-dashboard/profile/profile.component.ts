import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  ownerDetails: any = {};

  constructor() {}

  ngOnInit(): void {
    this.getOwnerDetails();
  }

  getOwnerDetails() {
    this.ownerDetails = {
      ownerName: 'Robert Loh',
      registeredAddress:
        'blk 344f bukit merah street 26 #19-08 singapore 598765',
      handphoneNumber: '98876554',
      emailAddress: 'robertloh@email.com',
      receiveHardCopyLetters: 'No',
    };
  }
}
