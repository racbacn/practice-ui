import {Type} from "@angular/core";

export interface Expansion {
    title: string;
    component: Type<any>;
    open: boolean;
    hidden: boolean;
}
