export interface RoadTaxDetails {
    tcoeNumber: string,
    coeRebateNumber: string,
    insuranceCompany: string;
    insuranceCoverNoteNumber: string;
    vehicleNumberAvailable: string;
    openMarketValue: string;
    roadTaxMonths: string;
    refundCoeTo: string;
    refundAmount: string
}
