export interface TcoeDetails {
    vitasApprovalCode: string;
    tcoe: string;
}
