import {RequestHeader} from "./request-header";
import {UploadedDocument} from "./uploaded-document";

export interface ConfidenceScoreRequest {
    requestHeader: RequestHeader;
    uploadedDocument: UploadedDocument
}
