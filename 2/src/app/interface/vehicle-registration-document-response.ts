import {ResponseHeader} from "./response-header";

export interface VehicleRegistrationDocumentResponse {
    responseHeader: ResponseHeader;
    documentId: number;
    documentRefNum: string;
}
