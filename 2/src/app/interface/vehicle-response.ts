import {ResponseHeader} from "./response-header";

export interface VehicleResponse {
    responseHeader: ResponseHeader;
    transactionStatusName: string;
    vehicleId: string;
    transactionId: string;
    transactionRefNum: string;
    transactionDate: string;
    transactionTime: string;
    transactionDocumentCount: string;
    assignedUserId: number;
}

