export interface Transaction {
  ownerId: string;
  statusType: string;
  transactionRefNum: string;
  transactionStatusDate: string;
  transactionType: string;
}
