export interface RequestHeader {
    requestAppId: string,
    userId: number
    userTypeKey: number;
}
