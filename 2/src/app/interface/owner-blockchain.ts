export interface OwnerBlockchain {
    nric: string;
    salutation: string;
    name: string;
    dob: string;
    mobile: string;
    address: string;
    registeredAddressType: string;
    email: string;
    gender: string;
}
