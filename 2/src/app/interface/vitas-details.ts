export interface VitasDetails {
    vitasApprovalCode: string;
    vehicleMake: string;
    vehicleModel: string;
    propellant: string;
    passengerCapacity: string;
    engineCapacity:  string;
    powerRating: string;
    noOfAxles: string;
    unladenWeight: string;
    maximumUnladenWeight: string;
    emissionStandardCode: string;
    firstRegistrationDate: string;
    originalRegistrationDate: string;
    motorNumber: string;
    trailerChasisNumber: string;
    noxEmission: string;
    co2Emission: string;
    coEmission: string;
    hcEmission: string;
    pmEmission: string;
}
