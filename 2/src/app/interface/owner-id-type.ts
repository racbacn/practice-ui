export interface OwnerIdType {
    name: string;
    value: string;
}
