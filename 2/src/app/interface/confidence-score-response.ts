import {ResponseHeader} from "./response-header";
import {ValueConfidence} from "./value-confidence";

export interface ConfidenceScoreResponse {
    responseHeader: ResponseHeader;
    documentId: number,
    documentRefNum: string,
    avgScore: number,
    fieldValueConfidence: Array<ValueConfidence>
}
