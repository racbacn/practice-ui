export interface SearchParameters {
  pageNo?: number;
  sortDirection?: string;
  schemeType?: number;
  transactionRefNum?: string;
  ownerId?: string;
  lastModifiedDateFrom?: Date;
  lastModifiedDateTo?: Date;
  statusType?: string;
}
