export interface Notification {
    notifId: string;
    notifDateTime:string;
    subject: string;
    transactionId: string;
    transactionRefNum: string;
    transactionComment: string;
    transactionTypeKey: string;
}
