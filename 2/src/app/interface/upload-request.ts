export interface UploadRequest {
    Bucket: string;
    Key: string;
    Body: File;
    ACL: string;
    ContentType: string;
}
