export interface UpdateApprovalLikelihoodResponse {
    transactionId: number;
    transactionRefNum: string;
}
