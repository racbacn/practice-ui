import { Transaction } from './transaction';

export interface TransactionList {
  authorizedDealerTransactionsList: Transaction[];
}
