import {RequestHeader} from "./request-header";

export interface VehicleRegistrationDocumentRequest {
    requestHeader: RequestHeader;
    documentTypeKey: number;
}
