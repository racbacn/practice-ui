import {RequestHeader} from "./request-header";
import {VehicleDetails} from "./vehicle-details";
import {OwnerDetails} from "./owner-details";
import {RoadTaxDetails} from "./road-tax-details";
import {VehicleRegistrationDocument} from "./vehicle-registration-document";

export interface VehicleRequest {
    requestHeader: RequestHeader;
    ownerDetails: OwnerDetails;
    vehicleDetails: VehicleDetails;
    roadTaxDetails: RoadTaxDetails;
    vehicleRegistrationDocuments: Array<VehicleRegistrationDocument>;
}
