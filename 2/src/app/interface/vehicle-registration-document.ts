export interface VehicleRegistrationDocument {
    documentId: number;
    documentRefNum: string;
    fileName: string;
}
