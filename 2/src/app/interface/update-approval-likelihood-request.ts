export interface UpdateApprovalLikelihoodRequest {
    transactionId: number;
    documentConfidenceAvg: number;
}
