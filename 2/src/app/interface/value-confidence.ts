export interface ValueConfidence {
    fieldLabel: string;
    fieldValue: string;
    confidenceScore: number;
}
