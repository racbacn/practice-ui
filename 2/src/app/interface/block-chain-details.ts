import {OwnerBlockchain} from "./owner-blockchain";
import {VehicleBlockchain} from "./vehicle-blockchain";

export interface BlockChainDetails {
        owner: OwnerBlockchain;
        vehicle: VehicleBlockchain;
}
