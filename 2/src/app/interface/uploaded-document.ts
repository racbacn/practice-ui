export interface UploadedDocument {
    documentId: number;
    filename: string;
}
