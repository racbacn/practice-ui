export interface OwnerDetails {
    ownerIdType: string;
    ownerIdTypeDesc: string;
    ownerId: string;
    ownerUserId: number;
    ownerName: string;
    salesAgreementId: string;
}
