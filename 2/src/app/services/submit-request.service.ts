import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubmitRequestService {

  constructor(private http: HttpClient) { }

  public submitRequest(bodyJSON: object): Observable<any> {
    //const url = environment.apiURL + AUTHENTICATE_ESA_LOGIN;
    const url = 'https://istio-ingressgateway-istio-system.apps.a6067fe2802840199acec1f28c4573a1.vehsystempoc.com/manage-vehicle-service/registerVehicle';
    // const url = 'http://localhost';


    const options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };
    return this.http.post<any>(url, bodyJSON, options)
  }

}
