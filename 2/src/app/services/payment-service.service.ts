import { Injectable } from '@angular/core';
import { HttpServicesService } from './http-services.service'

@Injectable({
  providedIn: 'root'
})
export class PaymentServiceService {

  constructor(private httpService: HttpServicesService) { }

  getPaymentDetails(transactionID: any){
   return this.httpService.httpGet("/manage-payment-service/getPaymentSummary/" + transactionID);
  }

  payNow(params: any){
    return this.httpService.httpPost("/manage-payment-service/makePayment", params)
  }


}
