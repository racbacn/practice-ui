import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {FormGroup} from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class FillFormValidationsService {
    vehicleDetailsFormGroup$: BehaviorSubject<FormGroup> = new BehaviorSubject<FormGroup>(new FormGroup({}));
    roadTaxFormGroup$: BehaviorSubject<FormGroup> = new BehaviorSubject<FormGroup>(new FormGroup({}));

    constructor() {
    }

    setVehicleDetailsFormGroup(formGroup: FormGroup) {
        if (this.vehicleDetailsFormGroup$.value !== formGroup) {
            this.vehicleDetailsFormGroup$.next(formGroup)
        }
    }

    getVehicleDetailsFormGroup() {
        return this.vehicleDetailsFormGroup$;
    }

    setRoadTaxDetailsFormGroup(formGroup: FormGroup) {
        if (this.roadTaxFormGroup$.value !== formGroup) {
            this.roadTaxFormGroup$.next(formGroup)
        }
    }

    getRoadTaxDetailsFormGroup() {
        return this.roadTaxFormGroup$;
    }
}
