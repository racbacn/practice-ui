import {Injectable} from '@angular/core';
import {UploadRequest} from "../interface/upload-request";
import * as S3 from 'aws-sdk/clients/s3';
import {environment} from "../../environments/environment";


@Injectable({
    providedIn: 'root'
})
export class UploadService {
    bucket: S3;

    constructor() {
        this.bucket = new S3(
            {
                accessKeyId: environment.aws.accessKeyId,
                secretAccessKey: environment.aws.secretAccessKey,
            }
        );
    }

    uploadFile(uploadRequest: UploadRequest) {
        console.log('called');
        return this.bucket.upload(uploadRequest);
    }
}
