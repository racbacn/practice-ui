import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {VitasDetails} from "../interface/vitas-details";
import {serverUrl} from '../constants/sharedValue';
import {Observable} from "rxjs";
import {TcoeDetails} from "../interface/tcoe-details";

@Injectable({
    providedIn: 'root'
})
export class VitasCodeService {
    constructor(private http: HttpClient) {
    }

    getVitasByCode(code: string): Observable<VitasDetails> {
        return this.http.get<VitasDetails>('/vitas/' + code);
    }

    getTcoeByCode(code: string): Observable<TcoeDetails> {
        return this.http.get<TcoeDetails>('/tcoe/' + code);
    }
}

