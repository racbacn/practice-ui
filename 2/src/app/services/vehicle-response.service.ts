import {Injectable} from '@angular/core';
import {VehicleResponse} from "../interface/vehicle-response";
import {BehaviorSubject} from "rxjs";
import {VEHICLE_RESPONSE} from "../constants/app.contants";

@Injectable({
    providedIn: 'root'
})
export class VehicleResponseService {
    vehicleResponse: VehicleResponse = VEHICLE_RESPONSE;
    vehicleResponse$: BehaviorSubject<VehicleResponse> = new BehaviorSubject<VehicleResponse>(this.vehicleResponse);

    constructor() {
    }

    setVehicleResponse(vehicleResponse: VehicleResponse) {
        this.vehicleResponse$.next(vehicleResponse);
    }

    getVehicleResponse() {
        return this.vehicleResponse$;
    }
}
