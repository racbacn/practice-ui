import { TestBed } from '@angular/core/testing';

import { VehicleResponseService } from './vehicle-response.service';

describe('SubmitResponseService', () => {
  let service: VehicleResponseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehicleResponseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
