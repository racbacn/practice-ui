import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AwsCognitoService} from "./aws-cognito.service";
import {Location} from '@angular/common';
import {catchError, finalize, switchMap} from 'rxjs/operators';
import {SessionUtil} from "../core/util/sessionUtil";
import {ManageUserService} from "./manage-user.service";
import {UserResponse} from "../interface/user-response";


@Injectable({
    providedIn: 'root'
})
export class TokenResolver implements Resolve<any> {

    constructor(private location: Location,
                private awsCognitoService: AwsCognitoService,
                private router: Router,
                private managerUserService: ManageUserService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const code: string | null = urlParams.get('code');

        if (!code) {
            return of(null);
        }

        return this.getTokenDetailsFromCognito(code).pipe(
            finalize(() => {
                this.location.replaceState(window.location.pathname);
            })
        );
    }

    getTokenDetailsFromCognito(code: string): Observable<any | null> {
        return this.awsCognitoService.getTokenDetailsFromCognito(code).pipe(
            switchMap((response: any) => {
                console.log('Response: ', response);

                SessionUtil.setSessionStorage(SessionUtil.TOKEN, response.access_token)

                if (response) {
                    this.managerUserService.getUserId('defaultUserAD').subscribe(data => {
                        let userResponse: UserResponse = data;

                        if (userResponse) {
                            SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(userResponse))
                            this.router.navigate(['/dealer-dashboard']);
                        }
                    })
                }

                return of(response);
            }),
            catchError((error) => {
                return error;
            })
        );
    }
}
