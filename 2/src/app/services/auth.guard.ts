import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private auth: AuthService, private router: Router) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        let isActivated: boolean = false;

        if (this.auth.isSSOAuthenticated()) {
            isActivated = true;
        } else {
            this.router.navigate(["error"])
            return false;
        }
        console.log('isActivated', this.auth.isSSOAuthenticated())

        return isActivated;
    }

}
