import { TestBed } from '@angular/core/testing';

import { VehicleRequestService } from './vehicle-request.service';

describe('VehicleRequestService', () => {
  let service: VehicleRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehicleRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
