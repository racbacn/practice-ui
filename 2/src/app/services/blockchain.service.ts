import { Injectable } from '@angular/core';
import { HttpServicesService } from './http-services.service'
import { token } from '../constants/apiurl-names.constants'
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import {BlockChainDetails} from "../interface/block-chain-details";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BlockchainService {

  BLOCKCHAIN_REPORT_URL = '/manage-vehicle-service/blockchainReportDetails'
  BLOCKCHAIN_APPROVED_URL = '/Vehicle%20Registration%20Approved'

  constructor(private httpClient: HttpClient) { }

  getBlockchainDetailsBySalesIdAgreement(salesIdAgreement: string) : Observable<BlockChainDetails> {
    return this.httpClient.get<BlockChainDetails>(this.BLOCKCHAIN_REPORT_URL+ "/" + salesIdAgreement +this.BLOCKCHAIN_APPROVED_URL);
  }
}
