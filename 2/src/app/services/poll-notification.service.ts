import {Injectable, OnDestroy} from '@angular/core';
import {HttpServicesService} from '../services/http-services.service';
import {NotificationResponse} from '../interface/notification-response';
import {Observable, Subject, timer} from 'rxjs';
import {delayWhen, retryWhen, share, switchMap, takeUntil, tap} from 'rxjs/operators';
import {serverUrl} from '../constants/sharedValue';

@Injectable({
  providedIn: 'root'
})
export class PollNotificationService implements OnDestroy {
  private pollNotification$!: Observable<NotificationResponse>;
  private stopPolling = new Subject();

  constructor(private httpServices: HttpServicesService) {
    //Retry every 3seconds
    this.pollNotification$ = timer(1, 3000).pipe(
      switchMap(() => this.httpServices.httpGet("/manage-notification-service/userNotifications/2")),
      // retry(),
      retryWhen(err =>
        err.pipe(
          delayWhen(() => timer(6000))
      )),
      share(),
      takeUntil(this.stopPolling)
   );
  }

  getNotifications(): Observable<NotificationResponse> {
    return this.pollNotification$.pipe(
      tap(() => console.log('data sent to subscriber'))
    );
  }

  ngOnDestroy() {
    this.stopPolling.next();
}
}
