import { HttpClient, HttpParams } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import { environment } from 'src/environments/environment';
import { SearchParameters } from '../interface/search-parameters';
import { TransactionList } from '../interface/transaction-list';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private transactionId$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private SERVER_URL = environment.serverUrl;
  private apiUrl = this.SERVER_URL + '/authorized-dealer/getTransactions';

  constructor(private http: HttpClient) { }

  getTransactionId(): BehaviorSubject<string> {
    return this.transactionId$;
  }

  getTransactions(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }

  filterTransactions(
    parameters: SearchParameters
  ): Observable<TransactionList> {
    const { pageNo, sortDirection, schemeType } = parameters;
    const { transactionRefNum, ownerId } = parameters;
    const { lastModifiedDateFrom, lastModifiedDateTo } = parameters;
    let params = new HttpParams();
    if (pageNo) {
      params = params.append('pageNo', pageNo);
    }
    if (sortDirection) {
      params = params.append('sortDirection', sortDirection);
    }
    if (schemeType) {
      params = params.append('schemeType', schemeType);
    }
    if (transactionRefNum) {
      params = params.append('transactionRefNum', transactionRefNum);
    }
    if (ownerId) {
      params = params.append('ownerId', ownerId);
    }
    if (lastModifiedDateFrom) {
      const dateFromString = lastModifiedDateFrom.toString();
      params = params.append('lastModifiedDateFrom', dateFromString);
    }
    if (lastModifiedDateTo) {
      const dateToString = lastModifiedDateTo.toString();
      params = params.append('lastModifiedDateTo', dateToString);
    }
    return this.http.get<TransactionList>(this.apiUrl, { params });
  }
}
