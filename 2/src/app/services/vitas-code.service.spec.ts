import { TestBed } from '@angular/core/testing';

import { VitasCodeService } from './vitas-code.service';

describe('VitasCodeService', () => {
  let service: VitasCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VitasCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
