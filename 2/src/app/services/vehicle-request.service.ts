import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {VehicleRequest} from "../interface/vehicle-request";
import {HttpClient} from "@angular/common/http";
import {INIT_VEHICLE_REQUEST_VALUE} from "../constants/app.contants";
import {VehicleResponse} from "../interface/vehicle-response";
import {serverUrl} from '../constants/sharedValue';
import {HttpServicesService} from './http-services.service'
import {UpdateApprovalLikelihoodRequest} from "../interface/update-approval-likelihood-request";
import {UpdateApprovalLikelihoodResponse} from "../interface/update-approval-likelihood-response";

@Injectable({
    providedIn: 'root'
})
export class VehicleRequestService {
    SAVE_AS_DRAFT_URL = '/manage-vehicle-service/saveDraftVehicle'
    REGISTER_VEHICLE_URL = '/manage-vehicle-service/registerVehicle'
    GET_VEHICLE_BY_ID_URL = '/manage-vehicle-service/vehicleDetails'
    UPDATE_LIKELIHOOD_URL = '/manage-vehicle-service/updateApprovalLikelihood';
    vehicleRequest: VehicleRequest = INIT_VEHICLE_REQUEST_VALUE;
    vehicleRequest$: BehaviorSubject<VehicleRequest> = new BehaviorSubject<VehicleRequest>(this.vehicleRequest);

    constructor(private http: HttpServicesService, private httpClient: HttpClient) {
    }

    getVehicleDetailsByTransactionID(transactionId: string) {
        return this.http.httpGet(this.GET_VEHICLE_BY_ID_URL + "/" + transactionId);
    }

    saveDraftVehicle(vehicleRequest: VehicleRequest) {
        return this.http.httpPost(this.SAVE_AS_DRAFT_URL, vehicleRequest);
    }

    registerVehicle(vehicleRequest: VehicleRequest) {
        return this.http.httpPost(this.REGISTER_VEHICLE_URL, vehicleRequest);
    }

    updateLikelihoodUrl(updateApprovalLikelihoodRequest: UpdateApprovalLikelihoodRequest) {
        return this.httpClient.patch<UpdateApprovalLikelihoodResponse>(this.UPDATE_LIKELIHOOD_URL, updateApprovalLikelihoodRequest);
    }

    setVehicleRequest(vehicleRequest: VehicleRequest) {
        console.log('test init', vehicleRequest)
        this.vehicleRequest$.next(vehicleRequest)
    }

    getVehicleRequest(): Observable<VehicleRequest> {
        return this.vehicleRequest$;
    }
}
