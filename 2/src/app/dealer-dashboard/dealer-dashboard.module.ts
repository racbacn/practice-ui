import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DealerDashboardRoutingModule } from './dealer-dashboard-routing.module';
import { DealerDashboardComponent } from './dealer-dashboard.component';
import { MaterialModule } from '../material.module';
import { WidgetsModule } from '../widgets/widgets.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TransactionsModule } from './transactions/transactions.module';


@NgModule({
  declarations: [
    DealerDashboardComponent
  ],
  imports: [
    CommonModule,
    DealerDashboardRoutingModule,
    MaterialModule,
    WidgetsModule,
    FlexLayoutModule,
    TransactionsModule
  ]
})
export class DealerDashboardModule { }
