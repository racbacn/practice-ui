import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerDashboardComponent } from './dealer-dashboard.component';

const routes: Routes = [{ path: '', component: DealerDashboardComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealerDashboardRoutingModule { }
