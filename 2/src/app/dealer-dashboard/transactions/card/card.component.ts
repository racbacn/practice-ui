import { Component, Input, OnInit } from '@angular/core';
import { Transaction } from 'src/app/interface/transaction';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() transaction!: Transaction;

  constructor() {}

  ngOnInit(): void {}
}
