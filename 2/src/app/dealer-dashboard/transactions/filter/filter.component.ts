import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  filterForm!: FormGroup;
  @Input() stats = [
    { label: 'all', value: 0 },
    { label: 'pending', value: 0 },
    { label: 'inprogress', value: 0 },
    { label: 'completed', value: 0 },
  ];

  statuses = [
    {
      label: 'Pending Submissions',
      value: 'Pending Submission',
    },
    {
      label: 'In-Progress Submission',
      value: 'Pending VO Approval',
    },
    {
      label: 'Completed Submissions',
      value: 'Completed',
    },
    {
      label: 'All Submissions',
      value: null,
    },
  ];
  transactionTypes = [
    {
      label: 'Normal Vehicle Registration',
      value: 1,
    },
    {
      label: 'Scheme Vehicle Registration',
      value: 2,
    },
    {
      label: 'All Types',
      value: 0,
    },
  ];
  sortDirectionSortDates = [
    {
      label: 'Last Modified - Newest to Oldest',
      value: 'DESC',
    },
    {
      label: 'Last Modified - Oldest to Newest',
      value: 'ASC',
    },
  ];
  @Output() filtered: EventEmitter<any> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  ngOnChanges() {
    this.statuses = this.statuses.map((status, index) => {
      const stat = this.stats[index].value;
      return {
        label: `${status.label} (${stat})`,
        value: status.value,
      };
    });
  }

  initializeForm() {
    this.filterForm = this.formBuilder.group({
      statusType: [null],
      schemeType: [null],
      sortDirection: [null],
    });
  }

  onFilterChange() {
    this.filtered.next(this.filterForm.value);
  }
}
