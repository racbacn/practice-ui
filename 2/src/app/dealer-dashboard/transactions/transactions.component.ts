import { Component, OnInit } from '@angular/core';
import { SearchParameters } from 'src/app/interface/search-parameters';
import { Transaction } from 'src/app/interface/transaction';
import { TransactionList } from 'src/app/interface/transaction-list';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  transactions: Transaction[] = [];
  stats = [
    { label: 'pending', value: 30 },
    { label: 'inprogress', value: 30 },
    { label: 'completed', value: 40 },
    { label: 'all', value: 100 },
  ];
  showTransactions = false;
  pageNo = 0;
  parameters: SearchParameters = { pageNo: 0 };

  constructor(private transactionService: TransactionService) {}

  ngOnInit(): void {
    this.initializeTransactions();
  }

  initializeTransactions() {
    this.transactionService.getTransactions().subscribe(
      (response: TransactionList): void => {
        this.transactions = response.authorizedDealerTransactionsList;
        this.setStats();
        this.showTransactions = true;
      },
      () => alert('Cannot get transactions.')
    );
  }

  countTransaction(stat: string) {
    if (stat === 'all') {
      return this.transactions.length;
    }
    const transactions = this.transactions.filter((transaction: any) => {
      if (stat === 'completed') {
        return transaction.statusType === 'Completed';
      } else if (stat === 'inprogress') {
        return (
          transaction.statusType === 'Pending VO Approval' ||
          transaction.statusType === 'Pending Officer Review'
        );
      } else if (stat === 'pending') {
        return transaction.statusType === 'Pending Submission';
      }
      return false;
    });
    return transactions.length;
  }

  setStats() {
    this.stats[3].value = this.countTransaction('all');
    this.stats[2].value = this.countTransaction('completed');
    this.stats[1].value = this.countTransaction('inprogress');
    this.stats[0].value = this.countTransaction('pending');
    console.log(this.stats);
  }

  filterStatusType() {
    this.transactions = this.transactions.filter((trasaction: any) => {
      if (this.parameters.statusType === 'Pending VO Approval') {
        return (
          trasaction.statusType === 'Pending VO Approval' ||
          trasaction.statusType === 'Pending Officer Review'
        );
      }
      return trasaction.statusType === this.parameters.statusType;
    });
  }

  filter(parameters: SearchParameters) {
    this.parameters = parameters;
    this.transactionService.filterTransactions(this.parameters).subscribe(
      (response) => {
        this.transactions = response.authorizedDealerTransactionsList;
        this.pageNo = this.parameters.pageNo ?? this.pageNo;
        this.setStats();
        if (this.parameters.statusType) {
          this.filterStatusType();
        }
      },
      (error) => {
        if (error?.error?.code === 5006) {
          return alert(error.error.message);
        }
        alert('Cannot get transactions.');
      }
    );
  }

  onPageMove(pageNumber: number) {
    this.parameters.pageNo = pageNumber;
    this.filter(this.parameters);
  }

  search(parameters: SearchParameters) {
    this.parameters = { ...this.parameters, ...parameters };
    this.filter(this.parameters);
  }
}
