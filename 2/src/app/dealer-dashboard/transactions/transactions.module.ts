import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter/filter.component';
import { PaginationComponent } from './pagination/pagination.component';
import { CardComponent } from './card/card.component';
import { TransactionsComponent } from './transactions.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from '../search/search.module';

@NgModule({
  declarations: [
    TransactionsComponent,
    FilterComponent,
    PaginationComponent,
    CardComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    SearchModule,
  ],
  exports: [TransactionsComponent],
})
export class TransactionsModule {}
