import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @ViewChild('searchFormPanel') searchFormPanel!: MatExpansionPanel;
  @Output() searched: EventEmitter<any> = new EventEmitter<any>();
  searchForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private elementRef: ElementRef
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm(): void {
    this.searchForm = this.formBuilder.group({
      transactionRefNum: [null],
      ownerId: [null],
      lastModifiedDateFrom: [null],
      lastModifiedDateTo: [null],
    });
  }

  onSearchFormSubmit(): void {
    const { value } = this.searchForm;
    if (this.searchForm.invalid) {
      return alert('Form is invalid...');
    }
    this.searchFormPanel.close();
    const parameters = { ...value };
    const format = 'YYYY-MM-DDTHH:mm:ss';
    if (parameters.lastModifiedDateFrom) {
      const dateFrom = moment(parameters.lastModifiedDateFrom).format(format);
      parameters.lastModifiedDateFrom = dateFrom;
    }
    if (parameters.lastModifiedDateTo) {
      const dateTo = moment(parameters.lastModifiedDateTo).format(format);
      parameters.lastModifiedDateTo = dateTo;
    }
    this.searched.next(parameters);
  }

  changeExpansionPanelBoxShadow(boxShadow: string): void {
    const nativeElement = this.elementRef.nativeElement;
    const expansionPanel = nativeElement.querySelector('mat-expansion-panel');
    expansionPanel.style.boxShadow = boxShadow;
  }

  onOpen(): void {
    this.changeExpansionPanelBoxShadow('0 0 0 1500px rgba(0, 0, 0, 0.3)');
  }

  onClose(): void {
    this.changeExpansionPanelBoxShadow('0px 4px 4px 0px #00000040');
  }
}
