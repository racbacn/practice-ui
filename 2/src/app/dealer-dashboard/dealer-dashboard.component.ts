import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PollNotificationService} from '../services/poll-notification.service';
import {NotificationResponse} from '../interface/notification-response';
import {Notification} from "../interface/notification";
import {proceedToPayment, REJECTED_SUBJECT} from "../constants/app.contants";
import {SessionUtil} from "../core/util/sessionUtil";
import {VehicleRequestService} from "../services/vehicle-request.service";
import {VehicleRequest} from "../interface/vehicle-request";


@Component({
    selector: 'app-dealer-dashboard',
    templateUrl: './dealer-dashboard.component.html',
    styleUrls: ['./dealer-dashboard.component.scss']
})
export class DealerDashboardComponent implements OnInit {
    rejectedSubject: string = REJECTED_SUBJECT;
    notificationsLength: number = 0;
    notificationList: Array<Notification> = [];

    constructor(private router: Router, private pollNotificationService: PollNotificationService,
                private vehicleRequestService: VehicleRequestService) {
    }

    ngOnInit(): void {
        SessionUtil.removeSessionStorage(SessionUtil.TRANSACTION_ID)
        SessionUtil.removeSessionStorage(SessionUtil.COMMENT)
        SessionUtil.removeSessionStorage(SessionUtil.TRANSACTION_REF_NUM)

        this.onDefaultVehicleRequest();
        this.onReceiveNotification();
    }

    onReceiveNotification() {
        this.pollNotificationService.getNotifications().subscribe(data => {
            let notificationResponse: NotificationResponse = data;

            this.notificationList = notificationResponse.notifications;

            this.notificationsLength = this.notificationList.length;
        })
    }

    registerNewVehicle() {
        this.router.navigate(['/register-new-vehicle']);
    }

    registerNewVehicleBlockChain() {
        this.router.navigate(['/register-new-vehicle-blockchain']);
    }

    logout() {
        this.router.navigate(['']);
    }

    openNotification(notificationIndex: number) {
        // let tmpTransID : TransactionID = JSON.parse(JSON.stringify(this.reviewNowData));
        // tmpTransID.transactionId=this.notificationData.notifications[notifNo].transactionId;
        // this.router.navigate(['review-vehicle-detail'], { state: { data: tmpTransID } });
        let notification: Notification = this.notificationList[notificationIndex]

        SessionUtil.setSessionStorage(SessionUtil.TRANSACTION_ID, notification.transactionId)

        if (notification.subject === this.rejectedSubject) {
            SessionUtil.setSessionStorage(SessionUtil.COMMENT, notification.transactionComment)
            this.router.navigate(['/register-new-vehicle']);
        } else if (notification.subject === proceedToPayment) {
            SessionUtil.setSessionStorage(SessionUtil.TRANSACTION_REF_NUM, notification.transactionRefNum)
            this.router.navigate(['/payment']);
        }

    }

    onDefaultVehicleRequest() {
        const defaultVehicleRequest: VehicleRequest = {
            requestHeader: {
                requestAppId: '',
                userId: 0,
                userTypeKey: 0,
            },
            ownerDetails: {
                ownerIdType: '',
                ownerIdTypeDesc: '',
                ownerId: '',
                ownerUserId: 0,
                ownerName: '',
                salesAgreementId: '',
            },
            vehicleDetails: {
                vehicleId: '',
                vehicleStatus: '',
                vitasApprovalCode: '',
                vehicleType: 'P10 - Passenger Motor car',
                vehicleScheme: '',
                attachmentOne: 0,
                attachmentTwo: 0,
                attachmentThree: 0,
                vehicleMake: '',
                vehicleModel: '',
                propellant: '',
                passengerCapacity: '4',
                engineCapacity: '',
                powerRating: '',
                maximumPowerOutput: '',
                numberOfAxles: '',
                tyreInformation: '',
                unladenWeight: '',
                maximumUnladenWeight: '',
                emissionStandardCode: '',
                carbonDioxideEmission: '1',
                carbonMonoxideEmission: '1',
                hydroCarbonEmission: '1',
                nitrousOxideEmission: '1',
                pmEmission: '1',
                primaryColour: '',
                secondaryColour: '',
                manufacturingYear: 0,
                firstRegistrationDate: '',
                originalRegistrationDate: '',
                chassisNumber: '',
                engineNumber: '',
                motorNumber: '',
                trailerChassisNumber: '',
            },
            roadTaxDetails: {
                tcoeNumber: '',
                coeRebateNumber: 'NA',
                insuranceCompany: '',
                insuranceCoverNoteNumber: '',
                vehicleNumberAvailable: '',
                openMarketValue: '100',
                roadTaxMonths: '6',
                refundCoeTo: '',
                refundAmount: ''
            },
            vehicleRegistrationDocuments: []
        };


        this.vehicleRequestService.setVehicleRequest(defaultVehicleRequest)
    }

}
