import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonsComponent} from './buttons/buttons.component';
import {MaterialModule} from '../material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LtaLogoHeaderComponent} from './lta-logo-header/lta-logo-header.component';
import {ErrorComponent} from "./error/error.component";


@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FlexLayoutModule
    ],
    declarations: [ButtonsComponent, LtaLogoHeaderComponent, ErrorComponent],
    exports: [ButtonsComponent, LtaLogoHeaderComponent]
})
export class WidgetsModule {
}
