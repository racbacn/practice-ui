import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {
  @Input() btnText?: any;
  @Input() btnType?: any;
  @Input() btnColor?: string;
  @Input() borderRadius?: string;
  @Input() fontConfig?: any;
  @Input() btnIcon?: any;
  @Input() btnIconPosition?: any;
  @Input() padding?: any;
  @Input() btnClass: any;
  @Input() btnConfig: any;
  @Output() textBtnClickEmt: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onTextBtnClick() {
    console.log(this.textBtnClickEmt.emit('You have clicked on a text button.'));
  }

}
