import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-lta-logo-header',
    templateUrl: './lta-logo-header.component.html',
    styleUrls: ['./lta-logo-header.component.scss'],
})
export class LtaLogoHeaderComponent implements OnInit {

    constructor(private router: Router) {
    }

    ngOnInit(): void {
    }

    goBackToDealerDashboard(): void {
        this.router.navigate(['/dealer-dashboard']);
    }
}
