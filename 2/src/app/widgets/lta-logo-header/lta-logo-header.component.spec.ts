import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LtaLogoHeaderComponent } from './lta-logo-header.component';

describe('LtaLogoHeaderComponent', () => {
  let component: LtaLogoHeaderComponent;
  let fixture: ComponentFixture<LtaLogoHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LtaLogoHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LtaLogoHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
