import {FillOwnerParticularsComponent} from "../register-new-vehicle/fill/fill-owner-particulars/fill-owner-particulars.component";
import {FillVehicleParticularsComponent} from "../register-new-vehicle/fill/fill-vehicle-particulars/fill-vehicle-particulars.component";
import {Expansion} from "../interface/expansion";
import {Company} from "../interface/company";
import {FillNewRegistrationAndRoadTaxFeesComponent} from "../register-new-vehicle/fill/fill-new-registration-and-road-tax-fees/fill-new-registration-and-road-tax-fees.component";
import {FillFeedbackComponent} from "../register-new-vehicle/fill/fill-feedback/fill-feedback.component";
import {VehicleRequest} from "../interface/vehicle-request";
import {Attachment} from "../interface/attachment";
import {VehicleResponse} from "../interface/vehicle-response";
import {FillOwnerParticularsBlockchainComponent} from "../register-new-vehicle-blockchain/fill/fill-owner-particulars/fill-owner-particulars-blockchain.component";
import {FillVehicleParticularsBlockchainComponent} from "../register-new-vehicle-blockchain/fill/fill-vehicle-particulars/fill-vehicle-particulars-blockchain.component";
import {FillNewRegistrationAndRoadTaxFeesBlockchainComponent} from "../register-new-vehicle-blockchain/fill/fill-new-registration-and-road-tax-fees/fill-new-registration-and-road-tax-fees-blockchain.component";
import {OwnerIdType} from "../interface/owner-id-type";

export const REGISTER_NEW_VEHICLE: string = 'Register New Vehicle';
export const STEPPER_HEADINGS: Array<string> = [
    'Initiate',
    'Fill',
    'Review',
    'Submit',
];
export const makePaymentParam = "VRLS_Make_Payment";
export const paymentType = {
    registration: "Registration_CAR",
}

export const USER_TYPE = {
    VEHICLE_OWNER: 1,
    AUTHORISED_DEALER: 2,
    LTA_OFFICER: 3,
    LTA_MANAGER: 4,
    PMD_OWNER: 5
}

export const proceedToPayment = "Proceed with Payment of Vehicle Registration Application";

const LOGIN_ERROR_MESSAGE: string = '';
export const INITIATE_LABELS = ['Owner ID Type', 'Owner ID', 'Owner Name', 'Sales Agreement ID'];

//dd - dropdown
//tf - text field
export const INITIATE_INPUT_TYPE = ['dd', 'tf', 'tf', 'tf'];

export const OWNER_ID_TYPE_VALUES: Array<OwnerIdType> = [
    {
        name: 'Singapore NRIC',
        value: "1"
    },
    {
        name: 'Malaysian NRIC',
        value: "2"
    }
];

export const FILL_EXPANSION_PANEL_LIST: Array<Expansion> = [
    {
        title: 'LTA FEEDBACK',
        component: FillFeedbackComponent,
        open: false,
        hidden: true
    },
    {
        title: 'OWNER PARTICULARS',
        component: FillOwnerParticularsComponent,
        open: false,
        hidden: false
    },
    {
        title: 'VEHICLE PARTICULARS',
        component: FillVehicleParticularsComponent,
        open: false,
        hidden: false
    },
    {
        title: 'NEW REGISTRATION & ROAD TAX FEES',
        component: FillNewRegistrationAndRoadTaxFeesComponent,
        open: false,
        hidden: false
    }
];

export const FILL_EXPANSION_PANEL_LIST_BLOCKCHAIN: Array<Expansion> = [
    {
        title: 'OWNER PARTICULARS',
        component: FillOwnerParticularsBlockchainComponent,
        open: false,
        hidden: false
    },
    {
        title: 'VEHICLE PARTICULARS',
        component: FillVehicleParticularsBlockchainComponent,
        open: false,
        hidden: false
    },
    {
        title: 'NEW REGISTRATION & ROAD TAX FEES',
        component: FillNewRegistrationAndRoadTaxFeesBlockchainComponent,
        open: false,
        hidden: false
    }
];

export const VEHICLE_SCHEME_LIST: Array<string> = ['Normal', 'Diplomatic'];

export const VEHICLE_COLOUR_LIST: Array<string> = ['Beige', 'Black', 'Blue', ' Brown', 'Gold', 'Green',
    "Grey", 'Maroon', 'Multi-Colored', 'Orange',
    'Purple', 'Red', 'Silver', 'White', 'Yellow'];

export const COMPANY_LIST: Array<Company> = [
    {
        serialNo: 1,
        companyCode: 'A04',
        companyName: 'AIG ASIA PACIFIC INSURANCE PTE LTD.'
    },
    {
        serialNo: 2,
        companyCode: 'A06',
        companyName: 'TOKIO MARINE INSURANCE SINGAPORE LTD'
    },
    {
        serialNo: 3,
        companyCode: 'A12',
        companyName: 'AXA INSURANCE PTE LTD'
    },
    {
        serialNo: 4,
        companyCode: 'A13',
        companyName: 'CHUBB INSURANCE SINGAPORE LIMITED'
    },
    {
        serialNo: 5,
        companyCode: 'A14',
        companyName: 'AVIVA LTD'
    },
    {
        serialNo: 6,
        companyCode: 'A15',
        companyName: 'ALLIED WORLD ASSURANCE COMPANY, LTD (SINGAPORE BRANCH)'
    },
    {
        serialNo: 7,
        companyCode: 'A16',
        companyName: 'AUTO & GENERAL INSURANCE (SINGAPORE) PTE. LIMITED'
    },
    {
        serialNo: 8,
        companyCode: 'A17',
        companyName: 'ALLIANZ GLOBAL CORPORATE & SPECIALITY SE SINGAPORE BRANCH'
    },
    {
        serialNo: 9,
        companyCode: 'A18',
        companyName: 'ALLIANZ INSURANCE SINGAPORE PTE. LTD.'
    },
    {
        serialNo: 10,
        companyCode: 'C01',
        companyName: 'CHINA TAIPING INSURANCE (SINGAPORE) PTE LTD'
    },
    {
        serialNo: 11,
        companyCode: 'C05',
        companyName: 'LIBERTY INS P L'
    },
    {
        serialNo: 12,
        companyCode: 'D01',
        companyName: 'DIRECT ASIA INSURANCE (SINGAPORE) PTE LTD'
    },
    {
        serialNo: 13,
        companyCode: 'E04',
        companyName: 'EQ INSURANCE COMPANY LTD'
    },
    {
        serialNo: 14,
        companyCode: 'E05',
        companyName: 'ETIQA INSURANCE PTE LTD'
    },
    {
        serialNo: 15,
        companyCode: 'E06',
        companyName: 'ECICS LIMITED'
    },
    {
        serialNo: 16,
        companyCode: 'F03',
        companyName: 'MS FIRST CAPITAL INSURANCE LIMITED'
    },
    {
        serialNo: 17,
        companyCode: 'F04',
        companyName: 'FWD SINGAPORE PTE. LTD.'
    },
    {
        serialNo: 18,
        companyCode: 'G03',
        companyName: 'GREAT AMERICAN INSURANCE COMPANY'
    },
    {
        serialNo: 19,
        companyCode: 'H03',
        companyName: 'HL ASSURANCE PTE. LTD.'
    },
    {
        serialNo: 20,
        companyCode: 'I01',
        companyName: 'MSIG INSURANCE (SINGAPORE) PTE LTD'
    },
    {
        serialNo: 21,
        companyCode: 'I05',
        companyName: 'INDIA INT\'L INS PTE LTD'
    },
    {
        serialNo: 22,
        companyCode: 'L06',
        companyName: 'LONPAC INSURANCE BHD'
    },
    {
        serialNo: 23,
        companyCode: 'N03',
        companyName: 'ERGO INSURANCE PTE. LTD.'
    },
    {
        serialNo: 24,
        companyCode: 'N12',
        companyName: 'NTUC INCOME INS CO-OP LTD'
    },
    {
        serialNo: 25,
        companyCode: 'O03',
        companyName: 'GREAT EASTERN GENERAL INSURANCE LIMITED'
    },
    {
        serialNo: 26,
        companyCode: 'Q01',
        companyName: 'QBE INSURANCE (SINGAPORE) PTE LTD'
    },
    {
        serialNo: 27,
        companyCode: 'S10',
        companyName: 'SOMPO INSURANCE SINGAPORE PTE. LTD.'
    },
    {
        serialNo: 28,
        companyCode: 'U06',
        companyName: 'UNITED OVERSEAS INS LTD'
    },
    {
        serialNo: 29,
        companyCode: 'Z03',
        companyName: 'ZURICH INSURANCE COMPANY (SINGAPORE BRANCH)'
    }
];

export const VEHICLE_ATTACHMENT_LIST: Array<Attachment> = [
    {
        id: 1,
        selection: 1,
        name: 'No Attachment'
    },
    {
        id: 2,
        selection: 2,
        name: 'With Wheelchair Lift'
    },
    {
        id: 3,
        selection: 3,
        name: 'Disabled'
    },
    {
        id: 4,
        selection: 4,
        name: 'With Sun Roof'
    },
    {
        id: 5,
        selection: 5,
        name: 'With Roof Rack',
    },
    {
        id: 6,
        selection: 6,
        name: 'Armoured/Bullet Proof'
    }
];

export const ROAD_TAX_MONTH_LIST: Array<string> = ['6 months', '12 months']

export const REFUND_COE_TO_LIST: Array<string> = ['Motor Firm', 'Registered Owner']

export const INIT_VEHICLE_REQUEST_VALUE: VehicleRequest = {
    requestHeader: {
        requestAppId: '',
        userId: 0,
        userTypeKey: 0,
    },
    ownerDetails: {
        ownerIdType: '',
        ownerIdTypeDesc: '',
        ownerId: '',
        ownerUserId: 0,
        ownerName: '',
        salesAgreementId: '',
    },
    vehicleDetails: {
        vehicleId: '',
        vehicleStatus: '',
        vitasApprovalCode: '',
        vehicleType: 'P10 - Passenger Motor car',
        vehicleScheme: '',
        attachmentOne: 0,
        attachmentTwo: 0,
        attachmentThree: 0,
        vehicleMake: '',
        vehicleModel: '',
        propellant: '',
        passengerCapacity: '4',
        engineCapacity: '',
        powerRating: '',
        maximumPowerOutput: '',
        numberOfAxles: '',
        tyreInformation: '',
        unladenWeight: '',
        maximumUnladenWeight: '',
        emissionStandardCode: '',
        carbonDioxideEmission: '1',
        carbonMonoxideEmission: '1',
        hydroCarbonEmission: '1',
        nitrousOxideEmission: '1',
        pmEmission: '1',
        primaryColour: '',
        secondaryColour: '',
        manufacturingYear: 0,
        firstRegistrationDate: '',
        originalRegistrationDate: '',
        chassisNumber: '',
        engineNumber: '',
        motorNumber: '',
        trailerChassisNumber: '',
    },
    roadTaxDetails: {
        tcoeNumber: '',
        coeRebateNumber: 'N/A',
        insuranceCompany: '',
        insuranceCoverNoteNumber: '',
        vehicleNumberAvailable: '',
        openMarketValue: '100',
        roadTaxMonths: '6',
        refundCoeTo: '',
        refundAmount: ''
    },
    vehicleRegistrationDocuments: []
};

export const VEHICLE_RESPONSE: VehicleResponse = {
    responseHeader: {
        responseAppId: '',
        responseDateTime: ''
    },
    transactionStatusName: '',
    vehicleId: '',
    transactionId: '',
    transactionRefNum: '',
    transactionDate: '',
    transactionTime: '',
    transactionDocumentCount: '',
    assignedUserId: 0
}

export const REJECTED_SUBJECT: string = "Amend Vehicle Registration Application";

export const FILE_SIZE_MAX: number = 28400000;

export const FILE_TYPE_PDF: string = 'application/pdf';

export const FILE_TYPE_JPG: string = 'image/jpeg';

export const FILE_TYPE_PNG: string = 'image/png';
