import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { ReceiptPageComponent } from './receipt-page.component';
import { SuccessPaymentComponent } from './success-payment.component';

const routes: Routes = 
[
  { path: '', component: SuccessPaymentComponent },
  // { path: "/success", component: SuccessPaymentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentSuccessRoutingModule { }
