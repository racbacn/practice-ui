import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { SessionUtil } from '../core/util/sessionUtil';

@Component({
  selector: 'app-success-payment',
  templateUrl: './success-payment.component.html',
  styleUrls: ['./success-payment.component.scss'],
})
export class SuccessPaymentComponent implements OnInit {
  paymentDate: any;
  home = {
    buttonText: 'Back to Dashboard',
    buttonClassName: 'purple-button',
    buttonFont: 'button-font-16',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonWidth: 'button-70',
  };

  constructor(private route: ActivatedRoute, private datePipe: DatePipe, private router: Router) {}

  amount!: string;
  data: any;

  ngOnInit(): void {
    this.data = SessionUtil.getSessionStorage(SessionUtil.PAYMENT);
    let obj = JSON.parse(this.data);
    let date = obj.date;
    this.amount = obj.amount;

    this.paymentDate = this.datePipe.transform(date, 'MMM d, y, h:mm:ss a');
  }

  backToDashboard() {
      this.router.navigate(['/dealer-dashboard']);
  }
}
