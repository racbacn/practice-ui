import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessPaymentComponent } from './success-payment.component';
import { PaymentSuccessRoutingModule } from './success-payment-routing.module';
import { WidgetsModule } from '../widgets/widgets.module';



@NgModule({
  declarations: [
    SuccessPaymentComponent, 
  ],
  imports: [
    PaymentSuccessRoutingModule,
    CommonModule,
    WidgetsModule
  ]
})
export class SuccessPaymentModule { }
