import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DatePipe} from '@angular/common'

import {MaterialModule} from './material.module';
import {CoreModule} from './core/core.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './core/components/header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {DireciveModule} from './directive/directives.module';
import {JwtModule, JwtModuleOptions} from "@auth0/angular-jwt";
import {SessionUtil} from "./core/util/sessionUtil";

const JWT_Module_Options: JwtModuleOptions = {
    config: {
        tokenGetter: () => (SessionUtil.getSessionStorage(SessionUtil.TOKEN))
    }
};

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        CoreModule,
        DireciveModule,
        JwtModule.forRoot(JWT_Module_Options)
    ],
    providers: [DatePipe],
    bootstrap: [AppComponent]
})
export class AppModule {
}
