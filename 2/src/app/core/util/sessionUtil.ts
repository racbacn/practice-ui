export class SessionUtil {
    public static readonly TOKEN = "TOKEN";
    public static readonly USER = "USER";
    public static readonly PAYMENT = "PAYMENT";
    public static readonly TRANSACTION_ID = "TRANSACTION_ID";
    public static readonly TRANSACTION_REF_NUM = "TRANSACTION_REF_NUM";
    public static readonly COMMENT = "COMMENT";
    public static readonly VITAS_CODE = "VITAS_CODE";

    public static setSessionStorage(key: string, value: string) {
        sessionStorage.setItem(key, value);
    }

    public static getSessionStorage(key: string): string | null {
        return sessionStorage.getItem(key)
    }

    public static removeSessionStorage(key: string): void {
        sessionStorage.removeItem(key);
    }

    public static clearSession(): void {
        sessionStorage.clear();
    }
}

