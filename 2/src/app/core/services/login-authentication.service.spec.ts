/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoginAuthenticationService } from './login-authentication.service';

describe('Service: LoginAuthentication', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginAuthenticationService]
    });
  });

  it('should ...', inject([LoginAuthenticationService], (service: LoginAuthenticationService) => {
    expect(service).toBeTruthy();
  }));
});
