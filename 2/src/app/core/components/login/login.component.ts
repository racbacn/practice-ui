import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {LoginAuthenticationService} from '../../services/login-authentication.service';
import {ManageUserService} from "../../../services/manage-user.service";
import {SessionUtil} from "../../util/sessionUtil";
import {UserResponse} from "../../../interface/user-response";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginform!: FormGroup;
    isErrorHidden: boolean = true;
    errorMessage!: string;

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private loginAuthenticationService: LoginAuthenticationService,
                private managerUserService: ManageUserService) {
    }

    ngOnInit(): void {
        this.loginform = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });

        SessionUtil.clearSession();
        console.log(SessionUtil.getSessionStorage(SessionUtil.TOKEN))
    }

    login(): void {
        let username = this.loginform.value.username
        let password = this.loginform.value.password

        this.isErrorHidden = true;

        this.loginAuthenticationService.login(username, password).then((data: any) => {
            let responseData: any = data;

            if (responseData) {
                this.managerUserService.getUserId(responseData.username).subscribe(data => {
                    let userResponse: UserResponse = data;

                    if (userResponse) {
                        SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(userResponse))
                        this.router.navigate(['/dealer-dashboard']);
                    }
                })
            }
        }).catch((error: any) => {
            let responseError: any = error;

            this.errorMessage = responseError.message;
            this.isErrorHidden = false;
        });
    }

    loginSSO() {
        this.loginAuthenticationService.loginSSO()
    }
}
