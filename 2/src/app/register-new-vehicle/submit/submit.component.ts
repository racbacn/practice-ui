import {Component, OnInit} from '@angular/core';
import {VehicleResponse} from "../../interface/vehicle-response";
import {VehicleResponseService} from "../../services/vehicle-response.service";

@Component({
    selector: 'app-submit',
    templateUrl: './submit.component.html',
    styleUrls: ['./submit.component.scss']
})
export class SubmitComponent implements OnInit {
    submitResponse!: VehicleResponse;

    constructor(private submitResponseService: VehicleResponseService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.submitResponseService.getVehicleResponse().subscribe(data => {
            console.log(data)
            this.submitResponse = data;
        })
    }

}
