import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterNewVehicleRoutingModule} from './register-new-vehicle-routing.module';
import {CoreModule, FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../material.module';
import {FillModule} from './fill/fill.module';
import {InitiateModule} from './initiate/initiate.module';
import {SubmitModule} from './submit/submit.module';
import {ReviewModule} from './review/review.module';
import {RegisterNewVehicleComponent} from "./register-new-vehicle.component";
import { WidgetsModule } from '../widgets/widgets.module';

@NgModule({
    declarations: [
        RegisterNewVehicleComponent
    ],
    imports: [
        CommonModule,
        RegisterNewVehicleRoutingModule,
        FlexLayoutModule,
        MaterialModule,
        FillModule,
        InitiateModule,
        CoreModule,
        SubmitModule,
        ReviewModule,
        WidgetsModule
    ],
    providers: [
    ]
})
export class RegisterNewVehicleModule {
}
