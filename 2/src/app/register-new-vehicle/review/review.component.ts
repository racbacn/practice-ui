import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {VehicleRequestService} from "../../services/vehicle-request.service";
import {VehicleRequest} from "../../interface/vehicle-request";
import {throwError} from "rxjs";
import {catchError} from 'rxjs/operators';
import {VEHICLE_ATTACHMENT_LIST} from 'src/app/constants/app.contants';
import {VehicleResponseService} from "../../services/vehicle-response.service";
import {DocumentService} from "../../services/document.service";
import {ConfidenceScoreRequest} from "../../interface/confidence-score-request";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {ConfidenceScoreResponse} from "../../interface/confidence-score-response";
import {UpdateApprovalLikelihoodRequest} from "../../interface/update-approval-likelihood-request";
import {VehicleResponse} from "../../interface/vehicle-response";
import {ManageUserService} from "../../services/manage-user.service";
import {UpdateApprovalLikelihoodResponse} from "../../interface/update-approval-likelihood-response";

@Component({
    selector: 'app-review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss']
})


export class ReviewComponent implements OnInit {
    @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
    @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

    reviewDetailsTitle: String = "Review details before submitting form";
    reviewReadAgreeInfo: String = "I have read and agree to the information presented in this form."
    reviewDeclarationInfo: String = "I am aware that LTA will use the Open Market Value (OMV) declared to and/or reassessed by Singapore Customs (SC) for computation of the vehicle tax [i.e. Additional Registration Fee (ARF)], and that the declaration of OMV to SC is also our declaration to LTA for taxation purposes. I will immediately inform LTA of any change in the OMV should the actual value differ from the declaration to SC. I undertake to pay the difference in the ARF should the OMV be higher upon reassessment by the SC. I confirm that the information furnished to LTA is true and correct and are aware that furnishing any false or misleading particulars, information or documents to LTA is an offence under Section 129 of the Road Traffic Act."

    submitCheckBox: boolean = false;
    finalDataToSend!: VehicleRequest;

    backButtonConfig = {
        buttonText: 'Back',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',

    };

    //Create the label for the Review Page
    labelExpansionDetails =
        [
            {headerName: "OWNER PARTICULARS", labelDetails: [], labelInfo: ["Owner ID Type", "Owner ID", "Owner Name"]},
            {
                headerName: "VEHICLE PARTICULARS",
                labelDetails: [],
                labelInfo: ["Vitas Approval Code", "Vehicle Type", "Vehicle Scheme", "Vehicle Attachment 1", "Vehicle Attachment 2", "Vehicle Attachment 3", "Vehicle Make", "Vehicle Model", "Propellant", "Passenger Capacity", "Engine Capacity", "Power Rating", "Maximum Power Output", "No. Of Axles", "Front/Rear Type Information", "Unladen Weight", "Maximum Unladen Weight", "Emission Standard Code", "CO2 Emission", "CO Emission", "HC Emission", "NOx Emission", "PM Emission", "Primary Colour", "Secondary Colour", "Manufacturing Year", "First Registration Date", "Original Registration Date", "Chasis No", "Engine No", "Motor No", "Trailer Chassis No"]
            },
            {
                headerName: "NEW REGISTRATION & ROAD TAX FEES",
                labelDetails: [],
                labelInfo: ["TCOE NO.", "PARF/COE Rebate No.", "Insurance Company", "Insurance Cover Note No", "Vehicle No Available", "Open Market Value", "Road Tax Months"]
            },
        ]

    //Mapping data to match incoming Data key with the element ID in html
    labelMapIncomingDataHTML: any = {
        "OWNER PARTICULARS": "ownerDetails",
        "VEHICLE PARTICULARS": "vehicleDetails",
        "NEW REGISTRATION & ROAD TAX FEES": "roadTaxDetails",
        "Owner ID Type": "ownerIdTypeDesc",
        "Owner ID": "ownerId",
        "Owner Name": "ownerName",
        "Vitas Approval Code": "vitasApprovalCode",
        "Vehicle Type": "vehicleType",
        "Vehicle Scheme": "vehicleScheme",
        "Vehicle Attachment 1": "attachmentOne",
        "Vehicle Attachment 2": "attachmentTwo",
        "Vehicle Attachment 3": "attachmentThree",
        "Vehicle Make": "vehicleMake",
        "Vehicle Model": "vehicleModel",
        "Propellant": "propellant",
        "Passenger Capacity": "passengerCapacity",
        "Engine Capacity": "engineCapacity",
        "Power Rating": "powerRating",
        "Maximum Power Output": "maximumPowerOutput",
        "No. Of Axles": "numberOfAxles",
        "Front/Rear Type Information": "tyreInformation",
        "Unladen Weight": "unladenWeight",
        "Maximum Unladen Weight": "maximumUnladenWeight",
        "Emission Standard Code": "emissionStandardCode",
        "CO2 Emission": "carbonDioxideEmission",
        "CO Emission": "carbonMonoxideEmission",
        "HC Emission": "hydroCarbonEmission",
        "NOx Emission": "nitrosOxideEmission",
        "PM Emission": "pmEmission",
        "Primary Colour": "primaryColour",
        "Secondary Colour": "secondaryColour",
        "Manufacturing Year": "manufacturingYear",
        "First Registration Date": "firstRegistrationDate",
        "Original Registration Date": "originalRegistrationDate",
        "Chasis No": "chassisNumber",
        "Engine No": "engineNumber",
        "Motor No": "motorNumber",
        "Trailer Chassis No": "trailerChassisNumber",
        "TCOE NO.": "tcoeNumber",
        "PARF/COE Rebate No.": "coeRebateNumber",
        "Insurance Company": "insuranceCompany",
        "Insurance Cover Note No": "insuranceCoverNoteNumber",
        "Vehicle No Available": "vehicleNumberAvailable",
        "Open Market Value": "openMarketValue",
        "Road Tax Months": "roadTaxMonths",
    }

    constructor(private fillDetailsService: VehicleRequestService, private submitResponseService: VehicleResponseService,
                private documentService: DocumentService) {
    }


    ngOnInit(): void {
        //Subscribe to the data coming from initiate and fill details
        this.fillDetailsService.getVehicleRequest().subscribe(data => {
            this.loadItems(data)
        })
    }

    clickBackPrevious() {
        this.onPreviousStepper.emit();
    }

    clickSubmit() {
        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

        let averageConfidenceScore: number = 0
        let count = 0;

        this.finalDataToSend.requestHeader = {
            requestAppId: 'VRLS_Vehicle',
            userId: userResponse.userId,
            userTypeKey: userResponse.userTypeKey
        }

        this.finalDataToSend.vehicleDetails.vehicleStatus = "UNREGISTERED"

        console.log("review submit", this.finalDataToSend)
        this.fillDetailsService.registerVehicle(this.finalDataToSend)
            .pipe(
                catchError(error => {
                    console.log(error.message);
                    return throwError(error);
                })
            )
            .subscribe(res => {
                let vehicleResponse: VehicleResponse = res;
                console.log(vehicleResponse)

                if (vehicleResponse) {
                    if (this.finalDataToSend.vehicleRegistrationDocuments.length <= 0) {
                        this.submitResponseService.setVehicleResponse(vehicleResponse)
                        this.onNextStepper.emit(true);
                    }

                    this.finalDataToSend.vehicleRegistrationDocuments.forEach(data => {
                        let confidenceScoreRequest: ConfidenceScoreRequest = {
                            requestHeader: {
                                requestAppId: 'VRLS_DOCUMENT_ATTRIBUTES_CONFIDENCE_SCORE',
                                userId: userResponse.userId,
                                userTypeKey: userResponse.userTypeKey
                            },
                            uploadedDocument: {
                                documentId: data.documentId,
                                filename: data.documentRefNum + '_' + data.fileName,
                            }
                        }

                        this.documentService.generateConfidenceScore(confidenceScoreRequest).toPromise().then(data => {
                            let confidenceScoreResponse: ConfidenceScoreResponse = data;

                            console.log(confidenceScoreResponse)

                            if (confidenceScoreResponse) {
                                averageConfidenceScore += confidenceScoreResponse.avgScore
                                count += 1;

                                if (count === 2) {
                                    console.log(count)

                                    averageConfidenceScore = averageConfidenceScore / 2;

                                    let updateApprovalLikelihoodRequest: UpdateApprovalLikelihoodRequest = {
                                        transactionId: parseInt(vehicleResponse.transactionId),
                                        documentConfidenceAvg: averageConfidenceScore
                                    }

                                    console.log(updateApprovalLikelihoodRequest)

                                    this.fillDetailsService.updateLikelihoodUrl(updateApprovalLikelihoodRequest).subscribe(data => {
                                        let updateApprovalLikelihoodResponse: UpdateApprovalLikelihoodResponse = data;

                                        console.log(updateApprovalLikelihoodResponse)

                                        if (updateApprovalLikelihoodResponse) {
                                            this.submitResponseService.setVehicleResponse(vehicleResponse)
                                            this.onNextStepper.emit(true);
                                        }
                                    })
                                }
                            }
                        })


                    });
                }
            });


    }

    //Load the incoming data with the label and details
    private loadItems(inputData: any): void {

        // labelExpansionDetails
        for (let i in this.labelExpansionDetails) {
            //Clear the information store in the labelDetails else it will keep pushing in data
            this.labelExpansionDetails[i].labelDetails = [];
            //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
            let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]]
            //Loop through label to find is there inputData to map to labelExpansionDetails
            this.labelExpansionDetails[i].labelInfo.forEach(labelinfo => {
                let incomingValue = incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]];
                //Check if there is data
                if (incomingValue) {
                    //cater for Vehicle attachment to match the ID with the name
                    if (labelinfo.includes("Vehicle Attachment")) {
                        //Return the
                        let vehAttachName = VEHICLE_ATTACHMENT_LIST.filter(vehAtt => vehAtt.id == incomingValue)
                        this.labelExpansionDetails[i].labelDetails.push(vehAttachName[0]["name"] as never);
                    } else {
                        this.labelExpansionDetails[i].labelDetails.push(incomingValue as never);
                    }
                } else {
                    //Set as - for no data
                    this.labelExpansionDetails[i].labelDetails.push("-" as never);
                }
            })
        }
        this.finalDataToSend = JSON.parse(JSON.stringify(inputData))
    }
}
