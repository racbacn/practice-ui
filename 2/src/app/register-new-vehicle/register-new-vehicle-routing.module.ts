import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterNewVehicleComponent} from './register-new-vehicle.component';

const routes: Routes = [{
    path: '', component: RegisterNewVehicleComponent,
    // children: [
    //     { path: 'initiate', loadChildren: () => import('../register-new-vehicle/initiate/initiate.module').then(m => m.InitiateModule) },
    //     {path: 'fill', loadChildren: () => import('../register-new-vehicle/fill/fill.module').then(m => m.FillModule)},
    //     { path: 'review', loadChildren: () => import('../register-new-vehicle/review/review.module').then(m => m.ReviewModule) },
    //     { path: 'submit', loadChildren: () => import('../register-new-vehicle/submit/submit.module').then(m => m.SubmitModule) }
    // ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegisterNewVehicleRoutingModule {
}
