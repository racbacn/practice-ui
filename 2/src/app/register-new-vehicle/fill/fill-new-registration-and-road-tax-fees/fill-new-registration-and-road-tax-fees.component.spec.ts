import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillNewRegistrationAndRoadTaxFeesComponent } from './fill-new-registration-and-road-tax-fees.component';

describe('FillNewRegistrationAndRoadTaxFeesComponent', () => {
  let component: FillNewRegistrationAndRoadTaxFeesComponent;
  let fixture: ComponentFixture<FillNewRegistrationAndRoadTaxFeesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillNewRegistrationAndRoadTaxFeesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillNewRegistrationAndRoadTaxFeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
