import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Company} from "../../../interface/company";
import {COMPANY_LIST, REFUND_COE_TO_LIST, ROAD_TAX_MONTH_LIST} from "../../../constants/app.contants";
import {VehicleRequest} from "../../../interface/vehicle-request";
import {RoadTaxDetails} from "../../../interface/road-tax-details";
import {VehicleRequestService} from "../../../services/vehicle-request.service";
import {TransactionService} from "../../../services/transaction.service";
import {FillFormValidationsService} from "../../../services/fill-form-validations.service";
import {VitasCodeService} from "../../../services/vitas-code.service";
import {SessionUtil} from "../../../core/util/sessionUtil";
import {TcoeDetails} from "../../../interface/tcoe-details";

@Component({
    selector: 'app-fill-new-registration-and-road-tax-fees',
    templateUrl: './fill-new-registration-and-road-tax-fees.component.html',
    styleUrls: ['./fill-new-registration-and-road-tax-fees.component.scss']
})
export class FillNewRegistrationAndRoadTaxFeesComponent implements OnInit {
    roadTaxFormGroup!: FormGroup;

    companyList: Array<Company> = COMPANY_LIST;
    roadTaxMonthList: Array<string> = ROAD_TAX_MONTH_LIST;
    refundCoeToList: Array<string> = REFUND_COE_TO_LIST;

    vehicleRequest!: VehicleRequest;
    roadTaxDetails!: RoadTaxDetails;
    isOMVErrorHidden: boolean = true;
    tceoDetails!: TcoeDetails;

    constructor(private formBuilder: FormBuilder,
                private vehicleRequestService: VehicleRequestService,
                private transactionService: TransactionService,
                private fillFormValidationsService: FillFormValidationsService,
                private vitasService: VitasCodeService) {

    }

    ngOnInit() {
        this.initForm();
        this.initData();
    }


    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe(data => {
            this.vehicleRequest = data;
            this.roadTaxDetails = this.vehicleRequest.roadTaxDetails;

            let vitasApprovalCode: string = this.vehicleRequest?.vehicleDetails?.vitasApprovalCode;

            if (vitasApprovalCode) {
                this.getTcoeDetails(vitasApprovalCode);
            }
        })

        let transactionID: string | null = SessionUtil.getSessionStorage(SessionUtil.TRANSACTION_ID);

        if (transactionID) {
            this.vehicleRequestService.getVehicleDetailsByTransactionID(transactionID).subscribe((data: VehicleRequest) => {
                let vehicleRequestData = data;

                this.vehicleRequest.roadTaxDetails = vehicleRequestData.roadTaxDetails;

                this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);

                this.initForm();
            });
        }
    }

    initForm() {
        console.log('pass', this.roadTaxDetails)
        this.roadTaxFormGroup = this.formBuilder.group({
            coeRebateNumber: [this.roadTaxDetails?.coeRebateNumber ? this.roadTaxDetails?.coeRebateNumber : 'N/A'],
            insuranceCompany: [this.roadTaxDetails?.insuranceCompany, Validators.required],
            insuranceCoverNoteNumber: [this.roadTaxDetails?.insuranceCoverNoteNumber, Validators.required],
            openMarketValue: [this.roadTaxDetails?.openMarketValue, [Validators.required, this.customValidator]],
            roadTaxMonths: [this.roadTaxDetails?.roadTaxMonths, Validators.required],
            refundCoeTo: [this.roadTaxDetails?.refundCoeTo, Validators.required],
            refundAmount: [this.roadTaxDetails?.refundAmount, Validators.required]
        })
        this.roadTaxFormGroup.get('coeRebateNumber')?.disabled;
        this.fillFormValidationsService.setRoadTaxDetailsFormGroup(this.roadTaxFormGroup);
        this.formValueChange();
    }

    formValueChange() {

        this.roadTaxFormGroup.valueChanges.subscribe(data => {
            let roadTaxDetailsFormValue: any = data;

            if (roadTaxDetailsFormValue.openMarketValue) {
                if (this.roadTaxFormGroup.get('openMarketValue')?.invalid) {
                    this.isOMVErrorHidden = false;
                } else {
                    this.isOMVErrorHidden = true;
                }
            }

            this.vehicleRequest.roadTaxDetails.insuranceCompany = roadTaxDetailsFormValue.insuranceCompany;
            this.vehicleRequest.roadTaxDetails.insuranceCoverNoteNumber = roadTaxDetailsFormValue.insuranceCoverNoteNumber;
            this.vehicleRequest.roadTaxDetails.openMarketValue = roadTaxDetailsFormValue.openMarketValue;
            this.vehicleRequest.roadTaxDetails.roadTaxMonths = roadTaxDetailsFormValue.roadTaxMonths;
            this.vehicleRequest.roadTaxDetails.refundCoeTo = roadTaxDetailsFormValue.refundCoeTo;
            this.vehicleRequest.roadTaxDetails.refundAmount = roadTaxDetailsFormValue.refundAmount;
            this.vehicleRequest.roadTaxDetails.coeRebateNumber = 'N/A';

            this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);
            this.fillFormValidationsService.setRoadTaxDetailsFormGroup(this.roadTaxFormGroup);
        })
    }

    getTcoeDetails(vitasApprovalCode: string) {
        this.vitasService.getTcoeByCode(vitasApprovalCode).subscribe(res => {
            let tcoeDetailsResponse: TcoeDetails = res;

            console.log("tcoe", tcoeDetailsResponse)

            if (tcoeDetailsResponse) {
                this.vehicleRequest.roadTaxDetails.tcoeNumber = tcoeDetailsResponse.tcoe
                this.tceoDetails = tcoeDetailsResponse;
            }
        });
    }

    customValidator(control: AbstractControl) {
        const value = control.value;

        if (!value) {
            return null;
        }

        const validatedValue = /[^0-9]+/.test(value);

        return (validatedValue || (value < 100)) ? {notValid: control.value} : null;
    }
}
