import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Expansion} from "../../interface/expansion";
import {VehicleRequest} from "../../interface/vehicle-request";
import {VehicleRequestService} from "../../services/vehicle-request.service";
import {FILL_EXPANSION_PANEL_LIST} from "../../constants/app.contants";
import {TransactionService} from "../../services/transaction.service";
import {FillFormValidationsService} from "../../services/fill-form-validations.service";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'app-fill',
    templateUrl: './fill.component.html',
    styleUrls: ['./fill.component.scss']
})

export class FillComponent implements OnInit {
    @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
    @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

    nextButtonConfig = {
        buttonText: 'Next',
        buttonClassName: 'purple-button',
        buttonFont: 'button-font-16',
        hasIcon: true,
        iconName: 'arrow_right_alt',
        buttonWidth: 'button-70',
    };

    saveButtonConfig = {
        buttonText: 'Save as Draft',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    backButtonConfig = {
        buttonText: 'Back',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    items: Array<Expansion> = FILL_EXPANSION_PANEL_LIST;

    vehicleRequest!: VehicleRequest;

    isErrorHidden: boolean = true;

    vehicleDetailsFormGroup!: FormGroup;
    roadTaxDetailsFormGroup!: FormGroup;


    constructor(private vehicleRequestService: VehicleRequestService,
                private transactionService: TransactionService,
                private fillFormValidationsService: FillFormValidationsService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe(data => {
            this.vehicleRequest = data;
        })


        let comment: string | null = SessionUtil.getSessionStorage(SessionUtil.COMMENT)

        this.hiddenFeedback(true);

        if (comment) {
            console.log('test', comment)
            this.hiddenFeedback(false);
        }

        this.fillFormValidationsService.getVehicleDetailsFormGroup().subscribe(data => {
            let vehicleFormGroup: FormGroup = data;

            vehicleFormGroup.updateValueAndValidity({onlySelf: false, emitEvent: true})

            this.vehicleDetailsFormGroup = vehicleFormGroup;
        })

        this.fillFormValidationsService.getRoadTaxDetailsFormGroup().subscribe(data => {
            let roadTaxDetailsFormGroup: FormGroup = data;

            roadTaxDetailsFormGroup.updateValueAndValidity({onlySelf: false, emitEvent: true})

            this.roadTaxDetailsFormGroup = roadTaxDetailsFormGroup;
        })
    }

    hiddenFeedback(hidden: boolean) {
        this.items[0].hidden = hidden;
    }

    onSave() {
        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

        this.vehicleRequest.requestHeader = {
            requestAppId: 'VRLS_Vehicle',
            userId: userResponse.userId,
            userTypeKey: userResponse.userTypeKey
        }

        this.vehicleRequest.vehicleDetails.vehicleStatus = "DRAFT"

        console.log("save params", this.vehicleRequest)
        this.vehicleRequestService.saveDraftVehicle(this.vehicleRequest).subscribe(data => {
            console.log(data)
        }, error => {
            console.log(error.message);
        });

    }

    onBack() {
        this.onPreviousStepper.emit(true);
    }

    onNext() {
        const invalid1 = [];
        const controls1 = this.vehicleDetailsFormGroup.controls;
        for (const name in controls1) {
            if (controls1[name] && controls1[name].invalid) {
                invalid1.push(name);
            }
        }

        const invalid2 = [];
        const controls2 = this.roadTaxDetailsFormGroup.controls;
        for (const name in controls2) {
            if (controls2[name] && controls2[name].invalid) {
                invalid2.push(name);
            }
        }

        console.log(invalid1, 'vehicle')
        console.log(invalid2, 'roadtax')

        if (this.vehicleDetailsFormGroup.valid && this.roadTaxDetailsFormGroup.valid) {
            this.onNextStepper.emit(true);
        } else {
            this.isErrorHidden = false;
        }
    }
}
