import {Component, OnInit} from '@angular/core';
import {SessionUtil} from "../../../core/util/sessionUtil";

@Component({
    selector: 'app-feedback',
    templateUrl: './fill-feedback.component.html',
    styleUrls: ['./fill-feedback.component.scss']
})
export class FillFeedbackComponent implements OnInit {

    feedBack!: string;

    constructor() {
    }

    ngOnInit(): void {
        let comment: string | null = SessionUtil.getSessionStorage(SessionUtil.COMMENT)

        if (comment) {
            this.feedBack = comment;
        }
    }

}
