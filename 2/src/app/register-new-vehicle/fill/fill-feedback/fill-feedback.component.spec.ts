import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillFeedbackComponent } from './fill-feedback.component';

describe('FillFeedbackComponent', () => {
  let component: FillFeedbackComponent;
  let fixture: ComponentFixture<FillFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillFeedbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
