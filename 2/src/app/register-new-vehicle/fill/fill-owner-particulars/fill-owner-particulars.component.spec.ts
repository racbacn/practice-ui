import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillOwnerParticularsComponent } from './fill-owner-particulars.component';

describe('FillOwnerParticularsComponent', () => {
  let component: FillOwnerParticularsComponent;
  let fixture: ComponentFixture<FillOwnerParticularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillOwnerParticularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillOwnerParticularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
