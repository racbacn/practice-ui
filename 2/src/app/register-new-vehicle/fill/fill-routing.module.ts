import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FillComponent } from './fill.component';

const routes: Routes = [
  { path: '', component: FillComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FillRoutingModule { }
