import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FillRoutingModule} from './fill-routing.module';
import {FillComponent} from './fill.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SubmitModule} from "../submit/submit.module";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {FillOwnerParticularsComponent} from "./fill-owner-particulars/fill-owner-particulars.component";
import {FillVehicleParticularsComponent} from "./fill-vehicle-particulars/fill-vehicle-particulars.component";
import {FillNewRegistrationAndRoadTaxFeesComponent} from "./fill-new-registration-and-road-tax-fees/fill-new-registration-and-road-tax-fees.component";
import { MaterialModule } from 'src/app/material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatRadioModule} from "@angular/material/radio";
import {FillFeedbackComponent} from "./fill-feedback/fill-feedback.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { DireciveModule } from 'src/app/directive/directives.module';


@NgModule({
    declarations: [
        FillComponent,
        FillOwnerParticularsComponent,
        FillVehicleParticularsComponent,
        FillNewRegistrationAndRoadTaxFeesComponent,
        FillFeedbackComponent
    ],
    imports: [
        CommonModule,
        FillRoutingModule,
        ReactiveFormsModule,
        SubmitModule,
        MatButtonToggleModule,
        MaterialModule,
        WidgetsModule,
        FlexLayoutModule,
        FormsModule,
        MatCheckboxModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        DireciveModule
    ],
    exports: [
        FillComponent
    ]
})
export class FillModule {
}
