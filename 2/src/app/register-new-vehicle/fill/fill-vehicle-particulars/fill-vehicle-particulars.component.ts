import {Component, OnInit} from '@angular/core';
import {
    FILE_SIZE_MAX,
    FILE_TYPE_JPG,
    FILE_TYPE_PDF,
    FILE_TYPE_PNG,
    VEHICLE_ATTACHMENT_LIST,
    VEHICLE_COLOUR_LIST,
    VEHICLE_SCHEME_LIST
} from '../../../constants/app.contants';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VehicleDetails} from '../../../interface/vehicle-details';
import {VehicleRequestService} from "../../../services/vehicle-request.service";
import {VehicleRequest} from "../../../interface/vehicle-request";
import {UploadService} from "../../../services/upload.service";
import {UploadRequest} from "../../../interface/upload-request";
import {environment} from "../../../../environments/environment";
import {UploadResponse} from "../../../interface/upload-response";
import {Attachment} from "../../../interface/attachment";
import {VitasCodeService} from "../../../services/vitas-code.service";
import {VitasDetails} from "../../../interface/vitas-details";
import {DocumentService} from "../../../services/document.service";
import {VehicleRegistrationDocumentRequest} from "../../../interface/vehicle-registration-document-request";
import {VehicleRegistrationDocumentResponse} from "../../../interface/vehicle-registration-document-response";
import {VehicleRegistrationDocument} from "../../../interface/vehicle-registration-document";
import {TransactionService} from "../../../services/transaction.service";
import {FillFormValidationsService} from "../../../services/fill-form-validations.service";
import {UserResponse} from "../../../interface/user-response";
import {SessionUtil} from "../../../core/util/sessionUtil";
import * as moment from "moment";

@Component({
    selector: 'app-fill-vehicle-particulars',
    templateUrl: './fill-vehicle-particulars.component.html',
    styleUrls: ['./fill-vehicle-particulars.component.scss']
})
export class FillVehicleParticularsComponent implements OnInit {
    vehicleDetailsFormGroup!: FormGroup;

    vitasApprovalCodeList: Array<string> = ['123']
    vehicleSchemeList: Array<string> = VEHICLE_SCHEME_LIST;
    vehicleAttachmentList: Array<Attachment> = VEHICLE_ATTACHMENT_LIST;
    vehicleColourList: Array<string> = VEHICLE_COLOUR_LIST;
    fileSizeMax: number = FILE_SIZE_MAX;
    fileTypePdf: string = FILE_TYPE_PDF;
    fileTypeJpg: string = FILE_TYPE_JPG;
    fileTypePng: string = FILE_TYPE_PNG;


    vehicleRequest!: VehicleRequest
    vehicleDetails!: VehicleDetails;

    responseDataOne!: UploadResponse;
    isSizeErrorHidden: boolean = true;
    isTypeErrorHidden: boolean = true;
    isChassisEngineFormatErrorHidden: boolean = true;
    isLoadingOneHidden: boolean = true;
    isLoadingTwoHidden: boolean = true;

    constructor(private formBuilder: FormBuilder, private vehicleRequestService: VehicleRequestService,
                private uploadService: UploadService, private vitasCodeService: VitasCodeService,
                private documentService: DocumentService, private transactionService: TransactionService,
                private fillFormValidationsService: FillFormValidationsService) {
    }

    ngOnInit(): void {
        this.initForm();
        this.initData();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe((data: VehicleRequest) => {
            this.vehicleRequest = data;
            this.vehicleDetails = this.vehicleRequest.vehicleDetails;

            this.vehicleDetails.firstRegistrationDate = moment(new Date(this.vehicleDetails.firstRegistrationDate)).format('DD/MM/YYYY');
            this.vehicleDetails.originalRegistrationDate = moment(new Date(this.vehicleDetails.originalRegistrationDate)).format('DD/MM/YYYY');
        })

        let transactionID: string | null = SessionUtil.getSessionStorage(SessionUtil.TRANSACTION_ID);

        if (transactionID) {
            this.vehicleRequestService.getVehicleDetailsByTransactionID(transactionID).subscribe((data: VehicleRequest) => {
                let vehicleRequestData = data;

                this.vehicleRequest.vehicleDetails = vehicleRequestData.vehicleDetails;

                this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);

                this.initForm();
            });
        }
    }

    initForm() {
        this.vehicleDetailsFormGroup = this.formBuilder.group({
            vitasApprovalCode: [this.vehicleDetails?.vitasApprovalCode, [Validators.required]],
            vehicleScheme: [this.vehicleDetails?.vehicleScheme ? this.vehicleDetails?.vehicleScheme : this.vehicleSchemeList[0], [Validators.required]],
            attachmentOne: [this.vehicleDetails?.attachmentOne ? this.vehicleDetails?.attachmentOne : this.vehicleAttachmentList[0].id, [Validators.required]],
            attachmentTwo: [this.vehicleDetails?.attachmentTwo ? this.vehicleDetails?.attachmentTwo : this.vehicleAttachmentList[0].id, [Validators.required]],
            attachmentThree: [this.vehicleDetails?.attachmentThree ? this.vehicleDetails?.attachmentThree : this.vehicleAttachmentList[0].id, [Validators.required]],
            primaryColour: [this.vehicleDetails?.primaryColour, [Validators.required]],
            secondaryColour: [this.vehicleDetails?.secondaryColour],
            manufacturingYear: [this.vehicleDetails?.manufacturingYear, [Validators.required, this.checkLimit(0, 9999)]],
            chassisNumber: [this.vehicleDetails?.chassisNumber, [Validators.required, this.customValidator]],
            engineNumber: [this.vehicleDetails?.engineNumber, [Validators.required, this.customValidator]],
            diplomaticId: [''],
            mfaApprovalLetter: ['']
        })

        this.fillFormValidationsService.setVehicleDetailsFormGroup(this.vehicleDetailsFormGroup);
        this.formValueChange();
    }

    formValueChange() {
        this.vehicleDetailsFormGroup.valueChanges.subscribe(data => {
            let vehicleDetailsFormValue: any = data;

            if (vehicleDetailsFormValue.chassisNumber && vehicleDetailsFormValue.engineNumber) {
                if (this.vehicleDetailsFormGroup.get('chassisNumber')?.invalid || this.vehicleDetailsFormGroup.get('engineNumber')?.invalid) {
                    this.isChassisEngineFormatErrorHidden = false;
                } else {
                    this.isChassisEngineFormatErrorHidden = true;
                }
            }

            if (vehicleDetailsFormValue.vehicleScheme === 'Diplomatic') {
                this.vehicleDetailsFormGroup.get('diplomaticId')?.setValidators(Validators.required);
                this.vehicleDetailsFormGroup.get('mfaApprovalLetter')?.setValidators(Validators.required);
            } else {
                this.vehicleDetailsFormGroup.get('diplomaticId')?.setValidators(null);
                this.vehicleDetailsFormGroup.get('mfaApprovalLetter')?.setValidators(null);
            }


            if (vehicleDetailsFormValue.vitasApprovalCode) {
                this.vitasCodeService.getVitasByCode(vehicleDetailsFormValue.vitasApprovalCode)
                    .subscribe((data: VitasDetails) => {

                        let vitasDetails: VitasDetails = data;

                        this.vehicleDetails.vitasApprovalCode = vitasDetails.vitasApprovalCode;
                        this.vehicleDetails.vehicleMake = vitasDetails.vehicleMake;
                        this.vehicleDetails.vehicleModel = vitasDetails.vehicleModel;
                        this.vehicleDetails.propellant = vitasDetails.propellant;
                        this.vehicleDetails.passengerCapacity = vitasDetails.passengerCapacity;
                        this.vehicleDetails.engineCapacity = vitasDetails.engineCapacity;
                        this.vehicleDetails.powerRating = vitasDetails.powerRating;
                        this.vehicleDetails.numberOfAxles = vitasDetails.noOfAxles;
                        this.vehicleDetails.unladenWeight = vitasDetails.unladenWeight;
                        this.vehicleDetails.maximumUnladenWeight = vitasDetails.maximumUnladenWeight;
                        this.vehicleDetails.emissionStandardCode = vitasDetails.emissionStandardCode;
                        this.vehicleDetails.firstRegistrationDate = vitasDetails.firstRegistrationDate;
                        this.vehicleDetails.originalRegistrationDate = vitasDetails.originalRegistrationDate;
                        this.vehicleDetails.motorNumber = vitasDetails.motorNumber;
                        this.vehicleDetails.trailerChassisNumber = vitasDetails.trailerChasisNumber;
                        this.vehicleDetails.nitrousOxideEmission = vitasDetails.noxEmission;
                        this.vehicleDetails.carbonDioxideEmission = vitasDetails.co2Emission;
                        this.vehicleDetails.carbonMonoxideEmission = vitasDetails.coEmission;
                        this.vehicleDetails.hydroCarbonEmission = vitasDetails.hcEmission;
                        this.vehicleDetails.pmEmission = vitasDetails.pmEmission;
                    })
            }

            this.vehicleDetails.vitasApprovalCode = vehicleDetailsFormValue.vitasApprovalCode;
            this.vehicleDetails.vehicleScheme = vehicleDetailsFormValue.vehicleScheme;
            this.vehicleDetails.attachmentOne = vehicleDetailsFormValue.attachmentOne;
            this.vehicleDetails.attachmentTwo = vehicleDetailsFormValue.attachmentTwo;
            this.vehicleDetails.attachmentThree = vehicleDetailsFormValue.attachmentThree;
            this.vehicleDetails.primaryColour = vehicleDetailsFormValue.primaryColour;
            this.vehicleDetails.secondaryColour = vehicleDetailsFormValue.secondaryColour;
            this.vehicleDetails.manufacturingYear = vehicleDetailsFormValue.manufacturingYear;
            this.vehicleDetails.chassisNumber = vehicleDetailsFormValue.chassisNumber;
            this.vehicleDetails.engineNumber = vehicleDetailsFormValue.engineNumber;

            this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);
            this.fillFormValidationsService.setVehicleDetailsFormGroup(this.vehicleDetailsFormGroup);
        })
    }

    onUploadFile(event: any) {
        const file: File = event.target.files[0];
        this.isSizeErrorHidden = true;
        this.isTypeErrorHidden = true;

        if (file.size >= this.fileSizeMax) {
            this.isSizeErrorHidden = false;
            return;
        }

        if (file.type.toString() === this.fileTypeJpg || file.type.toString() === this.fileTypePng) {
            this.isSizeErrorHidden = true;
            this.isTypeErrorHidden = true;

            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

            let vehicleRegistrationDocumentRequest: VehicleRegistrationDocumentRequest = {
                requestHeader: {
                    requestAppId: "VRLS_DOCUMENT_REFNUM",
                    userId: userResponse.userId,
                    userTypeKey: userResponse.userTypeKey
                },
                documentTypeKey: 1
            }

            this.isLoadingOneHidden = false;

            this.documentService.createDocumentId(vehicleRegistrationDocumentRequest)
                .subscribe((data: VehicleRegistrationDocumentResponse) => {

                    let vehicleRegistrationDocument: VehicleRegistrationDocument = {
                        documentId: data.documentId,
                        documentRefNum: data.documentRefNum,
                        fileName: ''
                    }

                    let uploadRequest: UploadRequest = {
                        Bucket: environment.aws.s3.bucket,
                        Key: data.documentRefNum + '_' + file.name,
                        Body: file,
                        ACL: 'private',
                        ContentType: file.type
                    }

                    if (file) {
                        this.uploadService.uploadFile(uploadRequest).promise().then(data => {
                            if (data) {

                                this.vehicleDetailsFormGroup.get('diplomaticId')?.setValue(file.name)

                                vehicleRegistrationDocument.fileName = file.name

                                this.vehicleRequest.vehicleRegistrationDocuments.push(vehicleRegistrationDocument);

                                this.isLoadingOneHidden = true;

                                this.responseDataOne = {
                                    isSuccessful: true,
                                    message: "Upload is successful"
                                }
                            }
                        }).catch(err => {
                            if (err) {
                                this.responseDataOne = {
                                    isSuccessful: true,
                                    message: err.message
                                }
                            }
                        })
                    }
                })
        } else {
            this.isTypeErrorHidden = false;
        }


    }

    onUploadFileTwo(event: any) {
        const file: File = event.target.files[0];

        if (file.size >= this.fileSizeMax) {
            this.isSizeErrorHidden = false;
            return;
        }

        if (file.type.toString() === this.fileTypeJpg || file.type.toString() === this.fileTypePng) {
            this.isSizeErrorHidden = true;

            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

            let vehicleRegistrationDocumentRequest: VehicleRegistrationDocumentRequest = {
                requestHeader: {
                    requestAppId: "VRLS_DOCUMENT_REFNUM",
                    userId: userResponse.userId,
                    userTypeKey: userResponse.userTypeKey
                },
                documentTypeKey: 2
            }

            this.isLoadingTwoHidden = false;

            this.documentService.createDocumentId(vehicleRegistrationDocumentRequest)
                .subscribe((data: VehicleRegistrationDocumentResponse) => {

                    let vehicleRegistrationDocument: VehicleRegistrationDocument = {
                        documentId: data.documentId,
                        documentRefNum: data.documentRefNum,
                        fileName: ''
                    }

                    let uploadRequest: UploadRequest = {
                        Bucket: environment.aws.s3.bucket,
                        Key: data.documentRefNum + '_' + file.name,
                        Body: file,
                        ACL: 'private',
                        ContentType: file.type
                    }

                    if (file) {
                        let responseDataTwo: UploadResponse;

                        this.uploadService.uploadFile(uploadRequest).promise().then(data => {

                            if (data) {
                                this.vehicleDetailsFormGroup.get('mfaApprovalLetter')?.setValue(file.name)

                                vehicleRegistrationDocument.fileName = file.name

                                this.vehicleRequest.vehicleRegistrationDocuments.push(vehicleRegistrationDocument);

                                this.isLoadingTwoHidden = true;

                                responseDataTwo = {
                                    isSuccessful: true,
                                    message: "Upload is successful"
                                }
                            }
                        }).catch(err => {
                            if (err) {
                                responseDataTwo = {
                                    isSuccessful: true,
                                    message: err.message
                                }
                            }
                        })
                    }
                })
        } else {
            this.isTypeErrorHidden = false;
        }
    }

    customValidator(control: AbstractControl) {
        const value = control.value;

        if (!value) {
            return null;
        }

        const validatedValue = /[^a-zA-Z0-9\\\/]+/.test(value);

        return validatedValue ? {notValid: control.value} : null;
    }

    checkLimit(min: number, max: number) {
        return (c: AbstractControl): { [key: string]: boolean } | null => {
            if (c.value && (isNaN(c.value) || c.value < min || c.value > max)) {
                return {'range': true};
            }
            return null;
        };
    }
}
