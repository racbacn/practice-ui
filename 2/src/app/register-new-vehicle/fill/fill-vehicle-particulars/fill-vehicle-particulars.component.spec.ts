import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillVehicleParticularsComponent } from './fill-vehicle-particulars.component';

describe('FillVehicleParticularsComponent', () => {
  let component: FillVehicleParticularsComponent;
  let fixture: ComponentFixture<FillVehicleParticularsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillVehicleParticularsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillVehicleParticularsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
