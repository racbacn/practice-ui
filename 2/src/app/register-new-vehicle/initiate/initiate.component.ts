import {Component, EventEmitter, OnInit, Output,} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {OWNER_ID_TYPE_VALUES} from '../../constants/app.contants';
import {VehicleRequestService} from '../../services/vehicle-request.service';
import {VehicleRequest} from '../../interface/vehicle-request';
import {OwnerDetails} from '../../interface/owner-details';
import {TransactionService} from "../../services/transaction.service";
import {OwnerIdType} from "../../interface/owner-id-type";
import {ManageUserService} from "../../services/manage-user.service";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {BehaviorSubject} from "rxjs";

@Component({
    selector: 'app-initiate',
    templateUrl: './initiate.component.html',
    styleUrls: ['./initiate.component.scss'],
})
export class InitiateComponent implements OnInit {
    initiateFormGroup!: FormGroup;

    vehicleRequest!: VehicleRequest;
    ownerDetails!: OwnerDetails;

    ownerIdTypeValues: Array<OwnerIdType> = OWNER_ID_TYPE_VALUES;

    @Output() allowNext: EventEmitter<any> = new EventEmitter();

    errorMsg: boolean = false;

    validationErrMsg: boolean = false;

    isUserExistErrorHidden: boolean = true;
    externalIdentificationError!: string;

    proceedButtonConfig = {
        buttonText: 'Proceed',
        buttonClassName: 'purple-button',
        buttonFont: 'button-font-16',
        hasIcon: true,
        iconName: 'arrow_right_alt',
        buttonWidth: 'button-70',
    };

    backButtonConfig = {
        buttonText: 'Back',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    constructor(private fb: FormBuilder,
                private vehicleRequestService: VehicleRequestService,
                private transactionService: TransactionService,
                private manageUserService: ManageUserService) {

    }

    ngOnInit(): void {
        this.initData();
        this.initForm();
        this.formValueChange();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe((data: VehicleRequest) => {
            this.vehicleRequest = data;
            this.ownerDetails = this.vehicleRequest.ownerDetails;
        });

        let transactionID: string | null = SessionUtil.getSessionStorage(SessionUtil.TRANSACTION_ID);

        if (transactionID) {
            this.vehicleRequestService.getVehicleDetailsByTransactionID(transactionID).subscribe((data: VehicleRequest) => {
                let vehicleRequestData = data;

                this.vehicleRequest = vehicleRequestData;

                this.vehicleRequest.vehicleRegistrationDocuments = [];

                this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);

                this.initForm();
            });
        }
    }

    proceedToFill(): void {
        let checkOnlyNumbers: Array<boolean> = [];
        let checkNumbersBoolean: boolean = false;

        let idType: string = this.initiateFormGroup.controls.ownerIdType.value;
        let ownerIdValue: string = this.initiateFormGroup.controls.ownerId.value;
        let salesAgreementId: string = this.initiateFormGroup.controls.salesAgreementId.value;

        let nricFirstchar: any = ownerIdValue.charAt(0);
        let nricLastchar: any = ownerIdValue.charAt(ownerIdValue.length - 1);

        if (idType && ownerIdValue !== '' && salesAgreementId !== '') {
            if (idType === '1') {
                let a = this.initiateFormGroup.controls.ownerId.value;
                let valueBetweenFirstAndLastCharacter = a.substring(1, a.length - 1);

                for (var i = 0; i < valueBetweenFirstAndLastCharacter.length; i++) {
                    if (
                        valueBetweenFirstAndLastCharacter.charAt(i) >= '0' &&
                        valueBetweenFirstAndLastCharacter.charAt(i) <= '9'
                    ) {
                        checkOnlyNumbers.push(true);
                    } else {
                        checkOnlyNumbers.push(false);
                    }
                }

                checkNumbersBoolean = checkOnlyNumbers.every(Boolean);

                if (this.ownerIdValidation(nricFirstchar, nricLastchar, checkNumbersBoolean)) {
                    this.onCheckExternalIdentification(ownerIdValue, idType);
                } else {
                    this.validationErrMsg = true;
                    this.allowNext.emit(false);
                }
            }

            if (idType === '2') {
                this.onCheckExternalIdentification(ownerIdValue, idType);
            }
        } else {
            this.errorMsg = true;
            this.allowNext.emit(false);
        }
    }

    ownerIdValidation(nricFirstchar: string, nricLastchar: any, checkNumbersBoolean: boolean) {
        let isValid: boolean = (nricFirstchar === 'S' || nricFirstchar === 'T') &&
            isNaN(nricLastchar) && checkNumbersBoolean

        return isValid;
    }

    initForm() {
        this.initiateFormGroup = this.fb.group({
            ownerIdType: [this.ownerDetails?.ownerIdType, Validators.required],
            ownerId: [
                this.ownerDetails?.ownerId,
                [
                    Validators.required,
                    Validators.maxLength(9),
                    Validators.pattern
                ]
            ],
            ownerName: [this.ownerDetails?.ownerName, Validators.required],
            salesAgreementId: [this.ownerDetails?.salesAgreementId, Validators.required],
        });
    }

    formValueChange() {
        // To control error message appearance on ui
        // continuously listens to input value changes
        this.initiateFormGroup.valueChanges.subscribe((data) => {
            let ownerDetailsFormValue: any = data;

            this.errorMsg = false;
            this.validationErrMsg = false;
            this.isUserExistErrorHidden = true;

            this.vehicleRequest.ownerDetails = {
                ownerIdType: ownerDetailsFormValue.ownerIdType,
                ownerIdTypeDesc: this.ownerIdTypeValues.filter(ownerIdType => {
                    return ownerIdType.value === ownerDetailsFormValue.ownerIdType
                })[0].name,
                ownerId: ownerDetailsFormValue.ownerId,
                ownerUserId: this.ownerDetails.ownerUserId,
                ownerName: ownerDetailsFormValue.ownerName,
                salesAgreementId: ownerDetailsFormValue.salesAgreementId,
            };
        });
    }

    onCheckExternalIdentification(ownerIdValue: string, idType: string) {
        this.manageUserService.getUserIdByExternalIdentification(ownerIdValue, idType).subscribe(data => {
            let userResponse: UserResponse = data

            console.log(userResponse)

            if (userResponse) {
                this.vehicleRequest.ownerDetails.ownerUserId = userResponse.userId

                this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);
                this.allowNext.emit(true);
            }
        }, error => {
            this.isUserExistErrorHidden = false;
            this.externalIdentificationError = error.error.message;
            this.allowNext.emit(false);
        })
    }
}
