import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitiateRoutingModule } from './initiate-routing.module';
import { InitiateComponent } from './initiate.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    InitiateComponent
  ],
  imports: [
    CommonModule,
    InitiateRoutingModule,
    WidgetsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    InitiateComponent
  ]
})
export class InitiateModule { }
