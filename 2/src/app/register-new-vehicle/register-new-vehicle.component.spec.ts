import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterNewVehicleComponent } from './register-new-vehicle.component';

describe('RegisterNewVehicleComponent', () => {
  let component: RegisterNewVehicleComponent;
  let fixture: ComponentFixture<RegisterNewVehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterNewVehicleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterNewVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
