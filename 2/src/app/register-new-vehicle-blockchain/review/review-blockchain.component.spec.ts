import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewBlockchainComponent } from './review-blockchain.component';

describe('ReviewComponent', () => {
  let component: ReviewBlockchainComponent;
  let fixture: ComponentFixture<ReviewBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
