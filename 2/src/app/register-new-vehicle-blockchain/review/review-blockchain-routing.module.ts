import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewBlockchainComponent } from './review-blockchain.component';

const routes: Routes = [{ path: '', component: ReviewBlockchainComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewBlockchainRoutingModule { }
