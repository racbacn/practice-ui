import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewBlockchainRoutingModule } from './review-blockchain-routing.module';
import { ReviewBlockchainComponent } from './review-blockchain.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    ReviewBlockchainComponent
  ],
  imports: [
    CommonModule,
    ReviewBlockchainRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatExpansionModule,
    MatDividerModule,
    MatCheckboxModule,
    MatButtonModule,
    FlexLayoutModule,
    WidgetsModule,
    MatIconModule,
  ],
  exports: [
    ReviewBlockchainComponent
  ]
})
export class ReviewBlockchainModule { }
