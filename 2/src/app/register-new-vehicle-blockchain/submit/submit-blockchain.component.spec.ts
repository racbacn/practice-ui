import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitBlockchainComponent } from './submit-blockchain.component';

describe('SubmitComponent', () => {
  let component: SubmitBlockchainComponent;
  let fixture: ComponentFixture<SubmitBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmitBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
