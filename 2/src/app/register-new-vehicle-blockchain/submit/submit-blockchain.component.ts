import {Component, OnInit} from '@angular/core';
import {VehicleResponse} from "../../interface/vehicle-response";
import {VehicleResponseService} from "../../services/vehicle-response.service";

@Component({
    selector: 'app-submit-blockchain',
    templateUrl: './submit-blockchain.component.html',
    styleUrls: ['./submit-blockchain.component.scss']
})
export class SubmitBlockchainComponent implements OnInit {
    vehicleResponse!: VehicleResponse;

    constructor(private vehicleResponseService: VehicleResponseService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.vehicleResponseService.getVehicleResponse().subscribe(data => {
            console.log(data)
            this.vehicleResponse = data;
        })
    }

}
