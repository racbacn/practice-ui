import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmitBlockchainRoutingModule } from './submit-blockchain-routing.module';
import { SubmitBlockchainComponent } from './submit-blockchain.component';


@NgModule({
  declarations: [
    SubmitBlockchainComponent
  ],
  imports: [
    CommonModule,
    SubmitBlockchainRoutingModule
  ],
  exports: [
    SubmitBlockchainComponent
  ]
})
export class SubmitBlockchainModule { }
