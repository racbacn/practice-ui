import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubmitBlockchainComponent } from './submit-blockchain.component';

const routes: Routes = [{ path: '', component: SubmitBlockchainComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubmitBlockchainRoutingModule { }
