import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillFeedbackBlockchainComponent } from './fill-feedback-blockchain.component';

describe('FillFeedbackBlockchainComponent', () => {
  let component: FillFeedbackBlockchainComponent;
  let fixture: ComponentFixture<FillFeedbackBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillFeedbackBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillFeedbackBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
