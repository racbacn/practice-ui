import {Component, OnInit} from '@angular/core';
import {SessionUtil} from "../../../core/util/sessionUtil";

@Component({
    selector: 'app-feedback-blockchain',
    templateUrl: './fill-feedback-blockchain.component.html',
    styleUrls: ['./fill-feedback-blockchain.component.scss']
})
export class FillFeedbackBlockchainComponent implements OnInit {

    feedBack!: string;

    constructor() {
    }

    ngOnInit(): void {
        let comment: string | null = SessionUtil.getSessionStorage(SessionUtil.COMMENT)

        if (comment) {
            this.feedBack = comment;
        }
    }

}
