import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillBlockchainComponent } from './fill-blockchain.component';

describe('FillBlockchainComponent', () => {
  let component: FillBlockchainComponent;
  let fixture: ComponentFixture<FillBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
