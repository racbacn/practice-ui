import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FillBlockchainComponent } from './fill-blockchain.component';

const routes: Routes = [
  { path: '', component: FillBlockchainComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FillBlockchainRoutingModule { }
