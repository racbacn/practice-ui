import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ROAD_TAX_MONTH_LIST} from "../../../constants/app.contants";
import {VehicleRequest} from "../../../interface/vehicle-request";
import {RoadTaxDetails} from "../../../interface/road-tax-details";
import {VehicleRequestService} from "../../../services/vehicle-request.service";
import {TransactionService} from "../../../services/transaction.service";
import {FillFormValidationsService} from "../../../services/fill-form-validations.service";


@Component({
    selector: 'app-fill-new-registration-and-road-tax-fees-blockchain',
    templateUrl: './fill-new-registration-and-road-tax-fees-blockchain.component.html',
    styleUrls: ['./fill-new-registration-and-road-tax-fees-blockchain.component.scss']
})
export class FillNewRegistrationAndRoadTaxFeesBlockchainComponent implements OnInit {
    roadTaxFormGroup!: FormGroup;
    roadTaxMonthList: Array<string> = ROAD_TAX_MONTH_LIST;
    roadtaxMonth!: string;

    vehicleRequest!: VehicleRequest;
    roadTaxDetails!: RoadTaxDetails;
    tcoeList: any;
    refundAmount!: string;
    isNormalReg!: boolean;

    constructor(private formBuilder: FormBuilder,
                private vehicleRequestService: VehicleRequestService,
                private transactionService: TransactionService,
                private fillFormValidationsService: FillFormValidationsService) {

    }

    ngOnInit() {
        this.initData();
        this.initForm();
        this.formValueChange();
    }


    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe(data => {
            this.vehicleRequest = data;
            this.roadTaxDetails = this.vehicleRequest.roadTaxDetails;
        })
    }

    initForm() {
        this.roadTaxFormGroup = this.formBuilder.group({
            openMarketValue: [this.roadTaxDetails?.openMarketValue, Validators.required],
            roadTaxMonths: [this.roadTaxDetails?.roadTaxMonths, Validators.required],
        })
    }


    formValueChange() {
        this.roadTaxFormGroup.valueChanges.subscribe(data => {
            let roadTaxDetailsFormValue: any = data;

            this.vehicleRequest.roadTaxDetails.openMarketValue = roadTaxDetailsFormValue.openMarketValue;
            this.vehicleRequest.roadTaxDetails.roadTaxMonths = roadTaxDetailsFormValue.roadTaxMonths;

            this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);
            console.log("formcahnge", this.vehicleRequest)

            this.fillFormValidationsService.setRoadTaxDetailsFormGroup(this.roadTaxFormGroup);
        })
    }
}
