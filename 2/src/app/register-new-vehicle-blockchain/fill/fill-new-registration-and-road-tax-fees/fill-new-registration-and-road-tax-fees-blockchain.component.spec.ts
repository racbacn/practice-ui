import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillNewRegistrationAndRoadTaxFeesBlockchainComponent } from './fill-new-registration-and-road-tax-fees-blockchain.component';

describe('FillNewRegistrationAndRoadTaxFeesBlockchainComponent', () => {
  let component: FillNewRegistrationAndRoadTaxFeesBlockchainComponent;
  let fixture: ComponentFixture<FillNewRegistrationAndRoadTaxFeesBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillNewRegistrationAndRoadTaxFeesBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillNewRegistrationAndRoadTaxFeesBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
