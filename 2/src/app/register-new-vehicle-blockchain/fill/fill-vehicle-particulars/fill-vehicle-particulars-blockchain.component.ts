import {Component, OnInit} from '@angular/core';
import {VEHICLE_ATTACHMENT_LIST} from '../../../constants/app.contants';
import {VehicleDetails} from '../../../interface/vehicle-details';
import {VehicleRequestService} from "../../../services/vehicle-request.service";
import {VehicleRequest} from "../../../interface/vehicle-request";
import {Attachment} from "../../../interface/attachment";

@Component({
    selector: 'app-fill-vehicle-particulars-blockchain',
    templateUrl: './fill-vehicle-particulars-blockchain.component.html',
    styleUrls: ['./fill-vehicle-particulars-blockchain.component.scss']
})
export class FillVehicleParticularsBlockchainComponent implements OnInit {
    vehicleAttachmentList: Array<Attachment> = VEHICLE_ATTACHMENT_LIST;

    vehicleRequest!: VehicleRequest
    vehicleDetails!: VehicleDetails;

    constructor(private vehicleRequestService: VehicleRequestService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe((data: VehicleRequest) => {
            this.vehicleRequest = data;

            this.vehicleDetails = this.vehicleRequest.vehicleDetails;
        })
    }

    filterVehicleNameById(id?: number) {
        return this.vehicleAttachmentList.filter(attachment => {
            return attachment.id === id;
        })[0]?.name

        return '';
    }

}
