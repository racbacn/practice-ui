import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Expansion} from "../../interface/expansion";
import {VehicleRequest} from "../../interface/vehicle-request";
import {VehicleRequestService} from "../../services/vehicle-request.service";
import {FILL_EXPANSION_PANEL_LIST_BLOCKCHAIN} from "../../constants/app.contants";
import {FillFormValidationsService} from "../../services/fill-form-validations.service";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'app-fill-blockchain',
    templateUrl: './fill-blockchain.component.html',
    styleUrls: ['./fill-blockchain.component.scss']
})

export class FillBlockchainComponent implements OnInit {
    @Output() onNextStepper: EventEmitter<any> = new EventEmitter();
    @Output() onPreviousStepper: EventEmitter<any> = new EventEmitter();

    nextButtonConfig = {
        buttonText: 'Next',
        buttonClassName: 'purple-button',
        buttonFont: 'button-font-16',
        hasIcon: true,
        iconName: 'arrow_right_alt',
        buttonWidth: 'button-70',
    };

    saveButtonConfig = {
        buttonText: 'Save as Draft',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    backButtonConfig = {
        buttonText: 'Back',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    items: Array<Expansion> = FILL_EXPANSION_PANEL_LIST_BLOCKCHAIN;

    vehicleRequest!: VehicleRequest;
    vehicleId!: string;

    isValidRoadTaxDetails!: boolean;
    isErrorHidden: boolean = true;

    constructor(private vehicleRequestService: VehicleRequestService,
                private fillFormValidationsService: FillFormValidationsService) {
    }

    ngOnInit(): void {
        this.fillFormValidationsService.getRoadTaxDetailsFormGroup().subscribe(data => {
            let roadTaxDetailsFormGroup: FormGroup = data;

            roadTaxDetailsFormGroup.updateValueAndValidity({onlySelf: false, emitEvent: true})

            this.isValidRoadTaxDetails = roadTaxDetailsFormGroup.valid;
        })
    }

    onSave() {
        let userResponse: UserResponse =  JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

        this.vehicleRequest.requestHeader = {
            requestAppId: 'VRLS_Vehicle',
            userId: userResponse.userId,
            userTypeKey: userResponse.userTypeKey
        }

        this.vehicleRequest.vehicleDetails.vehicleStatus = "DRAFT"

        console.log("save params", this.vehicleRequest)
        this.vehicleRequestService.saveDraftVehicle(this.vehicleRequest).subscribe(data => {
            console.log(data)
        }, error => {
            console.log(error.message);
        });
    }

    onBack() {
        this.onPreviousStepper.emit(true);
    }

    onNext() {
        if (this.isValidRoadTaxDetails) {
            this.onNextStepper.emit(true);
        } else {
            this.isErrorHidden = false;
        }
    }
}
