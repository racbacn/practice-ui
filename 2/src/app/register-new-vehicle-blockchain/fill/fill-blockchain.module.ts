import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FillBlockchainRoutingModule} from './fill-blockchain-routing.module';
import {FillBlockchainComponent} from './fill-blockchain.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SubmitBlockchainModule} from "../submit/submit-blockchain.module";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {FillOwnerParticularsBlockchainComponent} from "./fill-owner-particulars/fill-owner-particulars-blockchain.component";
import {FillVehicleParticularsBlockchainComponent} from "./fill-vehicle-particulars/fill-vehicle-particulars-blockchain.component";
import {FillNewRegistrationAndRoadTaxFeesBlockchainComponent} from "./fill-new-registration-and-road-tax-fees/fill-new-registration-and-road-tax-fees-blockchain.component";
import { MaterialModule } from 'src/app/material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatRadioModule} from "@angular/material/radio";
import {FillFeedbackBlockchainComponent} from "./fill-feedback/fill-feedback-blockchain.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { DireciveModule } from 'src/app/directive/directives.module';


@NgModule({
    declarations: [
        FillBlockchainComponent,
        FillOwnerParticularsBlockchainComponent,
        FillVehicleParticularsBlockchainComponent,
        FillNewRegistrationAndRoadTaxFeesBlockchainComponent,
        FillFeedbackBlockchainComponent
    ],
    imports: [
        CommonModule,
        FillBlockchainRoutingModule,
        ReactiveFormsModule,
        SubmitBlockchainModule,
        MatButtonToggleModule,
        MaterialModule,
        WidgetsModule,
        FlexLayoutModule,
        FormsModule,
        MatCheckboxModule,
        MatRadioModule,
        MatProgressSpinnerModule,
        DireciveModule
    ],
    exports: [
        FillBlockchainComponent
    ]
})
export class FillBlockchainModule {
}
