import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillOwnerParticularsBlockchainComponent } from './fill-owner-particulars-blockchain.component';

describe('FillOwnerParticularsBlockchainComponent', () => {
  let component: FillOwnerParticularsBlockchainComponent;
  let fixture: ComponentFixture<FillOwnerParticularsBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillOwnerParticularsBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillOwnerParticularsBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
