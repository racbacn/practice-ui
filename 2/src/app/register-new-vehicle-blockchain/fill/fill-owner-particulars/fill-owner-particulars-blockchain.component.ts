import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {OwnerDetails} from '../../../interface/owner-details';
import {VehicleRequestService} from "../../../services/vehicle-request.service";
import {VehicleRequest} from "../../../interface/vehicle-request";

@Component({
    selector: 'app-fill-owner-particulars',
    templateUrl: './fill-owner-particulars-blockchain.component.html',
    styleUrls: ['./fill-owner-particulars-blockchain.component.scss']
})
export class FillOwnerParticularsBlockchainComponent implements OnInit {
    ownerDetails!: OwnerDetails;

    constructor(private changeDetectorRef: ChangeDetectorRef, private vehicleRequestService: VehicleRequestService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe((data: VehicleRequest) => {
            let vehicleDetails = data;

            this.ownerDetails = vehicleDetails.ownerDetails
        })
    }
}
