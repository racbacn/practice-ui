import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {REGISTER_NEW_VEHICLE} from '../../app/constants/app.contants';
import {MatStepper} from '@angular/material/stepper';

@Component({
    selector: 'app-register-new-vehicle-blockchain',
    templateUrl: './register-new-vehicle-blockchain.component.html',
    styleUrls: ['./register-new-vehicle-blockchain.component.scss'],
    providers: [
        {
            provide: STEPPER_GLOBAL_OPTIONS,
            useValue: {showError: true, displayDefaultIndicatorType: false},
        },
    ],
})
export class RegisterNewVehicleBlockChainComponent implements OnInit, AfterViewInit, OnDestroy {
    registerHeading: string = REGISTER_NEW_VEHICLE;

    isLinear = false;
    @ViewChild('stepper')
    stepper!: MatStepper;

    constructor() {
    }


    ngAfterViewInit(): void {
        //throw new ErrorResponse('Method not implemented.');
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    allowToProceed(a: boolean) {
        if (a) {
            this.stepper.next();
        } else {
            console.log('wrong opp');
        }
    }

    onNext(isAllowed: boolean) {
        if (isAllowed) {
            this.stepper.next();
        } else {
            console.log('wrong opp');
        }
    }

    onSubmit(isAllowed: boolean) {
        if (isAllowed) {
            this.stepper.next();
        } else {
            console.log('wrong opp');
        }
    }

    onBack() {
        this.stepper.previous();
    }

}
