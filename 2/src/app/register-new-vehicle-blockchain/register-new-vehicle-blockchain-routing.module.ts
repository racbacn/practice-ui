import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterNewVehicleBlockChainComponent} from './register-new-vehicle-blockchain.component';

const routes: Routes = [{
    path: '', component: RegisterNewVehicleBlockChainComponent,
    // children: [
    //     { path: 'initiate', loadChildren: () => import('../register-new-vehicle/initiate/initiate.module').then(m => m.InitiateBlockchainModule) },
    //     {path: 'fill', loadChildren: () => import('../register-new-vehicle/fill/fill.module').then(m => m.FillBlockchainModule)},
    //     { path: 'review', loadChildren: () => import('../register-new-vehicle/review/review.module').then(m => m.ReviewBlockchainModule) },
    //     { path: 'submit', loadChildren: () => import('../register-new-vehicle/submit/submit.module').then(m => m.SubmitBlockchainModule) }
    // ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegisterNewVehicleBlockChainRoutingModule {
}
