import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterNewVehicleBlockChainRoutingModule} from './register-new-vehicle-blockchain-routing.module';
import {CoreModule, FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../material.module';
import {FillBlockchainModule} from './fill/fill-blockchain.module';
import {InitiateBlockchainModule} from './initiate/initiate-blockchain.module';
import {SubmitBlockchainModule} from './submit/submit-blockchain.module';
import {ReviewBlockchainModule} from './review/review-blockchain.module';
import {RegisterNewVehicleBlockChainComponent} from "./register-new-vehicle-blockchain.component";
import { WidgetsModule } from '../widgets/widgets.module';

@NgModule({
    declarations: [
        RegisterNewVehicleBlockChainComponent
    ],
    imports: [
        CommonModule,
        RegisterNewVehicleBlockChainRoutingModule,
        FlexLayoutModule,
        MaterialModule,
        FillBlockchainModule,
        InitiateBlockchainModule,
        CoreModule,
        SubmitBlockchainModule,
        ReviewBlockchainModule,
        WidgetsModule
    ],
    providers: [
    ]
})
export class RegisterNewVehicleBlockChainModule {
}
