import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InitiateBlockchainComponent } from './initiate-blockchain.component';

describe('InitiateBlockchainComponent', () => {
  let component: InitiateBlockchainComponent;
  let fixture: ComponentFixture<InitiateBlockchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InitiateBlockchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitiateBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
