import {Component, EventEmitter, OnInit, Output,} from '@angular/core';
import {FormBuilder} from '@angular/forms';

import {VEHICLE_ATTACHMENT_LIST} from '../../constants/app.contants';
import {VehicleRequestService} from '../../services/vehicle-request.service';
import {VehicleRequest} from '../../interface/vehicle-request';
import {OwnerDetails} from '../../interface/owner-details';
import {BlockchainService} from "../../services/blockchain.service";
import {BlockChainDetails} from "../../interface/block-chain-details";
import {Attachment} from "../../interface/attachment";
import * as moment from "moment/moment";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";

@Component({
    selector: 'app-initiate',
    templateUrl: './initiate-blockchain.component.html',
    styleUrls: ['./initiate-blockchain.component.scss'],
})
export class InitiateBlockchainComponent implements OnInit {
    vehicleRequest!: VehicleRequest;
    ownerDetails!: OwnerDetails;

    vehicleAttachmentList: Array<Attachment> = VEHICLE_ATTACHMENT_LIST;

    @Output() allowNext: EventEmitter<any> = new EventEmitter();

    idType: any;

    proceedButtonConfig = {
        buttonText: 'Proceed',
        buttonClassName: 'purple-button',
        buttonFont: 'button-font-16',
        hasIcon: true,
        iconName: 'arrow_right_alt',
        buttonWidth: 'button-70',
    };

    backButtonConfig = {
        buttonText: 'Back',
        buttonClassName: 'grey-button',
        buttonFont: 'button-font-16',
        hasIcon: false,
        buttonWidth: 'button-70',
    };

    constructor(private fb: FormBuilder,
                private vehicleRequestService: VehicleRequestService,
                private blockchainService: BlockchainService) {

    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.vehicleRequestService.getVehicleRequest().subscribe(data => {
            this.vehicleRequest = data;
            this.ownerDetails = this.vehicleRequest.ownerDetails
        })

        let salesIdAgreement: string = 'DAS100000017';

        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

        this.blockchainService.getBlockchainDetailsBySalesIdAgreement(salesIdAgreement).subscribe(data => {
            let blockChainDetails: BlockChainDetails = data;

            this.ownerDetails.ownerIdType = "1";
            this.ownerDetails.ownerIdTypeDesc = 'Singapore NRIC';
            this.ownerDetails.ownerUserId = userResponse.userId;
            this.ownerDetails.ownerId = blockChainDetails?.owner?.nric;
            this.ownerDetails.ownerName = blockChainDetails?.owner?.name;
            this.ownerDetails.salesAgreementId = salesIdAgreement;

            this.vehicleRequest.ownerDetails = this.ownerDetails;

            this.vehicleRequest.vehicleDetails.vitasApprovalCode = blockChainDetails?.vehicle?.vitasApprovalCode;
            this.vehicleRequest.vehicleDetails.chassisNumber = blockChainDetails?.vehicle?.chassisNo;
            this.vehicleRequest.vehicleDetails.engineNumber = blockChainDetails?.vehicle?.engineNo;
            this.vehicleRequest.vehicleDetails.motorNumber = blockChainDetails?.vehicle?.motorNo;
            this.vehicleRequest.vehicleDetails.firstRegistrationDate = moment(blockChainDetails?.vehicle?.firstRegDate).format('DD/MM/YYYY');
            this.vehicleRequest.vehicleDetails.originalRegistrationDate = moment(blockChainDetails?.vehicle?.originalRegDate).format('DD/MM/YYYY');
            this.vehicleRequest.vehicleDetails.primaryColour = blockChainDetails?.vehicle?.primaryColour;
            this.vehicleRequest.vehicleDetails.secondaryColour = blockChainDetails?.vehicle?.secondaryColour;
            this.vehicleRequest.vehicleDetails.vehicleType = blockChainDetails?.vehicle?.vehicleType;
            this.vehicleRequest.vehicleDetails.vehicleScheme = blockChainDetails?.vehicle?.vehicleScheme;
            this.vehicleRequest.vehicleDetails.attachmentOne = this.filterVehicleIdByName(blockChainDetails?.vehicle?.attachments);
            this.vehicleRequest.vehicleDetails.attachmentTwo = this.filterVehicleIdByName(blockChainDetails?.vehicle?.attachment2);
            this.vehicleRequest.vehicleDetails.attachmentThree = this.filterVehicleIdByName(blockChainDetails?.vehicle?.attachment3);
            this.vehicleRequest.vehicleDetails.vehicleMake = blockChainDetails?.vehicle?.vehicleMake;
            this.vehicleRequest.vehicleDetails.vehicleModel = blockChainDetails?.vehicle?.vehicleModel;
            this.vehicleRequest.vehicleDetails.propellant = "4";
            this.vehicleRequest.vehicleDetails.engineCapacity = blockChainDetails?.vehicle?.engineCapacity;
            // this.vehicleRequest.vehicleDetails.powerRating = blockChainDetails?.vehicle?.powerRating?.toString();
            this.vehicleRequest.vehicleDetails.maximumPowerOutput = blockChainDetails?.vehicle?.maxPowerOP + " KW/ " + blockChainDetails?.vehicle?.maxPowerOutputBHP + "BHP";
            this.vehicleRequest.vehicleDetails.numberOfAxles = blockChainDetails?.vehicle?.axles?.toString();
            this.vehicleRequest.vehicleDetails.tyreInformation = "Front: " + blockChainDetails?.vehicle?.tyreInfo + " Rear: " + blockChainDetails?.vehicle?.rearTyreInfo;
            this.vehicleRequest.vehicleDetails.unladenWeight = blockChainDetails?.vehicle?.unladenWeight;
            this.vehicleRequest.vehicleDetails.maximumUnladenWeight = blockChainDetails?.vehicle?.unladenWeight;
            this.vehicleRequest.vehicleDetails.emissionStandardCode = blockChainDetails?.vehicle?.emissionStdCode;
            this.vehicleRequest.vehicleDetails.carbonDioxideEmission = blockChainDetails?.vehicle?.co2Em;
            this.vehicleRequest.vehicleDetails.carbonMonoxideEmission = blockChainDetails?.vehicle?.coEm;
            this.vehicleRequest.vehicleDetails.hydroCarbonEmission = blockChainDetails?.vehicle?.hcEm;
            // this.vehicleRequest.vehicleDetails.nitrousOxideEmission = blockChainDetails?.vehicle?.noXEmission;
            this.vehicleRequest.vehicleDetails.pmEmission = blockChainDetails?.vehicle?.pmEm;


            this.vehicleRequest.roadTaxDetails.insuranceCompany = blockChainDetails?.vehicle?.insuranceCo;
            this.vehicleRequest.roadTaxDetails.insuranceCoverNoteNumber = blockChainDetails?.vehicle?.insuranceCoverNoteNo;
            this.vehicleRequest.roadTaxDetails.coeRebateNumber = blockChainDetails?.vehicle?.cOENo;
            // this.vehicleRequest.roadTaxDetails.vehicleNumberAvailable = blockChainDetails?.vehicle?.vehicleNo;

            this.vehicleRequestService.setVehicleRequest(this.vehicleRequest);
        })

    }

    proceedToFill(): void {
        this.allowNext.emit(true);
    }

    filterVehicleIdByName(name: string) {
        return this.vehicleAttachmentList.filter(attachment => {
            return attachment.name === name;
        })[0].id
    }

}
