import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitiateBlockchainComponent } from './initiate-blockchain.component';

const routes: Routes = [
  //{ path: '', component: InitiateBlockchainComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateBlockchainRoutingModule { }
