import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitiateBlockchainRoutingModule } from './initiate-blockchain-routing.module';
import { InitiateBlockchainComponent } from './initiate-blockchain.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    InitiateBlockchainComponent
  ],
  imports: [
    CommonModule,
    InitiateBlockchainRoutingModule,
    WidgetsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    InitiateBlockchainComponent
  ]
})
export class InitiateBlockchainModule { }
