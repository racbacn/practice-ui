import { Directive, ElementRef, forwardRef, HostListener, Input, Renderer2, StaticProvider } from '@angular/core';

@Directive({
    selector: '[numberOnly]',

})
export class RestrictInputDirective  {

constructor(private _el: ElementRef){}
@HostListener('input', ['$event']) onInputChange(event: any) {
    const initalValue = this._el.nativeElement.value;
    this._el.nativeElement.value = initalValue.replace(/[^0-9]*/g, '');
    if ( initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }


}