import { NgModule } from '@angular/core';
import { RestrictInputDirective } from './restrict-input.directive';

@NgModule({
  declarations: [
    RestrictInputDirective
  ],
  imports: [
    
  ],

  exports: [
    RestrictInputDirective
  ],

})
export class DireciveModule { }
