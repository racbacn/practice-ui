import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceiptPageComponent } from './receipt-page.component';
import { SuccessPaymentComponent } from '../success-payment/success-payment.component';

const routes: Routes = 
[
  { path: '', component: ReceiptPageComponent, children: [
    // { path: '/success', component: SuccessPaymentComponent}
  ]},
// { path: 'success', loadChildren: () => import('../success-payment/success-payment.module').then(m => m.SuccessPaymentModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptPageRoutingModule { }
