import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from 'src/app/material.module'
import { ReceiptPageRoutingModule } from './receipt-page-routing.module';
import { ReceiptPageComponent } from './receipt-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { WidgetsModule } from '../widgets/widgets.module';
import { SuccessPaymentModule } from '../success-payment/success-payment.module';

@NgModule({
  declarations: [
    ReceiptPageComponent
  ],
  imports: [
    CommonModule,
    ReceiptPageRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatExpansionModule,
    MatDividerModule,
    MatCheckboxModule,
    MatButtonModule,
    FlexLayoutModule,
    MaterialModule,
    MatIconModule,
    WidgetsModule,
    SuccessPaymentModule
  ]
})
export class ReceiptPageModule { }
