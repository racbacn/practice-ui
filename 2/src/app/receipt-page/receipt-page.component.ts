import {Component, OnInit} from "@angular/core";
import {PaymentServiceService} from "../services/payment-service.service"
import {Router} from "@angular/router";
import {DatePipe} from '@angular/common';
import {makePaymentParam, paymentType, USER_TYPE} from '../constants/app.contants';
import {ActivatedRoute} from "@angular/router";
import {VehicleRequestService} from "../services/vehicle-request.service";
import {SessionUtil} from "../core/util/sessionUtil";
import {OwnerDetails} from "../interface/owner-details";


@Component({
    selector: "app-receipt-page",
    templateUrl: "./receipt-page.component.html",
    styleUrls: ["./receipt-page.component.scss"],
})
export class ReceiptPageComponent implements OnInit {
  paymentDetails!: any;
  transactionID!: any;
  backButtonConfig = {
    buttonText: "Back",
    buttonClassName: "grey-button",
    buttonFont: "button-font-16",
    hasIcon: false,
    buttonWidth: "button-70",
  };
  rebateAmount!: any;
  regAmount!: any;
  roadTaxAmount!: any;
  transactionDate!: any;
  grandTotal!: number;
  ownerDetails!: OwnerDetails;
  transactionRefNum!: string | null;


    constructor(
        private paymentService: PaymentServiceService,
        private route: Router,
        private datePipe: DatePipe,
        private activatedRoute: ActivatedRoute,
        private vehicleRequestService: VehicleRequestService
    ) {
    }

    ngOnInit(): void {
        this.transactionID = SessionUtil.getSessionStorage(SessionUtil.TRANSACTION_ID);
        this.transactionRefNum = SessionUtil.getSessionStorage(SessionUtil.TRANSACTION_REF_NUM);
        console.log("transaction ID", this.transactionID)
        this.getPaymentDetails();
        this.getVehicleDetails()
    }

    getPaymentDetails() {

        this.paymentService.getPaymentDetails(this.transactionID).subscribe(res => {
            this.paymentDetails = res;
            this.rebateAmount = this.paymentDetails.RebateAmount;
            this.regAmount = this.paymentDetails.RegistrationAmount;
            this.roadTaxAmount = this.paymentDetails.RoadTaxAmount;
            this.grandTotal = this.paymentDetails.GrandTotal;
            let date = this.paymentDetails.responseHeader.responseDateTime;
            this.transactionDate = this.datePipe.transform(date, 'MMM d, y, h:mm:ss a');
            console.log("details", res)
        })
    }

    getVehicleDetails() {
        this.vehicleRequestService.getVehicleDetailsByTransactionID(this.transactionID)
            .subscribe(res => {
                this.ownerDetails = res.ownerDetails;
            });
    }

    payNow() {
        let reqDate = new Date();
        console.log("date", reqDate)
        let params = {
            "requestHeader": {
                "requestAppId": makePaymentParam,
                "requestDateTime": reqDate,
                "userId": USER_TYPE.VEHICLE_OWNER
            },
            "transactionId": this.transactionID,
            "paymentType": paymentType.registration,
            "paymentAmt": Math.round(this.grandTotal),
            "transactionRefNum": this.transactionRefNum,
            "paymentMethod": "TestPayment"
        }
        this.paymentService.payNow(params).subscribe(res => {
            if (res.paymentSuccess === true) {
                let params = {
                    date: res.responseHeader.responseDateTime,
                    amount: res.paymentAmt
                }
                SessionUtil.setSessionStorage(SessionUtil.PAYMENT, JSON.stringify(params));
                this.route.navigate(['success'])
            }
            console.log(res)
        })
    }
}
