import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './core/components/login/login.component';
import {SuccessPaymentComponent} from './success-payment/success-payment.component';
import {AuthGuard} from "./services/auth.guard";
import {TokenResolver} from "./services/token-resolver.service";
import {ErrorComponent} from "./widgets/error/error.component";

const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
        resolve: {
            access: TokenResolver
        }
    },
    {
        path: 'callback',
        redirectTo: ''
    },
    {path: "error", component: ErrorComponent},
    {
        path: 'dealer-dashboard',
        loadChildren: () =>
            import('./dealer-dashboard/dealer-dashboard.module').then(
                (m) => m.DealerDashboardModule
            ),
        canActivate: [AuthGuard]
    },
    {
        path: 'register-new-vehicle',
        loadChildren: () => import('./register-new-vehicle/register-new-vehicle.module').then(m => m.RegisterNewVehicleModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'register-new-vehicle-blockchain',
        loadChildren: () => import('./register-new-vehicle-blockchain/register-new-vehicle-blockchain.module').then(m => m.RegisterNewVehicleBlockChainModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'payment',
        loadChildren: () => import('./receipt-page/receipt-page.module').then(m => m.ReceiptPageModule),
        canActivate: [AuthGuard]
    },

    {path: 'success', component: SuccessPaymentComponent, canActivate: [AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
