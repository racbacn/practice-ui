import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/services/dashboard.service';
import { TransactionIdService } from 'src/app/services/transaction-id.service';
import * as moment from "moment";
import { officerUserId, USER_TYPES } from 'src/app/shared/constants/application.constants';
import { Transactions } from 'src/app/interface/transactions';
import { TransactionDueDates } from 'src/app/interface/due-dates';
import {SessionUtil} from "../../core/util/sessionUtil";

@Component({
  selector: 'app-dashboard-vqrofficer',
  templateUrl: './dashboard-vqrofficer.component.html',
  styleUrls: ['./dashboard-vqrofficer.component.scss']
})
export class DashboardVqrofficerComponent implements OnInit {
  mobileMargin: boolean = true;
  url = "review";
  filterVal!: string;
  pageNo = 0;
  pageSize!: any;
  transactions!: Transactions[];
  userType!: string;
  dueDates!: TransactionDueDates;
  disableNext!: boolean;
  vehicleTypeKey!: number;
  vehicleScheme!: number;

  constructor(private dashBoardService: DashboardService, private router: Router, private transactionIdService: TransactionIdService) { }

  ngOnInit(): void {
    this.getDetails();
    this.userType = SessionUtil.getSessionStorage(SessionUtil.USER_TYPE)!;
    console.log(this.userType)
    this.getDueDates();
  }

  getDetails() {
    let date = new Date();
    let params = {
      dueDate: date.toISOString,
      assignedUserId: officerUserId,
      statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
      pageNo: this.pageNo,
      pageSize: this.pageSize === undefined ? "" : this.pageSize
    }
    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  filterList(event: any) {
    let date: any;
    let vehicleScheme: any;
    let vehicleTypeKey: any;
    let params: any;
    switch (event) {
      case "today": {
        date = new Date();
      }
        break;
      case 'tomorrow': {
        let dateToday = new Date();
        date = new Date(dateToday.setDate(dateToday.getDate() + 1));
      }
        break;
      case 'nextweek': {
        let nextweek = new Date();
        date = new Date(nextweek.setDate(nextweek.getDate() + 7));
      }
        break;
      case 'vehiclescheme': {
        vehicleScheme = 2;
      }
        break;
      case 'pmdApproval': {
        vehicleTypeKey = 2;
      }
        break;
      case 'default': {
        this.getDetails();
      }
        break;
      default:

    }
    if ((vehicleScheme || vehicleTypeKey) === undefined) {
      params = {
        dueDateRange: moment(date).format("YYYY-MM-DD"),
        assignedUserId: officerUserId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    } else {
      params = {
        assignedUserId: officerUserId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  sortList(event: any) {
    let date = new Date();
    this.pageSize = event;
    let params = {
      dueDate: date.toISOString,
      assignedUserId: officerUserId,
      pageSize: this.pageSize,
      statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
      pageNo: this.pageNo === undefined ? "" : this.pageNo,
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  goToViewDetails(event: any) {
    let resp: any;
    for (let item of this.transactions) {
      if (item.transactionRefNum === event) {
        sessionStorage.setItem('transactionid', item.sourceTransactionId);
        sessionStorage.setItem('hideAprvRjct', 'dashboard');
        if (item.transactionType === "Register PMD") {
          this.url = "review-pmd";
        }
      }
    }
    console.log("fileNo", event)
  }

  getDueDates() {
    this.dashBoardService.getDueDates().subscribe(res => {
      this.dueDates = res;
      console.log(this.dueDates)
    })
  }

  getPageDetails(pageNumber: any) {
    let date = new Date();
    let params = {
      dueDate: date.toISOString,
      assignedUserId: officerUserId,
      statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
      pageNo: pageNumber,
      pageSize: this.pageSize
    }
    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      if (res.length === 0 || res === undefined) {
        return alert("No Data");
      } else {
        this.transactions = res;
      }
    })
  }

  onPageMove(pageNumber: number) {
    let date = new Date();

    let params = {
      dueDate: date.toISOString,
      assignedUserId: officerUserId,
      statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
      pageNo: pageNumber,
      pageSize: this.pageSize === undefined ? "" : this.pageSize,
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      if (res.length === 0 || res === undefined) {
        return alert("No Data");
      } else {
        this.transactions = res;
        this.pageNo = pageNumber;
      }
    }, (error) => {
      if (error?.error?.code === 500) {
        return alert(error.error.message);
      }
      alert('Cannot get transactions.');
    }
    )
  }
}
