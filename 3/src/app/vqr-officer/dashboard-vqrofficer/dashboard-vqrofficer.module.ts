import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardVqrofficerRoutingModule } from './dashboard-vqrofficer-routing.module';
import { DashboardVqrofficerComponent } from './dashboard-vqrofficer.component';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';


@NgModule({
    declarations: [
        DashboardVqrofficerComponent,
        // ReviewPmdVqrOfficerComponent
    ],
    imports: [
        CommonModule,
        DashboardVqrofficerRoutingModule,
        MaterialModule,
        SharedModule,
        WidgetsModule,
    ]
})
export class DashboardVqrofficerModule {
}
