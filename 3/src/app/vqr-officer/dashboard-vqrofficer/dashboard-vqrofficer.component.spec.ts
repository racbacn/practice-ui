import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVqrofficerComponent } from './dashboard-vqrofficer.component';

describe('DashboardVqrofficerComponent', () => {
  let component: DashboardVqrofficerComponent;
  let fixture: ComponentFixture<DashboardVqrofficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardVqrofficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVqrofficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
