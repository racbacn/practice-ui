import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardVqrofficerComponent } from './dashboard-vqrofficer.component';
import { ReviewVehicleVqrOfficerComponent } from "../review-vehicle-vqr-officer/review-vehicle-vqr-officer.component";
import { ReviewPmdVqrOfficerComponent } from '../review-pmd-vqr-officer/review-pmd-vqr-officer.component';

const routes: Routes = [
    {
        path: '', component: DashboardVqrofficerComponent, children: [
            { path: '', pathMatch: 'full', redirectTo: 'dashboard-vqrofficer' },
        ],
    },
    { path: 'review', component: ReviewVehicleVqrOfficerComponent },
    { path: 'review-pmd', component: ReviewPmdVqrOfficerComponent }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardVqrofficerRoutingModule {
}
