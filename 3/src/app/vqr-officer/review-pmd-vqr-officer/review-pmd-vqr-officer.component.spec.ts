import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPmdVqrOfficerComponent } from './review-pmd-vqr-officer.component';

describe('ReviewPmdVqrOfficerComponent', () => {
  let component: ReviewPmdVqrOfficerComponent;
  let fixture: ComponentFixture<ReviewPmdVqrOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewPmdVqrOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPmdVqrOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
