import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewPmdVqrOfficerRoutingModule } from './review-pmd-vqr-officer-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { ReviewPmdVqrOfficerComponent } from './review-pmd-vqr-officer.component';


@NgModule({
  declarations: [
    ReviewPmdVqrOfficerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    WidgetsModule,
    ReviewPmdVqrOfficerRoutingModule
  ]
})
export class ReviewPmdVqrOfficerModule { }
