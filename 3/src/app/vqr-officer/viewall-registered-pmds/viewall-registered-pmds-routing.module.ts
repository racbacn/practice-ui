import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewPmdVqrOfficerComponent } from '../review-pmd-vqr-officer/review-pmd-vqr-officer.component';
// import { ReviewPmdVehiclesComponent } from './review-pmd-vehicles/review-pmd-vehicles.component';
import { ViewallRegisteredPmdsComponent } from './viewall-registered-pmds.component';

const routes: Routes = 
[
{ path: '', component: ViewallRegisteredPmdsComponent },
// { path: 'review', component: ReviewPmdVehiclesComponent }
{
  path: 'review-pmd',
  component: ReviewPmdVqrOfficerComponent
}
]
                        ;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewallRegisteredPmdsRoutingModule { }
