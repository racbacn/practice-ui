import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewallRegisteredPmdsRoutingModule } from './viewall-registered-pmds-routing.module';
import { ViewallRegisteredPmdsComponent } from './viewall-registered-pmds.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { ReviewPmdVqrOfficerModule } from '../review-pmd-vqr-officer/review-pmd-vqr-officer.module';
 

@NgModule({
  declarations: [
    ViewallRegisteredPmdsComponent
  ],
  imports: [
    CommonModule,
    ViewallRegisteredPmdsRoutingModule,
    WidgetsModule,
    ReviewPmdVqrOfficerModule
  ]
})
export class ViewallRegisteredPmdsModule { }
