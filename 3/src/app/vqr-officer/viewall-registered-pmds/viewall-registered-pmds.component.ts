import {Component, OnInit} from '@angular/core';
import {RegisteredPMD} from 'src/app/interface/transactions';
import {PmdListService} from 'src/app/services/pmd-list.service';
import {TransactionIdService} from 'src/app/services/transaction-id.service';
import {officerUserId, USER_TYPES, VEHICLE_TYPE} from 'src/app/shared/constants/application.constants';
import * as moment from "moment";

@Component({
    selector: 'app-viewall-registered-pmds',
    templateUrl: './viewall-registered-pmds.component.html',
    styleUrls: ['./viewall-registered-pmds.component.scss']
})
export class ViewallRegisteredPmdsComponent implements OnInit {
    transactions!: RegisteredPMD[];
    userType!: string;
    url = "review-pmd"
    pageNo = 0;
    pageSize!: string;

    constructor(private transactionIdService: TransactionIdService, private pmdService: PmdListService) {
    }

    ngOnInit(): void {
        this.getPMDList();
        this.userType = "pmd"
    }

    getPMDList() {
        let params = {
            vehicleType: VEHICLE_TYPE.pmd
        }
        this.pmdService.getPmdListService(params).subscribe(data => {
            this.transactions = data;
        })
    }

    getSearchParams(event: any) {
        let params: any = {};

        if (event.fromUserId) {
            params['fromUserId'] = event.fromUserId;
        }
        if (event.transactionRefNum) {
            params['transactionRefNum'] = event.transactionRefNum;
        }
        if (event.regMarkId) {
            params['regMarkId'] = event.transactionRefNum;
        }
        if (event.actionDateTimeFrom) {
            params['actionDateTimeFrom'] = moment(event.actionDateTimeFrom).startOf("day").utcOffset(0, true).format();
        }
        if (event.actionDateTimeTo) {
            params['actionDateTimeTo'] = moment(event.actionDateTimeTo).endOf('day').utcOffset(0, true).format();
        }
        params['vehicleType'] = VEHICLE_TYPE.pmd

        this.pmdService.getSearchPmdList(params).subscribe(data => {
            this.transactions = data;
        })
    }

    goToViewDetails(event: any) {
        for (let item of this.transactions) {
            if (item.registrationNo === event) {
                sessionStorage.setItem('transactionid', item.sourceTransactionId);
                sessionStorage.setItem('hideAprvRjct', 'pmdVehicle');
            }
        }
    }

    sortList(event: any) {
        this.pageSize = event;
        let params = {
            pageSize: this.pageSize,
            vehicleType: VEHICLE_TYPE.pmd,
            pageNo: this.pageNo === undefined ? "" : this.pageNo,
        }

        this.pmdService.getPmdListService(params).subscribe(res => {
            this.transactions = res;

            console.log("details", this.transactions)
        })
    }

    onPageMove(pageNumber: number) {
        let params = {
            pageNo: pageNumber,
            pageSize: this.pageSize === undefined ? "" : this.pageSize,
            vehicleType: VEHICLE_TYPE.pmd
        }
        this.pmdService.getPmdListService(params).subscribe(res => {
                if (res.length === 0 || res === undefined) {
                    return alert("No Data");
                } else {
                    this.transactions = res;
                    this.pageNo = pageNumber;
                }
                console.log("pagination", this.transactions)
            }, (error) => {
                if (error?.error?.code === 500) {
                    return alert(error.error.message);
                }
                alert('Cannot get transactions.');
            }
        )
    }


}
