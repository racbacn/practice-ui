import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewallRegisteredPmdsComponent } from './viewall-registered-pmds.component';

describe('ViewallRegisteredPmdsComponent', () => {
  let component: ViewallRegisteredPmdsComponent;
  let fixture: ComponentFixture<ViewallRegisteredPmdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewallRegisteredPmdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewallRegisteredPmdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
