import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVehicleVqrOfficerComponent } from './review-vehicle-vqr-officer.component';

describe('ReviewVehicleVqrOfficerComponent', () => {
  let component: ReviewVehicleVqrOfficerComponent;
  let fixture: ComponentFixture<ReviewVehicleVqrOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewVehicleVqrOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVehicleVqrOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
