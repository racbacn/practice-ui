import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import {ReviewVehicleVqrOfficerComponent} from "./review-vehicle-vqr-officer.component";
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';

@NgModule({
    declarations: [
        ReviewVehicleVqrOfficerComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        FlexLayoutModule,
        WidgetsModule
    ]
})
export class ReviewVehicleVqrOfficerModule {
}
