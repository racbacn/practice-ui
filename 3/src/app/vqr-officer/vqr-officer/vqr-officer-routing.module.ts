import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VqrOfficerComponent } from './vqr-officer.component';

const routes: Routes = [
  { path: '', component: VqrOfficerComponent, children:[
    {path: '', pathMatch: 'full', redirectTo: 'dashboard-vqrofficer'},
    {
      path: 'dashboard-vqrofficer',
      loadChildren: () =>
        import(
          '../../vqr-officer/dashboard-vqrofficer/dashboard-vqrofficer.module'
        ).then((m) => m.DashboardVqrofficerModule),
    },
    {
      path: 'viewall-registered-vehicles',
      loadChildren: () =>
        import(
          '../../vqr-officer/viewall-registered-vehicles/viewall-registered-vehicles.module'
        ).then((m) => m.ViewallRegisteredVehiclesModule),
    },
    {
      path: 'viewall-registered-pmds',
      loadChildren: () =>
        import(
          '../../vqr-officer/viewall-registered-pmds/viewall-registered-pmds.module'
        ).then((m) => m.ViewallRegisteredPmdsModule),
    },
    {
      path: 'review-pmd',
      loadChildren: () =>
        import(
          '../review-pmd-vqr-officer/review-pmd-vqr-officer.module'
        ).then((m) => m.ReviewPmdVqrOfficerModule),
    },
    {
      path: 'review',
      loadChildren: () =>
        import(
          '../review-vehicle-vqr-officer/review-vehicle-vqr-officer.module'
        ).then((m) => m.ReviewVehicleVqrOfficerModule),
    }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VqrOfficerRoutingModule { }
