import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VqrOfficerComponent } from './vqr-officer.component';

describe('VqrOfficerComponent', () => {
  let component: VqrOfficerComponent;
  let fixture: ComponentFixture<VqrOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VqrOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VqrOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
