import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vqr-officer',
  templateUrl: './vqr-officer.component.html',
  styleUrls: ['./vqr-officer.component.scss']
})
export class VqrOfficerComponent implements OnInit {
  userdetails: Array<any> = [{name: 'Peter Tan', id: 'EID12345'}];

  sideNavNames: Array<any> = [
    {
    name: "DASHBOARD", icon: "home",
    link: "dashboard-vqrofficer"
  },
  {
    name: "VIEW ALL REGISTERED VEHICLES", icon: "home", link: "viewall-registered-vehicles"
  },
  {
    name: "VIEW ALL REGISTERED E-SCOOTER/PAB", icon: "home",
    link: "viewall-registered-pmds"
  },  
]

  constructor() { }

  ngOnInit(): void {
  }

}
