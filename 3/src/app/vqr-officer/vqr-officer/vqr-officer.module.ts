import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VqrOfficerRoutingModule} from './vqr-officer-routing.module';
import {VqrOfficerComponent} from './vqr-officer.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {WidgetsModule} from 'src/app/widgets/widgets.module';
import {MaterialModule} from 'src/app/material.module';
import {CoreModule} from 'src/app/core/core.module';


@NgModule({
  declarations: [
    VqrOfficerComponent
  ],
  imports: [
    CommonModule,
    VqrOfficerRoutingModule,
    SharedModule,
    WidgetsModule,
    MaterialModule,
    CoreModule
  ]
})
export class VqrOfficerModule { }
