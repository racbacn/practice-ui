import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewVehicleVqrOfficerComponent } from '../review-vehicle-vqr-officer/review-vehicle-vqr-officer.component';
import { ViewallRegisteredVehiclesComponent } from './viewall-registered-vehicles.component';

const routes: Routes = [{ path: '', component: ViewallRegisteredVehiclesComponent },
{
  path: 'review',
  component: ReviewVehicleVqrOfficerComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewallRegisteredVehiclesRoutingModule { }
