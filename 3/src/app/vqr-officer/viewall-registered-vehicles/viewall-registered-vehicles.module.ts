import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatExpansionModule } from '@angular/material/expansion';
import { FlexLayoutModule } from '@angular/flex-layout';
import {WidgetsModule} from 'src/app/widgets/widgets.module';

import { ViewallRegisteredVehiclesRoutingModule } from './viewall-registered-vehicles-routing.module';
import { ViewallRegisteredVehiclesComponent } from './viewall-registered-vehicles.component';


@NgModule({
  declarations: [
    ViewallRegisteredVehiclesComponent
  ],
  imports: [
    CommonModule,
    ViewallRegisteredVehiclesRoutingModule,
    MatExpansionModule,
    FlexLayoutModule,
    WidgetsModule
  ]
})
export class ViewallRegisteredVehiclesModule { }
