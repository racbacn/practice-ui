import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RegisteredVehicles} from 'src/app/interface/transactions';
import {VehicleListService} from 'src/app/services/vehicle-list.service';
import {VEHICLE_TYPE} from 'src/app/shared/constants/application.constants';
import * as moment from "moment";

@Component({
    selector: 'app-viewall-registered-vehicles',
    templateUrl: './viewall-registered-vehicles.component.html',
    styleUrls: ['./viewall-registered-vehicles.component.scss']
})
export class ViewallRegisteredVehiclesComponent implements OnInit {
    panelOpenState = false;
    transactions!: RegisteredVehicles[];
    url = "review";
    @Output() searchData: EventEmitter<any> = new EventEmitter<any>();
    userType!: string;
    pageNo = 0;
    pageSize!: string;

    constructor(private vehicleService: VehicleListService) {
    }

    ngOnInit(): void {
        this.getVehicleList();
        this.userType = "regVehicles"
    }

    getVehicleList() {
        let params = {
            vehicleType: VEHICLE_TYPE.vehicle
        }
        this.vehicleService.getVehicleListService(params).subscribe(data => {
            this.transactions = data;
        })
    }

    getSearchParams(event: any) {
        let params: any = {}

        if (event.fromUserId) {
            params['fromUserId'] = event.fromUserId;
        }
        if (event.transactionRefNum) {
            params['transactionRefNum'] = event.transactionRefNum;
        }
        if (event.actionDateTimeFrom) {
            params['actionDateTimeFrom'] = moment(event.actionDateTimeFrom).startOf("day").utcOffset(0, true).format();
        }
        if (event.actionDateTimeTo) {
            params['actionDateTimeTo'] = moment(event.actionDateTimeTo).endOf('day').utcOffset(0, true).format();
        }
        params['vehicleType'] = VEHICLE_TYPE.vehicle

        console.log(params)

        this.vehicleService.getSearchListService(params).subscribe(data => {
            this.transactions = data;

        })
    }

    goToViewDetails(event: any) {
        for (let item of this.transactions) {
            if (item.registrationNo === event) {
                sessionStorage.setItem('transactionid', item.sourceTransactionId);
                sessionStorage.setItem('hideAprvRjct', 'registeredVehicle');
            }
        }
    }

    sortList(event: any) {
        this.pageSize = event;
        let params = {
            pageSize: event,
            vehicleType: VEHICLE_TYPE.vehicle,
            pageNo: this.pageNo === undefined ? "" : this.pageNo,
        }
        this.vehicleService.getVehicleListService(params).subscribe(res => {
            this.transactions = res;

            console.log("details", this.transactions)
        })
    }


    onPageMove(pageNumber: number) {
        let params = {
            pageNo: pageNumber,
            vehicleType: VEHICLE_TYPE.vehicle,
            pageSize: this.pageSize === undefined ? "" : this.pageSize,
        }
        this.vehicleService.getVehicleListService(params).subscribe(res => {
                if (res.length === 0 || res === undefined) {
                    return alert("No Data");
                } else {
                    this.transactions = res;
                    this.pageNo = pageNumber;
                }

                console.log("pagination", this.transactions)
            }, (error) => {
                if (error?.error?.code === 500) {
                    return alert(error.error.message);
                }
                alert('Cannot get transactions.');
            }
        )
    }

}
