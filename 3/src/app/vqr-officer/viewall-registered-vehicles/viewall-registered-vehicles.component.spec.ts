import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewallRegisteredVehiclesComponent } from './viewall-registered-vehicles.component';

describe('ViewallRegisteredVehiclesComponent', () => {
  let component: ViewallRegisteredVehiclesComponent;
  let fixture: ComponentFixture<ViewallRegisteredVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewallRegisteredVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewallRegisteredVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
