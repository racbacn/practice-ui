import {Injectable, OnDestroy} from '@angular/core';
import {HttpServicesService} from '../services/http-services.service';
import {Observable, Subject, timer} from 'rxjs';
import {delayWhen, retryWhen, share, switchMap, takeUntil, tap} from 'rxjs/operators';
import {NotificationResponse} from "../interface/notification-response";
import {UserResponse} from "../interface/user-response";
import {SessionUtil} from "../core/util/sessionUtil";

@Injectable({
    providedIn: 'root'
})
export class PollNotificationService implements OnDestroy {
    private pollNotification$!: Observable<NotificationResponse>;
    private stopPolling = new Subject();

    user: string = "";
    mapUserNotiID: any = {
        "defaultUserVQROfficer": "3",
        "defaultUserVQRManager": "4",
        "defaultUserVLCOfficer": "5",
        "defaultUserVLCManager": "6",
    }

    constructor(private httpServices: HttpServicesService) {
        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))
        let username = userResponse.username;

        if (username) {
            this.user = this.mapUserNotiID[username];
            //Retry every 3seconds
            this.pollNotification$ = timer(1, 3000).pipe(
                switchMap(() => this.httpServices.httpGet("/manage-notification-service/userNotifications/" + this.user)),
                // retry(),
                retryWhen(err =>
                    err.pipe(
                        delayWhen(() => timer(6000))
                    )),
                share(),
                takeUntil(this.stopPolling)
            );
        }

    }

    getNotifications(): Observable<NotificationResponse> {
        return this.pollNotification$.pipe(
            tap(() => console.log('data sent to subscriber'))
        );
    }

    ngOnDestroy() {
        this.stopPolling.next();
    }
}
