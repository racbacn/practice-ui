import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {VehicleTransactionHistoryResponse} from "../interface/vehicle-transaction-history-response";

@Injectable({
    providedIn: 'root'
})
export class VrlsUtilityService {
    private VEHICLE_TRANSACTION_HISTORY_URL = "/vrls-utility-service/vehicleTransactionHistory"

    constructor(private httpClient: HttpClient) {
    }

    getVehicleTransactionHistory(params: any) {
        return this.httpClient.get<VehicleTransactionHistoryResponse>(this.VEHICLE_TRANSACTION_HISTORY_URL, {params})
    }
}
