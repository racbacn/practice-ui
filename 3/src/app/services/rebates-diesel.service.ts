import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionUtil } from '../core/util/sessionUtil';
import { UserResponse } from '../interface/user-response';
import { AMENDMENT_STATUS, APP_URL, baseRoadTax, managerUserId, saveDieselRebate, saveRebate, saveRebateDraftDiesel, VALUE_CATEGORY } from '../shared/constants/application.constants';
import { RebateDiselRequest, RebatesDiesel, TaxRebatesDieselRequest } from '../vlc-officer/amend-roadtax-formula/rebates-diesel/interface/rebates-diesel';
import { RebatesDieselRecords } from '../vlc-officer/amend-roadtax-formula/rebates-diesel/interface/rebates-diesel-records';
import { RebatesDieselTable } from '../vlc-officer/amend-roadtax-formula/rebates-diesel/interface/rebates-diesel-table';
import { FormulaCardService } from '../widgets/formula-card/formula-card.service';
import { HttpConfigService } from './http-config.service';

@Injectable({
  providedIn: 'root'
})
export class RebatesDieselService {
  manangerId: any;
  rebatesList!: RebatesDieselTable;
  rebatesDiesel$: BehaviorSubject<RebatesDieselTable> = new BehaviorSubject<RebatesDieselTable>(this.rebatesList);
  constructor(private httpService: HttpConfigService, 
    private formulaCardService: FormulaCardService, private datePipe: DatePipe) { }


  formatDate(date: string): Date {
    const parts = date.split('-');
    const year = +parts[2];
    const month = +parts[1] - 1;
    const day = +parts[0];
    return new Date(year, month, day);
  }

  setTooltipMessage(tax: RebatesDieselRecords) {
    const { amendmentStatus, rejectComment } = tax;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }

  formatResponse(data: RebatesDiesel): RebatesDieselTable[] {
    let parentIndexCounter = 0;
    let indexCounter = 0;
    let forResubmission = false;
    let noActiveRoadTax = false;
    let fromApi: boolean = true;
    const taxRebates: RebatesDieselTable[] = [];

    data.RebateList.find((record: RebatesDieselRecords) => {
      if (record.amendmentStatus === AMENDMENT_STATUS.managerRejected) {
        forResubmission = true;
        noActiveRoadTax = false;
        return true;
      }
      return false;
    });

    data.RebateList.forEach((taxRebate: RebatesDieselRecords) => {
      parentIndexCounter = indexCounter;
      if ((taxRebate === undefined || null) || (taxRebate.vehicleRegistrationFrom && taxRebate.vehicleRegistrationTo) === undefined || null) {
        return;
      }

      if(taxRebate.amendmentStatus && taxRebate.amendmentStatus === AMENDMENT_STATUS.draft){
        fromApi = false;
      }
      const parentRebates = { 
        active: taxRebate.active || false,
        rebateName: taxRebate.rebateName,
        rebateTypeKey: taxRebate.rebateTypeKey,
        rebateType: taxRebate.rebateType,
        rebateAmount: taxRebate.rebateAmount,
        vehicleRegistrationFrom: this.formatDate(taxRebate.vehicleRegistrationFrom),
        vehicleRegistrationTo: this.formatDate(taxRebate.vehicleRegistrationTo),
        effectiveDate: this.formatDate(taxRebate.effectiveDate),
        endDate: this.formatDate(taxRebate.endDate),
        isAParent: true,
        isExpanded: false,
        parentIndex: parentIndexCounter + 1,
        fromApi: fromApi,
        newlyAdded: false,
        index: indexCounter,
        isNewRow: false,
        transactionId: taxRebate.transactionId,
        tooltip: this.setTooltipMessage(taxRebate),
        amendmentStatus: taxRebate.amendmentStatus,
        rejectComment: taxRebate.rejectComment,
        roadTaxAmtCatKey: taxRebate.roadTaxAmtCatKey,
        isOfficer: true,
        parentNotEditable: false
      };
      taxRebates.push(parentRebates);
    

    });
    return taxRebates;
  }

   getRoadTaxRebatesDiesel(): Observable<RebatesDieselTable[]> {
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    let res: RebatesDieselTable[];
    const url = APP_URL + baseRoadTax;
    let params = {
      tabActive: 2,
      userId: userResponse.userId,
      ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
      userTypeKey: userResponse.userTypeKey,
      valueCategory: VALUE_CATEGORY.rebates
    }
    
    return this.httpService.get(url, params).pipe(
      map((response: RebatesDiesel) => {
        if (response.RebateList.length) {
         res = this.formatResponse(response);
        }
        console.log("roadTax Diesel res", res)
        return res;
      })
    );
  }

  formatValues(data: any, lastParentIndex: any){
    let rebatesChildren: any;
    let newData: any[]=[];
    let baseObj: any;
    let isRebatesTaxChild: boolean = false;
    let isRejected: boolean = false;
    data.forEach((rebates: RebatesDieselTable, parentIndex: number) => {
     
      if((rebates.amendmentStatus && rebates.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (rebates.amendmentStatus && rebates.amendmentStatus === AMENDMENT_STATUS.draft) || rebates.amendmentStatus === AMENDMENT_STATUS.completed || rebates.amendmentStatus === AMENDMENT_STATUS.pendingReview){
        data.filter((record: RebatesDieselRecords) => {
          rebatesChildren = data.filter(
            (child: RebatesDieselTable) => !child.fromApi || child.newlyAdded
          );
    
          if((record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.draft) || record.amendmentStatus === AMENDMENT_STATUS.completed || record.amendmentStatus === AMENDMENT_STATUS.pendingReview || rebates.isAParent) {
            isRejected = record.amendmentStatus === AMENDMENT_STATUS.managerRejected ? true : false;;
            const rejectRebates: RebatesDiesel = {
              RebateList: rebatesChildren.map(
                (child: RebatesDieselTable) => {
                  const {
                    effectiveDate,
                    endDate,
                    active,
                    rebateName,
                    rebateTypeKey,
                    rebateType,
                    rebateAmount,
                    transactionId,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom
                  } = child;
                  const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                  const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                  let registrationDates = vehicleRegistrationFromDate + " - " + vehicleRegistrationToDate
                  return {
                    effectiveDate,
                    endDate,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom,
                    active,
                    rebateName,
                    rebateTypeKey,
                    rebateType,
                    rebateAmount,
                    transactionId,
                    parent,
                    registrationDates
                  };
                }
              ),
            };
            newData = rejectRebates.RebateList
            return  baseObj = {
              rebates: newData,
              isRejected: isRejected
            }
          }

          return baseObj;
        });
      }
       if (rebates.newlyAdded && rebates.isNewRow) {
        const hasNewlyAdded = data.find(
          (child: RebatesDieselTable) => child.newlyAdded
        );

        if (hasNewlyAdded) {
          rebatesChildren = data.filter(
            (child: RebatesDieselTable) => child.newlyAdded
          );
        
          const newRebates: RebatesDiesel = {
            RebateList: rebatesChildren.map(
              (child: RebatesDieselTable) => {
                const {
                  effectiveDate,
                  endDate,
                  active,
                  rebateName,
                  rebateTypeKey,
                  rebateType,
                  rebateAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom
                } = child;
                const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                let registrationDates = vehicleRegistrationFromDate + "  -  " + vehicleRegistrationToDate
                return {
                  effectiveDate,
                  endDate,
                  active,
                  rebateName,
                  rebateTypeKey,
                  rebateType,
                  rebateAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom,
                  parent,
                  registrationDates
                };
              }
            ),
          };
    
          rebatesChildren.push(lastParentIndex);
          newData = newRebates.RebateList
       
        }
         
        return  baseObj = {
          dateValidationData: rebatesChildren,
          rebates: newData,
          isRebatesTaxChild: isRebatesTaxChild,
          isRejected: isRejected
        }
      }
      return  baseObj = {
        dateValidationData: rebatesChildren,
        rebates: newData,
        isRebatesTaxChild: isRebatesTaxChild,
        isRejected: isRejected
      }
    });
    return baseObj
  }

  getManagerUserId(userId: any): Promise<any> {
    const url = APP_URL + managerUserId + userId;

    return this.httpService
      .get(url)
      .toPromise()
      .then((res) => {
        return res.managerUserId;
      });
  }

  async saveDieselRebate(rebatesParams: any,isDraft: boolean) {
    let resp: any;
    let url: any;
    const rebateValue: RebateDiselRequest[] = [];
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );

    this.manangerId = await this.getManagerUserId(userResponse.userId);

    if(isDraft){
      url = APP_URL + saveRebateDraftDiesel;
    }else{
      url = APP_URL + saveDieselRebate;
    }

    rebatesParams.forEach((taxRebate: RebatesDieselRecords) => {
        const { effectiveDate, endDate, rebateName, rebateTypeKey, rebateAmount, transactionId, vehicleRegistrationFrom , vehicleRegistrationTo} =
        taxRebate;
        const effectiveDateAsDate = new Date(effectiveDate);
        const endDateAsDate = new Date(endDate);
        const vehicleRegistrationFromDate = new Date(vehicleRegistrationFrom);
        const vehicleRegistrationToDate = new Date(vehicleRegistrationTo);
 
        const rebates: RebateDiselRequest = {
          chargeTypeKey: rebateTypeKey,
          rebateName: rebateName,
          rebateAmount: rebateAmount,
          effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
          endDate: this.formulaCardService.toString(endDateAsDate),
          vehicleRegistrationFrom: this.formulaCardService.toString(vehicleRegistrationFromDate),
          vehicleRegistrationTo: this.formulaCardService.toString(vehicleRegistrationToDate),
          transactionId: transactionId
        };
        rebateValue.push(rebates);
      });
  

    const params: TaxRebatesDieselRequest = {
      requestHeader: {
        ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
        userTypeKey: userResponse.userTypeKey,
        userId: userResponse.userId,
        managerId: this.manangerId,
      },
      rebateValue,
    };
    return this.httpService.post(url, params).pipe(
      map(async (response: any) => {
        resp = await response;

        console.log("submit rebate", resp);
        return resp;
      })
    );
  }

  setRebatesDiesel(rebatesDiesel: any) {
    this.rebatesDiesel$.next(rebatesDiesel)
    }
    
    getRebatesDieselData(): Observable<any> {
    return this.rebatesDiesel$;
    }
    
}


