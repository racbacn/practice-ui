import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Router} from "@angular/router"
import {AuthService} from "./auth.service";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    constructor(private auth: AuthService) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        let isActivated: boolean = false;

        if (this.auth.isSSOAuthenticated()) {
            isActivated = true;
        }
        console.log('isActivated', this.auth.isSSOAuthenticated())

        return isActivated;
    }
}
