import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TransactionDocumentsResponse} from "../interface/transaction-documents-response";
import {DocumentConfidenceResponse} from "../interface/document-confidence-response";
import {UpdateResultFlagRequest} from "../interface/update-result-flag-request";

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  private TRANSACTION_DOCUMENTS_URL: string = "/vehicle-registration/transactionDocuments";
  private DOCUMENT_CONFIDENCE_URL: string = "/manage-document-service-textract/documentConfidence"
  private UPDATE_RESULT_FLAG_URL: string = "/manage-document-service/updateResultFlag"

  constructor(private http: HttpClient) {
  }

  getTransactionDocuments(transactionId: string) {
    return this.http.get<TransactionDocumentsResponse>(this.TRANSACTION_DOCUMENTS_URL + "/" + transactionId)
  }

  getDocumentConfidence(firstDocumentId: number, secondDocumentId: number) {
    return this.http.get<DocumentConfidenceResponse>(this.DOCUMENT_CONFIDENCE_URL + "/" + firstDocumentId + ',' + secondDocumentId)
  }

  updateResultFlag(updateResultFlagRequest: UpdateResultFlagRequest) {
    return this.http.put<UpdateResultFlagRequest>(this.UPDATE_RESULT_FLAG_URL, updateResultFlagRequest);
  }
}
