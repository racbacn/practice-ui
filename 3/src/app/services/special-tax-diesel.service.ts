import { Injectable } from "@angular/core";
import * as moment from "moment";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { SessionUtil } from "../core/util/sessionUtil";
import { UserResponse } from "../interface/user-response";
import {
  AMENDMENT_STATUS,
  APP_URL,
  baseRoadTax,
  managerUserId,
  saveSpecialTax,
  saveSpecialTaxDraft,
  VALUE_CATEGORY,
} from "../shared/constants/application.constants";
import {
  SpecialTaxDiesel,
  SpecialTaxDieselRequest,
  SpecialTaxDieselResponse,
  SpecialTaxRequest,
} from "../vlc-officer/amend-roadtax-formula/special-tax-diesel/interface/special-tax-diesel";
import { SpecialTaxDieselRecords } from "../vlc-officer/amend-roadtax-formula/special-tax-diesel/interface/special-tax-diesel-records";
import { SpecialTaxDieselTable } from "../vlc-officer/amend-roadtax-formula/special-tax-diesel/interface/special-tax-diesel-table";
import { FormulaCardService } from "../widgets/formula-card/formula-card.service";
import { HttpConfigService } from "./http-config.service";

@Injectable({
  providedIn: "root",
})
export class SpecialTaxDieselService {
  specialTax!: SpecialTaxDieselTable;
  specialTax$: BehaviorSubject<SpecialTaxDieselTable> =
    new BehaviorSubject<SpecialTaxDieselTable>(this.specialTax);
  manangerId: any;

  constructor(
    private httpService: HttpConfigService,
    private formulaCardService: FormulaCardService
  ) {}

  setTooltipMessage(tax: SpecialTaxDieselRecords) {
    const { amendmentStatus, rejectComment } = tax;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return "";
  }

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }

  formatResponse(data: SpecialTaxDieselResponse): SpecialTaxDieselTable[] {
    const roadTaxSpecialTax: SpecialTaxDieselTable[] = [];
    let indexCounter = 0;
    let parentIndexCounter = 0;
    let forResubmission = false;
    let noActiveSurcharge = false;
    let  inactive: any; 
    data.RoadTaxSpecialTax.forEach((baseTax: SpecialTaxDiesel) => {
      baseTax.specialTax.find((record: SpecialTaxDieselRecords) => {
        if (record.amendmentStatus === AMENDMENT_STATUS.managerRejected) {
          forResubmission = true;
          noActiveSurcharge = false;
          return true;
        }
        return false;
      });
    });
  

    data.RoadTaxSpecialTax.forEach((roadTaxSpecial: SpecialTaxDiesel) => {
      if (!roadTaxSpecial.specialTax[0]) {
        return;
      }
      const { powerRating, roadTaxAmtCatKey, specialTax } = roadTaxSpecial;
      let activeRoadTax = specialTax.filter(
        (baseRoadTax: SpecialTaxDieselRecords) => baseRoadTax.active || !baseRoadTax.active
      )[0];

      if (!activeRoadTax) {
        inactive = specialTax.filter(
          (baseRoadTax: any) => !baseRoadTax.active
        )[0];       
        return;
      }
      if (!specialTax) {
        if (!specialTax[0]) {
          return;
        }
        activeRoadTax = specialTax[0];
        noActiveSurcharge = true;
      }

      const {
        effectiveDate,
        endDate,
        amount,
        minPaymentFee,
        amendmentStatus,
        specialTaxChargeType,
        specialTaxChargeTypeKey,
      } = activeRoadTax;
 
      indexCounter++;
      parentIndexCounter = indexCounter;

      const parentBaseRoadTax = {
        ...inactive,
        powerRating,
        roadTaxAmtCatKey,
        ...activeRoadTax,
        amount: amount,
        minPaymentFee: minPaymentFee,
        amendmentStatus: amendmentStatus,
        specialTaxChargeType: specialTaxChargeType,
        specialTaxChargeTypeKey: specialTaxChargeTypeKey,
        effectiveDate: this.formatDate(effectiveDate),
        endDate: this.formatDate(endDate),
        isAParent: true,
        isExpanded: false,
        parentIndex: null,
        hasChildren: specialTax.length > 1,
        newlyAdded: this.isRejected(amendmentStatus) || false,
        index: indexCounter,
        fromApi: true,
        isNewRow: false,
        tooltip: this.setTooltipMessage(activeRoadTax),
        forResubmission,
        isDraft: false,
        full: activeRoadTax,
        isOfficer: true,
        parentNotEditable: true,
      };

      roadTaxSpecialTax.push(parentBaseRoadTax);
      const historyTaxes = roadTaxSpecial.specialTax.filter(
        (baseRoadTax: SpecialTaxDieselRecords) => !baseRoadTax.active
      );
      if (!historyTaxes.length) {
        return;
      }
      let fromApi: boolean = true;
      let isDraft: boolean = false;

      historyTaxes.forEach((child: any, childIndex: number ) => {
        if (noActiveSurcharge && !childIndex) {
          return;
        }
        indexCounter++;
    if(child.amendmentStatus === AMENDMENT_STATUS.draft){
      fromApi = false;
      isDraft = true;
    }
        const chilBaseRoadTaxFee = {
          engineCapacity: "",
          ...child,
          amount: child.amount,
          minPaymentFee: child.minPaymentFee,
          amendmentStatus: child.amendmentStatus,
          specialTaxChargeType: child.specialTaxChargeType,
          specialTaxChargeTypeKey: child.specialTaxChargeTypeKey,
          effectiveDate: this.formatDate(child.effectiveDate),
          endDate: this.formatDate(child.endDate),
          isAParent: false,
          isExpanded: false,
          parentIndex: parentIndexCounter - 1,
          hasChildren: false,
          newlyAdded: this.isRejected(child.amendmentStatus) || false || isDraft,
          fromApi: fromApi,
          index: indexCounter,
          isNewRow: false,
          roadTaxAmtCatKey: roadTaxAmtCatKey,
          tooltip: this.setTooltipMessage(child),
          isDraft: isDraft
        };
        roadTaxSpecialTax.push(chilBaseRoadTaxFee);
      });
    });
    console.log("roadTaxSpecialTax", roadTaxSpecialTax)
    return roadTaxSpecialTax;
  }

  formatDate(dateString: string): Date {
    const dateParts: string[] = dateString.split("-");
    const formattedDate = new Date(
      +dateParts[2],
      +dateParts[1] - 1,
      +dateParts[0]
    );
    return formattedDate;
  }



  getSpecialTax(): Observable<SpecialTaxDieselTable[]> {
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    let res: SpecialTaxDieselTable[];
    const url = APP_URL + baseRoadTax;
    let params = {
      tabActive: 2,
      userId: userResponse.userId,
      ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
      userTypeKey: userResponse.userTypeKey,
      valueCategory: VALUE_CATEGORY.specialTax,
    };

    return this.httpService.get(url, params).pipe(
      map((response: SpecialTaxDieselResponse) => {
        let resp = response;
        console.log("specialtax response", resp);
        if (resp.RoadTaxSpecialTax.length) {
          res = this.formatResponse(resp);
        }
        return res;
      })
    );
  }

  getManagerUserId(userId: any): Promise<any> {
    const url = APP_URL + managerUserId + userId;

    return this.httpService
      .get(url)
      .toPromise()
      .then((res) => {
        return res.managerUserId;
      });
  }

  async saveSpecialTax(baseRoadTaxFeeParams: any, isDraft: boolean) {
    console.log("saveSpecialTax Params", baseRoadTaxFeeParams);
    let resp: any;
    let roadTaxAmtCatKey: any;
    let url: any;
    const specialRoadTax: SpecialTaxDieselRequest[] = [];
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    this.manangerId = await this.getManagerUserId(userResponse.userId);

    if (isDraft) {
      url = APP_URL + saveSpecialTaxDraft;
    } else {
      url = APP_URL + saveSpecialTax;
    }

    baseRoadTaxFeeParams.forEach((record: SpecialTaxDieselTable) => {
      const {
        effectiveDate,
        endDate,
        specialTaxChargeTypeKey,
        amount,
        minPaymentFee,
        transactionId,
        roadTaxAmtCatKey
      } = record;
      const effectiveDateAsDate = new Date(effectiveDate);
      const endDateAsDate = new Date(endDate);

      const specialRoadTaxObj: SpecialTaxDieselRequest = {
        roadTaxAmtCatKey: roadTaxAmtCatKey,
        chargeTypeKey: specialTaxChargeTypeKey,
        specialTaxAmt: amount,
        minPaymentFee: minPaymentFee,
        effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
        endDate: this.formulaCardService.toString(endDateAsDate),
        transactionId: transactionId,
      };
      let base = this.removeEmptyValues(specialRoadTax);

      specialRoadTax.push(specialRoadTaxObj);
    });

    const params: SpecialTaxRequest = {
      requestHeader: {
        ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
        userTypeKey: userResponse.userTypeKey,
        userId: userResponse.userId,
        managerId: this.manangerId,
      },
      specialRoadTax,
    };
    return this.httpService.post(url, params).pipe(
      map(async (response: any) => {
        resp = await response;

        console.log("special tax submit", resp);
        return resp;
      })
    );
  }

  removeEmptyValues(obj: any) {
    for (var propName in obj) {
      if (
        obj[propName] === null ||
        obj[propName] === undefined ||
        obj[propName] === " "
      ) {
        delete obj[propName];
      }
    }
    return obj;
  }

  setSpecialTaxDieselData(specialTax: any) {
    this.specialTax$.next(specialTax);
  }

  getSpecialTaxDieselData(): Observable<any> {
    return this.specialTax$;
  }

  toString(date: Date): string {
    return moment(date).format("DD-MM-yyyy");
  }

  formatValues(data: any) {
    let baseChildren: any;
    let newData: any[] = [];
    let baseObj: any;
    let isBaseTaxChild: boolean = false;
    let hasNewlyAdded: any;
    let isRejected: boolean;

    data.forEach((baseFee: SpecialTaxDieselTable, parentIndex: number) => {
      if (baseFee.isAParent && baseFee.amendmentStatus !== AMENDMENT_STATUS.managerRejected || (baseFee.isAParent && baseFee.amendmentStatus ===  AMENDMENT_STATUS.managerRejected)) {
        const parent = baseFee;
        hasNewlyAdded = data.find(
          (child: SpecialTaxDieselTable) =>
            (child.parentIndex === parentIndex && child.newlyAdded) || child.newlyAdded
        );

        if (hasNewlyAdded) {
          isBaseTaxChild = true;
          baseChildren = data.filter(
            (child: SpecialTaxDieselTable) =>
            child.parentIndex === parentIndex && child.newlyAdded
          );

          if (parent.newlyAdded) {
            baseChildren.push(parent);
          }

          let powerRating = parent.powerRating;
          let roadTaxAmtCatKey = parent.roadTaxAmtCatKey;
          const newBaseFee: SpecialTaxDiesel = {
            powerRating: powerRating,
            roadTaxAmtCatKey: roadTaxAmtCatKey,
            specialTax: baseChildren.map((child: SpecialTaxDieselTable) => {
              const {
                effectiveDate,
                endDate,
                active,
                amount,
                minPaymentFee,
                specialTaxChargeTypeKey,
                specialTaxChargeType,
                transactionId,
              } = child;

              if (child.amendmentStatus === AMENDMENT_STATUS.managerRejected || child.amendmentStatus === AMENDMENT_STATUS.draft || (baseFee.amendmentStatus === AMENDMENT_STATUS.managerRejected && baseFee.isAParent) || child.isAParent) {
  
                return {
                  roadTaxAmtCatKey,
                  powerRating,
                  amount,
                  effectiveDate,
                  endDate,
                  active,
                  parent,
                  minPaymentFee,
                  specialTaxChargeTypeKey,
                  specialTaxChargeType,
                  transactionId,
                };
              } else {
                return {
                  roadTaxAmtCatKey,
                  powerRating,
                  amount,
                  effectiveDate,
                  endDate,
                  active,
                  parent,
                  minPaymentFee,
                  specialTaxChargeTypeKey,
                  specialTaxChargeType,
                };
              }
            }),
          };
          baseChildren.push(parent);
          newData.push(newBaseFee);
        }
      }

      return (baseObj = {
        dateValidationData: baseChildren,
        baseTax: newData,
        isBaseTaxChild: isBaseTaxChild,
        isRejected: isRejected,
      });
    });
    console.log(baseObj);
    return baseObj;
  }
}
