import { TestBed } from '@angular/core/testing';

import { RebatesDieselService } from './rebates-diesel.service';

describe('RebatesDieselService', () => {
  let service: RebatesDieselService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RebatesDieselService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
