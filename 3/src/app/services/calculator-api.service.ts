import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { CalculatorOutputs } from "../interface/calculator-outputs";

@Injectable({
  providedIn: "root",
})
export class CalculatorApiService {
  private CALCULATE_ROADTAX_URL = "/manage-roadtax-service/calculateRoadTax";
  constructor(private httpClient: HttpClient) {}

  calculateRoadTax(params: any): Observable<CalculatorOutputs> {
    console.log(params);
    return this.httpClient.get<CalculatorOutputs>(this.CALCULATE_ROADTAX_URL, {
      params,
    });
  }
}
