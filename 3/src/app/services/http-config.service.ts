import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpConfigService {


  constructor(private http: HttpClient) {

  }

  get(url: any, param?: any, options?: Object): Observable<any> {
    // const headers = new HttpHeaders().append('header', param);
    const params = new HttpParams().append('params', JSON.stringify(param));
    return this.http.get(url,{ params: new HttpParams({ fromObject: param}) });
  }

  post(url: any, params?: any, options?: Object): Observable<any> {
    return this.http.post<any>(url, params, options).pipe(map((response: Response) => response));
  }

  put(url: any, params?: any, options?: Object): Observable<any> {
    return this.http.put<any>(url, params, options).pipe(map((response: Response) => response));
  }


  httpOptions(params: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptions;
  }
}
