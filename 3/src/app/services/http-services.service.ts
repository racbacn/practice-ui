import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { serverUrl } from '../constants/sharedValue';

@Injectable({
  providedIn: 'root'
})
export class HttpServicesService {

  constructor(private http: HttpClient) { }

  public submitRequest(bodyJSON: object): Observable<any> {
    //const url = environment.apiURL + AUTHENTICATE_ESA_LOGIN;
    const url = 'https://istio-ingressgateway-istio-system.apps.a6067fe2802840199acec1f28c4573a1.vehsystempoc.com/manage-vehicle-service/registerVehicle';
    // const url = 'http://localhost';


    const options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };
    return this.http.post<any>(url, bodyJSON, options)
  }

  public httpGet(urlGet : string): Observable<any> {
    //const url = environment.apiURL + AUTHENTICATE_ESA_LOGIN;
    // const url = 'https://istio-ingressgateway-istio-system.apps.a6067fe2802840199acec1f28c4573a1.vehsystempoc.com/manage-vehicle-service/registerVehicle';
    const url : string = urlGet;


    const options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };

    return this.http.get<any>(serverUrl + url, options)
  }

  public httpPost(urlPost : string, bodyJSON?: object): Observable<any> {
    const url : string = urlPost;
    const options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };
    return this.http.post<any>(serverUrl + url, bodyJSON, options)
  }

  public httpPut(urlPost : string, bodyJSON: object): Observable<any> {
    const url : string = urlPost;
    const options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
      }),
    };
    return this.http.put<any>(url, bodyJSON, options)
  }
}
