import { TestBed } from '@angular/core/testing';

import { AdditionalChargeDieselService } from './additional-charge-diesel.service';

describe('AdditionalChargeDieselService', () => {
  let service: AdditionalChargeDieselService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdditionalChargeDieselService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
