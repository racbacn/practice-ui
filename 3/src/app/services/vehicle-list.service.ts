import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_URL, vehicleRegistrationList } from '../shared/constants/application.constants';
import { HttpServicesService } from './http-services.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleListService {

  constructor(private httpService: HttpClient) { }

  getVehicleListService(params: any) {
    let apiURL = APP_URL + vehicleRegistrationList;
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.approvedRegistrations;
      return  response;
    }))
  }

  getSearchListService(params: any) {
    let apiURL = APP_URL + vehicleRegistrationList;
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.approvedRegistrations;
      return  response;
    }))
  }

}
