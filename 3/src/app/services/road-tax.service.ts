import { Injectable } from "@angular/core";
import { BehaviorSubject, from, Observable } from "rxjs";
import { map } from "rxjs/operators";
import {
  AMENDMENT_STATUS,
  APP_URL,
  baseRoadTax,
  managerUserId,
  removeDraft,
  saveBaseFee,
  saveBaseFeeDraft,
  VALUE_CATEGORY,
  VLC_USERS_ID,
} from "../shared/constants/application.constants";
import { RoadTaxRecord } from "../widgets/formula-card/road-tax-table/interface/road-tax-record";
import { RoadTaxTable } from "../widgets/formula-card/road-tax-table/interface/road-tax-table";
import {
  RoadTax,
  RoadTaxList,
  RoadTaxResponse,
} from "../widgets/formula-card/road-tax-table/interface/road-tax";
import { HttpConfigService } from "./http-config.service";
import { RemoveDraftParams, RemoveDraftRequest, RoadTaxRequest } from "../widgets/formula-card/road-tax-table/interface/road-tax-request";
import { BaseRoadTaxRequest } from "../widgets/formula-card/road-tax-table/interface/road-tax-records-request";
import { FormulaCardService } from "../widgets/formula-card/formula-card.service";
import { ManageUserService } from "./manage-user.service";
import { SessionUtil } from "../core/util/sessionUtil";
import { UserResponse } from "../interface/user-response";
import * as moment from "moment";



@Injectable({
  providedIn: "root",
})


export class RoadTaxService {
  roadTaxList!: RoadTaxTable;
  rebates!: any;
  additional!: any;
  baseFees$: BehaviorSubject<RoadTaxTable> = new BehaviorSubject<RoadTaxTable>(this.roadTaxList);
  rebates$: BehaviorSubject<any> = new BehaviorSubject<any>(this.rebates);
  additional$: BehaviorSubject<any> = new BehaviorSubject<any>(this.additional);
  
  manangerId: any;
  baseFeeData : any[] = [];
  constructor(
    private httpService: HttpConfigService,
    private formulaCardService: FormulaCardService,
    private managerService: ManageUserService
  ) {}

  setTooltipMessage(surcharge: RoadTaxRecord) {
    const { amendmentStatus, rejectComment } = surcharge;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }
  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }


  formatResponse(data: RoadTaxResponse): RoadTaxTable[] {
 
    const baseRoadTaxFees: RoadTaxTable[] = [];
    let indexCounter = -1;
    let parentIndexCounter = -1;
    let forResubmission = false;
    let noActiveSurcharge = false;
    let showEcMultiplierParent: boolean = true;  

    data.BaseRoadTaxFee.forEach((baseTax: RoadTax) => {
   
      baseTax.roadTaxRecords.find((record: RoadTaxRecord) => {
        if (record.amendmentStatus === AMENDMENT_STATUS.managerRejected) {
          forResubmission = true;
          noActiveSurcharge = false;
          showEcMultiplierParent = true;
          return true;
        }
        return false;
      });
    });


  
    data.BaseRoadTaxFee.forEach((baseRoadTaxFee: RoadTax) => {
      if (!baseRoadTaxFee.roadTaxRecords[0]) {
        return;
      }
      const { engineCapacity,roadTaxAmtCatKey, roadTaxRecords } =
        baseRoadTaxFee;

      let activeRoadTax = roadTaxRecords.filter(
        (baseRoadTax: RoadTaxRecord) => baseRoadTax.active
      )[0];
      if (!activeRoadTax) {
        if (!roadTaxRecords[0]) {
          return;
        }
        activeRoadTax = roadTaxRecords[0];
        noActiveSurcharge = true;
      }

      const { effectiveDate, endDate, baseAmt, discount, ecMultiplier, amendmentStatus, transactionId  } =
      activeRoadTax;
      const formula = this.formulaCardService.getPetrolFormula(
        baseAmt,
        ecMultiplier,
        discount,
        engineCapacity
      );
    
      indexCounter++;
      parentIndexCounter = indexCounter;
      const parentBaseRoadTax = {
        engineCapacity,
        roadTaxAmtCatKey,
        ...activeRoadTax,
        monthlyTax: formula,
        newValue: formula,
        baseAmt: baseAmt,
        baseAmount: formula,
        discount: discount,
        transactionId: transactionId,
        ecMultiplier: ecMultiplier,
        effectiveDate: this.formatDate(effectiveDate),
        endDate: this.formatDate(endDate),
        isAParent: true,
        isExpanded: false,
        parentIndex: null,
        hasChildren: roadTaxRecords.length > 1,
        newlyAdded: this.isRejected(amendmentStatus) || false,
        index: indexCounter,
        fromApi: true,
        isNewRow: false,
        isBaseRoadTax: true,
        amendmentStatus: amendmentStatus,
        tooltip: this.setTooltipMessage(activeRoadTax),
        forResubmission,
        isOfficer: true,
        minusText: this.formulaCardService.getECMinusValue(engineCapacity),
        showEcMultiplier: showEcMultiplierParent,
        isDraft: false,
        full: activeRoadTax,
        parentNotEditable: true,
      };

      baseRoadTaxFees.push(parentBaseRoadTax);
      const historyTaxes = baseRoadTaxFee.roadTaxRecords.filter(
        (baseRoadTax: RoadTaxRecord) => !baseRoadTax.active
      );
      if (!historyTaxes.length) {
        return;
      }
      
      historyTaxes.forEach((child: any, childIndex: number) => {
        if (noActiveSurcharge && !childIndex) {
          return;
        }
        indexCounter++;
        let fromApi: boolean = true;
        let isDraft: boolean = false;
        let minusText: string ='';
        let showEcMultiplier: boolean = true;
        const formulaChild = this.formulaCardService.getPetrolFormula(
          child.baseAmt,
          child.ecMultiplier,
          child.discount,
          engineCapacity
        );
        if(child.amendmentStatus === AMENDMENT_STATUS.draft){
          fromApi = false;
          isDraft = true;
          minusText = this.formulaCardService.getECMinusValue(engineCapacity);
          showEcMultiplier = false;
        }

        if(child.amendmentStatus === AMENDMENT_STATUS.managerRejected){
          fromApi = false;
          minusText = this.formulaCardService.getECMinusValue(engineCapacity);
          showEcMultiplier = true;
        }
  
  
        const chilBaseRoadTaxFee = {
          engineCapacity: "",
          ...child,
          baseAmt: child.baseAmt,
          monthlyTax: child.monthlyTax,
          baseAmount: formulaChild,
          newValue: formulaChild,
          discount: child.discount,
          ecMultiplier: child.ecMultiplier,
          effectiveDate: this.formulaCardService.formatDate(child.effectiveDate),
          endDate: this.formulaCardService.formatDate(child.endDate),
          isAParent: false,
          isExpanded: false,
          parentIndex: parentIndexCounter,
          hasChildren: false,
          newlyAdded: this.isRejected(child.amendmentStatus) || false || isDraft,
          fromApi: fromApi,
          index: indexCounter,
          isNewRow: false,
          roadTaxAmtCatKey: roadTaxAmtCatKey,
          isBaseRoadTax: true,
          isDraft: isDraft,
          forResubmission,
          amendmentStatus: child.amendmentStatus,
          tooltip: this.setTooltipMessage(child),
          showEcMultiplier: showEcMultiplier,
          full: { ...activeRoadTax, activeRoadTax: activeRoadTax || null },
          minusText: minusText,

        };
        baseRoadTaxFees.push(chilBaseRoadTaxFee);
      });
    });
   
    return baseRoadTaxFees;
  }

  formatDate(dateString: string): Date {
    const dateParts: string[] = dateString.split("-");
    const formattedDate = new Date(
      +dateParts[2],
      +dateParts[1] - 1,
      +dateParts[0]
    );
    return formattedDate;
  }

  getBaseRoadtax(): Observable<RoadTaxTable[]> {
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    let res: RoadTaxTable[];
    const url = APP_URL + baseRoadTax;
    let params = {
      tabActive: 1,
      userId: userResponse.userId,
      ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
      userTypeKey: userResponse.userTypeKey,
      valueCategory: VALUE_CATEGORY.baseFee,
    };

    return this.httpService.get(url, params).pipe(
      map((response: RoadTaxResponse) => {
        let resp = response;
        console.log("roadTax response", resp);
        if (resp.BaseRoadTaxFee.length) {
          res = this.formatResponse(resp);
        }
        return res;
      })
    );
  }

  getManagerUserId(userId: any): Promise<any> {
    const url = APP_URL + managerUserId + userId;

    return this.httpService
      .get(url)
      .toPromise()
      .then((res) => {
        return res.managerUserId;
      });
  }

  async saveBaseFee(baseRoadTaxFeeParams: any, isDraft: boolean) {
    console.log("baseRoadTaxFeeParams Params", baseRoadTaxFeeParams)
    let url: any;
    let resp: any;
    let roadTaxAmtCatKey: any;
    const baseRoadTaxFee: RoadTaxRequest[] = [];
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );

    this.manangerId = await this.getManagerUserId(userResponse.userId);
    if(isDraft){
    url = APP_URL + saveBaseFeeDraft;
    }else {
      url = APP_URL + saveBaseFee;
    }

        baseRoadTaxFeeParams.forEach((record: RoadTaxTable) => {
        const { effectiveDate, endDate, baseAmt, discount, ecMultiplier, transactionId, roadTaxAmtCatKey } =
        record;
        const effectiveDateAsDate = new Date(effectiveDate);
        const endDateAsDate = new Date(endDate);
 
        const baseFees: RoadTaxRequest = {
          roadTaxAmtCatKey: roadTaxAmtCatKey,
          baseAmt: baseAmt,
          discount: discount,
          ecMultiplier: ecMultiplier,
          effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
          endDate: this.formulaCardService.toString(endDateAsDate),
          transactionId: transactionId
        };
        let base = this.removeEmptyValues(baseFees);

        baseRoadTaxFee.push(baseFees);
      });
  
    const params: BaseRoadTaxRequest = {
      requestHeader: {
        ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
        userTypeKey: userResponse.userTypeKey,
        userId: userResponse.userId,
        managerId: this.manangerId,
      },
      baseRoadTaxFee,
    };

    return this.httpService.post(url, params).pipe(
      map(async (response: any) => {
        resp = await response;

        console.log("base submit", resp);
        return resp;
      })
    );
    
  }

  removeEmptyValues(obj: any) {
    for (var propName in obj) {
      if (
        obj[propName] === null ||
        obj[propName] === undefined ||
        obj[propName] === " "
      ) {
        delete obj[propName];
      }
    }
    return obj;
  }



saveBaseFeeData(data:any){
    this.baseFeeData= data;
}
retrieveSaveFeeData(){
    return this.baseFeeData;
}

setBaseRoadTax(baseFee: any) {
  this.baseFees$.next(baseFee)
 
}

getRoadTax(): Observable<any> {
  return this.baseFees$;
}

setRebates(rebates: any) {
  this.rebates$.next(rebates)
}

getRebates(): Observable<any> {
  return this.rebates$;
}

setAdditionalTax(additional: any) {
  this.additional$.next(additional)
}

getAdditionalTax(): Observable<any> {
  return this.additional$;
}
toString(date: Date): string {
  return moment(date).format('DD-MM-yyyy');
}

formatValues(data: any){
  let baseChildren: any;
  let newData: any[]=[];
  let baseObj: any;
  let isBaseTaxChild: boolean = false;
  let hasNewlyAdded: any;
  let isRejected: boolean;
  let engineCapacity: any;

  data.forEach((baseFee: RoadTaxTable, parentIndex: number) => {
      if ((baseFee.isAParent && baseFee.amendmentStatus !==  AMENDMENT_STATUS.managerRejected) || (baseFee.isAParent && baseFee.amendmentStatus ===  AMENDMENT_STATUS.managerRejected)) {
        const parent = baseFee;
        hasNewlyAdded = data.find(
          (child: RoadTaxTable) =>
            (child.parentIndex === parentIndex && child.newlyAdded) || child.newlyAdded
        );

     
        if (hasNewlyAdded) {
          isBaseTaxChild = true;
          baseChildren = data.filter(
            (child: RoadTaxTable) => child.parentIndex === parentIndex && child.newlyAdded
          );
          if (parent.newlyAdded) {
            baseChildren.push(parent);
          }
         engineCapacity = parent.engineCapacity;
         let roadTaxAmtCatKey = parent.roadTaxAmtCatKey;
          const newBaseFee: RoadTaxList = {
            engineCapacity:  parent.engineCapacity,
            roadTaxAmtCatKey: parent.roadTaxAmtCatKey,
            roadTaxRecords: baseChildren.map((child: RoadTaxTable) => {
              const {
                effectiveDate,
                endDate,
                active,
                baseAmt,
                discount,
                ecMultiplier,
                transactionId
              } = child;

              const formulaChild = this.formulaCardService.getPetrolFormula(
                baseAmt,
                ecMultiplier,
                discount,
                engineCapacity
              );
              const baseAmount = formulaChild
         if(child.amendmentStatus === AMENDMENT_STATUS.managerRejected || child.amendmentStatus === AMENDMENT_STATUS.draft  || (baseFee.amendmentStatus === AMENDMENT_STATUS.managerRejected && baseFee.isAParent) || child.isAParent){
          return {
            roadTaxAmtCatKey,
            engineCapacity,
            baseAmount,
            effectiveDate,
            endDate,
            active,
            parent,
            baseAmt,
            discount,
            ecMultiplier,
            transactionId
          };
         }else {
          return {
            roadTaxAmtCatKey,
            engineCapacity,
            baseAmount,
            effectiveDate,
            endDate,
            active,
            parent,
            baseAmt,
            discount,
            ecMultiplier,
          };
         }
            }), 
          };
          baseChildren.push(parent);
          newData.push(newBaseFee);
        }
    } 
    return  baseObj = {
      dateValidationData: baseChildren,
      baseTax: newData,
      isBaseTaxChild: isBaseTaxChild,
      isRejected: isRejected
    }

    });
 
    return baseObj
}

removeDraft(removeParams: any){
  let resp: any;
  const url = APP_URL + removeDraft;
  const roadTaxValues: RemoveDraftParams[] = [];

  removeParams.forEach((record: RoadTaxTable) => {
      const { transactionId, roadTaxAmtCatKey } =
      record;

      const baseFees: RemoveDraftParams= {
        roadTaxAmtCatKey: roadTaxAmtCatKey,
        transactionId: transactionId
      };
      roadTaxValues.push(baseFees);
    });

  const params: RemoveDraftRequest = {
    roadTaxValues
  };
  return this.httpService.put(url, params).pipe(map((res: any) => {
    let response = res;
    return response;
}))
}
}
