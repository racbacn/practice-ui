import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    constructor() {
    }

    private subjectApprove = new Subject<object>();
    private subjectReject = new Subject<object>();
    private subjectRejectAPIPut = new Subject<object>();
    private subjectConfirm = new Subject<object>();
    private approveCertifyRefundResponse = new Subject<object>();
    private rejectCertifyRefundResponse = new Subject<object>();

    sendApproveClick(dataInput: object) {
        this.subjectApprove.next(dataInput);
    }

    getApproveClick(): Observable<any> {
        return this.subjectApprove;
    }

    sendRejectClick(dataInput: object) {
        this.subjectReject.next(dataInput);
    }

    getRejectClick(): Observable<any> {
        return this.subjectReject;
    }

    sendConfirmApproveClick(dataInput: object) {
        this.subjectConfirm.next(dataInput);
    }

    getConfirmApproveClick(): Observable<any> {
        return this.subjectConfirm;
    }

    sendRejectAPIPut(dataInput: object) {
        this.subjectRejectAPIPut.next(dataInput);
    }

    getRejectAPIPut(): Observable<any> {
        return this.subjectRejectAPIPut;
    }

    sendApproveCertifyRefundResponse(dataInput: any) {
        this.approveCertifyRefundResponse.next(dataInput);
    }

    getApproveCertifyRefundResponse(): Observable<any> {
        return this.approveCertifyRefundResponse;
    }

    sendRejectCertifyRefundResponse(dataInput: any) {
        this.rejectCertifyRefundResponse.next(dataInput);
    }

    getRejectCertifyRefundResponse(): Observable<any> {
        return this.rejectCertifyRefundResponse;
    }

}
