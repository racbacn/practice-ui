import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {CertifyRefundResponse} from "../interface/certify-refund-response";
import {ApproveCertifyRefundRequest} from "../interface/approve-certify-refund-request";
import {ApproveCertifyRefundResponse} from "../interface/approve-certify-refund-response";
import {CertifyRoadTaxRefundTable} from "../interface/certify-road-tax-refund-table";

@Injectable({
    providedIn: 'root'
})
export class RoadTaxCertifyRefundService {
    private CERTIFY_REFUND_URL = "/manage-roadtax-service/certifyRefund";
    private APPROVE_CERTIFY_REFUND_URL = "/manage-roadtax-service/approveCertifyRefund"
    private REJECT_CERTIFY_REFUND_URL = "/manage-roadtax-service/rejectCertifyRefund"

    constructor(private httpClient: HttpClient) {
    }

    getCertifyRefundByTransId(id: number) {
        return this.httpClient.get<CertifyRefundResponse>(this.CERTIFY_REFUND_URL + "/" + id);
    }

    getCertifyRefundList() {
        return this.httpClient.get<CertifyRoadTaxRefundTable>(this.CERTIFY_REFUND_URL);
    }

    getCertifyRefundListWithParams(params: any) {
        return this.httpClient.get<CertifyRoadTaxRefundTable>(this.CERTIFY_REFUND_URL, {params});
    }

    approveCertifyRefund(approveCertifyRefundRequest: ApproveCertifyRefundRequest) {
        return this.httpClient.put<ApproveCertifyRefundResponse>(this.APPROVE_CERTIFY_REFUND_URL, approveCertifyRefundRequest);
    }

    rejectCertifyRefund(approveCertifyRefundRequest: ApproveCertifyRefundRequest) {
        return this.httpClient.put<ApproveCertifyRefundResponse>(this.REJECT_CERTIFY_REFUND_URL, approveCertifyRefundRequest);
    }
}
