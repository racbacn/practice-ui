import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APP_URL, officerdahsboardEndpoint, managerdahsboardEndpoint,dueDatesEndpoint, officersByManager, managerUserId, roadTaxDashboard } from '../shared/constants/application.constants';
import { HttpConfigService } from './http-config.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpService: HttpClient) { }


  getOfficerDashBoard(params: any): Observable<any>{
    let apiURL = APP_URL + officerdahsboardEndpoint;
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.transactions;
      console.log("table response", response)
      return  response;
    }))
  }

  getManagerDashBoard(params: any): Observable<any>{
    let apiURL = APP_URL + managerdahsboardEndpoint;
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.transactions;
      console.log("table response", response)
      return  response;
    }))
  }
  getDueDates(): Observable<any>{
    let apiURL = APP_URL + dueDatesEndpoint;
  
    return this.httpService.get(apiURL).pipe(map((res: any) => {
      let response = res.transactionDueDates;
      console.log("transactionDueDates response", response)
      return response;
    }))
  }

getOfficersByManager(params: any){
  let apiURL = APP_URL + officersByManager;
  return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
    let response = res.officerUserIds;
    return  response;
  }))
}

getRoadTaxDashBoard(params: any): Observable<any>{
  let apiURL = APP_URL + roadTaxDashboard;
  return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
    let response = res.transactions;
    console.log("table response", response)
    return  response;
  }))
}
}
