import { TestBed } from '@angular/core/testing';

import { SpecialTaxDieselService } from './special-tax-diesel.service';

describe('SpecialTaxDieselService', () => {
  let service: SpecialTaxDieselService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecialTaxDieselService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
