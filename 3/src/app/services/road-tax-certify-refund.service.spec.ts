import { TestBed } from '@angular/core/testing';

import { RoadTaxCertifyRefundService } from './road-tax-certify-refund.service';

describe('RoadTaxCertifyRefundService', () => {
  let service: RoadTaxCertifyRefundService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoadTaxCertifyRefundService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
