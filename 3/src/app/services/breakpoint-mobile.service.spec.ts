/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BreakpointMobileService } from './breakpoint-mobile.service';

describe('Service: BreakpointMobile', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BreakpointMobileService]
    });
  });

  it('should ...', inject([BreakpointMobileService], (service: BreakpointMobileService) => {
    expect(service).toBeTruthy();
  }));
});
