import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { DatePipe } from '@angular/common'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionUtil } from '../core/util/sessionUtil';
import { UserResponse } from '../interface/user-response';
import { AMENDMENT_STATUS, APP_URL, baseRoadTax, managerUserId, saveAddCharge, saveAddChargeDraft, VALUE_CATEGORY } from '../shared/constants/application.constants';
import { AdditionalRoadRequest, AdditionalRoadTaxRequest, AdditionalRoadTaxResponse } from '../widgets/additional-road-tax/interface/additional-road-tax';
import { AdditionalRoadTaxRecords } from '../widgets/additional-road-tax/interface/additional-road-tax-records';
import { AdditionalRoadTaxTable } from '../widgets/additional-road-tax/interface/additional-road-tax-table';
import { FormulaCardService } from '../widgets/formula-card/formula-card.service';
import { HttpConfigService } from './http-config.service';

@Injectable({
  providedIn: 'root'
})
export class AdditionalRoadTaxService {
  manangerId: any;

  constructor(private httpService: HttpConfigService, private formulaCardService: FormulaCardService, private datePipe: DatePipe) { }

  formatDate(dateString: string): Date {
    const dateParts: string[] = dateString.split('-');
    const formattedDate = new Date(
      +dateParts[2],
      +dateParts[1] - 1,
      +dateParts[0]
    );
    return formattedDate;
  }

  setTooltipMessage(tax: AdditionalRoadTaxRecords) {
    const { amendmentStatus, rejectComment } = tax;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }


  formatResponse(data: AdditionalRoadTaxResponse): AdditionalRoadTaxTable[] {
    let parentIndexCounter = 0;
    let indexCounter = 0;
    let forResubmission = false;
    let noActiveRoadTax = false;
    let fromApi: boolean = true;
    
    const taxRebates: AdditionalRoadTaxTable[] = [];

    data.AdditionalChargeList.find((record: AdditionalRoadTaxRecords) => {
      if (record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.managerRejected) {
        forResubmission = true;
        noActiveRoadTax = false;
        return true;
      }
      return false;
    });


    data.AdditionalChargeList.forEach((records: AdditionalRoadTaxRecords) => {
      parentIndexCounter = indexCounter;
      if ((records === undefined || null) || (records.vehicleRegistrationFrom && records.vehicleRegistrationTo) === undefined || null) {
        return;
      }
      if(records.amendmentStatus && records.amendmentStatus=== AMENDMENT_STATUS.draft){
        fromApi = false;
      }
      const parentRebates = { 
        active: records.active || false,
        addChargeName: records.addChargeName,
        addChargeTypeKey: records.addChargeTypeKey,
        addChargeType: records.addChargeType,
        addChargeAmount: records.addChargeAmount,
        effectiveDate: this.formatDate(records.effectiveDate),
        endDate: this.formatDate(records.endDate),
        vehicleRegistrationTo: this.formatDate(records.vehicleRegistrationTo),
        vehicleRegistrationFrom: this.formatDate(records.vehicleRegistrationFrom),
        transactionId: records.transactionId,
        isAParent: true,
        isExpanded: false,
        parentIndex: parentIndexCounter + 1,
        fromApi: fromApi,
        newlyAdded: this.isRejected(records.amendmentStatus)  || false,
        index: indexCounter,
        isNewRow: false,
        amendmentStatus: records.amendmentStatus,
        rejectComment: records.rejectComment,
        isOfficer: true,
        tooltip: this.setTooltipMessage(records),
        forResubmission,
        roadTaxAmtCatKey: records.roadTaxAmtCatKey,
        parentNotEditable: false
       
      };
      taxRebates.push(parentRebates);
    

    });
    return taxRebates;
  }

   getRAdditionalRoadTax(): Observable<AdditionalRoadTaxTable[]> {
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    let res: AdditionalRoadTaxTable[];
    const url = APP_URL + baseRoadTax;
    let params = {
      tabActive: 1,
      userId: userResponse.userId,
      ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
      userTypeKey: userResponse.userTypeKey,
      valueCategory: VALUE_CATEGORY.addCharge
    }
    
    return this.httpService.get(url, params).pipe(
      map((response: AdditionalRoadTaxResponse) => {
        if (response.AdditionalChargeList.length) {
         res = this.formatResponse(response);
        }
        console.log("additiona raod tax res", res)
        return res;
      })
    );
  }


  formatValues(data: any, lastParentIndex: any){
    let additonalTaxChildren: any;
    let newData: any[]=[];;
    let baseObj: any;
    let isRejected: boolean = false;
 
    let isAdditionalTaxChild: boolean = false;
    data.forEach((charge: AdditionalRoadTaxTable, parentIndex: number) => {
      if((charge.amendmentStatus && charge.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (charge.amendmentStatus && charge.amendmentStatus === AMENDMENT_STATUS.draft) || charge.amendmentStatus === AMENDMENT_STATUS.completed || charge.amendmentStatus === AMENDMENT_STATUS.pendingReview){
        data.filter((record: AdditionalRoadTaxRecords) => {
          let additonalTaxChildren = data.filter(
            (child: AdditionalRoadTaxTable) =>
              !child.fromApi || child.newlyAdded
          );
          if ((record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.draft) || record.amendmentStatus === AMENDMENT_STATUS.completed || record.amendmentStatus === AMENDMENT_STATUS.pendingReview || charge.isAParent) {
            isRejected = true;
            const rejectRebates: AdditionalRoadTaxResponse = {
              AdditionalChargeList: additonalTaxChildren.map(
                (child: AdditionalRoadTaxTable) => {
                  const {
                    effectiveDate,
                    endDate,
                    active,
                    addChargeName,
                    addChargeTypeKey,
                    addChargeType,
                    addChargeAmount,
                    transactionId,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom,
                  } = child;
                  const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                  const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                  let registrationDates = vehicleRegistrationFromDate + " - " + vehicleRegistrationToDate

                  return {
                    effectiveDate,
                    endDate,
                    active,
                    addChargeName,
                    addChargeTypeKey,
                    addChargeType,
                    addChargeAmount,
                    transactionId,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom,
                    parent,
                    registrationDates
                  };
                }
              ),
            };
            newData = rejectRebates.AdditionalChargeList
   
            return  baseObj = {
              addCharge: newData,
              isRejected: isRejected
            }
          }
        
          return baseObj;
        });
      }

      if (charge.newlyAdded && charge.isNewRow) {
        const hasNewlyAdded = data.find(
          (child: AdditionalRoadTaxTable) => child.newlyAdded
        );

        if (hasNewlyAdded) {
          additonalTaxChildren = data.filter(
            (child: AdditionalRoadTaxTable) => child.newlyAdded
          );

          const newAdditionalTax: AdditionalRoadTaxResponse = {
            AdditionalChargeList: additonalTaxChildren.map(
              (child: AdditionalRoadTaxTable) => {
                const {
                  effectiveDate,
                  endDate,
                  active,
                  addChargeName,
                  addChargeTypeKey,
                  addChargeType,
                  addChargeAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom,
                } = child;
                const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                let registrationDates = vehicleRegistrationFromDate + " - " + vehicleRegistrationToDate
                return {
                  effectiveDate,
                  endDate,
                  active,
                  addChargeName,
                  addChargeTypeKey,
                  addChargeType,
                  addChargeAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom,
                  parent,
                  registrationDates
                };
              }
            ),
          };
          additonalTaxChildren.push(lastParentIndex);
          newData = newAdditionalTax.AdditionalChargeList;
        }
        return  baseObj = {
          dateValidationData: additonalTaxChildren,
          addCharge: newData,
          isAdditionalTaxChild: isAdditionalTaxChild,
          isRejected: isRejected
        }
      }
      return  baseObj = {
        dateValidationData: additonalTaxChildren,
        addCharge: newData,
        isAdditionalTaxChild: isAdditionalTaxChild,
        isRejected: isRejected
      }
    });
    return baseObj
  }

  getManagerUserId(userId: any): Promise<any> {
    const url = APP_URL + managerUserId + userId;

    return this.httpService
      .get(url)
      .toPromise()
      .then((res) => {
        return res.managerUserId;
      });
  }

  async saveAdditionalCharge(chargeParams: any, isDraft: boolean) {
    console.log("saveAdditionalChargeParams", chargeParams)
    let url: any;
    let resp: any;
    const additionalCharges: AdditionalRoadTaxRequest[] = [];
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    this.manangerId = await this.getManagerUserId(userResponse.userId);

    if(isDraft){
      url = APP_URL + saveAddChargeDraft
    }else{
      url = APP_URL + saveAddCharge;
    }

    chargeParams.forEach((taxRebate: AdditionalRoadTaxRecords) => {
        const { effectiveDate, endDate, addChargeName, addChargeTypeKey, addChargeAmount, transactionId, vehicleRegistrationTo, vehicleRegistrationFrom } =
        taxRebate;
        const effectiveDateAsDate = new Date(effectiveDate);
        const endDateAsDate = new Date(endDate);
        const vehicleRegistrationFromDate = new Date(vehicleRegistrationFrom);
        const vehicleRegistrationToDate = new Date(vehicleRegistrationTo);
 
        const addCharge: AdditionalRoadTaxRequest = {
          addChargeTypeKey: addChargeTypeKey,
          addChargeName: addChargeName,
          addChargeValue: addChargeAmount,
          effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
          vehicleRegistrationFrom: this.formulaCardService.toString(vehicleRegistrationFromDate),
          vehicleRegistrationTo: this.formulaCardService.toString(vehicleRegistrationToDate),
          endDate: this.formulaCardService.toString(endDateAsDate),
          transactionId: transactionId
        };
        additionalCharges.push(addCharge);
      });
  

    const params: AdditionalRoadRequest = {
      requestHeader: {
        ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
        userTypeKey: userResponse.userTypeKey,
        userId: userResponse.userId,
        managerId: this.manangerId,
      },
      additionalCharges,
    };
    return this.httpService.post(url, params).pipe(
      map(async (response: any) => {
        resp = await response;

        console.log("submit additionalCharges", resp);
        return resp;
      })
    );
  }
}
