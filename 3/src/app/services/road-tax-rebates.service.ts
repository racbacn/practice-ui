import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SessionUtil } from '../core/util/sessionUtil';
import { UserResponse } from '../interface/user-response';
import { AMENDMENT_STATUS, APP_URL, baseRoadTax, managerUserId, saveRebate, saveRebateDraft, VALUE_CATEGORY } from '../shared/constants/application.constants';
import { FormulaCardService } from '../widgets/formula-card/formula-card.service';
import { RoadTaxRebatesRecord } from '../widgets/formula-card/road-tax-rebates/interface/road-tax-rebate-record';
import { RoadTaxRebatesTable } from '../widgets/formula-card/road-tax-rebates/interface/road-tax-rebate-table';
import { RebateRequest, RoadTaxRebatesResponse, TaxRebatesRequest } from '../widgets/formula-card/road-tax-rebates/interface/road-tax-rebates';
import { HttpConfigService } from './http-config.service';

@Injectable({
  providedIn: 'root'
})
export class RoadTaxRebatesService {

  manangerId: any;

  constructor(private httpService: HttpConfigService, 
    private formulaCardService: FormulaCardService,private datePipe: DatePipe ) { }


  formatDate(date: string): Date {
    const parts = date.split('-');
    const year = +parts[2];
    const month = +parts[1] - 1;
    const day = +parts[0];
    return new Date(year, month, day);
  }

  setTooltipMessage(tax: RoadTaxRebatesRecord) {
    const { amendmentStatus, rejectComment } = tax;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }

  formatResponse(data: RoadTaxRebatesResponse): RoadTaxRebatesTable[] {
    let parentIndexCounter = 0;
    let indexCounter = 0;
    let forResubmission = false;
    let noActiveRoadTax = false;
    let fromApi: boolean = true;

    const taxRebates: RoadTaxRebatesTable[] = [];
   
      data.RebateList.find((record: RoadTaxRebatesRecord) => {
        if (record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.managerRejected) {
          forResubmission = true;
          noActiveRoadTax = false;
          return true;
        }
        return false;
      });

    data.RebateList.forEach((taxRebate: RoadTaxRebatesRecord) => {
      parentIndexCounter = indexCounter;
      if ((taxRebate === undefined || null) || (taxRebate.vehicleRegistrationFrom && taxRebate.vehicleRegistrationTo) === undefined || null) {
        return;
      }

      if(taxRebate.amendmentStatus && taxRebate.amendmentStatus === AMENDMENT_STATUS.draft){
        fromApi = false;
      }
      const parentRebates = { 
        roadTaxAmtCatKey: taxRebate.roadTaxAmtCatKey,
        active: taxRebate.active || false,
        rebateName: taxRebate.rebateName,
        rebateTypeKey: taxRebate.rebateTypeKey,
        rebateType: taxRebate.rebateType,
        rebateAmount: taxRebate.rebateAmount,
        effectiveDate: this.formulaCardService.formatDate(taxRebate.effectiveDate),
        endDate: this.formulaCardService.formatDate(taxRebate.endDate),
        vehicleRegistrationFrom: this.formatDate(taxRebate.vehicleRegistrationFrom),
        vehicleRegistrationTo: this.formatDate(taxRebate.vehicleRegistrationTo),
        isAParent: true,
        isExpanded: false,
        parentIndex: parentIndexCounter + 1,
        fromApi: fromApi,
        newlyAdded: false,
        index: indexCounter,
        isNewRow: false,
        forResubmission,
        transactionId: taxRebate.transactionId,
        tooltip: this.setTooltipMessage(taxRebate),
        amendmentStatus: taxRebate.amendmentStatus,
        rejectComment: taxRebate.rejectComment,
        isOfficer: true,
        parentNotEditable: false
       
      };
      taxRebates.push(parentRebates);
    

    });
    console.log("taxRebates", taxRebates)
    return taxRebates;
  }

   getRoadTaxRebates(): Observable<RoadTaxRebatesTable[]> {
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );
    let res: RoadTaxRebatesTable[];
    const url = APP_URL + baseRoadTax;
    let params = {
      tabActive: 1,
      userId: userResponse.userId,
      ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
      userTypeKey: userResponse.userTypeKey,
      valueCategory: VALUE_CATEGORY.rebates
    }
    
    return this.httpService.get(url, params).pipe(
      map((response: RoadTaxRebatesResponse) => {
        console.log("roadTaxRebates response", response)
        if (response.RebateList.length) {
         res = this.formatResponse(response);
        }
        return res;
      })
    );
  }

  formatValues(data: any, lastParentIndex: any){
    let rebatesChildren: any;
    let newData: any[]=[];
    let baseObj: any;
    let isRebatesTaxChild: boolean = false;
    let isRejected: boolean = false;

    data.forEach((rebates: RoadTaxRebatesTable, parentIndex: number) => {
     
      if((rebates.amendmentStatus && rebates.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (rebates.amendmentStatus && rebates.amendmentStatus === AMENDMENT_STATUS.draft) || rebates.amendmentStatus === AMENDMENT_STATUS.completed || rebates.amendmentStatus === AMENDMENT_STATUS.pendingReview){
        data.filter((record: RoadTaxRebatesRecord) => {
          rebatesChildren = data.filter(
            (child: RoadTaxRebatesTable) => !child.fromApi || child.newlyAdded
          );
    
          if ((record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.managerRejected) || (record.amendmentStatus && record.amendmentStatus === AMENDMENT_STATUS.draft) || record.amendmentStatus === AMENDMENT_STATUS.completed || record.amendmentStatus === AMENDMENT_STATUS.pendingReview || rebates.isAParent) {
            isRejected = record.amendmentStatus === AMENDMENT_STATUS.managerRejected ? true : false;
            const rejectRebates: RoadTaxRebatesResponse = {
              RebateList: rebatesChildren.map(
                (child: RoadTaxRebatesTable) => {
                  const {
                    effectiveDate,
                    endDate,
                    active,
                    rebateName,
                    rebateTypeKey,
                    rebateType,
                    rebateAmount,
                    transactionId,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom
                  } = child;
                  const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                  const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                  let registrationDates = vehicleRegistrationFromDate + " - " + vehicleRegistrationToDate
                  return {
                    effectiveDate,
                    endDate,
                    vehicleRegistrationTo,
                    vehicleRegistrationFrom,
                    active,
                    rebateName,
                    rebateTypeKey,
                    rebateType,
                    rebateAmount,
                    transactionId,
                    parent,
                    registrationDates
                  };
                }
              ),
            };
            newData = rejectRebates.RebateList
 
            return  baseObj = {
              rebates: newData,
              isRejected: isRejected
            }
  
          }
 
          return baseObj;
        });
      }
       if (rebates.newlyAdded && rebates.isNewRow) {
        const hasNewlyAdded = data.find(
          (child: RoadTaxRebatesTable) => child.newlyAdded
        );

        if (hasNewlyAdded) {
          rebatesChildren = data.filter(
            (child: RoadTaxRebatesTable) => child.newlyAdded
          );
        
          const newRebates: RoadTaxRebatesResponse = {
            RebateList: rebatesChildren.map(
              (child: RoadTaxRebatesTable) => {
                const {
                  effectiveDate,
                  endDate,
                  active,
                  rebateName,
                  rebateTypeKey,
                  rebateType,
                  rebateAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom
                } = child;
                const vehicleRegistrationFromDate =  this.datePipe.transform(vehicleRegistrationFrom, "d MMM yyyy");
                const vehicleRegistrationToDate =  this.datePipe.transform(vehicleRegistrationTo, "d MMM yyyy");
                let registrationDates = vehicleRegistrationFromDate + "  -  " + vehicleRegistrationToDate
                return {
                  effectiveDate,
                  endDate,
                  active,
                  rebateName,
                  rebateTypeKey,
                  rebateType,
                  rebateAmount,
                  vehicleRegistrationTo,
                  vehicleRegistrationFrom,
                  parent,
                  registrationDates
                };
              }
            ),
          };
        
          newData = newRebates.RebateList
       
        }
         
        return  baseObj = {
          dateValidationData: rebatesChildren,
          rebates: newData,
          isRebatesTaxChild: isRebatesTaxChild,
          isRejected: isRejected
        }
      }
      return  baseObj = {
        dateValidationData: rebatesChildren,
        rebates: newData,
        isRebatesTaxChild: isRebatesTaxChild,
        isRejected: isRejected
      }
    });
    return baseObj
  }

  getManagerUserId(userId: any): Promise<any> {
    const url = APP_URL + managerUserId + userId;

    return this.httpService
      .get(url)
      .toPromise()
      .then((res) => {
        return res.managerUserId;
      });
  }

  async saveRebate(rebatesParams: any, isDraft: boolean) {
    console.log("rebates", rebatesParams)
    let url: any;
    let resp: any;
    const rebateValue: RebateRequest[] = [];
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    );

    this.manangerId = await this.getManagerUserId(userResponse.userId);

    if(isDraft){
      url = APP_URL + saveRebateDraft
    }else {
      url = APP_URL + saveRebate;
    }
    
    rebatesParams.forEach((taxRebate: RoadTaxRebatesRecord) => {
        const { effectiveDate, endDate, rebateName, rebateTypeKey, rebateAmount, transactionId, vehicleRegistrationFrom , vehicleRegistrationTo} =
        taxRebate;
        const effectiveDateAsDate = new Date(effectiveDate);
        const endDateAsDate = new Date(endDate);
        const vehicleRegistrationFromDate = new Date(vehicleRegistrationFrom);
        const vehicleRegistrationToDate = new Date(vehicleRegistrationTo);
 
        const rebates: RebateRequest = {
          chargeTypeKey: rebateTypeKey,
          rebateName: rebateName,
          rebateAmount: rebateAmount,
          effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
          endDate: this.formulaCardService.toString(endDateAsDate),
          vehicleRegistrationFrom: this.formulaCardService.toString(vehicleRegistrationFromDate),
          vehicleRegistrationTo: this.formulaCardService.toString(vehicleRegistrationToDate),
          transactionId: transactionId
        };
        rebateValue.push(rebates);
      });
  

    const params: TaxRebatesRequest = {
      requestHeader: {
        ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
        userTypeKey: userResponse.userTypeKey,
        userId: userResponse.userId,
        managerId: this.manangerId,
      },
      rebateValue,
    };
    return this.httpService.post(url, params).pipe(
      map(async (response: any) => {
        resp = await response;

        console.log("submit rebate", resp);
        return resp;
      })
    );
  }

}
