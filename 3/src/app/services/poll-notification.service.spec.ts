import { TestBed } from '@angular/core/testing';

import { PollNotificationService } from './poll-notification.service';

describe('PollNotificationService', () => {
  let service: PollNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PollNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
