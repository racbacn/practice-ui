import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PMDRequest } from '../interface/pmd-request';
import { VehicleRequest } from '../interface/vehicle-request';
import { APP_URL, reviewEndpoint, approveEndpoint, reviewPmdEndpoint, approvePmdOfficerEndpoint, approvePmdManagerEndpoint } from '../shared/constants/application.constants';
import { HttpConfigService } from './http-config.service';

@Injectable({
    providedIn: 'root'
})
export class ReviewService {

    constructor(private httpService: HttpConfigService) {
    }


  getVehicleDetails(param: any): Observable<VehicleRequest> {
    let apiURL = APP_URL + reviewEndpoint + param;
    return this.httpService.get(apiURL).pipe(map((res: any) => {
      let response = res;
      console.log("review response", response)
      return response;
    }))
  }
    approveVehicleReg(params: any): Observable<any> {
        let apiURL = APP_URL + approveEndpoint;
        return this.httpService.put(apiURL, params).pipe(map((res: any) => {
            let response = res;
            return response;
        }))
    }

    getPmdDetails(param: any): Observable<PMDRequest> {
        let apiURL = APP_URL + reviewPmdEndpoint + param;
        return this.httpService.get(apiURL).pipe(map((res: any) => {
            let response = res;
            return response;
        }))
    }

    approvePmdRegOfficer(params: any): Observable<any> {
        let apiURL = APP_URL + approvePmdOfficerEndpoint;
        return this.httpService.put(apiURL, params).pipe(map((res: any) => {
            let response = res;
            return response;
        }))
    }

    approvePmdRegManager(params: any): Observable<any> {
        let apiURL = APP_URL + approvePmdManagerEndpoint;
        return this.httpService.put(apiURL, params).pipe(map((res: any) => {
            let response = res;
            return response;
        }))
    }
}
