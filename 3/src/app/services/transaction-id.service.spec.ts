import { TestBed } from '@angular/core/testing';

import { TransactionIdService } from './transaction-id.service';

describe('TransactionIdService', () => {
  let service: TransactionIdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionIdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
