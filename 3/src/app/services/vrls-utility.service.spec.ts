import { TestBed } from '@angular/core/testing';

import { VrlsUtilityService } from './vrls-utility.service';

describe('VrlsUtilityService', () => {
  let service: VrlsUtilityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VrlsUtilityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
