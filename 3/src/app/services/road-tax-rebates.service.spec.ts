import { TestBed } from '@angular/core/testing';

import { RoadTaxRebatesService } from './road-tax-rebates.service';

describe('RoadTaxRebatesService', () => {
  let service: RoadTaxRebatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoadTaxRebatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
