import {Injectable} from '@angular/core';
import {SessionUtil} from "../core/util/sessionUtil";
import {UserResponse} from "../interface/user-response";
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    vqrOfficer: string = 'defaultUserVQROfficer';
    vlcOfficer: string = 'defaultUserVLCOfficer';
    vqrManager: string = 'defaultUserVQRManager';
    vlcManager: string = 'defaultUserVLCManager';


    constructor(private jwtHelper: JwtHelperService) {
    }

    isSSOAuthenticated(): boolean {
        let isValid: boolean = false;

        let token: string | null = SessionUtil.getSessionStorage(SessionUtil.TOKEN);
        let userResponse: UserResponse | null = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

        if (userResponse) {
            isValid = (userResponse.username === 'defaultUserVQROfficer')
                || (userResponse.username === 'defaultUserVLCOfficer')
                || (userResponse.username === 'defaultUserVQRManager')
                || (userResponse.username === 'defaultUserVLCManager')
        }

        if (token) {
            isValid = ((!this.jwtHelper.isTokenExpired(token))
                && (this.jwtHelper.decodeToken(token).username === this.vqrOfficer.toLowerCase()
                    || this.jwtHelper.decodeToken(token).username === this.vlcOfficer.toLowerCase()
                    || this.jwtHelper.decodeToken(token).username === this.vqrManager.toLowerCase()
                    || this.jwtHelper.decodeToken(token).username === this.vlcManager.toLowerCase()));
        }

        return isValid;
    }

}
