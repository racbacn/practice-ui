import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import * as S3 from "aws-sdk/clients/s3";

@Injectable({
    providedIn: 'root'
})
export class GetFilesService {
    bucket: S3;

    constructor() {
        this.bucket = new S3(
            {
                accessKeyId: environment.aws.accessKeyId,
                secretAccessKey: environment.aws.secretAccessKey,
            }
        );
    }

    getFile(key: string) {
        let data = {
            Bucket: environment.aws.s3.bucket,
            Key: key
        }

        return this.bucket.getSignedUrl('getObject', data);
    }

    
    getPMDFile(key: string) {
        let data = {
            Bucket: environment.pmdBucket.s3.bucket,
            Key: key
        }

        return this.bucket.getSignedUrl('getObject', data);
    }
}
