import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AwsCognitoService} from "./aws-cognito.service";
import {Location} from '@angular/common';
import {catchError, finalize, switchMap} from 'rxjs/operators';
import {SessionUtil} from "../core/util/sessionUtil";
import {ManageUserService} from "./manage-user.service";
import {UserResponse} from "../interface/user-response";
import {JwtHelperService} from "@auth0/angular-jwt";


@Injectable({
    providedIn: 'root'
})
export class TokenResolver implements Resolve<any> {
    vqrOfficer: string = 'defaultUserVQROfficer';
    vlcOfficer: string = 'defaultUserVLCOfficer';
    vqrManager: string = 'defaultUserVQRManager';
    vlcManager: string = 'defaultUserVLCManager';

    userTypeOfficer: string = 'officer';
    userTypeManager: string = 'manager';

    errorMessage: boolean = false;

    constructor(private location: Location,
                private awsCognitoService: AwsCognitoService,
                private router: Router,
                private managerUserService: ManageUserService,
                private jwtHelper: JwtHelperService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const urlParams: URLSearchParams = new URLSearchParams(window.location.search);
        const code: string | null = urlParams.get('code');

        if (!code) {
            return of(null);
        }

        return this.getTokenDetailsFromCognito(code).pipe(
            finalize(() => {
                this.location.replaceState(window.location.pathname);
            })
        );
    }

    getTokenDetailsFromCognito(code: string): Observable<any | null> {
        return this.awsCognitoService.getTokenDetailsFromCognito(code).pipe(
            switchMap((response: any) => {
                console.log('Response: ', response);

                SessionUtil.setSessionStorage(SessionUtil.TOKEN, response.access_token)

                if (response) {
                    let username = this.jwtHelper.decodeToken(response.access_token).username

                    console.log(username)

                    if (username) {
                        this.managerUserService.getUserId(username).subscribe(data => {
                            let userResponse: UserResponse = data;

                            if (userResponse) {
                                SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(userResponse))

                                if (userResponse.username.toLowerCase() === this.vqrOfficer.toLowerCase()) {
                                    SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeOfficer)

                                    this.router.navigate(['/vqr-officer']);
                                }
                                if (userResponse.username.toLowerCase() === this.vqrManager.toLowerCase()) {
                                    SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeManager)

                                    this.router.navigate(["/vqr-manager"]);
                                }
                                if (userResponse.username.toLowerCase() === this.vlcOfficer.toLowerCase()) {
                                    SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeOfficer)

                                    this.router.navigate(["/vlc-officer"])
                                }
                                if (userResponse.username.toLowerCase() === this.vlcManager.toLowerCase()) {
                                    SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeManager)

                                    this.router.navigate(["/vlc-manager"]);
                                } else {
                                    this.errorMessage = true;
                                }
                            }
                        });
                    }
                }

                return of(response);
            }),
            catchError((error) => {
                return error;
            })
        );
    }
}
