import { TestBed } from '@angular/core/testing';

import { PmdListService } from './pmd-list.service';

describe('PmdListService', () => {
  let service: PmdListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PmdListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
