import { TestBed } from '@angular/core/testing';

import { AdditionalRoadTaxService } from './additional-road-tax.service';

describe('AdditionalRoadTaxService', () => {
  let service: AdditionalRoadTaxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdditionalRoadTaxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
