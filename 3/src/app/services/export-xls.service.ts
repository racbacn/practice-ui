import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Workbook } from 'exceljs';
import { saveAs } from 'file-saver';
import { TableHeader } from '../interface/table-header';
import { ExportXlsDialogComponent } from '../widgets/export-xls-dialog/export-xls-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class ExportXlsService {
  private rows!: Array<Array<string>>;
  private generatingDialog!: MatDialogRef<any>;
  private successDialog!: MatDialogRef<any>;
  private blob!: Blob;
  private filename: string = 'file.xlsx';

  constructor(private dialog: MatDialog) {}

  openDialog(message: string, showIcon: boolean, showDownloadButton: boolean) {
    return this.dialog.open(ExportXlsDialogComponent, {
      data: { message, showIcon, showDownloadButton },
      height: '272px',
      width: '703px',
      disableClose: true,
    });
  }

  export(
    headers: Array<TableHeader>,
    rows: Array<Array<string>>,
    filename: string = 'file.xlsx'
  ) {
    if (!rows.length) {
      this.openDialog('Select a row or rows first.', false, false);
      return;
    }
    this.rows = rows;
    this.filename = filename;
    this.generateDownload(headers);
  }

  async generateDownload(headers: Array<TableHeader>) {
    this.generatingDialog = this.openDialog('Generating Download', true, false);
    const workbook = new Workbook();
    workbook.creator = 'Company Name';
    workbook.created = new Date();
    const worksheet = workbook.addWorksheet('Worksheet 1');
    const headersRow = headers.map((header: TableHeader) => header.label);
    worksheet.addRow(headersRow);
    worksheet.getRow(1).font = { bold: true };
    this.rows.forEach((row: any) => {
      const headerProperties = headers.map(
        (header: TableHeader) => header.property
      );
      const rowArray = headerProperties.map(
        (property: string) => row[property]
      );
      worksheet.addRow(rowArray);
    });
    const type =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    try {
      const data = await workbook.xlsx.writeBuffer();
      this.blob = new Blob([data], { type });
      this.generatingDialog.close();
      const successMessage =
        'Your report has been successfully generated and is ready for download.';
      this.successDialog = this.openDialog(successMessage, false, true);
    } catch (error) {
      this.openDialog('Cannot export data...', false, false);
    }
  }

  async downloadXls() {
    saveAs(this.blob, this.filename);
    this.successDialog.close();
  }
}
