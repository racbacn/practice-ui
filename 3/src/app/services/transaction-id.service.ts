import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TransactionIdService {
  private transactionId$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private userType$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() { }

  setTransactionId(transactionId: string) {
    this.transactionId$.next(transactionId);
  }

  getTransactionId(): BehaviorSubject<string> {
    return this.transactionId$;
  }

  setUserType(userType: string) {
    this.userType$.next(userType);
  }

  getUserType(): BehaviorSubject<string> {
    return this.userType$;
  }
}
