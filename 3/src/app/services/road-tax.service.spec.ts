import { TestBed } from '@angular/core/testing';

import { RoadTaxService } from './road-tax.service';

describe('RoadTaxService', () => {
  let service: RoadTaxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoadTaxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
