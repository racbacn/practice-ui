import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { APP_URL, pmgRegistrationList } from '../shared/constants/application.constants';

@Injectable({
  providedIn: 'root'
})
export class PmdListService {

  constructor(private httpService: HttpClient) { }

  getPmdListService(params: any){
    let apiURL = APP_URL + pmgRegistrationList;
    
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.approvedRegistrations;
      console.log("review response", response)
      return  response;
    }))
  }

  getSearchPmdList(params: any){
    let apiURL = APP_URL + pmgRegistrationList;
  
    const paramsObj = new HttpParams({ fromObject: params });
    return this.httpService.get(apiURL, {params}).pipe(map((res: any) => {
      let response = res.approvedRegistrations;
      console.log("search response", response)
      return  response;
    }))
  }
}
