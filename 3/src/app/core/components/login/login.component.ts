import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ManageUserService} from "../../../services/manage-user.service";
import {UserResponse} from "../../../interface/user-response";
import {LoginAuthenticationService} from "../../services/login-authentication.service";
import {SessionUtil} from "../../util/sessionUtil";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
    formdata!: FormGroup;
    errorMessage: boolean = false;
    message!: string;

    vqrOfficer: string = 'defaultUserVQROfficer';
    vlcOfficer: string = 'defaultUserVLCOfficer';
    vqrManager: string = 'defaultUserVQRManager';
    vlcManager: string = 'defaultUserVLCManager';

    userTypeOfficer: string = 'officer';
    userTypeManager: string = 'manager';


    constructor(private formBuilder: FormBuilder, private router: Router,
                private managerUserService: ManageUserService,
                private loginAuthenticationService: LoginAuthenticationService) {
    }

    ngOnInit(): void {
        this.formdata = this.formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required],
        });
    }

    login(): void {
        let username = this.formdata.value.username;
        let password = this.formdata.value.password;

        this.loginAuthenticationService.login(username, password).then((data: any) => {
            let responseData: any = data;

            if (responseData) {
                this.managerUserService.getUserId(responseData.username).subscribe(data => {
                    let userResponse: UserResponse = data;

                    if (userResponse) {
                        SessionUtil.setSessionStorage(SessionUtil.USER, JSON.stringify(userResponse))

                        console.log(userResponse)
                        if (userResponse.ltaTeamTypeKey === 1) {
                            if (userResponse.userTypeKey === 3) {
                                SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeOfficer)

                                this.router.navigate(['/vqr-officer']);
                                return;
                            }
                            if (userResponse.userTypeKey === 4) {
                                SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeManager)

                                this.router.navigate(["/vqr-manager"]);
                                return;
                            }
                        }

                        if (userResponse.ltaTeamTypeKey === 2) {
                            if (userResponse.userTypeKey === 3) {
                                SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeOfficer)

                                this.router.navigate(["/vlc-officer"])
                                return;
                            }
                            if (userResponse.userTypeKey === 4) {
                                SessionUtil.setSessionStorage(SessionUtil.USER_TYPE, this.userTypeManager)

                                this.router.navigate(["/vlc-manager"]);
                                return;
                            }
                        }
                    }
                })
            }
        }, (error) => {
            this.message = error.message
            this.errorMessage = true;
        });
    }

    loginSSO() {
        this.loginAuthenticationService.loginSSO()
    }
}
