import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PollNotificationService} from '../../../services/poll-notification.service';
import {TransactionIdService} from "../../../services/transaction-id.service";
import {NotificationResponse} from "../../../interface/notification-response";
import {Notification} from "../../../interface/notification";
import {UserResponse} from "../../../interface/user-response";
import {SessionUtil} from "../../util/sessionUtil";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    notificationsLength: number = 0;
    notificationList: Array<Notification> = [];

    vqrOfficer: string = 'defaultUserVQROfficer';
    vlcOfficer: string = 'defaultUserVLCOfficer';
    vqrManager: string = 'defaultUserVQRManager';
    vlcManager: string = 'defaultUserVLCManager';
    SURCHARGE: string = 'surcharge';
    PETROL: string = 'petrol';
    LATEFEE: string = 'latefee';
    ELECTRIC: string = 'electric';
    DIESEL: string = 'diesel';

    constructor(private router: Router, private pollNotificationService: PollNotificationService,
                private transactionIdService: TransactionIdService) {
    }

    ngOnInit(): void {
        this.onReceiveNotification();
    }

    onReceiveNotification() {
        this.pollNotificationService.getNotifications().subscribe(data => {
            let notificationResponse: NotificationResponse = data;

            this.notificationList = notificationResponse.notifications;

            this.notificationsLength = this.notificationList.length;
        })
    }

    goToPage() {
        SessionUtil.clearSession();
        this.router.navigate(['/login']);
    }

    goToApproval(path: string, id: string, refNum: string) {
        const full = `vlc-manager/dashboard/${path}/${id}/${refNum}`;
        this.router.navigateByUrl(full);
    }

    openNotification(notificationIndex: number) {
        let notification: Notification = this.notificationList[notificationIndex]
        //Set the transaction id in session and show the reject/approve button for hideAprvRjct
        sessionStorage.setItem('transactionid', notification.transactionId);
        sessionStorage.setItem('hideAprvRjct', 'notified');

        this.transactionIdService.setTransactionId(notification.transactionId);

        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))
        let username = userResponse.username;
        const subject = notification.subject.toLowerCase();
        const {transactionId, transactionRefNum} = notification;

        if (subject.includes(this.SURCHARGE)) {
            this.goToApproval(this.SURCHARGE, transactionId, transactionRefNum);
            return;
        } else if (subject.includes(this.PETROL)) {
            this.goToApproval(this.PETROL, transactionId, transactionRefNum);
            return;
        } else if (subject.includes(this.LATEFEE)) {
            this.goToApproval(this.LATEFEE, transactionId, transactionRefNum);
            return;
        } else if (subject.includes(this.ELECTRIC)) {
            this.goToApproval(this.ELECTRIC, transactionId, transactionRefNum);
            return;
        } else if (subject.includes(this.DIESEL)) {
            this.goToApproval(this.DIESEL, transactionId, transactionRefNum);
            return;
        } else if (subject.includes('road tax amendment')) {
            this.router.navigateByUrl('vlc-officer/amend-roadtax-formula');
            location.reload();
            return;
        }

        console.log("Notify response");
        console.log(notification);
        if (notification.transactionTypeKey == "1") {
            if (username == this.vqrManager) {
                this.router.navigate(['vqr-manager/dashboard-vqrmanager/review-vqr-manager']);
            } else if (username == this.vqrOfficer) {
                this.router.navigate(['vqr-officer/dashboard-vqrofficer/review']);
            }
        } else if (notification.transactionTypeKey == "2") {
            if (username == this.vqrManager) {
                this.router.navigate(['vqr-manager/dashboard-vqrmanager/review-pmd']);
            } else if (username == this.vqrOfficer) {
                this.router.navigate(['vqr-officer/dashboard-vqrofficer/review-pmd']);
            }
        } else if (notification.transactionTypeKey == "3") {
            if (username == this.vlcOfficer) {
                SessionUtil.setSessionStorage(SessionUtil.REFUND_REF_NO, notification.transactionId);
                this.router.navigate(['vlc-officer/certify-roadtax-refund/review-refund']);
            }
        } else {
            //will remove because there is no transactionType in notification for now
            this.router.navigate(['vqr-officer/viewall-registered-pmds/review-pmd']);
        }
    }

}
