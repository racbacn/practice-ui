import {Injectable} from '@angular/core';
import Amplify from '@aws-amplify/core';
import {Auth} from "@aws-amplify/auth";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {SessionUtil} from "../util/sessionUtil";

@Injectable({
    providedIn: 'root',
})
export class LoginAuthenticationService {
    constructor(private httpClient: HttpClient) {
        Amplify.configure({
            Auth: environment.auth
        })
    }

    login(username: string, password: string) {
        return Auth.signIn(username, password);
    }

    logout() {
        SessionUtil.clearSession();
        return Auth.signOut();
    }

    loginSSO() {
        window.location.assign(environment.loginUrl);
    }

    public logoutUserFromCognito(): Observable<any> {
        SessionUtil.clearSession();
        return this.httpClient.get<any>(environment.logoutUrl);
    }

}
