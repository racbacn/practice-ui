import { TestBed } from '@angular/core/testing';

import { DateOverlapValidation } from './date-overlap-validation.service';

describe('DateOverlapValidation', () => {
  let service: DateOverlapValidation;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DateOverlapValidation);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
