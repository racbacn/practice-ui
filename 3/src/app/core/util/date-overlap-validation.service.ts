import { Injectable } from "@angular/core";
import * as moment from "moment";
import { extendMoment } from "moment-range";

export class DateOverlapValidation {
  constructor() {}

  public static checkOverlapDates(dateRanges: any) {
    const moments = extendMoment(moment);
    const data = dateRanges;
    console.log("dates", dateRanges);
    let start: any[] = [];

    dateRanges.forEach((e: any) => {
      let d;
      if(dateRanges.includes("vehicleRegistrationTo") && dateRanges.includes("vehicleRegistrationFrom")){
        d = [
          moment(e.effectiveDate).format("MM-DD-YYYY"),
          moment(e.endDate).format("MM-DD-YYYY"),
          moment(e.vehicleRegistrationFrom).format("MM-DD-YYYY"),
          moment(e.vehicleRegistrationTo).format("MM-DD-YYYY")
        ];
      }else{
        d = [
          moment(e.effectiveDate).format("MM-DD-YYYY"),
          moment(e.endDate).format("MM-DD-YYYY"),
        ];
      }
      console.log("dates", d);
      start.push(d);
      return;
    });

    let dates = false;
    let i = 0;
    while (!dates && i < start.length - 1) {
      let seg1 = start[i];
      let seg2 = start[i + 1];

  
      let range1 = moments.range(moment(seg1[0]), moment(seg1[1]));
      let range2 = moments.range(moment(seg2[0]), moment(seg2[1]));
      if (range1.overlaps(range2)) {
        dates = true;
      }
      i++;
    }
    console.log("overlap>>", dates);
    return dates;
  }
}
