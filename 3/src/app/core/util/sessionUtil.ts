export class SessionUtil {
    public static readonly TOKEN = "TOKEN";
    public static readonly USER = "USER";
    public static readonly USER_TYPE = "USER_TYPE";
    public static readonly REFUND_REF_NO = "REFUND_REF_NO";

    public static setSessionStorage(key: string, value: string) {
        sessionStorage.setItem(key, value);
    }

    public static getSessionStorage(key: string) : string | null{
        return sessionStorage.getItem(key)
    }

    public static clearSession() {
        sessionStorage.clear();
    }
}

