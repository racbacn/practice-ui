import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateformat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var date = value.getDate();
    var month = value.toLocaleString('default', { month: 'long' });
    var year = value.getFullYear();
    var day = value.toLocaleString('en-us', {  weekday: 'long' });
    var greetdate = date + ' ' + month + ' ' + year + ',' + ' ' + day;

    return greetdate;
  }

}
