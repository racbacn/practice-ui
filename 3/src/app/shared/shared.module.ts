import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { SamplePipe } from './pipes/sample.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { DateFormatPipe } from './pipes/dateformat.pipe';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SamplePipe,
    DateFormatPipe
  ],
  exports: [
    SamplePipe,
    DateFormatPipe,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
