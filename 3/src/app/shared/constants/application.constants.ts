export const LOGOICON = 'manju';
export const APP_URL = 'https://istio-ingressgateway-istio-system.apps.a6067fe2802840199acec1f28c4573a1.vehsystempoc.com';



//Endoints
export const officerdahsboardEndpoint = '/vrls-utility-service/vehicleRegistrations'
export const managerdahsboardEndpoint = '/vrls-utility-service/vehicleRegistrations'
export const dueDatesEndpoint = '/manage-vehicle-service/vehicleRegistrations/dueDates/1'
export const reviewEndpoint = '/manage-vehicle-service/vehicleDetails/'
export const approveEndpoint = '/manage-vehicle-service/approveVehicleRegistration'
export const reviewPmdEndpoint = '/manage-pmd-service/pmdDetails/'
export const approvePmdOfficerEndpoint = '/manage-pmd-service/officer/approvePMDRegistration'
export const approvePmdManagerEndpoint = '/manage-pmd-service/manager/approvePMDRegistration'
export const vehicleRegistrationList = '/vrls-utility-service/approvedRegistrations'
export const pmgRegistrationList = '/vrls-utility-service/approvedRegistrations'
export const officersByManager = '/manage-user-service/user/officersByManager'
export const baseRoadTax = '/manage-roadtax-service/get/RoadTaxValues'
export const saveBaseFee = '/manage-roadtax-service/petrol/saveBaseFee'
export const managerUserId = '/manage-user-service/user/managerOfOfficer/'
export const saveRebate = '/manage-roadtax-service/petrol/saveRebate'
export const saveDieselRebate = '/manage-roadtax-service/diesel/saveRebate'
export const saveAddCharge = '/manage-roadtax-service/petrol/saveAdditionalCharge'
export const saveAddChargeDiesel  = '/manage-roadtax-service/diesel/saveAdditionalCharge'
export const saveSpecialTax = '/manage-roadtax-service/diesel/saveSpecialTax'
export const saveBaseFeeDraft = '/manage-roadtax-service/petrol/baseFee/draft'
export const saveAddChargeDraft = '/manage-roadtax-service/petrol/additionalCharge/draft'
export const saveRebateDraft = '/manage-roadtax-service/petrol/rebate/draft'
export const saveAddChargeDraftDiesel = '/manage-roadtax-service/diesel/additionalCharge/draft'
export const saveRebateDraftDiesel = '/manage-roadtax-service/diesel/rebate/draft'
export const saveSpecialTaxDraft = '/manage-roadtax-service/diesel/specialTax/draft'
export const removeDraft = '/manage-roadtax-service/removeRoadTaxDraft'
export const roadTaxDashboard = '/vrls-utility-service/roadTaxTransactions'
export const officerUserId = "3"
export const PENDING_OFFICER_REVIEW = "5"

export const USER_TYPES = {
    PENDING_MANAGER_REVIEW: "103",
    PENDING_OFFICER_REVIEW: "5",
    MANAGER_REJECTED: "102"
}

export const VEHICLE_TYPE = {
    pmd: 2,
    vehicle: 1
}

export const REQUEST_APP_ID = {
    approvePMD: "VRLS_PMD_Approval"
}

export const VLC_USERS_ID = {
    userId: 1,
    managerId: 3
}

export const VALUE_CATEGORY = {
    specialTax: "specialTax",
    baseFee: "baseFee",
    rebates: "rebate",
    addCharge: "addCharge"
}

export const AMENDMENT_STATUS = {
    draft: "Draft",
    managerRejected: "Manager Rejected",
    completed: "Completed",
    pendingReview: "Pending Manager Review"
}

