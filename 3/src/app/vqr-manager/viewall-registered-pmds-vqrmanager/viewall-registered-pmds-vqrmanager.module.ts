import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ViewallRegisteredPmdsVqrmanagerRoutingModule } from './viewall-registered-pmds-vqrmanager-routing.module';
import { ViewallRegisteredPmdsVqrmanagerComponent } from './viewall-registered-pmds-vqrmanager.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';


@NgModule({
  declarations: [
    ViewallRegisteredPmdsVqrmanagerComponent
  ],
  imports: [
    CommonModule,
    ViewallRegisteredPmdsVqrmanagerRoutingModule,
    WidgetsModule,
    SharedModule
  ]
})
export class ViewallRegisteredPmdsVqrmanagerModule { }
