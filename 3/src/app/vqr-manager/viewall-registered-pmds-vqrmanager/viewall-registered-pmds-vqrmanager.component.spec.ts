import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewallRegisteredPmdsVqrmanagerComponent } from './viewall-registered-pmds-vqrmanager.component';

describe('ViewallRegisteredPmdsVqrmanagerComponent', () => {
  let component: ViewallRegisteredPmdsVqrmanagerComponent;
  let fixture: ComponentFixture<ViewallRegisteredPmdsVqrmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewallRegisteredPmdsVqrmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewallRegisteredPmdsVqrmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
