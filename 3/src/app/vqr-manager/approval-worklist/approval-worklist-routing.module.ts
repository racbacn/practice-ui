import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewPmdVqrManagerComponent } from '../review-pmd-vqr-manager/review-pmd-vqr-manager.component';
import { ApprovalWorklistComponent } from './approval-worklist.component';

const routes: Routes = [{ path: '', component: ApprovalWorklistComponent },
{
  path: 'review-pmd',
  component: ReviewPmdVqrManagerComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalWorklistRoutingModule { }
