import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApprovalWorklistRoutingModule } from './approval-worklist-routing.module';
import { ApprovalWorklistComponent } from './approval-worklist.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';


@NgModule({
  declarations: [
    ApprovalWorklistComponent
  ],
  imports: [
    CommonModule,
    ApprovalWorklistRoutingModule,
    WidgetsModule
  ]
})
export class ApprovalWorklistModule { }
