import {DatePipe} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {filter} from 'rxjs/operators';
import {DashboardService} from 'src/app/services/dashboard.service';
import {ReviewService} from 'src/app/services/review-service.service';
import {SharedService} from 'src/app/services/shared.service';
import {managerUserId, REQUEST_APP_ID, USER_TYPES, VEHICLE_TYPE} from 'src/app/shared/constants/application.constants';
import {ConfirmDialogComponent} from 'src/app/widgets/confirm-dialog/confirm-dialog.component';
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
@Component({
  selector: 'app-approval-worklist',
  templateUrl: './approval-worklist.component.html',
  styleUrls: ['./approval-worklist.component.scss']
})
export class ApprovalWorklistComponent implements OnInit {
  userType!: string;
  isManager: boolean = false;
  transactions!: any;
  url = "review-pmd"
  datePipe: DatePipe = new DatePipe("en-US");
  transactionId: any;
  confirmDialogRef!: MatDialogRef<ConfirmDialogComponent>;
  approve: any;
  pageNo = 0;
  pageSize = 10;
  sortVal: any;

  constructor(private dashBoardService: DashboardService,
    private reviewService: ReviewService,
    private sharedService: SharedService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.userType = "approvalList";
    this.getDetails();


  }

  getDetails() {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let date = new Date();
    let params = {
      dueDate: date.toISOString,
      assignedUserId: userResponse.userId,
      statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
      vehicleTypeKey: VEHICLE_TYPE.pmd
    }
    this.dashBoardService.getManagerDashBoard(params).subscribe(res => {
      this.transactions = res;

      console.log("details", this.transactions)
    })
  }


  goToViewDetails(event: any) {
    for (let item of this.transactions) {
      if (item.transactionRefNum === event) {
        this.transactionId = item.sourceTransactionId;
        sessionStorage.setItem('transactionid', item.sourceTransactionId);
        sessionStorage.setItem('hideAprvRjct', "approvalWorkList");
      }
    }
  }



  openAddFileDialog(event: any) {
    let transactionId;
    for (let item of this.transactions) {
      if (item.transactionRefNum === event) {
        transactionId = item.sourceTransactionId;
      }
    }
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let finalObj = {} as any;
    let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
    console.log(timeNow)
    finalObj = {
      "requestHeader": {
        "requestAppId": REQUEST_APP_ID.approvePMD,
        "requestDateTime": timeNow,
        "userId": userResponse.userId,
        "userTypeKey": userResponse.userTypeKey
      },
      "approvingLTAManagerUserId": userResponse.userId,
      "transactionId": transactionId
    }

    this.confirmDialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { pageValue: event }
    });
    console.log("transaction", event)
    this.confirmDialogRef.afterClosed().pipe(filter(res => res)
    ).subscribe(res => {
      this.approve = res;
      if (this.approve === "approve") {
        this.reviewService.approvePmdRegManager(finalObj).subscribe(res => {
          console.log(res);
          let tempTransaction = {
            "transactionId": res.transactionId
          }; 
          this.sharedService.sendApproveClick(tempTransaction);

        });
      }
      console.log("approveee", this.approve)
    })
  }

  sortList(event: any) {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    this.pageNo = 0;
    let date = new Date();
    this.sortVal = event;
    let params = {
      dueDate: date.toISOString,
      assignedUserId: userResponse.userId,
      pageSize: event,
      statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: this.pageNo,
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;

      console.log("details", this.transactions)
    })
  }

  onPageMove(pageNumber: number) {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let date = new Date();
    let params = {
      dueDate: date.toISOString,
      assignedUserId: userResponse.userId,
      statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: pageNumber,
      pageSize: this.pageSize
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      if (res.length === 0 || res === undefined) {
        return alert("No Data");
      } else {
        this.transactions = res;
        this.pageNo = pageNumber;
      }

      console.log("pagination", this.transactions)
    },(error) => {
      if (error?.error?.code === 500) {
        return alert(error.error.message);
      }
      alert('Cannot get transactions.');
    }
    )
  }
}
