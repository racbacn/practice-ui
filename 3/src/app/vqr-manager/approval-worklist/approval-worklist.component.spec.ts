import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalWorklistComponent } from './approval-worklist.component';

describe('ApprovalWorklistComponent', () => {
  let component: ApprovalWorklistComponent;
  let fixture: ComponentFixture<ApprovalWorklistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovalWorklistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalWorklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
