import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardVqrmanagerRoutingModule } from './dashboard-vqrmanager-routing.module';
import { DashboardVqrmanagerComponent } from './dashboard-vqrmanager.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    DashboardVqrmanagerComponent
  ],
  imports: [
    CommonModule,
    DashboardVqrmanagerRoutingModule,
    WidgetsModule,
    SharedModule
  ]
})
export class DashboardVqrmanagerModule { }
