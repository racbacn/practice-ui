import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import * as moment from "moment";
import { managerUserId, officerUserId, USER_TYPES, VEHICLE_TYPE } from 'src/app/shared/constants/application.constants';
import { Transactions } from 'src/app/interface/transactions';
import { TransactionDueDates } from 'src/app/interface/due-dates';
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
@Component({
  selector: 'app-dashboard-vqrmanager',
  templateUrl: './dashboard-vqrmanager.component.html',
  styleUrls: ['./dashboard-vqrmanager.component.scss']
})
export class DashboardVqrmanagerComponent implements OnInit {
  mobileMargin: boolean = true;
  transactions!: Transactions[];
  dueDates!: TransactionDueDates;
  url = "review-pmd";
  username: string = "Peter";
  manager!: string;
  officer!: string;
  userType!: string;
  pageNo = 0;
  pageSize!: string;
  disableNext!: boolean;
  officesUserId: any;


  constructor(private dashBoardService: DashboardService) { }


  ngOnInit(): void {
    this.getDetails();
    this.userType = SessionUtil.getSessionStorage(SessionUtil.USER_TYPE)!;
    console.log("userType", this.userType)
    this.getDueDates();
  }

  getDetails() {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let officerParams: any;
    let date = new Date();
    let pathParams = {
      managerUserId: userResponse.userId
    }
    this.dashBoardService.getOfficersByManager(pathParams).subscribe(res => {
      this.officesUserId = res;
      for (let officer of this.officesUserId) {
        if (this.officesUserId.length > 0) {
          officerParams = officer
        }
      }

      let assignedUserId = userResponse.userId + ',' + officerParams;
      let statusTypeKey = USER_TYPES.PENDING_MANAGER_REVIEW + ',' + USER_TYPES.PENDING_OFFICER_REVIEW
      console.log("assignedUserId", assignedUserId);
      let params = {
        dueDate: date.toISOString,
        assignedUserId: assignedUserId,
        statusTypeKey: statusTypeKey,
        pageSize: this.pageSize === undefined ? "" : this.pageSize
      }
      this.dashBoardService.getManagerDashBoard(params).subscribe(res => {
        this.transactions = res;
      })
    })

  }

  goToViewDetails(event: any) {
    let resp: any;
    for (let item of this.transactions) {
      if (item.transactionRefNum === event) {
        resp = item.sourceTransactionId;
        sessionStorage.setItem("transactionid", resp)
        if (item.transactionType === "Register Vehicle") {
          this.url = "review";
        }
      }
    }
  }

  getDueDates() {
    this.dashBoardService.getDueDates().subscribe(res => {
      this.dueDates = res;
      console.log(this.dueDates)
    })
  }

  filterList(event: any) {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))
    
    let date: any;
    let vehicleScheme: any;
    let vehicleTypeKey: any;
    let params: any;
    switch (event) {
      case "today": {
        date = new Date();
      }
        break;
      case 'tomorrow': {
        let dateToday = new Date();
        date = new Date(dateToday.setDate(dateToday.getDate() + 1));
      }
        break;
      case 'nextweek': {
        let nextweek = new Date();
        date = new Date(nextweek.setDate(nextweek.getDate() + 7));
      }
        break;
      case 'vehiclescheme': {
        vehicleScheme = 2;
      }
        break;
      case 'pmdApproval': {
        vehicleTypeKey = 2;
      }
        break;
      case 'default': {
        this.getDetails();
      }
        break;
      default:

    }
    if ((vehicleScheme || vehicleTypeKey) === undefined) {
      params = {
        dueDateRange: moment(date).format("YYYY-MM-DD"),
        assignedUserId: userResponse.userId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    } else {
      params = {
        assignedUserId: userResponse.userId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: USER_TYPES.PENDING_OFFICER_REVIEW,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }


  sortList(event: any) {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let date = new Date();
    this.pageSize = event;
 
    let params = {
      dueDate: date.toISOString,
      assignedUserId: userResponse.userId,
      pageSize: this.pageSize === undefined ? "" : this.pageSize,
      statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: this.pageNo === undefined ? "" : this.pageNo,
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  onPageMove(pageNumber: number) {
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

    let date = new Date();
    let params = {
      dueDate: date.toISOString,
      assignedUserId: userResponse.userId,
      statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: pageNumber,
      pageSize: this.pageSize === undefined ? "" : this.pageSize,
    }

    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      if (res.length === 0 || res === undefined) {
        return alert("No Data");
      } else {
        this.transactions = res;
        this.pageNo = pageNumber;
      }

      console.log("pagination", this.transactions)
    }, (error) => {
      if (error?.error?.code === 500) {
        return alert(error.error.message);
      }
      alert('Cannot get transactions.');
    }
    )
  }
}
