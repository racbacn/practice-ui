import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVqrmanagerComponent } from './dashboard-vqrmanager.component';

describe('DashboardVqrmanagerComponent', () => {
  let component: DashboardVqrmanagerComponent;
  let fixture: ComponentFixture<DashboardVqrmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardVqrmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVqrmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
