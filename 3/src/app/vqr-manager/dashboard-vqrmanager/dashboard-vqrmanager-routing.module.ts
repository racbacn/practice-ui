import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardVqrmanagerComponent } from './dashboard-vqrmanager.component';
import { ReviewVqrManagerComponent } from '../review-vqr-manager/review-vqr-manager.component';
import { ReviewPmdVqrManagerComponent } from '../review-pmd-vqr-manager/review-pmd-vqr-manager.component';

const routes: Routes = [
  { path: '', component: DashboardVqrmanagerComponent },
  { path: 'review', component: ReviewVqrManagerComponent },
  { path: 'review-pmd', component: ReviewPmdVqrManagerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardVqrmanagerRoutingModule { }
