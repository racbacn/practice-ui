import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReviewPmdVqrManagerRoutingModule } from './review-pmd-vqr-manager-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { ReviewPmdVqrManagerComponent } from './review-pmd-vqr-manager.component';

@NgModule({
  declarations: [
    ReviewPmdVqrManagerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    WidgetsModule,
    ReviewPmdVqrManagerRoutingModule
  ]
})
export class ReviewPmdVqrManagerModule { }
