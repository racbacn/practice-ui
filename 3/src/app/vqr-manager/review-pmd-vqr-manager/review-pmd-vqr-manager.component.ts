import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpConfigService } from 'src/app/services/http-config.service';
import { serverUrl } from 'src/app/constants/sharedValue';
import { TransactionIdService } from "../../services/transaction-id.service";
import { SharedService } from '../../services/shared.service';
import { DatePipe, Location } from '@angular/common';
import { ReviewService } from '../../services/review-service.service'
import { PMDRequest } from 'src/app/interface/pmd-request';
import { GetFilesService } from 'src/app/services/get-files.service';
import { managerUserId, REQUEST_APP_ID } from 'src/app/shared/constants/application.constants';
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
@Component({
  selector: 'app-review-pmd-vqr-manager',
  templateUrl: './review-pmd-vqr-manager.component.html',
  styleUrls: ['./review-pmd-vqr-manager.component.scss']
})
export class ReviewPmdVqrManagerComponent implements OnInit {
     //Create the label for the Review Page
     labelExpansionDetails = [
        { headerName: 'OWNER PARTICULARS', labelDetails: [''], labelInfo: ['Owner ID Type', 'NRIC No. / FIN / ACRA No. / UEN', 'Birth Date','Owner Name', 'Handphone No', 'Email Address', 'Address', 'Unit No', 'Postal Code',], },
        { headerName: 'E-SCOOTER PARTICULARS', labelDetails: [''], labelInfo: ['Make', 'Model', '*No. of Wheels', '*Colour', 'Seat','chk_weightDeclaration', 'chk_speedDeclaration', 'chk_widthDeclaration', 'chk_certifiedSafety'], },
        { headerName: 'ATTACHMENT', labelDetails: [''], labelInfo: ['Photo of E-scooter', 'E-scooter Safety Standard Sticker'], },
        { headerName: 'AMOUNT PAYABLE', labelDetails: [''], labelInfo: [], },
    ];
    
    //Mapping data to match incoming Data key with the element ID in html
    labelMapIncomingDataHTML: any = {
        "OWNER PARTICULARS": "ownerDetails",
        "E-SCOOTER PARTICULARS": "pmdDetails",
        "PAYMENT SUMMARY": "paymentDetails",
        "Owner ID Type": "ownerIdType",
        "NRIC No. / FIN / ACRA No. / UEN": "ownerId",
        "Birth Date": "ownerDOB",
        "Owner Name": "ownerName",
        "Handphone No": "ownerContactNum",
        "Email Address": "ownerEmailAddress",
        "Address": "ownerAddress",
        "Unit No": "ownerAddressUnitNo",
        "Postal Code": "ownerPostalCode",
        "Make": "pmdMake",
        "Model": "pmdModel",
        "*No. of Wheels": "pmdWheelsNum",
        "*Colour": "pmdColour",
        "Seat": "hasSeat",
        "chk_weightDeclaration": "weightDeclaration",
        "chk_speedDeclaration": "speedDeclaration",
        "chk_widthDeclaration": "widthDeclaration",
        "chk_certifiedSafety": "certifiedSafety",
        "Safety Standard Serial No.": "safetyStandardSerialNum",
        "E-scooter Safety Standard Sticker": "safetyStandardStickerImage",
        "Photo of E-scooter": "pmdImage",
    };

    amountPayment = [
        {
            'key': 'E-scooter Registration Fee',
            'amt_Before_GST': 20.00,
            'gst_Amount': 0.00,
            'amt_After_GST': 20.00
        },
        {
            'key': 'E-scooter Processing Fee',
            'amt_Before_GST': 20.00,
            'gst_Amount': 0.00,
            'amt_After_GST': 0.00
        },
        {
            'key': 'Total Amount Payable',
            'amt_Before_GST': '',
            'gst_Amount': '',
            'amt_After_GST': 20.00
        },

    ];
    excludeInputTxt = ["chk_weightDeclaration", "chk_speedDeclaration", "chk_widthDeclaration", "chk_certifiedSafety"];

    chkBoxContent: any = {
        "chk_weightDeclaration": "Weight does not exceed 20kg (Weight <= 20kg)",
        "chk_speedDeclaration": "Speed does not exceed 25kph (Speed <= 25kph)",
        "chk_widthDeclaration": "Width does not exceed 700mm (Width <= 700mm)",
        "chk_certifiedSafety": "Certified to safety standard"
      }

    displayedColumns: string[] = ['key', 'amt_Before_GST', 'gst_Amount', 'amt_After_GST'];

    finalDataToSend: any = {};
    imageSrc!: string;
    imageSrc2!: string;
    apiUrl: string = serverUrl;
    diplomaScheme: Boolean = false;
    transactionId!: any;
    showApproveReject!: boolean;
    datePipe: DatePipe = new DatePipe("en-US");
      
    constructor(private route: ActivatedRoute, private httpConfigService: HttpConfigService,
        private transactionIdService: TransactionIdService,
        private reviewService: ReviewService, private sharedService: SharedService,
        private location: Location,
        private getFileService: GetFilesService) {
    }

    ngOnInit(): void {
        //To subscribe the event from the notification to reload the pmd details
        this.transactionIdService.getTransactionId().subscribe(res => {
            let data = sessionStorage.getItem('hideAprvRjct');
            this.transactionId = sessionStorage.getItem('transactionid');
            if(data === 'pmdVehicle'){
                this.showApproveReject = false;
            } else {
                this.showApproveReject = true;
            }
            this.loadVehicle(this.transactionId);

           
        });
        //For review all PMD, not going to the notification windows
        let data = sessionStorage.getItem('hideAprvRjct');
        if(data === 'pmdVehicle'){
            this.showApproveReject = false;
        } else {
            this.showApproveReject = true;
        }
        this.transactionId = sessionStorage.getItem('transactionid');
        console.log( "pmd",this.transactionId)
        this.loadVehicle(this.transactionId);

    }

    loadVehicle(transactionId: string) {
        this.reviewService.getPmdDetails(transactionId).subscribe((res: PMDRequest) => {
            console.log(res)
            if (res) {

                this.loadItems(res);
                this.imageSrc = this.getFileService.getPMDFile(res.pmdRegistrationDocuments[0].documentRefNum + '_' + res.pmdRegistrationDocuments[0].fileName)
                this.imageSrc2 = this.getFileService.getPMDFile(res.pmdRegistrationDocuments[1].documentRefNum + '_' + res.pmdRegistrationDocuments[1].fileName);
                this.labelExpansionDetails[2].labelDetails[0]= res.pmdRegistrationDocuments[0].fileName;
                this.labelExpansionDetails[2].labelDetails[1]= res.pmdRegistrationDocuments[1].fileName;
                console.log(this.labelExpansionDetails[2])
            }
        })
    }

    //Load the incoming data with the label and details
    loadItems(inputData: any): void {
        // labelExpansionDetails
        for (let i in this.labelExpansionDetails) {
            //Clear the information store in the labelDetails else it will keep pushing in data
            this.labelExpansionDetails[i].labelDetails = [];
            //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
            let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
            //Loop through label to find is there inputData to map to labelExpansionDetails
            this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
                if (typeof incomingDataFilter !== "undefined") {
                    //Check if there is data
                    if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
                        this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
                    } else {
                        //Set as - for no data
                        this.labelExpansionDetails[i].labelDetails.push('-' as never);
                    }
                }
            });
        }
        this.finalDataToSend = JSON.parse(JSON.stringify(inputData));
    }



    approveBtn() {
        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))
        
        let finalObj = {} as any;
        let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
        console.log(timeNow)
        finalObj = {
            "requestHeader": {
              "requestAppId": REQUEST_APP_ID.approvePMD,
              "requestDateTime": timeNow,
              "userId": userResponse.userId,
              "userTypeKey": userResponse.userTypeKey
            },
            "approvingLTAManagerUserId": userResponse.userId,
            "transactionId": this.transactionId
          }
        console.log(finalObj)
        this.reviewService.approvePmdRegManager(finalObj).subscribe(res => {
            console.log(res);
            let tempTransaction = {
                "transactionId": res.transactionId
            };
            this.sharedService.sendApproveClick(tempTransaction);

        })

    }

    rejectBtn() {
        console.log(this.labelExpansionDetails[0].labelDetails)
        let tempTransaction = {
            "transactionId": this.transactionId,
            "vehicleType": "PMD",
            "userType": 4,          //4 for vqr manager
        };
        console.log(tempTransaction)
        this.sharedService.sendRejectClick(tempTransaction);
    }

    goBackDashboard() {
        this.location.back();
    }

}
