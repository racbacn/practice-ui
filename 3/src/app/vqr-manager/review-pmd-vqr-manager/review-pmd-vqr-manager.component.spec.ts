import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPmdVqrManagerComponent } from './review-pmd-vqr-manager.component';

describe('ReviewPmdVqrManagerComponent', () => {
  let component: ReviewPmdVqrManagerComponent;
  let fixture: ComponentFixture<ReviewPmdVqrManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewPmdVqrManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPmdVqrManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
