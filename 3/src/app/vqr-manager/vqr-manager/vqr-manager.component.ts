import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from "@angular/cdk/layout";

@Component({
  selector: 'app-vqr-manager',
  templateUrl: './vqr-manager.component.html',
  styleUrls: ['./vqr-manager.component.scss']
})
export class VqrManagerComponent implements OnInit {
  userdetails: Array<any> = [{ name: "James Tan", id: "EID12345" }];

  sideNavNames: Array<any> = [
    {
      name: "DASHBOARD",
      icon: "home",
      link: "dashboard-vqrmanager",
    },
    {
      name: "APPROVAL WORK LIST",
      icon: "home",
      link: "approval-worklist-vqr",
    },
    {
      name: "VIEW ALL REGISTERED VEHICLES",
      icon: "home",
      link: "viewall-registered-vehicles-vqrmanager",
    },
    {
      name: "VIEW ALL REGISTERED E-SCOOTER/PAB",
      icon: "home",
      link: "viewall-registered-pmds-vqrmanager",
    }
  ];

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
  }

}
