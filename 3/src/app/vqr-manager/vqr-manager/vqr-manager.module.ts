import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { VqrManagerRoutingModule} from 'src/app/vqr-manager/vqr-manager/vqr-manager-routing.module';
import { VqrManagerComponent } from './vqr-manager.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
  declarations: [
    VqrManagerComponent                                                                           
  ],
  imports: [
    CommonModule,
    VqrManagerRoutingModule,
    MaterialModule,
    WidgetsModule,
    SharedModule,
    FormsModule,
    CoreModule
  ]
})
export class VqrManagerModule { }
