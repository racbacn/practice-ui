import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VqrManagerComponent } from './vqr-manager.component';

describe('VqrManagerComponent', () => {
  let component: VqrManagerComponent;
  let fixture: ComponentFixture<VqrManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VqrManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VqrManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
