import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VqrManagerComponent } from './vqr-manager.component';

const routes: Routes = [
  {
    path: '',
    component: VqrManagerComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard-vqrmanager' },
      {
        path: 'dashboard-vqrmanager',
        loadChildren: () =>
          import('../dashboard-vqrmanager/dashboard-vqrmanager.module').then(
            (m) => m.DashboardVqrmanagerModule
          ),
      },
      {
        path: 'approval-worklist-vqr',
        loadChildren: () =>
          import('../approval-worklist/approval-worklist.module').then(
            (m) => m.ApprovalWorklistModule
          ),
      },
      {
        path: 'viewall-registered-vehicles-vqrmanager',
        loadChildren: () =>
          import('../viewall-registered-vehicles-vqrmanager/viewall-registered-vehicles-vqrmanager.module').then(
            (m) => m.ViewallRegisteredVehiclesVqrmanagerModule
          ),
      },
      
      {
        path: 'viewall-registered-pmds-vqrmanager',
        loadChildren: () =>
          import(
            '../viewall-registered-pmds-vqrmanager/viewall-registered-pmds-vqrmanager.module'
          ).then((m) => m.ViewallRegisteredPmdsVqrmanagerModule),
      },
      {
        path: 'review-pmd',
        loadChildren: () =>
          import(
            '../review-pmd-vqr-manager/review-pmd-vqr-manager.module'
          ).then((m) => m.ReviewPmdVqrManagerModule),
      },
      {
        path: 'review',
        loadChildren: () =>
          import(
            '../review-vqr-manager/review-vqr-manager.module'
          ).then((m) => m.ReviewVqrManagerModule),
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VqrManagerRoutingModule {}


