import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewallRegisteredVehiclesVqrmanagerComponent } from './viewall-registered-vehicles-vqrmanager.component';

describe('ViewallRegisteredVehiclesVqrmanagerComponent', () => {
  let component: ViewallRegisteredVehiclesVqrmanagerComponent;
  let fixture: ComponentFixture<ViewallRegisteredVehiclesVqrmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewallRegisteredVehiclesVqrmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewallRegisteredVehiclesVqrmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
