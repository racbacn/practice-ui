import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewVqrManagerComponent } from '../review-vqr-manager/review-vqr-manager.component';
import { ViewallRegisteredVehiclesVqrmanagerComponent } from './viewall-registered-vehicles-vqrmanager.component';

const routes: Routes = [{ path: '', component: ViewallRegisteredVehiclesVqrmanagerComponent },
{
  path: 'review',
  component: ReviewVqrManagerComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewallRegisteredVehiclesVqrmanagerRoutingModule { }
