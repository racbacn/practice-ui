import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ViewallRegisteredVehiclesVqrmanagerRoutingModule } from './viewall-registered-vehicles-vqrmanager-routing.module';
import { ViewallRegisteredVehiclesVqrmanagerComponent } from './viewall-registered-vehicles-vqrmanager.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';

@NgModule({
  declarations: [
    ViewallRegisteredVehiclesVqrmanagerComponent
  ],
  imports: [
    CommonModule,
    ViewallRegisteredVehiclesVqrmanagerRoutingModule,
    WidgetsModule,
    SharedModule
  ]
})
export class ViewallRegisteredVehiclesVqrmanagerModule { }
