import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewVqrManagerComponent } from './review-vqr-manager.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WidgetsModule } from 'src/app/widgets/widgets.module';


@NgModule({
  declarations: [
    ReviewVqrManagerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    FlexLayoutModule,
    WidgetsModule
  ]
})
export class ReviewVqrManagerModule { }
