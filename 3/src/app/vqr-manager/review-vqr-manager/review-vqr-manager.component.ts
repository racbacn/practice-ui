import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpConfigService} from 'src/app/services/http-config.service';
import {serverUrl} from 'src/app/constants/sharedValue';
import {ConfidenceTable} from "../../interface/confidence-table";
import {VehicleRequest} from "../../interface/vehicle-request";
import {SharedService} from '../../services/shared.service';
import {DatePipe, Location} from '@angular/common';
import {ReviewService} from '../../services/review-service.service'
import {GetFilesService} from "../../services/get-files.service";
import {DocumentService} from "../../services/document.service";
import {TransactionDocument} from "../../interface/transaction-document";
import {TransactionDocumentsResponse} from "../../interface/transaction-documents-response";
import {DocumentConfidenceResponse} from "../../interface/document-confidence-response";
import {DocumentConfidence} from "../../interface/document-confidence";
import * as moment from "moment";
import {TransactionIdService} from "../../services/transaction-id.service";
import {DocumentResult} from "../../interface/document-result";
import {UpdateResultFlagRequest} from "../../interface/update-result-flag-request";
import {DocumentAttributeConfidence} from "../../interface/document-attribute-confidence";
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
@Component({
  selector: 'app-review-vqr-manager',
  templateUrl: './review-vqr-manager.component.html',
  styleUrls: ['./review-vqr-manager.component.scss']
})
export class ReviewVqrManagerComponent implements OnInit {
//Create the label for the Review Page
labelExpansionDetails = [
  {
      headerName: 'OWNER PARTICULARS',
      labelDetails: [''],
      labelInfo: ['Owner ID Type', 'NRIC No. / FIN / ACRA No. / UEN', 'Birth Date','Owner Name', 'Handphone No', 'Email Address', 'Address', 'Unit No', 'Postal Code'],
  },
  {
      headerName: 'VEHICLE PARTICULARS',
      labelDetails: [''],
      labelInfo: ['Vitas Approval Code', 'Vehicle Type', 'Vehicle Scheme', 'Vehicle Attachment 1', 'Vehicle Attachment 2', 'Vehicle Attachment 3', 'Vehicle Make', 'Vehicle Model', 'Propellant', 'Passenger Capacity', 'Engine Capacity', 'Power Rating', 'Maximum Power Output', 'No. Of Axles', 'Front/Rear Type Information', 'Unladen Weight', 'Maximum Unladen Weight', 'Emission Standard Code', 'CO2 Emission', 'CO Emission', 'HC Emission', 'NOx Emission', 'PM Emission', 'Primary Colour', 'Secondary Colour', 'Manufacturing Year', 'First Registration Date', 'Original Registration Date', 'Chasis No', 'Engine No', 'Motor No', 'Trailer Chassis No',],
  },
  {
      headerName: 'NEW REGISTRATION & ROAD TAX FEES',
      labelDetails: [''],
      labelInfo: ['Insurance Company', 'Insurance Cover Note No', 'Vehicle No Available', 'Open Market Value', 'Road Tax Months',],
  },
];

//Mapping data to match incoming Data key with the element ID in html
labelMapIncomingDataHTML: any = {
  "OWNER PARTICULARS": "ownerDetails",
  "VEHICLE PARTICULARS": "vehicleDetails",
  'NEW REGISTRATION & ROAD TAX FEES': 'roadTaxDetails',
  "Owner ID Type": "ownerIdType",
  "NRIC No. / FIN / ACRA No. / UEN": "ownerId",
  "Birth Date": "ownerDOB",
  "Owner Name": "ownerName",
  "Handphone No": "ownerContactNum",
  "Email Address": "ownerEmailAddress",
  "Address": "ownerAddressLine1",
  "Unit No": "ownerAddressLine2",
  "Postal Code": "ownerPostalCode",
  "Vitas Approval Code": "vitasApprovalCode",
  "Vehicle Type": "vehicleType",
  "Vehicle Scheme": "vehicleScheme",
  "Vehicle Attachment 1": "vehicleAttachment1",
  "Vehicle Attachment 2": "vehicleAttachment2",
  "Vehicle Attachment 3": "vehicleAttachment3",
  "Vehicle Make": "vehicleMake",
  "Vehicle Model": "vehicleModel",
  "Propellant": "propellant",
  "Passenger Capacity": "passengerCapacity",
  "Engine Capacity": "engineCapacity",
  "Power Rating": "powerRating",
  "Maximum Power Output": "maximumPowerOutput",
  "No. Of Axles": "numberOfAxles",
  "Front/Rear Type Information": "tyreInformation",
  "Unladen Weight": "unladenWeight",
  "Maximum Unladen Weight": "maximumUnladenWeight",
  "Emission Standard Code": "emissionStandardCode",
  "CO2 Emission": "carbonDioxideEmission",
  "CO Emission": "carbonMonoxideEmission",
  "HC Emission": "hydroCarbonEmission",
  "NOx Emission": "nitrosOxideEmission",
  "PM Emission": "pmEmission",
  "Primary Colour": "primaryColour",
  "Secondary Colour": "secondaryColour",
  "Manufacturing Year": "manufacturingYear",
  "First Registration Date": "firstRegistrationDate",
  "Original Registration Date": "originalRegistrationDate",
  "Chasis No": "chassisNumber",
  "Engine No": "engineNumber",
  "Motor No": "motorNumber",
  "Trailer Chassis No": "trailerChassisNumber",
  "Insurance Company": "insuranceCompany",
  "Insurance Cover Note No": "insuranceCoverNoteNumber",
  "Vehicle No Available": "vehicleNumberAvailable",
  "Open Market Value": "openMarketValue",
  "Road Tax Months": "roadTaxMonths",
};

diplomaticIdDataSource: Array<ConfidenceTable> = [
  {
      key: 'Ministry of Foreign Affairs',
      documentField: 'ID Title',
      approveCriteria: 'ID Title is “Ministry of Foreign Affairs” ?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'DIPLOMATIC',
      documentField: 'ID Sub-title',
      approveCriteria: 'ID Sub-title is “Diplomat” ?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'FIN',
      documentField: 'FIN',
      approveCriteria: 'Matches FIN number specified in Application? ',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Date of Issue',
      documentField: 'Date of Issue',
      approveCriteria: 'Application date is within Date of Issue and Date of Expiry range?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Date of Expiry',
      documentField: 'Date of Expiry',
      approveCriteria: 'Application date is within Date of Issue and Date of Expiry range?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  }
];

mfaApprovalLetterDataSource: Array<ConfidenceTable> = [
  {
      key: 'Registration Number',
      documentField: 'Registration No.',
      approveCriteria: 'Matches Vehicle Registration Application Number?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Make and model',
      documentField: 'Make and Model.',
      approveCriteria: 'Matches Vehicle Make and Model Specified in Application?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Year of Manufacture',
      documentField: 'Year of Manufacture',
      approveCriteria: 'Matches Year of Manufacture Specified in Application?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Chassis No',
      documentField: 'Chassis No.',
      approveCriteria: 'Matches Chassis No. Specified in Application?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Engine No',
      documentField: 'Engine No.',
      approveCriteria: 'Matches Engine No. Specified in Application?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  },
  {
      key: 'Colour',
      documentField: 'Colour',
      approveCriteria: 'Matches Vehicle Colour Specified in Application?',
      retrievedValue: '',
      confidenceScore: '',
      dacId: 0,
      result: 2
  }
];

documentResultList: Array<DocumentResult> = [{name: 'Yes', value: 1}, {name: 'No', value: 2}];

displayedColumns: string[] = ['documentField', 'approveCriteria', 'retrievedValue', 'confidenceScore', 'result'];

finalDataToSend: any = {};
imageSrc!: string;
imageSrc2!: string;
apiUrl: string = serverUrl;
diplomaScheme: Boolean = false;
transactionId!: any;
passingScore: number = 90;
likelihoodOfApproval: string = "0";
transactionDocuments!: Array<TransactionDocument>;
showApproveReject!: boolean;
updateResultFlagRequestID!: UpdateResultFlagRequest;
updateResultFlagRequestMfa!: UpdateResultFlagRequest;
datePipe: DatePipe = new DatePipe("en-US");

constructor(private route: ActivatedRoute, private httpConfigService: HttpConfigService,
          private reviewService: ReviewService, private sharedService: SharedService,
          private location: Location, private getFileService: GetFilesService,
          private documentService: DocumentService, private transactionIdService: TransactionIdService) {
}

ngOnInit(): void {
  this.transactionIdService.getTransactionId().subscribe(res => {
      let data = sessionStorage.getItem('hideAprvRjct');
      this.transactionId = sessionStorage.getItem('transactionid');
      if (data === 'dashboard' || data === 'notified' || data === 'approvalWorkList') {
          this.showApproveReject = true;
      } else if (data === 'registeredVehicle') {
          this.showApproveReject = false;
      }
    this.loadVehicle(this.transactionId);

  });

  let data = sessionStorage.getItem('hideAprvRjct');
  this.transactionId = sessionStorage.getItem('transactionid');
  if (data === 'dashboard' || data === 'notified') {
      this.showApproveReject = true;
  } else if (data === 'registeredVehicle') {
      this.showApproveReject = false;
  }

  this.loadVehicle(this.transactionId);
}

loadVehicle(id: string) {
  console.log("Transaction ID", this.transactionId)

  this.reviewService.getVehicleDetails(id).subscribe((res: VehicleRequest) => {
      console.log(res)
      if (res) {
          if (res?.vehicleDetails?.vehicleScheme === "Diplomatic") {
              console.log("HERE Diplmtc")
              this.loadDocuments(res);
              this.loadItems(res);
              this.diplomaScheme = true;
          } else {
              console.log("HERE normal")
              this.loadItems(res);
              this.showApproveReject = false;
              this.diplomaScheme = false;
          }

      }
  })
}

//Load the incoming data with the label and details
loadItems(inputData: any): void {
  // labelExpansionDetails
  for (let i in this.labelExpansionDetails) {
      //Clear the information store in the labelDetails else it will keep pushing in data
      this.labelExpansionDetails[i].labelDetails = [];
      //Get the labelInfo based on the header name e.g "OWNER PARTICULARS"
      let incomingDataFilter = inputData[this.labelMapIncomingDataHTML[this.labelExpansionDetails[i].headerName]];
      //Loop through label to find is there inputData to map to labelExpansionDetails
      this.labelExpansionDetails[i].labelInfo.forEach((labelinfo) => {
          if (typeof incomingDataFilter !== "undefined") {
              //Check if there is data
              if (incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]) {
                  this.labelExpansionDetails[i].labelDetails.push(incomingDataFilter[this.labelMapIncomingDataHTML[labelinfo]]);
              } else {
                  //Set as - for no data
                  this.labelExpansionDetails[i].labelDetails.push('-' as never);
              }
          }
      });
  }
  this.finalDataToSend = JSON.parse(JSON.stringify(inputData));
}

loadDocuments(vehicleRequest: VehicleRequest) {
  this.documentService.getTransactionDocuments(this.transactionId).subscribe(data => {
      let transactionDocumentsResponse: TransactionDocumentsResponse = data;

      this.transactionDocuments = transactionDocumentsResponse.transactionDocuments;

      if (this.transactionDocuments.length > 0) {
          this.getConfidenceScore(vehicleRequest);
      }
  });
}

getConfidenceScore(vehicleRequest: VehicleRequest) {
  this.documentService.getDocumentConfidence(this.transactionDocuments[0].documentId, this.transactionDocuments[1].documentId).subscribe(data => {
      let documentConfidenceResponse: DocumentConfidenceResponse = data;

      if (documentConfidenceResponse.documentConfidenceList.length > 0) {
          let documentConfidenceList: Array<DocumentConfidence> = documentConfidenceResponse.documentConfidenceList.sort((a: any, b: any) => a.documentId - b.documentId)

          this.likelihoodOfApproval = ((documentConfidenceList[0].documentConfidenceAvg + documentConfidenceList[1].documentConfidenceAvg) / 2).toFixed(2)

          documentConfidenceList.forEach(data => {
              let documentConfidence: DocumentConfidence = data;

              if (documentConfidence.documentTypeKey === 1) {
                  this.calculateIdResult(vehicleRequest, documentConfidence);
              }
              if (documentConfidence.documentTypeKey === 2) {
                  this.calculateMfaResult(vehicleRequest, documentConfidence);
              }
          })
      }
  })
}

calculateIdResult(vehicleRequest: VehicleRequest, documentConfidence: DocumentConfidence) {
  this.transactionDocuments.forEach(transactionDocument => {
      if (transactionDocument.documentRefNum === documentConfidence.documentRefNum) {
          this.imageSrc = this.getFileService.getFile(transactionDocument.documentRefNum + '_' + transactionDocument.fileName);
      }
  });

  console.log(documentConfidence)

  documentConfidence.fieldValueConfidence.forEach(data => {
      if (data.fieldLabel == this.diplomaticIdDataSource[0].key && data.confidenceScore >= this.passingScore) {
          this.diplomaticIdDataSource[0].retrievedValue = data.fieldValue;
          this.diplomaticIdDataSource[0].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.diplomaticIdDataSource[0].dacId = data.dacId;
          this.diplomaticIdDataSource[0].result = 1;
      }
      if (data.fieldLabel === this.diplomaticIdDataSource[1].key && data.confidenceScore >= this.passingScore) {
          this.diplomaticIdDataSource[1].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.diplomaticIdDataSource[1].retrievedValue = data.fieldValue;
          this.diplomaticIdDataSource[1].dacId = data.dacId;
          this.diplomaticIdDataSource[1].result = 1;
      }
      if (data.fieldLabel === this.diplomaticIdDataSource[2].key && data.confidenceScore >= this.passingScore) {
          this.diplomaticIdDataSource[2].retrievedValue = data.fieldValue;
          this.diplomaticIdDataSource[2].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.diplomaticIdDataSource[2].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.ownerDetails.ownerId) {
              this.diplomaticIdDataSource[2].result = 1;
          }
      }
      if (data.fieldLabel === this.diplomaticIdDataSource[3].key && data.confidenceScore >= this.passingScore) {
          this.diplomaticIdDataSource[3].retrievedValue = data.fieldValue;
          this.diplomaticIdDataSource[3].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.diplomaticIdDataSource[3].dacId = data.dacId;
          if (moment().isSameOrAfter(data.fieldValue)) {
              this.diplomaticIdDataSource[3].result = 1;
          }
      }
      if (data.fieldLabel === this.diplomaticIdDataSource[4].key && data.confidenceScore >= this.passingScore) {
          this.diplomaticIdDataSource[4].retrievedValue = data.fieldValue;
          this.diplomaticIdDataSource[4].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.diplomaticIdDataSource[4].dacId = data.dacId;
          if (moment().isSameOrBefore(data.fieldValue)) {
              this.diplomaticIdDataSource[4].result = 1;
          }
      }
  });

  this.updateResultFlagRequestID = {
      documentId: documentConfidence.documentId,
      documentAttributeConfidence: new Array<DocumentAttributeConfidence>()
  };
}

calculateMfaResult(vehicleRequest: VehicleRequest, documentConfidence: DocumentConfidence) {
  this.transactionDocuments.forEach(transactionDocument => {
      if (transactionDocument.documentRefNum === documentConfidence.documentRefNum) {
          this.imageSrc2 = this.getFileService.getFile(transactionDocument.documentRefNum + '_' + transactionDocument.fileName);
      }
  })

  documentConfidence.fieldValueConfidence.forEach(data => {
      if (data.fieldLabel == this.mfaApprovalLetterDataSource[0].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[0].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[0].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[0].dacId = data.dacId;
          this.mfaApprovalLetterDataSource[0].result = 1;
      }
      if (data.fieldLabel === this.mfaApprovalLetterDataSource[1].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[1].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[1].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[1].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.vehicleDetails?.vehicleMake) {
              this.mfaApprovalLetterDataSource[1].result = 1;
          }
      }
      if (data.fieldLabel === this.mfaApprovalLetterDataSource[2].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[2].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[2].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[2].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.vehicleDetails.manufacturingYear.toString()) {
              this.mfaApprovalLetterDataSource[2].result = 1;
          }

      }
      if (data.fieldLabel === this.mfaApprovalLetterDataSource[3].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[3].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[3].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[3].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.vehicleDetails.chassisNumber) {
              this.mfaApprovalLetterDataSource[3].result = 1;
          }
      }
      if (data.fieldLabel === this.mfaApprovalLetterDataSource[4].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[4].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[4].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[4].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.vehicleDetails.engineNumber) {
              this.mfaApprovalLetterDataSource[4].result = 1;
          }
      }
      if (data.fieldLabel === this.mfaApprovalLetterDataSource[5].key && data.confidenceScore >= this.passingScore) {
          this.mfaApprovalLetterDataSource[5].retrievedValue = data.fieldValue;
          this.mfaApprovalLetterDataSource[5].confidenceScore = data.confidenceScore.toFixed(2) + "%";
          this.mfaApprovalLetterDataSource[5].dacId = data.dacId;
          if (data.fieldValue === vehicleRequest?.vehicleDetails.primaryColour) {
              this.mfaApprovalLetterDataSource[5].result = 1;
          }
      }
  });

  this.updateResultFlagRequestMfa = {
      documentId: documentConfidence.documentId,
      documentAttributeConfidence: new Array<DocumentAttributeConfidence>()
  };
}

approveBtn() {
let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

  let finalObj = {} as any;
  let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
  console.log(timeNow)
  finalObj = {
      "requestHeader": {
          "requestAppId": "VRLS_Vehicle",
          "requestDateTime": timeNow,
          "userId": userResponse.userId,
          "userTypeKey": userResponse.userTypeKey
      },
  }
  //Declare the final object to be sent to the api approve
  finalObj["ownerDetails"] = {};
  //Acess the array index with the object name, e.g [1, "NRIC No. / FIN / ACRA No. / UEN"]
  for (let info of this.labelExpansionDetails[0].labelInfo.entries()) {
      finalObj["ownerDetails"][this.labelMapIncomingDataHTML[info[1]]] = this.labelExpansionDetails[0].labelDetails[info[0]]
  }
  //Send the transaction ID
  finalObj["transactionId"] = this.transactionId;
  finalObj["isApprovedWithScheme"] = true;
  //Future implementations
  // finalObj["userType"] = 3;
  console.log(finalObj)
  this.reviewService.approveVehicleReg(finalObj).subscribe(res => {
      console.log(res);
      let tempTransaction = {
          "transactionId": res.transactionId
      };
      this.sharedService.sendApproveClick(tempTransaction);

  });

  this.updateResultFlagRequestID.documentAttributeConfidence = new Array<DocumentAttributeConfidence>();
  this.updateResultFlagRequestMfa.documentAttributeConfidence = new Array<DocumentAttributeConfidence>();

  console.log(this.diplomaticIdDataSource)

  this.diplomaticIdDataSource.forEach((confidenceTable: ConfidenceTable) => {
      if (confidenceTable.dacId) {
          this.updateResultFlagRequestID.documentAttributeConfidence.push({
              dacId: confidenceTable.dacId,
              resultFlagKey: confidenceTable.result
          });
      }
  })

  this.mfaApprovalLetterDataSource.forEach((confidenceTable: ConfidenceTable) => {
      if (confidenceTable.dacId) {
          this.updateResultFlagRequestMfa.documentAttributeConfidence.push({
              dacId: confidenceTable.dacId,
              resultFlagKey: confidenceTable.result
          });
      }
  })

  console.log(this.updateResultFlagRequestID)
  console.log(this.updateResultFlagRequestMfa)


  this.documentService.updateResultFlag(this.updateResultFlagRequestID).subscribe(data => {
      console.log(data)
  })

  this.documentService.updateResultFlag(this.updateResultFlagRequestMfa).subscribe(data => {
      console.log(data)
  })
}

rejectBtn() {
  console.log(this.labelExpansionDetails[0].labelDetails)
  let tempTransaction = {
      "transactionId": this.transactionId,
      "vehicleType": "VEHICLE"
  };
  console.log(tempTransaction)
  this.sharedService.sendRejectClick(tempTransaction);
}

updateIDToggle(index: number, toggleValue: number) {
  this.diplomaticIdDataSource[index].result = toggleValue;
}

updateMfaToggle(index: number, toggleValue: number) {
  this.mfaApprovalLetterDataSource[index].result = toggleValue;
}

goBackDashboard() {
  this.location.back();
}
}

