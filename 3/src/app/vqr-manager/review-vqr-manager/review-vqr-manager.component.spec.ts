import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVqrManagerComponent } from './review-vqr-manager.component';

describe('ReviewVqrManagerComponent', () => {
  let component: ReviewVqrManagerComponent;
  let fixture: ComponentFixture<ReviewVqrManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewVqrManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVqrManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
