import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../app/material.module';


import { DashboardVlcmanagerRoutingModule } from './dashboard-vlcmanager-routing.module';
import { DashboardVlcmanagerComponent } from './dashboard-vlcmanager.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReviewVlcManagerModule } from './review-vlc-manager/review-vlc-manager.module';



@NgModule({
  declarations: [
    DashboardVlcmanagerComponent,
    // ReviewVehicleVqrOfficerComponent
  ],
  imports: [
    CommonModule,
    DashboardVlcmanagerRoutingModule,
    WidgetsModule,
    SharedModule,
    ReviewVlcManagerModule,
  ]
})
export class DashboardVlcmanagerModule { }
