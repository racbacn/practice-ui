import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVlcmanagerComponent } from './dashboard-vlcmanager.component';

describe('DashboardVlcmanagerComponent', () => {
  let component: DashboardVlcmanagerComponent;
  let fixture: ComponentFixture<DashboardVlcmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardVlcmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVlcmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
