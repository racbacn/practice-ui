import {Component, Input, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {Router} from '@angular/router';
import {DashboardService} from '../../services/dashboard.service';
import {managerUserId, USER_TYPES} from 'src/app/shared/constants/application.constants';
import {SessionUtil} from "../../core/util/sessionUtil";
import { UserResponse } from 'src/app/interface/user-response';
import * as moment from 'moment';


@Component({
    selector: 'app-dashboard-vlcmanager',
    templateUrl: './dashboard-vlcmanager.component.html',
    styleUrls: ['./dashboard-vlcmanager.component.scss']
})
export class DashboardVlcmanagerComponent implements OnInit {
    url= "petrol";
    mobileMargin: boolean = true;
    transactions!: any;
    pageSize!: string;
    pageNo = 0;
    userType!: string;
    SURCHARGE: string = 'surcharge';
    PETROL: string = 'petrol';
    LATEFEE: string = 'latefee';
    ELECTRIC: string = 'electric';
    DIESEL: string = 'diesel';
    dates = {
        "transactionDueDates": {
            "dueDateToday": 0,
            "dueDateTomorrow": 0,
            "dueDateWeek": 1,
            "dueDateMonth": 1
        }

    }

    constructor(private breakpointObserver: BreakpointObserver,
                private router: Router,
                private dashBoardService: DashboardService
    ) {

    }

    ngOnInit(): void {
        this.userType = SessionUtil.getSessionStorage(SessionUtil.USER_TYPE)!;
        this.getDetails();
    }


    getDetails() {
        let date = new Date();
        let userResponse: UserResponse = JSON.parse(
          <string>SessionUtil.getSessionStorage(SessionUtil.USER)
        ); 
        let params = {
          dueDate: date.toISOString,
          assignedUserId:  userResponse.userId,
          pageNo: this.pageNo,
          pageSize: this.pageSize === undefined ? "" : this.pageSize,
          statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
        }
        this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
            this.transactions = res;
            console.log("details", this.transactions.roadTaxAmendmentType)
        })
    }

    goToApproval(path: string) {
      this.url = path;
  }

    goToViewDetails(event: any) {
        let resp: any;
        for (let item of this.transactions) {
          if (item.transactionRefNum === event) {
            resp = item.transactionId;
           let roadTaxAmendmentType = item.roadTaxAmendmentType.toLowerCase();
            if (roadTaxAmendmentType.includes(this.SURCHARGE)) {
              this.url = this.SURCHARGE;
              return;
          } else if (roadTaxAmendmentType.includes(this.PETROL)) {
            this.url = this.PETROL;
              return;
          } else if (roadTaxAmendmentType.includes("late")) {
            this.url = this.LATEFEE;
              return;
          } else if (roadTaxAmendmentType.includes(this.ELECTRIC)) {
            this.url = this.ELECTRIC;
              return;
          } else if (roadTaxAmendmentType.includes(this.DIESEL)) {
            this.url = this.DIESEL;
              return;
          }

          console.log
            sessionStorage.setItem("transactionid", resp)
          }
        }
      }
      filterList(event: any) {
        let date: any;
        let vehicleScheme: any;
        let vehicleTypeKey: any;
        let params: any;
        let userResponse: UserResponse = JSON.parse(
          <string>SessionUtil.getSessionStorage(SessionUtil.USER)
        ); 
        switch (event) {
          case "today": {
            date = new Date();
          }
            break;
          case 'tomorrow': {
            let dateToday = new Date();
            date = new Date(dateToday.setDate(dateToday.getDate() + 1));
          }
            break;
          case 'nextweek': {
            let nextweek = new Date();
            date = new Date(nextweek.setDate(nextweek.getDate() + 7));
          }
            break;
          case 'weeklater': {
            let nextweek = new Date();
            date = new Date(nextweek.setDate(nextweek.getDate() + 14));
          }
            break;
          case 'certifyRefunds': {
            vehicleScheme = 2;
          }
            break;
          case 'roadTax': {
            vehicleTypeKey = 2;
          }
            break;
          case 'default': {
            this.getDetails();
          }
            break;
          default:
        }
        if ((vehicleScheme || vehicleTypeKey) === undefined) {
          params = {
            dueDateRange: moment(date).format("YYYY-MM-DD"),
            assignedUserId: userResponse.userId,
            vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
            statusTypeKey: userResponse.userTypeKey,
            vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
          }
        } else {
          params = {
            assignedUserId: userResponse.userId,
            vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
            statusTypeKey: userResponse.userTypeKey,
            vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
          }
        }
        this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
          this.transactions = res;
        })
      }
    
      sortList(event: any) {
        let date = new Date();
        this.pageSize = event;
        let userResponse: UserResponse = JSON.parse(
          <string>SessionUtil.getSessionStorage(SessionUtil.USER)
        ); 
        let params = {
          assignedUserId:  userResponse.userId,
          dueDate: date.toISOString,
          pageSize: this.pageSize,
          statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
          pageNo: this.pageNo === undefined ? "" : this.pageNo,
        }
        this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
          this.transactions = res;
        })
      }
    
      onPageMove(pageNumber: number) {
        let date = new Date();
        let userResponse: UserResponse = JSON.parse(
          <string>SessionUtil.getSessionStorage(SessionUtil.USER)
        ); 
        let params = {
          assignedUserId:  userResponse.userId,
          dueDate: date.toISOString,
          statusTypeKey: USER_TYPES.PENDING_MANAGER_REVIEW,
          pageNo: pageNumber,
          pageSize: this.pageSize === undefined ? "" : this.pageSize,
        }
    
    
          this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
            if (res.length === 0 || res === undefined) {
              return alert("No Data");
            } else {
              this.transactions = res;
              this.pageNo = pageNumber;
            }
          }, (error) => {
            if (error?.error?.code === 500) {
              return alert(error.error.message);
            }
        alert('Cannot get transactions.');
          }
          )
      }
}
