import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardVlcmanagerComponent } from './dashboard-vlcmanager.component';
import { PetrolVehiclesComponent } from 'src/app/widgets/approve-amended-road-tax/petrol-vehicles/petrol-vehicles.component';

import { SurchargeComponent } from 'src/app/widgets/approve-amended-road-tax/surcharge/surcharge.component';
import { DieselVehiclesComponent } from 'src/app/widgets/approve-amended-road-tax/diesel-vehicles/diesel-vehicles.component';
import { ElectricVehiclesComponent } from 'src/app/widgets/approve-amended-road-tax/electric-vehicles/electric-vehicles.component';
import { LateFeesComponent } from 'src/app/widgets/approve-amended-road-tax/late-fees/late-fees.component';

const routes: Routes = [
  {path: '', component: DashboardVlcmanagerComponent},
  {path: 'petrol', component: PetrolVehiclesComponent},
  {path: 'diesel', component: DieselVehiclesComponent},
  {path: 'surcharge', component: SurchargeComponent},
  {path: 'electric', component: ElectricVehiclesComponent},
  {path: 'latefee', component: LateFeesComponent},
  {
    path: 'latefee/:transactionId/:transactionRefNum',
    loadChildren: () =>
      import('../../widgets/approve-amended-road-tax/late-fees/late-fees.module').then(
        (m) => m.LateFeesModule
      ),
  },
  {
    path: 'petrol/:transactionId/:transactionRefNum',
    loadChildren: () =>
      import('../../widgets/approve-amended-road-tax/petrol-vehicles/petrol-vehicles.module').then(
        (m) => m.PetrolVehiclesModule
      ),
  },
  {
    path: 'surcharge/:transactionId/:transactionRefNum',
    loadChildren: () =>
      import('../../widgets/approve-amended-road-tax/surcharge/surcharge.module').then(
        (m) => m.SurchargeModule
      ),
  },
  {
    path: 'electric/:transactionId/:transactionRefNum',
    loadChildren: () =>
      import('../../widgets/approve-amended-road-tax/electric-vehicles/electric-vehicles.module').then(
        (m) => m.ElectricVehiclesModule
      ),
  },
  {
    path: 'diesel/:transactionId/:transactionRefNum',
    loadChildren: () =>
      import('../../widgets/approve-amended-road-tax/diesel-vehicles/diesel-vehicles.module').then(
        (m) => m.DieselVehiclesModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardVlcmanagerRoutingModule {}
