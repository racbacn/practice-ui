import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewVlcManagerComponent } from './review-vlc-manager.component';



@NgModule({
  declarations: [
    ReviewVlcManagerComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ReviewVlcManagerModule { }
