import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-vlc-manager',
  templateUrl: './review-vlc-manager.component.html',
  styleUrls: ['./review-vlc-manager.component.scss']
})
export class ReviewVlcManagerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    let id = sessionStorage.getItem("transactionid")
    console.log("transaction id", id)
  }

}
