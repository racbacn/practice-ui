import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVlcManagerComponent } from './review-vlc-manager.component';

describe('ReviewVlcManagerComponent', () => {
  let component: ReviewVlcManagerComponent;
  let fixture: ComponentFixture<ReviewVlcManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewVlcManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVlcManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
