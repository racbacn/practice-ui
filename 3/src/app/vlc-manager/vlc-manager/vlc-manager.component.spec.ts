import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VlcManagerComponent } from './vlc-manager.component';

describe('VlcManagerComponent', () => {
  let component: VlcManagerComponent;
  let fixture: ComponentFixture<VlcManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VlcManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VlcManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
