import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VlcManagerComponent } from './vlc-manager.component';

const routes: Routes = [
  {
    path: '',
    component: VlcManagerComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('../dashboard-vlcmanager/dashboard-vlcmanager.module').then(
            (m) => m.DashboardVlcmanagerModule
          ),
      },
      {
        path: 'certify-refund',
        loadChildren: () =>
          import('../certify-refund/certify-refund.module').then(
            (m) => m.CertifyRefundModule
          ),
      },
      {
        path: 'approval-worklist-vlc',
        loadChildren: () =>
          import('../approval-worklist/approval-worklist.module').then(
            (m) => m.ApprovalWorklistModule
          ),
      },
      {
        path: 'amend-roadtax-selfservice',
        loadChildren: () =>
          import(
            '../amend-roadtax-selfservice/amend-roadtax-selfservice.module'
          ).then((m) => m.AmendRoadtaxSelfserviceModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VlcManagerRoutingModule {}
