import {Component, OnInit} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';

@Component({
  selector: 'app-vlc-manager',
  templateUrl: './vlc-manager.component.html',
  styleUrls: ['./vlc-manager.component.scss']
})
export class VlcManagerComponent implements OnInit {

  userdetails: Array<any> = [{name: 'Joana Tan', id: 'EID12345'}];

  sideNavNames: Array<any> = [
    {
    name: "DASHBOARD", icon: "home",
    link: "dashboard"
  },
  {
    name: "APPROVAL WORKLIST", icon: "home", link: "approval-worklist-vlc"
  },
  {
    name: "AMEND ROAD TAX FORMULA VALUES(S)", icon: "home",
    link: "amend-roadtax-selfservice"
  },
  {
    name: "CERTIFY REFUND", icon: "home", link: "certify-refund"
  },
  
]

  // isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  // .pipe(
  //   map(result => result.matches),
  //   shareReplay()
  // );

constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
  }

}
