import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../app/material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { VlcManagerRoutingModule } from './vlc-manager-routing.module';
import { VlcManagerComponent } from './vlc-manager.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
  declarations: [
    VlcManagerComponent
  ],
  imports: [
    CommonModule,
    VlcManagerRoutingModule,
    MaterialModule,
    WidgetsModule,
    SharedModule,
    CoreModule
  ]
})
export class VlcManagerModule { }
