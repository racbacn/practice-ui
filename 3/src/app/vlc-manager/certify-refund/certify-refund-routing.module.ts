import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertifyRefundComponent } from './certify-refund.component';

const routes: Routes = [{ path: '',  component: CertifyRefundComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertifyRefundRoutingModule { }
