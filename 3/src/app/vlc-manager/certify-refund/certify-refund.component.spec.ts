import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertifyRefundComponent } from './certify-refund.component';

describe('CertifyRefundComponent', () => {
  let component: CertifyRefundComponent;
  let fixture: ComponentFixture<CertifyRefundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertifyRefundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertifyRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
