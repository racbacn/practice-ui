import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertifyRefundRoutingModule } from './certify-refund-routing.module';
import { CertifyRefundComponent } from './certify-refund.component';


@NgModule({
  declarations: [
    CertifyRefundComponent
  ],
  imports: [
    CommonModule,
    CertifyRefundRoutingModule
  ],
  exports: [
    CertifyRefundComponent
  ]
})
export class CertifyRefundModule { }
