import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendRoadtaxSelfserviceComponent } from './amend-roadtax-selfservice.component';

const routes: Routes = [{ path: '', component: AmendRoadtaxSelfserviceComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendRoadtaxSelfserviceRoutingModule { }
