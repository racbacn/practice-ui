import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmendRoadtaxSelfserviceComponent } from './amend-roadtax-selfservice.component';

describe('AmendRoadtaxSelfserviceComponent', () => {
  let component: AmendRoadtaxSelfserviceComponent;
  let fixture: ComponentFixture<AmendRoadtaxSelfserviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmendRoadtaxSelfserviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendRoadtaxSelfserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
