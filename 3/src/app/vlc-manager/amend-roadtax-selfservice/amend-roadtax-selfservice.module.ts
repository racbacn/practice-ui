import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AmendRoadtaxSelfserviceRoutingModule } from './amend-roadtax-selfservice-routing.module';
import { AmendRoadtaxSelfserviceComponent } from './amend-roadtax-selfservice.component';


@NgModule({
  declarations: [
    AmendRoadtaxSelfserviceComponent
  ],
  imports: [
    CommonModule,
    AmendRoadtaxSelfserviceRoutingModule
  ]
})
export class AmendRoadtaxSelfserviceModule { }
