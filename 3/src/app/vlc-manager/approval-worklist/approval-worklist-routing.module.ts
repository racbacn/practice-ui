import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApprovalWorklistComponent } from './approval-worklist.component';

const routes: Routes = [{ path: '', component: ApprovalWorklistComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalWorklistRoutingModule { }
