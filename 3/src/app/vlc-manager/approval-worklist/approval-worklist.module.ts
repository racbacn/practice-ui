import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApprovalWorklistRoutingModule } from './approval-worklist-routing.module';
import { ApprovalWorklistComponent } from './approval-worklist.component';


@NgModule({
  declarations: [
    ApprovalWorklistComponent
  ],
  imports: [
    CommonModule,
    ApprovalWorklistRoutingModule
  ]
})
export class ApprovalWorklistModule { }
