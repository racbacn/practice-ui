import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {CdkTableModule} from '@angular/cdk/table';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { DatePipe } from '@angular/common'
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreModule } from './core/core.module';
import { MaterialModule } from './material.module';
import { HttpConfigService } from './services/http-config.service';
import { DashboardService } from './services/dashboard.service';
import { ReviewService } from './services/review-service.service'
import { AuthGuardService } from './services/auth-guard.service';
import { SharedModule } from 'src/app/shared/shared.module';
import {JwtModule, JwtModuleOptions} from "@auth0/angular-jwt";
import {SessionUtil} from "./core/util/sessionUtil";


const JWT_Module_Options: JwtModuleOptions = {
    config: {
        tokenGetter: () => (SessionUtil.getSessionStorage(SessionUtil.TOKEN))
    }
};

@NgModule({
  declarations: [	
    AppComponent
   ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        CdkTableModule,
        MaterialModule,
        FlexLayoutModule,
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,

        JwtModule.forRoot(JWT_Module_Options)
    ],
  providers: [HttpConfigService,DashboardService, ReviewService,DatePipe,AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
