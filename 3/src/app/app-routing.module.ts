import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./core/components/login/login.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {ErrorComponent} from "./widgets/error/error.component";
import {TokenResolver} from "./services/token-resolver.service";

const routes: Routes = [
    {
        path: "login",
        component: LoginComponent,
        resolve: {
            access: TokenResolver
        }
    },
    {
        path: 'callback',
        redirectTo: '/login'
    },
    {path: "", redirectTo: "login", pathMatch: "full"},
    {path: "error", component: ErrorComponent},
    {
        path: "approval-worklist-vqr",
        loadChildren: () =>
            import("./vqr-manager/approval-worklist/approval-worklist.module").then(
                (m) => m.ApprovalWorklistModule
            ),
        canActivate: [AuthGuardService],
    },
    {
        path: "dashboard-vqrofficer",
        loadChildren: () =>
            import(
                "./vqr-officer/dashboard-vqrofficer/dashboard-vqrofficer.module"
                ).then((m) => m.DashboardVqrofficerModule),
        canActivate: [AuthGuardService],
    },
    {
        path: "dashboard-vqrmanager",
        loadChildren: () =>
            import(
                "./vqr-manager/dashboard-vqrmanager/dashboard-vqrmanager.module"
                ).then((m) => m.DashboardVqrmanagerModule),
        canActivate: [AuthGuardService],
    },
    {
        path: "viewall-registered-pmds-vqrmanager",
        loadChildren: () =>
            import(
                "./vqr-manager/viewall-registered-pmds-vqrmanager/viewall-registered-pmds-vqrmanager.module"
                ).then((m) => m.ViewallRegisteredPmdsVqrmanagerModule),
        canActivate: [AuthGuardService],
    },
    {
        path: "viewall-registered-vehicles-vqrmanager",
        loadChildren: () =>
            import(
                "./vqr-manager/viewall-registered-vehicles-vqrmanager/viewall-registered-vehicles-vqrmanager.module"
                ).then((m) => m.ViewallRegisteredVehiclesVqrmanagerModule),
        canActivate: [AuthGuardService],
    },
    {
        path: "vlc-manager",
        loadChildren: () =>
            import("./vlc-manager/vlc-manager/vlc-manager.module").then(
                (m) => m.VlcManagerModule
            ),
        canActivate: [AuthGuardService],
    },
    {
        path: "vlc-officer",
        loadChildren: () =>
            import("./vlc-officer/vlc-officer/vlc-officer.module").then(
                (m) => m.VlcOfficerModule
            ),
        canActivate: [AuthGuardService],
    },
    {
        path: "vqr-manager",
        loadChildren: () =>
            import("./vqr-manager/vqr-manager/vqr-manager.module").then(
                (m) => m.VqrManagerModule
            ),
        canActivate: [AuthGuardService],
    },
    {
        path: "vqr-officer",
        loadChildren: () =>
            import("./vqr-officer/vqr-officer/vqr-officer.module").then(
                (m) => m.VqrOfficerModule
            ),
        canActivate: [AuthGuardService],
    },
    {path: "**", component: ErrorComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
