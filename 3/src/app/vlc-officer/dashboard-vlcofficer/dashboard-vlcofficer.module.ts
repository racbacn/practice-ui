import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardVlcofficerRoutingModule } from './dashboard-vlcofficer-routing.module';
import { DashboardVlcofficerComponent } from './dashboard-vlcofficer.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReviewVlcOfficerModule } from './review-vlc-officer/review-vlc-officer.module';

@NgModule({
  declarations: [
    DashboardVlcofficerComponent
  ],
  imports: [
    CommonModule,
    DashboardVlcofficerRoutingModule,
    WidgetsModule,
    SharedModule,
    ReviewVlcOfficerModule
  ]
})
export class DashboardVlcofficerModule { }
