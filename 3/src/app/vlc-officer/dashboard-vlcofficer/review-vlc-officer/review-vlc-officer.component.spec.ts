import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewVlcOfficerComponent } from './review-vlc-officer.component';

describe('ReviewVlcOfficerComponent', () => {
  let component: ReviewVlcOfficerComponent;
  let fixture: ComponentFixture<ReviewVlcOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewVlcOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewVlcOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
