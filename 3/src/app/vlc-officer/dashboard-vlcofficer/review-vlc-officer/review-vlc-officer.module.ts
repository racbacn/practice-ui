import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewVlcOfficerComponent } from './review-vlc-officer.component';



@NgModule({
  declarations: [
    ReviewVlcOfficerComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ReviewVlcOfficerModule { }
