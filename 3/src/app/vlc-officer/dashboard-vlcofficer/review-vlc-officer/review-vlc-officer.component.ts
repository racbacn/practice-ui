import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review-vlc-officer',
  templateUrl: './review-vlc-officer.component.html',
  styleUrls: ['./review-vlc-officer.component.scss']
})
export class ReviewVlcOfficerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    let id = sessionStorage.getItem("transactionid");
    console.log("transactionID", id)
  }

}
