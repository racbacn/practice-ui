import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVlcofficerComponent } from './dashboard-vlcofficer.component';

describe('DashboardVlcofficerComponent', () => {
  let component: DashboardVlcofficerComponent;
  let fixture: ComponentFixture<DashboardVlcofficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardVlcofficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVlcofficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
