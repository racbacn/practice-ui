import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendRoadtaxFormulaComponent } from '../amend-roadtax-formula/amend-roadtax-formula.component';
import { DashboardVlcofficerComponent } from './dashboard-vlcofficer.component';
import { ReviewVlcOfficerComponent } from './review-vlc-officer/review-vlc-officer.component';

const routes: Routes = [{ path: '', component: DashboardVlcofficerComponent },
{path: 'review-vlc-officer', component: ReviewVlcOfficerComponent},
{path: 'amend-roadtax-formula', component: AmendRoadtaxFormulaComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardVlcofficerRoutingModule { }
