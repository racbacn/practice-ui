import { Component, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { DashboardService } from 'src/app/services/dashboard.service';
import { officerUserId, USER_TYPES } from 'src/app/shared/constants/application.constants';
import * as moment from "moment";
import {SessionUtil} from "../../core/util/sessionUtil";
import { UserResponse } from 'src/app/interface/user-response';

@Component({
  selector: 'app-dashboard-vlcofficer',
  templateUrl: './dashboard-vlcofficer.component.html',
  styleUrls: ['./dashboard-vlcofficer.component.scss']
})
export class DashboardVlcofficerComponent implements OnInit {
  transactions!: any;
  url = 'amend-roadtax-formula';
  mobileMargin: boolean = true;
  pageNo = 0;
  pageSize!: any;

  data = {
    "transactionDueDates": {
      "dueDateToday": 0,
      "dueDateTomorrow": 0,
      "dueDateWeek": 1,
      "dueDateMonth": 1
    }

  }
  dueDates: any;
  userType!: string;

  constructor(private breakpointObserver: BreakpointObserver,
    private dashBoardService: DashboardService
  ) {

  }

 async ngOnInit() {
 
    this.getDetails();
    this.userType = SessionUtil.getSessionStorage(SessionUtil.USER_TYPE)!;
  }

   getDetails() {
    let date = new Date();
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    ); 
    let params = {
      dueDate: date.toISOString,
      pageNo: this.pageNo,
      pageSize: this.pageSize === undefined ? "" : this.pageSize,
      statusTypeKey: USER_TYPES.MANAGER_REJECTED + "," + USER_TYPES.PENDING_MANAGER_REVIEW,
    }
    this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
      this.transactions = res;
      console.log("details", this.transactions)
    })
  }

  goToViewDetails(event: any) {
    let resp: any;
    for (let item of this.transactions) {
      if (item.transactionRefNum === event) {
        resp = item.transactionId;
        sessionStorage.setItem("transactionid", resp)
      }
    }
  }
  filterList(event: any) {
    let date: any;
    let vehicleScheme: any;
    let vehicleTypeKey: any;
    let params: any;
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    ); 
    switch (event) {
      case "today": {
        date = new Date();
      }
        break;
      case 'tomorrow': {
        let dateToday = new Date();
        date = new Date(dateToday.setDate(dateToday.getDate() + 1));
      }
        break;
      case 'nextweek': {
        let nextweek = new Date();
        date = new Date(nextweek.setDate(nextweek.getDate() + 7));
      }
        break;
      case 'weeklater': {
        let nextweek = new Date();
        date = new Date(nextweek.setDate(nextweek.getDate() + 14));
      }
        break;
      case 'certifyRefunds': {
        vehicleScheme = 2;
      }
        break;
      case 'roadTax': {
        vehicleTypeKey = 2;
      }
        break;
      case 'default': {
        this.getDetails();
      }
        break;
      default:
    }
    if ((vehicleScheme || vehicleTypeKey) === undefined) {
      params = {
        dueDateRange: moment(date).format("YYYY-MM-DD"),
        assignedUserId: userResponse.userId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: userResponse.userTypeKey,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    } else {
      params = {
        assignedUserId: userResponse.userId,
        vehicleSchemeKey: vehicleScheme === undefined ? "" : vehicleScheme,
        statusTypeKey: userResponse.userTypeKey,
        vehicleTypeKey: vehicleTypeKey === undefined ? "" : vehicleTypeKey
      }
    }
    this.dashBoardService.getOfficerDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  sortList(event: any) {
    let date = new Date();
    this.pageSize = event;
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    ); 
    let params = {
      dueDate: date.toISOString,
      pageSize: this.pageSize,
      statusTypeKey: USER_TYPES.MANAGER_REJECTED + "," + USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: this.pageNo === undefined ? "" : this.pageNo,
    }
    this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
      this.transactions = res;
    })
  }

  onPageMove(pageNumber: number) {
    let date = new Date();
    let userResponse: UserResponse = JSON.parse(
      <string>SessionUtil.getSessionStorage(SessionUtil.USER)
    ); 
    let params = {
      dueDate: date.toISOString,
      statusTypeKey: USER_TYPES.MANAGER_REJECTED + "," + USER_TYPES.PENDING_MANAGER_REVIEW,
      pageNo: pageNumber,
      pageSize: this.pageSize === undefined ? "" : this.pageSize,
    }


      this.dashBoardService.getRoadTaxDashBoard(params).subscribe(res => {
        if (res.length === 0 || res === undefined) {
          return alert("No Data");
        } else {
          this.transactions = res;
          this.pageNo = pageNumber;
        }
      }, (error) => {
        if (error?.error?.code === 500) {
          return alert(error.error.message);
        }
    alert('Cannot get transactions.');
      }
      )
  }

}
