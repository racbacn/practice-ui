import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertifyRoadTaxRefundComponent } from './certify-road-tax-refund.component';

describe('CertifyRoadtaxRefundComponent', () => {
  let component: CertifyRoadTaxRefundComponent;
  let fixture: ComponentFixture<CertifyRoadTaxRefundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertifyRoadTaxRefundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertifyRoadTaxRefundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
