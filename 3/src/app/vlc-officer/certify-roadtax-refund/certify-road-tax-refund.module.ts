import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CertifyRoadTaxRefundRoutingModule} from './certify-road-tax-refund-routing.module';
import {CertifyRoadTaxRefundComponent} from './certify-road-tax-refund.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {FlexModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import {ReviewRefundVlcOfficerComponent} from './review-refund-vlc-officer/review-refund-vlc-officer.component';
import {WidgetsModule} from "../../widgets/widgets.module";
import {ExpansionTableComponent} from './review-refund-vlc-officer/expansion-table/expansion-table.component';
import {ExpansionTableDetailComponent} from './review-refund-vlc-officer/expansion-table/expansion-table-detail/expansion-table-detail.component';


@NgModule({
    declarations: [
        CertifyRoadTaxRefundComponent,
        ReviewRefundVlcOfficerComponent,
        ExpansionTableComponent,
        ExpansionTableDetailComponent
    ],
    imports: [
        CommonModule,
        CertifyRoadTaxRefundRoutingModule,
        MatProgressBarModule,
        FlexModule,
        MatCardModule,
        WidgetsModule
    ]
})
export class CertifyRoadTaxRefundModule {
}
