import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CertifyRoadTaxRefundComponent} from './certify-road-tax-refund.component';
import {ReviewRefundVlcOfficerComponent} from "./review-refund-vlc-officer/review-refund-vlc-officer.component";

const routes: Routes = [{path: '', component: CertifyRoadTaxRefundComponent}, {
    path: 'review-refund',
    component: ReviewRefundVlcOfficerComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CertifyRoadTaxRefundRoutingModule {
}
