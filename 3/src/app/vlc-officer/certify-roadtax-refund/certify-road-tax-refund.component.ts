import {Component, OnInit} from '@angular/core';
import {RoadTaxCertifyRefundService} from "../../services/road-tax-certify-refund.service";
import {CertifyRoadTaxRefundTable} from "../../interface/certify-road-tax-refund-table";
import {SessionUtil} from "../../core/util/sessionUtil";
import * as moment from "moment";

@Component({
    selector: 'app-certify-roadtax-refund',
    templateUrl: './certify-road-tax-refund.component.html',
    styleUrls: ['./certify-road-tax-refund.component.scss']
})
export class CertifyRoadTaxRefundComponent implements OnInit {
    transactions!: any[];
    pageNo = 0;
    pageSize!: string;
    url = "review-refund"
    userType!: string;

    constructor(private roadTaxCertifyRefundService: RoadTaxCertifyRefundService) {
    }

    ngOnInit(): void {
        this.userType = "reviewRefund"

        this.getRefundList();
    }

    getRefundList() {
        this.roadTaxCertifyRefundService.getCertifyRefundList().subscribe(data => {
            let certifyRoadTaxRefundTable: CertifyRoadTaxRefundTable = data

            if (certifyRoadTaxRefundTable) {
                this.transactions = certifyRoadTaxRefundTable.certifyRefundRecords;
                this.pageNo = certifyRoadTaxRefundTable.pageNo;
                this.pageSize = certifyRoadTaxRefundTable.pageSize.toString();
            }
        });
    }

    getSearchParams(event: any) {
        event.creationDateTimeFrom = moment(event.creationDateTimeFrom).startOf("day").utcOffset(0, true).format();
        event.creationDateTimeTo = moment(event.creationDateTimeTo).endOf('day').utcOffset(0, true).format();

        this.roadTaxCertifyRefundService.getCertifyRefundListWithParams(event).subscribe(data => {
            let certifyRoadTaxRefundTable: CertifyRoadTaxRefundTable = data

            if (certifyRoadTaxRefundTable) {
                this.transactions = certifyRoadTaxRefundTable.certifyRefundRecords;
                this.pageNo = certifyRoadTaxRefundTable.pageNo;
                this.pageSize = certifyRoadTaxRefundTable.pageSize.toString();
            }
        });
    }

    goToViewDetails(event: any) {
        SessionUtil.setSessionStorage(SessionUtil.REFUND_REF_NO, event);
    }

    sortList(event: any) {
        this.pageSize = event;

        let params = {
            pageNo: this.pageNo,
            pageSize: this.pageSize
        }

        this.roadTaxCertifyRefundService.getCertifyRefundListWithParams(params).subscribe(data => {
            let certifyRoadTaxRefundTable: CertifyRoadTaxRefundTable = data

            if (certifyRoadTaxRefundTable) {
                this.transactions = certifyRoadTaxRefundTable.certifyRefundRecords;
                this.pageNo = certifyRoadTaxRefundTable.pageNo;
                this.pageSize = certifyRoadTaxRefundTable.pageSize.toString();
            }
        });
    }

    onPageMove(pageNumber: number) {
        let params = {
            pageNo: pageNumber,
            pageSize: this.pageSize,
        }
        this.roadTaxCertifyRefundService.getCertifyRefundListWithParams(params).subscribe(data => {
                let certifyRoadTaxRefundTable: CertifyRoadTaxRefundTable = data

                if (certifyRoadTaxRefundTable) {
                    this.transactions = certifyRoadTaxRefundTable.certifyRefundRecords;
                    this.pageNo = certifyRoadTaxRefundTable.pageNo;
                    this.pageSize = certifyRoadTaxRefundTable.pageSize.toString();
                } else {
                    return alert("No Data");
                }
            }, (error) => {
                if (error?.error?.code === 500) {
                    return alert(error.error.message);
                }
            }
        )
    }
}
