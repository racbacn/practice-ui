import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewRefundVlcOfficerComponent } from './review-refund-vlc-officer.component';

describe('ReviewRefundVlcOfficerComponent', () => {
  let component: ReviewRefundVlcOfficerComponent;
  let fixture: ComponentFixture<ReviewRefundVlcOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewRefundVlcOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewRefundVlcOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
