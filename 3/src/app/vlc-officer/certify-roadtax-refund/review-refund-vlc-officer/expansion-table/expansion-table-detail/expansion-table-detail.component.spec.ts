import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpansionTableDetailComponent } from './expansion-table-detail.component';

describe('ExpansionTableDetailComponent', () => {
  let component: ExpansionTableDetailComponent;
  let fixture: ComponentFixture<ExpansionTableDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpansionTableDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpansionTableDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
