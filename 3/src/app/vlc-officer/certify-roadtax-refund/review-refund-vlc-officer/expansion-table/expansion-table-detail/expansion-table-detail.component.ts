import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {LabelDetail} from "../../../../../interface/label-detail";
import {CertifyRefundResponse} from "../../../../../interface/certify-refund-response";

@Component({
    selector: 'app-expansion-table-detail',
    templateUrl: './expansion-table-detail.component.html',
    styleUrls: ['./expansion-table-detail.component.scss']
})
export class ExpansionTableDetailComponent implements OnInit {
    refund: string = "REFUND";
    vehicle: string = "VEHICLE";

    labelDetails!: any;
    mapDetails!: any;
    tableHeader!: string;

    @Input() index!: number;
    @Input() tableType!: string;
    @Input() isHeaderHidden: boolean = false;
    @Input() data!: any;

    constructor() {
    }

    ngOnInit(): void {
        switch (this.tableType) {
            case this.refund: {
                this.labelDetails = this.getPendingRefundLabelDetails();
                this.mapDetails = this.getPendingRefundMapDetails();
                this.loadPendingRefundDetails(this.data);
                break;
            }
            case this.vehicle: {
                this.labelDetails = this.getVehicleTransactionLabelDetails();
                this.mapDetails = this.getVehicleTransactionMapDetails();
                this.loadVehicleTransactionDetails(this.data);
                break;
            }
            default:
                break;
        }
    }

    ngOnChanges() {
        switch (this.tableType) {
            case this.refund: {
                this.loadPendingRefundDetails(this.data);
                break;
            }
            case this.vehicle: {
                this.loadVehicleTransactionDetails(this.data);
                break;
            }
            default:
                break;
        }
    }

    loadPendingRefundDetails(mapDetail: any): void {
        let customMap: any = {
            refundDetails: {
                ownerId: mapDetail.ownerDetails.ownerId,
                ownerName: mapDetail.ownerDetails.ownerName,
                refundRemarks: mapDetail.refundRemarks,
                vehicleNumber: mapDetail.vehicleNumber,
                transactionRefNum: mapDetail.transactionRefNum,
                assignedTo: mapDetail.assignedOfficerId,
                refundStatus: mapDetail.certifyStatusTypeDescription,
                refundTypeDesc: mapDetail.refundTypeDesc,
            },
            refundItemAmountDetails: {
                refundRefNo: mapDetail.transactionRefNum,
                refundType: mapDetail.transactionRefNum,
                refundAmountBeforeGST: mapDetail.refundAmountBeforeGST,
                refundGSTAmount: mapDetail.refundGSTAmount,
                refundAmountAfterGST: mapDetail.refundAmountAfterGST,
            },
            ownerDetails: mapDetail.ownerDetails
        }

        for (let i in this.labelDetails) {
            this.labelDetails[i].values = [];

            let data: any = customMap[this.mapDetails[this.labelDetails[i].headerName]];

            this.labelDetails[i].labels.forEach((label: any) => {
                let value = data[this.mapDetails[label]];

                if (value !== undefined || value !== null || value !== "") {
                    this.labelDetails[i].values.push(value as never);
                } else {
                    this.labelDetails[i].values.push("-" as never);
                }
            });
        }
    }

    getPendingRefundMapDetails(): any {
        return {
            "REFUND DETAILS": "refundDetails",
            "REFUND ITEM AMOUNT DETAILS": "refundItemAmountDetails",
            "REFUNDEE (OWNER) DETAILS": "ownerDetails",
            "Owner ID": "ownerId",
            "Owner Name": "ownerName",
            "Refund Remarks": "refundRemarks",
            "Vehicle No.": "vehicleNumber",
            "Transaction Ref No.": "transactionRefNum",
            "Assigned To": "assignedTo",
            "Refund Status": "refundStatus",
            "Refund Type & Description": "refundTypeDesc",
            "Refund Ref No.": "refundRefNo",
            "Refund Type": "refundRefNo",
            "Amount before": "refundAmountBeforeGST",
            "GST Amount (S$)": "refundGSTAmount",
            "Amount after GST (S$)": "refundAmountAfterGST",
            "Owner ID Type": "ownerIdTypeDesc",
            "Contact No.": "ownerContactNum",
            "Date of Birth": "ownerDOB",
            "Address": "ownerAddressLine1",
            "Unit No.": "ownerAddressLine2",
            "Postal Code": "ownerPostalCode",
        }
    }

    getPendingRefundLabelDetails(): Array<LabelDetail> {
        return [
            {
                headerName: "REFUND DETAILS",
                values: [],
                labels: [
                    "Owner ID", "Owner Name", "Refund Remarks",
                    "Vehicle No.", "Transaction Ref No.", "Assigned To",
                    "Refund Status", "Refund Type & Description"
                ]
            },
            {
                headerName: "REFUND ITEM AMOUNT DETAILS",
                values: [],
                labels: [
                    "Refund Ref No.", "Refund Type", "Amount before",
                    "GST Amount (S$)", "Amount after GST (S$)"
                ]
            },
            {
                headerName: "REFUNDEE (OWNER) DETAILS",
                values: [],
                labels: [
                    "Owner ID Type", "Owner ID", "Owner Name",
                    "Contact No.", "Date of Birth", "Address",
                    "Unit No.", "Postal Code"]
            }
        ]
    }

    loadVehicleTransactionDetails(mapDetail: any) {
        let customMap: any = {
            hidden: {
                ownerId: mapDetail.ownerId,
                ownerName: mapDetail.ownerName,
                transactionStatusTypeDesc: mapDetail.transactionStatusTypeDesc,
                logDateTime: mapDetail.logDateTime,
                createdBy: "Owner",
            }
        }

        for (let i in this.labelDetails) {
            this.labelDetails[i].values = [];

            let data: any = customMap[this.mapDetails[this.labelDetails[i].headerName]];

            this.labelDetails[i].labels.forEach((label: any) => {
                let value = data[this.mapDetails[label]];

                if (value) {
                    this.labelDetails[i].values.push(value as never);
                } else {
                    this.labelDetails[i].values.push("-" as never);
                }
            });
        }
    }

    getVehicleTransactionMapDetails(): any {
        return {
            "hidden": "hidden",
            "Owner ID": "ownerId",
            "Owner Name": "ownerName",
            "Transaction Status": "transactionStatusTypeDesc",
            "Deregistration Date": "logDateTime",
            "Created By": "createdBy"
        }
    }

    getVehicleTransactionLabelDetails(): Array<LabelDetail> {
        return [
            {
                headerName: "hidden",
                values: [],
                labels: [
                    "Owner ID", "Owner Name", "Transaction Status",
                    "Deregistration Date", "Created By"
                ]
            }
        ]
    }
}
