import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {TableHeader} from "../../../../interface/table-header";

@Component({
    selector: 'app-expansion-table',
    templateUrl: './expansion-table.component.html',
    styleUrls: ['./expansion-table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class ExpansionTableComponent implements OnInit {
    refund: string = "REFUND";
    vehicle: string = "VEHICLE";

    @Output() onExpandRow: EventEmitter<any> = new EventEmitter<any>();
    @Input() tableType!: string;
    @Input() data!: any;


    dataSource!: MatTableDataSource<any>;
    columns!: Array<any>;
    displayedColumns!: Array<any>;
    isHeaderHidden: boolean = false;
    expandedElement!: string;

    constructor() {
    }

    ngOnInit(): void {
        this.initTable();
    }

    initTable(): void {
        if (this.tableType) {
            switch (this.tableType) {
                case this.refund: {
                    this.columns = this.getRefundColumnList();
                    this.displayedColumns = this.getDisplayedColumns();
                    this.dataSource = this.getRefundData();
                    this.isHeaderHidden = false;
                    break;
                }
                case this.vehicle: {
                    this.columns = this.getVehicleColumnList();
                    this.displayedColumns = this.getDisplayedColumns();
                    this.dataSource = this.getVehicleData();
                    this.isHeaderHidden = true;
                    break;
                }
                default:
                    break;
            }
        }

    }

    getDisplayedColumns() {
        return this.columns.map(column => column.property);
    }

    ngOnChanges() {
        console.log(this.getVehicleData())

        switch (this.tableType) {
            case this.refund: {
                this.dataSource = this.getRefundData();
                break;
            }
            case this.vehicle: {
                this.dataSource = this.getVehicleData();
                break;
            }
            default:
                break;
        }
    }

    getRefundData() {
        return new MatTableDataSource(this.data);
    }

    getRefundColumnList(): Array<TableHeader> {
        return [
            {
                property: "transactionRefNum",
                label: 'Refund Reference  No.',
            },
            {
                property: "refundMethodTypeDesc",
                label: 'Refund Method',
            },
            {
                property: "createdBy",
                label: 'Created By',
            },
            {
                property: "refundAmountBeforeGST",
                label: 'Amount (S$)',
            },
            {
                property: "createdDateTime",
                label: 'Creation Date/Time',
            },
            {
                property: "action",
                label: 'Action',
            }
        ]
    }

    getVehicleData() {
        return new MatTableDataSource(this.data)
    }

    getVehicleColumnList(): Array<TableHeader> {
        return [
            {
                property: "transactionRefNum",
                label: 'Transaction Ref No.',
            },
            {
                property: "vehicleNumber",
                label: 'Vehicle No',
            },
            {
                property: "transactionTypeDesc",
                label: 'Transaction Type',
            },
            {
                property: "transactionAmt",
                label: 'Transaction Amount (S$)',
            },
            {
                property: "logDateTime",
                label: 'Log Date/Time',
            },
            {
                property: "action",
                label: 'Action',
            }
        ]
    }

}
