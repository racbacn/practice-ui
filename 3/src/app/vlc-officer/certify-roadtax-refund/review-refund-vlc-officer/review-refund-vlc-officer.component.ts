import {Component, Input, OnInit} from '@angular/core';
import {CertifyRefundResponse} from "../../../interface/certify-refund-response";
import {RoadTaxCertifyRefundService} from "../../../services/road-tax-certify-refund.service";
import {ApproveCertifyRefundResponse} from "../../../interface/approve-certify-refund-response";
import {ApproveCertifyRefundRequest} from "../../../interface/approve-certify-refund-request";
import {UserResponse} from "../../../interface/user-response";
import {SessionUtil} from "../../../core/util/sessionUtil";
import {SharedService} from "../../../services/shared.service";
import {VrlsUtilityService} from "../../../services/vrls-utility.service";
import {VehicleTransactionHistoryResponse} from "../../../interface/vehicle-transaction-history-response";
import * as moment from "moment";
import {VehicleTransaction} from "../../../interface/vehicle-transaction";

export const REFUND: string = "REFUND";
export const VEHICLE: string = "VEHICLE";

@Component({
    selector: 'app-review-refund-vlc-officer',
    templateUrl: './review-refund-vlc-officer.component.html',
    styleUrls: ['./review-refund-vlc-officer.component.scss']
})
export class ReviewRefundVlcOfficerComponent implements OnInit {
    refund: string = "REFUND";
    vehicle: string = "VEHICLE";

    @Input() certifyRefundResponseList!: Array<CertifyRefundResponse>;
    @Input() vehicleTransactionHistory!: Array<VehicleTransaction>;

    transactionId!: number;
    likelihoodOfApproval: string = "0";
    refundDetails!: Array<any>;
    vehicleDetails!: Array<any>;
    showCertifyReject: boolean = false;

    constructor(private roadTaxCertifyRefundService: RoadTaxCertifyRefundService, private vrlsUtilityService: VrlsUtilityService, private sharedService: SharedService) {
    }

    ngOnInit(): void {
        this.initData();
    }

    initData() {
        this.certifyRefundResponseList = new Array<CertifyRefundResponse>();

        let certifyRefundResponse: CertifyRefundResponse = {
            "transactionRefNum": "",
            "transactionId": 0,
            "vehicleNumber": "",
            "refundRemarks": "",
            "refundMethodTypeKey": 0,
            "refundMethodTypeDesc": "",
            "assignedOfficerId": 0,
            "createdBy": "",
            "createdDateTime": "",
            "refundTypeKey": 0,
            "refundTypeDesc": "",
            "refundAmountBeforeGST": 0,
            "refundAmountAfterGST": 0,
            "refundGSTAmount": 0,
            "certifyStatusTypeKey": 0,
            "certifyStatusTypeDescription": "REFUND_REJECTED",
            "ownerDetails": {
                "ownerIdType": "",
                "ownerIdTypeDesc": "",
                "ownerId": "",
                "ownerName": "",
                "ownerDOB": "",
                "ownerAddressLine1": "",
                "ownerAddressLine2": "",
                "ownerPostalCode": "",
                "ownerContactNum": "",
            }
        }

        this.certifyRefundResponseList.push(certifyRefundResponse);

        let transactionId: number | null = parseInt(<string>SessionUtil.getSessionStorage(SessionUtil.REFUND_REF_NO));

        if (transactionId) {
            this.roadTaxCertifyRefundService.getCertifyRefundByTransId(transactionId).subscribe(data => {
                let certifyRefundResponse: CertifyRefundResponse = data;

                if (certifyRefundResponse) {
                    if (certifyRefundResponse.certifyStatusTypeKey === 1) {
                        this.showCertifyReject = true
                        this.likelihoodOfApproval = "50"
                    }

                    if (certifyRefundResponse.certifyStatusTypeKey === 2) {
                        this.likelihoodOfApproval = "100"
                    }

                    console.log(certifyRefundResponse.transactionId)

                    certifyRefundResponse.createdDateTime = moment(certifyRefundResponse.createdDateTime, 'DD-MM-YYYY HH:mm:ss').format("YYYY-MM-DD HH:mm:ss")


                    this.certifyRefundResponseList = new Array<CertifyRefundResponse>();
                    this.certifyRefundResponseList.push(certifyRefundResponse)

                    let vehicleTransactionHistoryParams = {
                        matchRefund: true,
                        certifyRefundDateTime: certifyRefundResponse.createdDateTime,
                        statusType: 4,
                        vehicleNumber: certifyRefundResponse.vehicleNumber,
                    }

                    console.log(vehicleTransactionHistoryParams)

                    this.vrlsUtilityService.getVehicleTransactionHistory(vehicleTransactionHistoryParams).subscribe((data: any) => {
                        let vehicleTransactionHistoryResponse: VehicleTransactionHistoryResponse = data;


                        if (vehicleTransactionHistoryResponse) {
                            this.vehicleTransactionHistory = vehicleTransactionHistoryResponse.vehicleTransactionHistory;

                            console.log(this.vehicleTransactionHistory)
                        }
                    })
                }
            })


        }
    }

    onCertify() {
        if (this.certifyRefundResponseList) {
            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER));

            let approveCertifyRefundResponse: ApproveCertifyRefundRequest = {
                requestHeader: {
                    ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
                    managerId: 0,
                    userId: userResponse.userId,
                    userTypeKey: userResponse.userTypeKey
                },
                transactionId: this.certifyRefundResponseList[0].transactionId
            }

            this.roadTaxCertifyRefundService.approveCertifyRefund(approveCertifyRefundResponse).subscribe(data => {
                let responseData: ApproveCertifyRefundResponse = data;

                if (responseData) {
                    this.sharedService.sendApproveCertifyRefundResponse(responseData);
                }
            })
        }
    }

    onReject() {
        if (this.certifyRefundResponseList) {
            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER));

            let approveCertifyRefundResponse: ApproveCertifyRefundRequest = {
                requestHeader: {
                    ltaTeamTypeKey: userResponse.ltaTeamTypeKey,
                    managerId: 0,
                    userId: userResponse.userId,
                    userTypeKey: userResponse.userTypeKey
                },
                transactionId: this.certifyRefundResponseList[0].transactionId
            }

            this.roadTaxCertifyRefundService.rejectCertifyRefund(approveCertifyRefundResponse).subscribe(data => {
                let responseData: ApproveCertifyRefundResponse = data;

                if (responseData) {
                    this.sharedService.sendRejectCertifyRefundResponse(responseData);
                }
            })
        }
    }
}
