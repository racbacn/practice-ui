import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../../material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { VlcManagerRoutingModule} from './vlc-officer-routing.module';
import { VlcOfficerComponent } from './vlc-officer.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  declarations: [
    VlcOfficerComponent

  ],
  imports: [
    CommonModule,
    MaterialModule,
    WidgetsModule,
    VlcManagerRoutingModule,
    SharedModule,
    CoreModule
  ]
})
export class VlcOfficerModule { }
