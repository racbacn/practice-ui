import { Component, OnInit } from "@angular/core";
import { BreakpointObserver } from "@angular/cdk/layout";

@Component({
  selector: "app-vlc-officer",
  templateUrl: "./vlc-officer.component.html",
  styleUrls: ["./vlc-officer.component.scss"],
})
export class VlcOfficerComponent implements OnInit {
  userdetails: Array<any> = [{ name: "David Tan", id: "EID12345" }];

  sideNavNames: Array<any> = [
    {
      name: "DASHBOARD",
      icon: "home",
      link: "dashboard-vlcofficer",
    },
    {
      name: "AMEND ROAD TAX FORMULA VALUES(S)",
      icon: "home",
      link: "amend-roadtax-formula",
    },
    {
      name: "CERTIFY REFUND",
      icon: "home",
      link: "certify-roadtax-refund",
    }
  ];

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {}
}
