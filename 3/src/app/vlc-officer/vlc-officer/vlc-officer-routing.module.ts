import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VlcOfficerComponent } from './vlc-officer.component';

const routes: Routes = [
  {
    path: '',
    component: VlcOfficerComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard-vlcofficer' },
      {
        path: 'dashboard-vlcofficer',
        loadChildren: () =>
          import('../dashboard-vlcofficer/dashboard-vlcofficer.module').then(
            (m) => m.DashboardVlcofficerModule
          ),
      },
      {
        path: 'amend-roadtax-formula',
        loadChildren: () =>
          import('../amend-roadtax-formula/amend-roadtax-formula.module').then(
            (m) => m.AmendRoadtaxFormulaModule
          ),
      },
      {
        path: 'certify-roadtax-refund',
        loadChildren: () =>
          import('../certify-roadtax-refund/certify-road-tax-refund.module').then(
            (m) => m.CertifyRoadTaxRefundModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VlcManagerRoutingModule {}
