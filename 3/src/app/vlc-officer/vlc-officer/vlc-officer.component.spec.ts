import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VlcOfficerComponent } from './vlc-officer.component';

describe('VlcOfficerComponent', () => {
  let component: VlcOfficerComponent;
  let fixture: ComponentFixture<VlcOfficerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VlcOfficerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VlcOfficerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
