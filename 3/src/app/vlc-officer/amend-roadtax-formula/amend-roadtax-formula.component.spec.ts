import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmendRoadtaxFormulaComponent } from './amend-roadtax-formula.component';

describe('AmendRoadtaxFormulaComponent', () => {
  let component: AmendRoadtaxFormulaComponent;
  let fixture: ComponentFixture<AmendRoadtaxFormulaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmendRoadtaxFormulaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendRoadtaxFormulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
