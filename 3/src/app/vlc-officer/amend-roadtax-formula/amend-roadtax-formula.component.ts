import { Component, OnInit } from "@angular/core";
import { DateOverlapValidation } from "src/app/core/util/date-overlap-validation.service";
import { AdditionalRoadTaxService } from "src/app/services/additional-road-tax.service";
import { RoadTaxRebatesService } from "src/app/services/road-tax-rebates.service";
import { RoadTaxService } from "src/app/services/road-tax.service";
import { baseRoadTax } from "src/app/shared/constants/application.constants";
import { FormulaCardService } from "src/app/widgets/formula-card/formula-card.service";
import { Row } from "src/app/widgets/formula-card/popup/interfaces/row";
import { UiData } from "src/app/widgets/formula-card/popup/interfaces/ui-data";
import { RoadTaxTable } from "src/app/widgets/formula-card/road-tax-table/interface/road-tax-table";
import { PopupService } from "src/app/widgets/formula-card/services/popup.service";

type Selected = "baseFee" | "rebate" | "addCharge";

interface Header {
  label: string;
  property: string;
}

@Component({
  selector: "app-amend-roadtax-formula",
  templateUrl: "./amend-roadtax-formula.component.html",
  styleUrls: ["./amend-roadtax-formula.component.scss"],
})
export class AmendRoadtaxFormulaComponent implements OnInit {
  options = [
    {
      label: "Base Road Tax Fee ( Petrol, CNG or Diesel Cars)",
      value: "baseFee",
    },
    {
      label: "Road Tax Rebate(s)",
      value: "rebate",
    },
    {
      label: "Additional Road Tax Charge(s)",
      value: "addCharge",
    },
  ];

  commonPopupHeaders = [
    {
      label: "Effective Date:",
      property: "effectiveDate",
    },
    {
      label: "End Date:",
      property: "endDate",
    },
  ];

  transactions: any;
  baseFees: any;
  showRoadTaxRebates!: boolean;
  showBaseRoadTax: boolean = true;
  showAdditionalRoadTax!: boolean;
  selected: Selected = "baseFee";
  submitButtonText = "Proceed";
  rebates: any;
  additionalRoadTax: any;
  baseRoadTax: any;
  additionalTax: any;
  lastParentIndexRebates: any;
  lastParentIndexAdditional: any;
  isSuccessful!: boolean;
  taxRebates: any;

  constructor(
    private roadTaxService: RoadTaxService,
    private rebatesService: RoadTaxRebatesService,
    private additionalroadTaxService: AdditionalRoadTaxService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService
  ) {}

  async ngOnInit() {
    await this.getRoadTax();
    await this.getRebates();
    await this.getAdditionalTax();
  }

  getRoadTax() {
    this.roadTaxService.getBaseRoadtax().subscribe((res) => {
      this.baseFees = res;
      this.roadTaxService.setBaseRoadTax(this.baseFees);
    }, (error: Error) =>
    this.formulaCardService.openErrorPopup(
      error.message || "Server Error"
    ));
  }

  getAdditionalTax() {
    this.additionalroadTaxService.getRAdditionalRoadTax().subscribe((res) => {
      this.additionalRoadTax = res;
      this.lastParentIndexAdditional =   res[res.length - 1];
      this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
    },
    (error: Error) =>
    this.formulaCardService.openErrorPopup(
      error.message || "Server Error"
    ));
  }

  getRebates() {
    this.rebatesService.getRoadTaxRebates().subscribe((res) => {
      this.rebates = res;
      this.lastParentIndexRebates = res[res.length - 1];
      this.roadTaxService.setRebates(this.rebates);
    },    (error: Error) =>
    this.formulaCardService.openErrorPopup(
      error.message || "Server Error"
    ));
  }

  onSelectionChange(event: any) {
    if (event === "rebate") {
      this.showRoadTaxRebates = true;
      this.showBaseRoadTax = false;
      this.showAdditionalRoadTax = false;
      return;
    }
    if (event === "baseFee") {
      this.showBaseRoadTax = true;
      this.showRoadTaxRebates = false;
      this.showAdditionalRoadTax = false;

      return;
    }
    if (event === "addCharge") {
      this.showBaseRoadTax = false;
      this.showRoadTaxRebates = false;
      this.showAdditionalRoadTax = true;
      return;
    }
  }

  setPopupRows(rowData: { fees: any; headers: Header[] }) {
    const { fees, headers } = rowData;
    let displayedRows: any[] = [];
    fees.forEach((taxRebate: any) => {
      const firstLabel = headers[0].label;
      const row = [{ label: "", value: "", subvalue: "" }];
      Object.values(headers).forEach((header, index) => {
        const value = taxRebate[header.property];
        
        row.push({ label: header.label, value, subvalue: "" });
      });
      displayedRows.push(row);
    });
    return displayedRows;
  }

  getBaseFeeRows(fees: any[]) {
    const headers = [
      {
        label: "Engine Capacity (EC):",
        property: "engineCapacity",
      },
      {
        label: "6-month Base Fee:",
        property: "baseAmount",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getRebateRows(fees: any[]) {
    const headers = [
      {
        label: "Rebate Name:",
        property: "rebateName",
      },
      {
        label: "Rebate Amount:",
        property: "rebateAmount",
      },
      {
        label: "For Vehicles with Registration Date(s)",
        property: "registrationDates",
        property2: "vehicleRegistrationTo",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getAddChargeRows(fees: any[]) {
    const headers = [
      {
        label: "Additional Charge Name:",
        property: "addChargeName",
      },
      {
        label: "Charge Amount:",
        property: "addChargeAmount",
      },
      {
        label: "For Vehicles with Registration Date(s)",
        property: "registrationDates",
        property2: "vehicleRegistrationTo",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  async submit(isDraft: any) {
    console.log("event", event)
    let baseFeePopoverRows: any[] = [];
    let rebatePopoverRows: any[] = [];
    let additionalTaxRows: any[] = [];
    let  confirmationPopup: any;
    this.baseRoadTax = this.getRoadTaxChild();
    this.taxRebates = this.getRebatesChild();
    this.additionalTax = this.getAddChargeChild();

    if(this.baseRoadTax.hasDateOverlap || this.taxRebates.hasDateOverlap || this.additionalTax.hasDateOverlap ){
      return  this.formulaCardService.openErrorPopup("Invalid Dates");

    }
    baseFeePopoverRows = this.getBaseFeeRows(this.baseRoadTax.data);
    rebatePopoverRows = this.getRebateRows(this.taxRebates.data);
    additionalTaxRows = this.getAddChargeRows(this.additionalTax.data);
   

    const popoverRows = [
      ...baseFeePopoverRows,
      ...rebatePopoverRows,
      ...additionalTaxRows,
    ];
    if(!isDraft){
      confirmationPopup = this.openConfirmationPopup(popoverRows);
      confirmationPopup.afterClosed().subscribe(async (data: any) => {
        if (data) {
          await this.saveBaseRoadTax(isDraft);
          await this.saveRebates(isDraft);
          await this.saveAddCharge(isDraft);
          this.formulaCardService.openSuccessPopup();
        }
   
      });
    }else {
      await this.saveBaseRoadTax(isDraft);
      await this.saveRebates(isDraft);
      await this.saveAddCharge(isDraft);
      let message = 'Your values have been saved.'
      this.formulaCardService.openSuccessPopup(message);
    }
  }

  saveDraft(event:any){
    this.submit(event);
  }

  async saveAddCharge(event: any) {
    if ( (this.additionalTax.data[0].addChargeType && this.additionalTax.data[0].addChargeAmount) !== "-" ||undefined) {
      (await this.additionalroadTaxService.saveAdditionalCharge(this.additionalTax.data, event)
      ).subscribe(
        () => {
          this.isSuccessful = true;
          this.getAdditionalTax();
      
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }
    return this.isSuccessful;
  }

  async saveRebates(event: any) {

    if (
      (this.taxRebates.data[0].rebateAmount && this.taxRebates.data[0].rebateName) !== "-" ||
      undefined
    ) {
    
      (await this.rebatesService.saveRebate(this.taxRebates.data, event)).subscribe(
        () => {
          this.isSuccessful = true;
          this.getRebates();    
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }
    return this.isSuccessful;
  }
  
  async saveBaseRoadTax(event: any) {
    if (
      (this.baseRoadTax.data[0].engineCapacity && this.baseRoadTax.data[0].baseAmount) !==
        "-" ||
      undefined
    ) {
      (await this.roadTaxService.saveBaseFee(this.baseRoadTax.data, event)).subscribe(
        () => {
          this.getRoadTax();
          this.isSuccessful = true;    
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }

    return this.isSuccessful;
  }

  openConfirmationPopup(displayedRows: Row[][]) {
    const uiData: UiData = {
      icon: "error",
      header: "Submit your VALUES for review?",
      subheader: "You will not be able to make amendments to your submission",
      displayedRows,
      buttons: [
        {
          text: "Back to Main",
          function: "close",
          class: "blue-button",
        },
        {
          text: "Approve",
          function: "submit",
          class: "blue-button",
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  getRoadTaxChild() {
    let baseFees: any;
    let basetax: any[]=[];
    let obj: any;
    let hasDateOverlap: boolean = false;
    this.roadTaxService.getRoadTax().subscribe((res) => {
      let base = this.roadTaxService.formatValues(res);
      if (base.baseTax === undefined || base.baseTax.length === 0 || null ) {
        basetax = [
          {
            engineCapacity: "-",
            baseAmount: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return  obj = {
          data: basetax,
          hasDateOverlap: hasDateOverlap
        }
      } else {
        if(!base.isRejected){
        hasDateOverlap = this.formulaCardService.hasDateOverlap(res)
              base.baseTax.forEach((data: any)=>{
                data.roadTaxRecords.forEach((record: any)=> {
                 basetax.push(record)
                })
             })
        }
      }
      return  obj = {
        data: basetax,
        hasDateOverlap: hasDateOverlap
      }
    });
 
    return obj;
  }

  getRebatesChild() {
    let hasDateOverlap: boolean = false;
    let rebatestax: any;
    let obj: any;
    this.roadTaxService.getRebates().subscribe((res) => {
      let rebate = this.rebatesService.formatValues(res, this.lastParentIndexRebates);
      if (rebate.rebates === undefined || rebate.rebates.length === 0 || null) {
        rebatestax = [
          {
            rebateName: "-",
            rebateAmount: "-",
            rebateType: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return  obj = {
          data: rebatestax,
          hasDateOverlap: hasDateOverlap
        }
      } else {
        if(!rebate.isRejected){
           hasDateOverlap = this.formulaCardService.hasRegDateOverlap(res);
        }
      }
      return  obj = {
        data:  rebate.rebates,
        hasDateOverlap: hasDateOverlap
      }
    });
    return obj;
  }

  getAddChargeChild() {
    let charge: any;
    let hasDateOverlap = false;
    let obj: any;
    this.roadTaxService.getAdditionalTax().subscribe((res) => {
      let additionalTax = this.additionalroadTaxService.formatValues(
        res,
        this.lastParentIndexAdditional
      );
      if (additionalTax.addCharge === undefined || additionalTax.addCharge.length === 0 || null) {
        charge = [
          {
            addChargeName: "-",
            addChargeAmount: "-",
            addChargeType: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return  obj = {
          data: charge,
          hasDateOverlap: hasDateOverlap
        }
      } else {
        if(!additionalTax.isRejected){
          hasDateOverlap = this.formulaCardService.hasRegDateOverlap(res)
        }
      }

      return  obj = {
        data:  additionalTax.addCharge,
        hasDateOverlap: hasDateOverlap
      }

    });

    return obj;
  }
}
