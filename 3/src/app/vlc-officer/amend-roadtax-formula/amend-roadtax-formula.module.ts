import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';

import { AmendRoadtaxFormulaRoutingModule } from './amend-roadtax-formula-routing.module';
import { AmendRoadtaxFormulaComponent } from './amend-roadtax-formula.component';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { RoadTaxTableModule } from 'src/app/widgets/formula-card/road-tax-table/road-tax-table.module';
import { SurchargeTabModule } from 'src/app/widgets/surcharge-tab/surcharge-tab.module';
import { FormulaCardExportModule } from 'src/app/widgets/formula-card/formula-card-export/formula-card-export.module';
import { FormulaCardSelectModule } from 'src/app/widgets/formula-card/formula-card-select/formula-card-select.module';
import { RoadTaxRebatesModule } from 'src/app/widgets/formula-card/road-tax-rebates/road-tax-rebates.module';
import { FormulaCardLegendModule } from 'src/app/widgets/formula-card/formula-card-legend/formula-card-legend.module';
import { LateFeesTabModule } from 'src/app/widgets/late-fees-tab/late-fees-tab.module';
import { AdditionalRoadTaxModule } from 'src/app/widgets/additional-road-tax/additional-road-tax.module';
import { DieselTabModule } from './diesel-tab/diesel-tab.module';
import { SubmitButtonModule } from 'src/app/widgets/formula-card/submit-button/submit-button.module';
import { ElectricVehiclesTabModule } from 'src/app/widgets/electric-vehicles-tab/electric-vehicles-tab.module';
import {CalculatorComponent} from "../../widgets/calculator/calculator.component";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AmendRoadtaxFormulaComponent,
    CalculatorComponent
  ],
  imports: [
    CommonModule,
    AmendRoadtaxFormulaRoutingModule,
    MaterialModule,
    WidgetsModule,
    RoadTaxTableModule,
    SurchargeTabModule,
    FormulaCardExportModule,
    FormulaCardSelectModule,
    RoadTaxTableModule,
    RoadTaxRebatesModule,
    FormulaCardLegendModule,
    LateFeesTabModule,
    AdditionalRoadTaxModule,
    DieselTabModule,
    SubmitButtonModule,
    ElectricVehiclesTabModule,
    FormsModule
  ]
})
export class AmendRoadtaxFormulaModule { }
