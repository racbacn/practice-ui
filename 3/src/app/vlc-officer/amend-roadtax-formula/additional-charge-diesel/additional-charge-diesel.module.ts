import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdditionalChargeDieselComponent } from './additional-charge-diesel.component';
import { ContextualTableModule } from 'src/app/widgets/formula-card/contextual-table/contextual-table.module';



@NgModule({
  declarations: [
    AdditionalChargeDieselComponent
  ],
  imports: [
    CommonModule,
    ContextualTableModule
  ],
  exports: [AdditionalChargeDieselComponent]
})
export class AdditionalChargeDieselModule { }
