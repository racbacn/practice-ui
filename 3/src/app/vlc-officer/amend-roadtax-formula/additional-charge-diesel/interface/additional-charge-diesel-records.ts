export interface AdditionalChargeDieselRecords {
    addChargeName: string;
    addChargeTypeKey: number | null;
    addChargeType: string;
    addChargeAmount: number | null;
    effectiveDate: string;
    endDate: string;
    vehicleRegistrationFrom: string,
    vehicleRegistrationTo: string,
    active?: boolean | undefined;
    transactionId: number;
    amendmentStatus: string;
    rejectComment: string;
    roadTaxAmtCatKey: number;
}
