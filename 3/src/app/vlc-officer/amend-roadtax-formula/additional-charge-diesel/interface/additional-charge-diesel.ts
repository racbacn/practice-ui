import { AdditionalChargeDieselRecords } from "./additional-charge-diesel-records";

export interface AdditionalChargeDiesel {
    AdditionalChargeList: AdditionalChargeDieselRecords[];
}

export interface AdditionalRoadTaxDieselRequest {
    addChargeTypeKey: number | null;
    addChargeName: string;
    addChargeValue: number | null;
    effectiveDate: string;
    endDate: string;
    vehicleRegistrationFrom: string;
    vehicleRegistrationTo: string;
    transactionId: number | null;
  
  }
  
  export interface AdditionalRoadDieselRequest {
    requestHeader: {
      userId: number;
      managerId: any;
      ltaTeamTypeKey: number,
      userTypeKey: number
  }
  additionalCharges: AdditionalRoadTaxDieselRequest[];
  }
