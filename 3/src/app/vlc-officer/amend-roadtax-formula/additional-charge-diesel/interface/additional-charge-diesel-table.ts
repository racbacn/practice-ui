export interface AdditionalChargeDieselTable {
    addChargeName: string;
    addChargeTypeKey: number | null;
    addChargeType: string;
    addChargeAmount: number | null;
    effectiveDate: Date;
    endDate: Date;
    active?: boolean | undefined;
    transactionId: number;
    isAParent: boolean;
    isExpanded: boolean;
    parentIndex: number | null;
    index: number;
    newlyAdded: boolean;
    fromApi: boolean;
    isNewRow: boolean;
    vehicleRegistrationTo: Date;
    vehicleRegistrationFrom: Date;
    amendmentStatus: string;
    roadTaxAmtCatKey: number;
}
