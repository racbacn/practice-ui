import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { AdditionalChargeDieselService } from "src/app/services/additional-charge-diesel.service";
import { RoadTaxService } from "src/app/services/road-tax.service";
import { AMENDMENT_STATUS } from "src/app/shared/constants/application.constants";
import { FormulaCardService } from "src/app/widgets/formula-card/formula-card.service";
import { AdditionalChargeDieselTable } from "./interface/additional-charge-diesel-table";

@Component({
  selector: "app-additional-charge-diesel",
  templateUrl: "./additional-charge-diesel.component.html",
  styleUrls: ["./additional-charge-diesel.component.scss"],
})
export class AdditionalChargeDieselComponent implements OnInit {
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();

  placeholder = "Select Aditional Charge Type";
  columnHeaders: any[] = [
    {
      label: "Additional Charge Name",
      property: "addChargeName",
      type: "textField",
    },
    {
      label: "Charge Type",
      property: "addChargeType",
      type: "selectField",
    },
    {
      label: "For Vehicles with Registration Date(s)",
      property: "vehicleRegistrationFrom",
      property2: "vehicleRegistrationTo",
      type: "rangeField",
    },
    {
      label: "Charge Amount",
      property: "addChargeAmount",
      type: "numberField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];

  headers: any[] = [
    {
      label: "",
      property: "warningTooltipButton",
      type: "",
    },
    ...this.columnHeaders,
    {
      label: "",
      property: "addButton",
      type: "",
    },
  ];
  additionalRoadTax: any;

  addRowCount = 0;
  selection = [
    {
      value: "Flat Amount",
    },
    {
      value: "% of total Road Tax Amount",
    },
  ];
  showError: boolean | undefined;
  lastParentIndex: any;
  constructor(
    private roadTaxService: RoadTaxService,
    private chargeService: AdditionalChargeDieselService,
    private formulaCardService: FormulaCardService
  ) {}

  ngOnInit(): void {
    this.chargeService.getAdditionalTaxData().subscribe((res) => {
      this.additionalRoadTax = res;
    });
  }

  addChargesRow(data: any) {
    if (data.index || data.index !== 0) {
      const lastChildIndex = data[data.length - 1];
      const add = this.createChild(lastChildIndex);
      this.additionalRoadTax.push({
        active: false,
        effectiveDate: this.addDaysToDate(add.endDate, 1),
        endDate: this.addDaysToDate(add.endDate, 2),
        vehicleRegistrationFrom: this.addDaysToDate(
          add.vehicleRegistrationTo,
          1
        ),
        vehicleRegistrationTo: this.addDaysToDate(add.vehicleRegistrationTo, 2),
        index: 0,
        isAParent: true,
        isExpanded: false,
        newlyAdded: true,
        parentIndex: null,
        addChargeAmount: add.addChargeAmount,
        addChargeName: add.addChargeName,
        addChargeTypeKey: add.addChargeTypeKey,
        addChargeType: add.addChargeType,
        fromApi: false,
        isNewRow: true,
      });
      this.additionalRoadTax = [...this.additionalRoadTax];
    }

    this.addRowCount++;
  }
  getLastChildIndex(data: any): number {
    const lastChild = data[data.length - 1];
    return lastChild.index || 0;
  }

  createChild(
    parentCharge: AdditionalChargeDieselTable
  ): AdditionalChargeDieselTable {
    const {
      effectiveDate,
      endDate,
      addChargeAmount,
      addChargeName,
      addChargeTypeKey,
      addChargeType,
      vehicleRegistrationTo,
    } = {
      ...parentCharge,
    };
    return {
      ...parentCharge,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      vehicleRegistrationFrom: this.addDaysToDate(vehicleRegistrationTo, 1),
      vehicleRegistrationTo: this.addDaysToDate(vehicleRegistrationTo, 2),
      addChargeAmount: addChargeAmount,
      addChargeName: addChargeName,
      addChargeTypeKey: addChargeTypeKey,
      addChargeType: addChargeType,
      isAParent: true,
      isExpanded: false,
      parentIndex: parentCharge?.index || null,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true,
    };
  }

  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.additionalRoadTax.filter(
      (child: AdditionalChargeDieselTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  setParent(parentIndex: number) {
    const children = this.additionalRoadTax.filter(
      (data: any) => data.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.additionalRoadTax[parentIndex];
      parent.hasChildren = false;
    }
  }

  onDatepickerChange(changed: AdditionalChargeDieselTable) {
    const index = changed.index ?? 0;
    const dateChanged = this.additionalRoadTax[index];
    const effectiveDateChanged =
      dateChanged.effectiveDate.getTime() !== changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      dateChanged.effectiveDate = changed.effectiveDate;
      dateChanged.newlyAdded = true;
    }

    const endDateChanged =
      dateChanged.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      dateChanged.endDate = changed.endDate;
      dateChanged.newlyAdded = true;
    }
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.chargeService.setAdditionalTaxDiesel(this.additionalRoadTax);
  }

  onRangeDatepickerChange(changed: AdditionalChargeDieselTable) {
    const index = changed.index ?? 0;
    const dateChanged = this.additionalRoadTax[index];
    const effectiveDateChanged =
      dateChanged.vehicleRegistrationFrom.getTime() !==
      changed.vehicleRegistrationFrom.getTime();

    if (effectiveDateChanged) {
      dateChanged.vehicleRegistrationFrom = changed.vehicleRegistrationFrom;
      dateChanged.newlyAdded = true;
    }

    const endDateChanged =
      dateChanged.vehicleRegistrationTo.getTime() !== changed.vehicleRegistrationTo.getTime();
    if (endDateChanged) {
      dateChanged.vehicleRegistrationTo = changed.vehicleRegistrationTo;
      dateChanged.newlyAdded = true;
    }

    this.additionalRoadTax = [...this.additionalRoadTax];
    this.chargeService.setAdditionalTaxDiesel(this.additionalRoadTax);
  }

  onInputChange(changed: AdditionalChargeDieselTable) {
    const index = changed.index ?? 0;
    const changedFees = this.additionalRoadTax[index];
    changedFees.addChargeName = changed.addChargeName;
    changedFees.addChargeAmount = changed.addChargeAmount;
    changedFees.newlyAdded = true;
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.chargeService.setAdditionalTaxDiesel(this.additionalRoadTax);
  }

  selectChange(select: AdditionalChargeDieselTable) {
    const index = select.index ?? 0;
    const changedFees = this.additionalRoadTax[index];
    changedFees.addChargeType = select.addChargeType;
    changedFees.newlyAdded = true;
    changedFees.addChargeTypeKey = this.formulaCardService.getTypeKey(
      select.addChargeType
    );
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.chargeService.setAdditionalTaxDiesel(this.additionalRoadTax);
  }



  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }

  onRemoveRow(charge: AdditionalChargeDieselTable) {
    let draftsParams: any[] = []
    draftsParams.push(charge)
    if(charge.amendmentStatus === AMENDMENT_STATUS.draft){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = charge?.index;
    this.additionalRoadTax.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = charge.parentIndex ?? 0;
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.additionalRoadTax = this.additionalRoadTax.map(
      (fee: any, index: any) => {
        fee.index = index;
        if (fee.isAParent) {
          currentParentIndex = index;
          return fee;
        }
        fee.parentIndex = currentParentIndex;
        return fee;
      }
    );
  }

  submit(event: any) {
    this.proceedButtonClick.emit();
  }

  saveAsDraft(event: any) {
    let isDraft = true;
    this.saveDraft.emit(isDraft);
  }
}
