import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalChargeDieselComponent } from './additional-charge-diesel.component';

describe('AdditionalChargeDieselComponent', () => {
  let component: AdditionalChargeDieselComponent;
  let fixture: ComponentFixture<AdditionalChargeDieselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalChargeDieselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalChargeDieselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
