import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialTaxDieselComponent } from './special-tax-diesel.component';
import { MaterialModule } from 'src/app/material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { FormulaCardLegendModule } from 'src/app/widgets/formula-card/formula-card-legend/formula-card-legend.module';
import { FormulaCardExportModule } from 'src/app/widgets/formula-card/formula-card-export/formula-card-export.module';
import { FormulaCardSelectModule } from 'src/app/widgets/formula-card/formula-card-select/formula-card-select.module';
import { ContextualTableModule } from 'src/app/widgets/formula-card/contextual-table/contextual-table.module';



@NgModule({
  declarations: [
    SpecialTaxDieselComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    WidgetsModule,
    FormulaCardLegendModule,
    FormulaCardExportModule,
    FormulaCardSelectModule,
    ContextualTableModule,
  ],
  exports: [
    SpecialTaxDieselComponent
  ]
})
export class SpecialTaxDieselModule { }
