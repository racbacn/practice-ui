import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialTaxDieselComponent } from './special-tax-diesel.component';

describe('SpecialTaxDieselComponent', () => {
  let component: SpecialTaxDieselComponent;
  let fixture: ComponentFixture<SpecialTaxDieselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialTaxDieselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialTaxDieselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
