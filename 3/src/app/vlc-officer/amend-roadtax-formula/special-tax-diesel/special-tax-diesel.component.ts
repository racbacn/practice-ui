import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { TableHeader } from "src/app/interface/table-header";
import { RoadTaxService } from "src/app/services/road-tax.service";
import { SpecialTaxDieselService } from "src/app/services/special-tax-diesel.service";
import { FormulaCardService } from "src/app/widgets/formula-card/formula-card.service";
import { SpecialTaxDieselTable } from "./interface/special-tax-diesel-table";

@Component({
  selector: "app-special-tax-diesel",
  templateUrl: "./special-tax-diesel.component.html",
  styleUrls: ["./special-tax-diesel.component.scss"],
})
export class SpecialTaxDieselComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() exportData: any = { headers: [], rows: [], filename: "test.xlsx" };
  @Input() headers: TableHeader[] = [];
  @Input() specialTax: SpecialTaxDieselTable[] = [];
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();

  spreadsheetHeaders: any[] = [
    {
      label: "Emission Standard",
      property: "powerRating",
      type: "collapsible",
    },
    {
      label: "Charge Type",
      property: "specialTaxChargeType",
      type: "selectField",
    },
    {
      label: "Amount",
      property: "amount",
      type: "textField",
    },
    {
      label: "Min. Payment Fee",
      property: "minPaymentFee",
      type: "textField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];
  uiTableHeaders: any[] = [
    {
      label: "",
      property: "warningTooltipButton",
      type: "",
    },
    ...this.spreadsheetHeaders,
    {
      label: "",
      property: "rowAddButton",
      type: "",
    },
  ];
  settings = {
    submitButtonText: "Proceed",
    hasSelectOptions: false,
    headerText: "",
    hasPreviewButton: false,
  };
  selection = [
    {
      value: "per Engine Capacity",
    },
    {
      value: "% of total Road Tax Amount",
    },
  ];
  addRowCount = 0;
  placeholder = "Select Charge Type";

  constructor(private specialTaxService: SpecialTaxDieselService, 
    private roadTaxService: RoadTaxService,
    private formulaCardService: FormulaCardService) {}

  ngOnInit(): void {
    this.specialTaxService.getSpecialTaxDieselData().subscribe((res) => {
      this.specialTax = res;
    });
  }

  onInputChange(changed: SpecialTaxDieselTable) {
    const index = changed.index ?? 0;
    const changedFees = this.specialTax[index];
    changedFees.specialTaxChargeType = changed.specialTaxChargeType;
    changedFees.minPaymentFee = changed.minPaymentFee;
    changedFees.amount = changed.amount;
    changedFees.newlyAdded = true;
    this.specialTax = [...this.specialTax];
    this.specialTaxService.setSpecialTaxDieselData(this.specialTax);
  }

  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.specialTax.filter(
      (child: SpecialTaxDieselTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  onAddNewChildSurchargeClick(parentRoadTax: any) {
    if (!parentRoadTax.isExpanded) {
      this.closeAllRows();
    }
    const parentIndex = parentRoadTax?.index ?? null;
    this.expandParent(parentIndex);
    this.expandChildren(parentIndex);
    if (parentIndex || parentIndex === 0) {
      this.addChildToSpecialTax(parentRoadTax);
      this.updateParentIndex();
    }
  }

  expandParent(index: number | null) {
    if (index || index === 0) {
      const parentFee = this.specialTax[index];
      parentFee.isExpanded = true;
      parentFee.hasChildren = true;
    }
  }

  expandChildren(parentSurchargeIndex: number | null) {
    this.specialTax = this.specialTax.map((child) => {
      if (child.parentIndex === parentSurchargeIndex) {
        child.isExpanded = true;
      }
      return child;
    });
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.specialTax = this.specialTax.map((fee, index) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  onToggleCollapse(data: any) {
    const { collapsedFee, currentFees } = data;
    this.updateRoadTax(currentFees);
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }

    this.specialTax = this.specialTax.map((fee, index) => {
      console.log(collapsedFee.index);
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      console.log(fee);
      return fee;
    });
  }

  updateRoadTax(currentFees: any) {
    this.specialTax = currentFees;
  }

  closeAllRows() {
    this.specialTax = this.specialTax.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  addChildToSpecialTax(parent: SpecialTaxDieselTable) {
    if (parent.index || parent.index === 0) {
      const lastChildIndex = this.getLastChildIndex(parent.index);
      const hasNoChildren = parent.index === lastChildIndex || this.addRowCount;
      const position = lastChildIndex + (hasNoChildren ? 1 : 0);
      const itemsToBeRemoved = 0;
      const taxToBeAdded = this.createChild(parent);
      this.specialTax.splice(position, itemsToBeRemoved, taxToBeAdded);
      this.specialTax = [...this.specialTax];
      this.specialTaxService.setSpecialTaxDieselData(this.specialTax);
    }
    this.addRowCount++;
  }

  createChild(roadTax: SpecialTaxDieselTable): SpecialTaxDieselTable {
    const lastChildIndex = this.getLastChildIndex(roadTax.index);
    const newChildrenLength = this.getNewlyAddedChildrenLength(roadTax.index);
    let base = lastChildIndex;
    if (!newChildrenLength) {
      base = roadTax.index;
    }
    const { endDate, effectiveDate, amount, minPaymentFee } = {
      ...this.specialTax[base],
    };
    return {
      ...roadTax,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      amount: amount,
      minPaymentFee: minPaymentFee,
      powerRating: "",
      isAParent: false,
      isExpanded: true,
      parentIndex: roadTax?.index || null,
      hasChildren: false,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true,
    };
  }

  getLastChildIndex(parentIndex: number): number {
    const children = this.specialTax.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index || 0;
  }

  onDatepickerChange(changed: SpecialTaxDieselTable) {
    const index = changed.index ?? 0;
    const changedTax = this.specialTax[index];
    const effectiveDateChanged =
      changedTax.effectiveDate.getTime() !== changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedTax.effectiveDate = changed.effectiveDate;
      changedTax.newlyAdded = true;
    }
    const endDateChanged =
      changedTax.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedTax.endDate = changed.endDate;
      changedTax.newlyAdded = true;
    }
    this.specialTax = [...this.specialTax];
    this.specialTaxService.setSpecialTaxDieselData(this.specialTax);
  }

  onRemoveRow(tax: SpecialTaxDieselTable) {
    let draftsParams: any[] = []
    draftsParams.push(tax)
    if(tax.amendmentStatus.toLowerCase() === 'draft'){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = tax?.index;
    this.specialTax.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = tax.parentIndex ?? 0;
    this.specialTaxService.setSpecialTaxDieselData(this.specialTax);
  }

  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }

  submit(event: any) {
    this.proceedButtonClick.emit();
  }

  saveAsDraft(event: any) {
    let isDraft = true;
    this.saveDraft.emit(isDraft);
  }
}
