export interface SpecialTaxDieselTable {
  transactionId: number;
  amount: number;
  minPaymentFee: number;
  effectiveDate: Date;
  endDate: Date;
  active: boolean;
  amendmentStatus: string;
  roadTaxAmtCatKey: number;
  powerRating: string;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  isNewRow: boolean;
  specialTaxChargeTypeKey: number;
  specialTaxChargeType: string;
  isDraft: boolean;
  parentNotEditable?: boolean;
}
