export interface SpecialTaxDieselRecords {
    transactionId: number;
    amount: number;
    minPaymentFee: number;
    effectiveDate: string;
    endDate: string;
    active: boolean;
    amendmentStatus: string;
    specialTaxChargeTypeKey: number;
    specialTaxChargeType: string;
    rejectComment:string;
}
