import { SpecialTaxDieselRecords } from "./special-tax-diesel-records";

export interface Header {
    label: string;
    property: string;
  }

export interface SpecialTaxDiesel {
    roadTaxAmtCatKey: number,
    powerRating: string,
    specialTax: SpecialTaxDieselRecords[]
}


export interface SpecialTaxDieselResponse {
    RoadTaxSpecialTax: SpecialTaxDiesel[];
  }

  export interface SpecialTaxDieselRequest {
    roadTaxAmtCatKey: number;
    chargeTypeKey: number;
    specialTaxAmt: number;
    effectiveDate: string;
    endDate: string;
    minPaymentFee: number;
    transactionId?: number;
  }

  export interface SpecialTaxRequest {
    requestHeader: {
      userId: number;
      managerId: any;
      ltaTeamTypeKey: number,
      userTypeKey: number
  }
  specialRoadTax: SpecialTaxDieselRequest[];
  }
  

