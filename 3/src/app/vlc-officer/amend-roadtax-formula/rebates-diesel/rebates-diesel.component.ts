import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DateOverlapValidation } from 'src/app/core/util/date-overlap-validation.service';
import { RebatesDieselService } from 'src/app/services/rebates-diesel.service';
import { RoadTaxService } from 'src/app/services/road-tax.service';
import { AMENDMENT_STATUS } from 'src/app/shared/constants/application.constants';
import { FormulaCardService } from 'src/app/widgets/formula-card/formula-card.service';
import { RebatesDieselTable } from './interface/rebates-diesel-table';

@Component({
  selector: 'app-rebates-diesel',
  templateUrl: './rebates-diesel.component.html',
  styleUrls: ['./rebates-diesel.component.scss']
})
export class RebatesDieselComponent implements OnInit {
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();
  
  placeHolder = "Select Rebate Type"
  columnHeaders: any[] = [
    {
      label: "Rebate Name",
      property: "rebateName",
      type: "textField",
    },
    {
      label: "Rebate Type",
      property: "rebateType",
      type: "selectField",
    },
    {
      label: "For Vehicles with Registration Date(s)",
      property: "vehicleRegistrationFrom",
      property2: "vehicleRegistrationTo",
      type: "rangeField",
    },
    {
      label: "Rebate Amount",
      property: "rebateAmount",
      type: "numberField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];

  headers: any[] = [
    {
      label: "",
      property: "warningTooltipButton",
      type: "",
    },
    ...this.columnHeaders,
    {
      label: "",
      property: "addButton",
      type: "",
    },
  ];
  rebates: any;

  addRowCount = 0;
  selection = [
    {
      value: "Flat Amount",
    },
    {
      value: "% of total Road Tax Amount",
    },
  ];
  showError: boolean | undefined;
  lastParentIndex!: any;
  baseRoadTax: any;
  additionalTax: any;
  constructor(private rebatesDieselService: RebatesDieselService, 
    private roadTaxService: RoadTaxService,
    private formulaCardService: FormulaCardService) { }

  ngOnInit(): void {
    this.rebatesDieselService.getRebatesDieselData().subscribe(res => {
      this.rebates = res;
      console.log("diesel rebates", this.rebates)
    })
  }

  onInputChange(changed: RebatesDieselTable) {
    const index = changed.index ?? 0;
    const changedFees = this.rebates[index];
    changedFees.rebateName = changed.rebateName;
    changedFees.rebateAmount = changed.rebateAmount;
    changedFees.newlyAdded = true;
    this.rebates = [...this.rebates];
    this.rebatesDieselService.setRebatesDiesel(this.rebates);
  }

  selectChange(select: RebatesDieselTable) {
    const index = select.index ?? 0;
    const changedFees = this.rebates[index];
    changedFees.rebateType = select.rebateType;
    changedFees.newlyAdded = true;
    changedFees.rebateTypeKey = this.formulaCardService.getTypeKey(
      select.rebateType
    );
    console.log(changedFees);
    this.rebates = [...this.rebates];
    this.rebatesDieselService.setRebatesDiesel(this.rebates);
  }

  addRebatesRow(data: any) {
    if (data.index || data.index !== 0) {
      const lastChildIndex = data[data.length - 1];
      const rebatesToBeAdded = this.createChild(lastChildIndex);
      this.rebates.push({
        active: false,
        effectiveDate: rebatesToBeAdded.effectiveDate,
        endDate: rebatesToBeAdded.endDate,
        vehicleRegistrationFrom: rebatesToBeAdded.vehicleRegistrationFrom,
        vehicleRegistrationTo: rebatesToBeAdded.vehicleRegistrationTo,
        index: data?.index || null,
        isAParent: true,
        isExpanded: false,
        newlyAdded: true,
        parentIndex: data?.index || null,
        rebateAmount: rebatesToBeAdded.rebateAmount,
        rebateName: rebatesToBeAdded.rebateName,
        rebateType: rebatesToBeAdded.rebateType,
        rebateTypeKey: rebatesToBeAdded.rebateTypeKey,
        fromApi: false,
        isNewRow: true,
      });
      this.rebates = [...this.rebates];
      this.rebatesDieselService.setRebatesDiesel(this.rebates);
    }

    this.addRowCount++;
  }
  getLastParentIndex(data: any): number {
    const lastChild = data[data.length - 1];
    return lastChild.index;
  }

  createChild(roadTax: RebatesDieselTable): RebatesDieselTable {
    const { effectiveDate, endDate, rebateAmount, rebateName, rebateTypeKey, rebateType} = {
      ...roadTax,
    };
    return {
      ...roadTax,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      vehicleRegistrationFrom: this.addDaysToDate(endDate, 1),
      vehicleRegistrationTo: this.addDaysToDate(endDate, 2),
      rebateAmount: rebateAmount,
      rebateName: rebateName,
      rebateTypeKey: rebateTypeKey,
      rebateType: rebateType,
      isAParent: true,
      isExpanded: false,
      parentIndex: roadTax?.index || undefined,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true,
    };
  }

  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.rebates.filter(
      (child: RebatesDieselTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  setParent(parentIndex: number) {
    const children = this.rebates.filter(
      (data: any) => data.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.rebates[parentIndex];
      parent.hasChildren = false;
    }
  }

  onProceedButtonClick(data: any) {
    const hasOverlappedDates = DateOverlapValidation.checkOverlapDates(
      this.rebates
    );
    console.log(hasOverlappedDates);
    if (hasOverlappedDates) {
      return alert("Invalid Dates");
    }
  }

  onDatepickerChange(changed: RebatesDieselTable) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.rebates[index];
    const effectiveDateChanged =
      changedSurcharge.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedSurcharge.effectiveDate = changed.effectiveDate;
      changedSurcharge.newlyAdded = true;
    }
    const endDateChanged =
      changedSurcharge.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedSurcharge.endDate = changed.endDate;
      changedSurcharge.newlyAdded = true;
    }
    this.rebates = [...this.rebates];
    this.rebatesDieselService.setRebatesDiesel(this.rebates);
  }

  onRangeDatepickerChange(changed: RebatesDieselTable) {
    const index = changed.index ?? 0;
    const changedDates = this.rebates[index];
    const effectiveDateChanged =
    changedDates.vehicleRegistrationFrom.getTime() !==
      changed.vehicleRegistrationFrom.getTime();
     
    if (effectiveDateChanged) {
      changedDates.vehicleRegistrationFrom = changed.vehicleRegistrationFrom;
      changedDates.newlyAdded = true;
    }
 
    const endDateChanged =
    changedDates.vehicleRegistrationTo.getTime() !== changed.vehicleRegistrationTo.getTime();
    if (endDateChanged) {
      changedDates.vehicleRegistrationTo = changed.vehicleRegistrationTo;
      changedDates.newlyAdded = true;
    }

    this.rebates = [...this.rebates];
    this.rebatesDieselService.setRebatesDiesel(this.rebates);
  }
  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }

  onRemoveRow(rebates: RebatesDieselTable) {
    let draftsParams: any[] = []
    draftsParams.push(rebates)
    if(rebates.amendmentStatus === AMENDMENT_STATUS.draft){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = rebates?.index;
    this.rebates.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = rebates.parentIndex ?? 0;
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.rebates = this.rebates.map((fee: any, index: any) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  submit(event: any){
this.proceedButtonClick.emit();
  }

  saveAsDraft(event: any) {
    let isDraft = true;
    this.saveDraft.emit(isDraft);
  }

}
