import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RebatesDieselComponent } from './rebates-diesel.component';

describe('RebatesDieselComponent', () => {
  let component: RebatesDieselComponent;
  let fixture: ComponentFixture<RebatesDieselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RebatesDieselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RebatesDieselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
