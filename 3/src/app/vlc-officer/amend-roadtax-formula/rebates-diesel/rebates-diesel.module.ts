import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RebatesDieselComponent } from './rebates-diesel.component';
import { ContextualTableModule } from 'src/app/widgets/formula-card/contextual-table/contextual-table.module';



@NgModule({
  declarations: [
    RebatesDieselComponent
  ],
  imports: [
    CommonModule,
    ContextualTableModule
  ],
  exports: [RebatesDieselComponent]
})
export class RebatesDieselModule { }
