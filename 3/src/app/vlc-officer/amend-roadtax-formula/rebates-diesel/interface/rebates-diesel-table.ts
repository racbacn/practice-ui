export interface RebatesDieselTable {
    rebateName: string;
    rebateAmount: number | undefined;
    effectiveDate: Date;
    rebateTypeKey: number | undefined;
    rebateType: string;
    endDate: Date;
    vehicleRegistrationFrom: Date;
    vehicleRegistrationTo: Date;
    active?: boolean | undefined;
    isAParent: boolean;
    isExpanded: boolean;
    parentIndex: number | undefined;
    index: number;
    newlyAdded: boolean;
    fromApi: boolean;
    isNewRow: boolean;
    transactionId: number | undefined;
    amendmentStatus: string;
}
