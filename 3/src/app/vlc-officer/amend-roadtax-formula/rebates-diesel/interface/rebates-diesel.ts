import { RebatesDieselRecords } from "./rebates-diesel-records";

export interface RebatesDiesel {
    RebateList: RebatesDieselRecords[];
}

export interface RebateDiselRequest {
    chargeTypeKey: number | undefined;
    rebateName: string;
    rebateAmount: number | undefined;
    effectiveDate: string;
    endDate: string;
    vehicleRegistrationTo: string;
    vehicleRegistrationFrom: string;
    transactionId?: number | undefined;

  }
  
  export interface TaxRebatesDieselRequest {
    requestHeader: {
      userId: number;
      managerId: any;
      ltaTeamTypeKey: number,
      userTypeKey: number
  }
  rebateValue: RebateDiselRequest[];
  }
