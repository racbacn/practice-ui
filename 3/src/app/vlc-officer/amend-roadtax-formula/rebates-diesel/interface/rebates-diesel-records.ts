export interface RebatesDieselRecords {
    rebateName: string;
    rebateTypeKey: number | undefined;
    rebateType: string;
    rebateAmount: number | undefined;
    vehicleRegistrationFrom: string;
    vehicleRegistrationTo: string;
    effectiveDate: string;
    endDate: string;
    active?: boolean | undefined;
    transactionId: number;
    amendmentStatus: string;
    rejectComment: string;
    roadTaxAmtCatKey: number;
}
