import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmendRoadtaxFormulaComponent } from './amend-roadtax-formula.component';

const routes: Routes = [{ path: '', component: AmendRoadtaxFormulaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmendRoadtaxFormulaRoutingModule { }
