import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DieselTabComponent } from './diesel-tab.component';
import { MaterialModule } from 'src/app/material.module';
import { WidgetsModule } from 'src/app/widgets/widgets.module';
import { FormulaCardLegendModule } from 'src/app/widgets/formula-card/formula-card-legend/formula-card-legend.module';
import { FormulaCardExportModule } from 'src/app/widgets/formula-card/formula-card-export/formula-card-export.module';
import { FormulaCardSelectModule } from 'src/app/widgets/formula-card/formula-card-select/formula-card-select.module';
import { SpecialTaxDieselModule } from '../special-tax-diesel/special-tax-diesel.module';
import { RebatesDieselModule } from '../rebates-diesel/rebates-diesel.module';
import { AdditionalChargeDieselModule } from '../additional-charge-diesel/additional-charge-diesel.module';



@NgModule({
  declarations: [
    DieselTabComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    WidgetsModule,
    FormulaCardLegendModule,
    FormulaCardExportModule,
    FormulaCardSelectModule,
    SpecialTaxDieselModule,
    RebatesDieselModule,
    AdditionalChargeDieselModule
  ],
  exports: [ DieselTabComponent ]
})
export class DieselTabModule { }
