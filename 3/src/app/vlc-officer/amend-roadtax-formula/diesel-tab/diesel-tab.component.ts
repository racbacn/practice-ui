import { Component, OnInit } from "@angular/core";
import { DateOverlapValidation } from "src/app/core/util/date-overlap-validation.service";
import { AdditionalChargeDieselService } from "src/app/services/additional-charge-diesel.service";
import { RebatesDieselService } from "src/app/services/rebates-diesel.service";
import { SpecialTaxDieselService } from "src/app/services/special-tax-diesel.service";
import { FormulaCardService } from "src/app/widgets/formula-card/formula-card.service";
import { Row } from "src/app/widgets/formula-card/popup/interfaces/row";
import { UiData } from "src/app/widgets/formula-card/popup/interfaces/ui-data";
import { PopupService } from "src/app/widgets/formula-card/services/popup.service";
import { Header } from "../special-tax-diesel/interface/special-tax-diesel";

@Component({
  selector: "app-diesel-tab",
  templateUrl: "./diesel-tab.component.html",
  styleUrls: ["./diesel-tab.component.scss"],
})
export class DieselTabComponent implements OnInit {
  options = [
    {
      label: "Special Tax (For Diesel or Diesel-CNG Cars)",
      value: "ST",
    },
    {
      label: "Road Tax Rebate(s)",
      value: "RTR",
    },
    {
      label: "Additional Road Tax Charge(s)",
      value: "ART",
    },
  ];
  commonPopupHeaders = [
    {
      label: "Effective Date:",
      property: "effectiveDate",
    },
    {
      label: "End Date:",
      property: "endDate",
    },
  ];
  showRoadTaxRebates!: boolean;
  showSpecialTax: boolean = true;
  showAdditionalRoadTax!: boolean;
  specialTax: any;
  rebates: any;
  additionalRoadTax: any;
  specialTaxData: any;
  rebatesData: any;
  additionalTaxData: any;
  isSuccessful!: boolean;
  lastParentIndexRebates: any;
  lastParentIndexAdditional: any;

  constructor(
    private formulaCardService: FormulaCardService,
    private specialTaxService: SpecialTaxDieselService,
    private rebatesDieselService: RebatesDieselService,
    private chargeService: AdditionalChargeDieselService,
    private popupService: PopupService
  ) {}

  async ngOnInit() {
    await this.getSpecialTax();
    await this.getRebatesDiesel();
    await this.getAdditionalTaxDiesel();
  }

  onSelectionChange(event: any) {
    if (event === "RTR") {
      this.showRoadTaxRebates = true;
      this.showSpecialTax = false;
      this.showAdditionalRoadTax = false;
      return;
    }
    if (event === "ST") {
      this.showSpecialTax = true;
      this.showRoadTaxRebates = false;
      this.showAdditionalRoadTax = false;
      return;
    }
    if (event === "ART") {
      this.showSpecialTax = false;
      this.showRoadTaxRebates = false;
      this.showAdditionalRoadTax = true;
      return;
    }
  }

  getSpecialTax() {
    this.specialTaxService.getSpecialTax().subscribe(
      (res) => {
        this.specialTax = res;
        this.specialTaxService.setSpecialTaxDieselData(this.specialTax);
      },
      (error: Error) =>
        this.formulaCardService.openErrorPopup(error.message || "Server Error")
    );
  }

  getRebatesDiesel() {
    this.rebatesDieselService.getRoadTaxRebatesDiesel().subscribe(
      (res) => {
        this.rebates = res;
        this.lastParentIndexRebates = res[res.length - 1];
        this.rebatesDieselService.setRebatesDiesel(this.rebates);
      },
      (error: Error) =>
        this.formulaCardService.openErrorPopup(error.message || "Server Error")
    );
  }

  getAdditionalTaxDiesel() {
    this.chargeService.getRAdditionalChargeDiesel().subscribe(
      (res) => {
        this.additionalRoadTax = res;
        this.lastParentIndexAdditional = res[res.length - 1];
        this.chargeService.setAdditionalTaxDiesel(this.additionalRoadTax);
      },
      (error: Error) =>
        this.formulaCardService.openErrorPopup(error.message || "Server Error")
    );
  }

  setPopupRows(rowData: { fees: any; headers: Header[] }) {
    const { fees, headers } = rowData;
    let displayedRows: any[] = [];
    fees.forEach((taxRebate: any) => {
      const firstLabel = headers[0].label;
      const row = [{ label: "", value: "", subvalue: "" }];
      Object.values(headers).forEach((header, index) => {
        const value = taxRebate[header.property];

        row.push({ label: header.label, value, subvalue: "" });
      });
      displayedRows.push(row);
    });
    return displayedRows;
  }

  getSpecialTaxRows(fees: any[]) {
    const headers = [
      {
        label: "Emission Standard",
        property: "powerRating",
      },
      {
        label: "Charge Type",
        property: "specialTaxChargeType",
      },
      {
        label: "Amount",
        property: "amount",
      },
      {
        label: "Min. Payment Fee",
        property: "minPaymentFee",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getRebatesDieselRows(fees: any[]) {
    const headers = [
      {
        label: "Rebate Name",
        property: "rebateName",
      },
      {
        label: "Rebate Type",
        property: "rebateType",
      },
      {
        label: "For Vehicles with Registration Date(s)",
        property: "registrationDates",
      },
      {
        label: "Rebate Amount",
        property: "rebateAmount",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getAdditionalTaxDieselRows(fees: any[]) {
    const headers = [
      {
        label: "Additional Charge Name",
        property: "addChargeName",
      },
      {
        label: "Charge Type",
        property: "addChargeType",
      },
      {
        label: "For Vehicles with Registration Date(s)",
        property: "registrationDates",
      },
      {
        label: "Charge Amount",
        property: "addChargeAmount",
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  async submit(isDraft: any) {
    let specialTaxopoverRows: any[] = [];
    let rebatePopoverRows: any[] = [];
    let additionalTaxRows: any[] = [];
    this.specialTaxData = this.getSpecialTaxChild();
    this.rebatesData = this.getRebatesChild();
    this.additionalTaxData = this.getAddChargeChild();
    if(this.specialTaxData.hasDateOverlap || this.rebatesData.hasDateOverlap || this.additionalTaxData.hasDateOverlap ){
      return  this.formulaCardService.openErrorPopup("Invalid Dates");

    }
  

    specialTaxopoverRows = this.getSpecialTaxRows(this.specialTaxData.data);
    rebatePopoverRows = this.getRebatesDieselRows(this.rebatesData.data);
    additionalTaxRows = this.getAdditionalTaxDieselRows(this.additionalTaxData.data);

    const popoverRows = [
      ...specialTaxopoverRows,
      ...rebatePopoverRows,
      ...additionalTaxRows,
    ];

    if (!isDraft) {
      const confirmationPopup = this.openConfirmationPopup(popoverRows);
      confirmationPopup.afterClosed().subscribe(async (data) => {
        if (data) {
          await this.saveSpecialTax(isDraft);
          await this.saveRebates(isDraft);
          await this.saveAddCharge(isDraft);
          this.formulaCardService.openSuccessPopup();
        }
      });
    } else {
      await this.saveSpecialTax(isDraft);
      await this.saveRebates(isDraft);
      await this.saveAddCharge(isDraft);
      let message = "Your values have been saved.";
      this.formulaCardService.openSuccessPopup(message);
    }
  }

  saveDraft(event: any) {
    console.log("drafta", event);
    this.submit(event);
  }

  async saveAddCharge(event: any) {
    if (
      (this.additionalTaxData.data[0].addChargeType &&
        this.additionalTaxData.data[0].addChargeAmount) !== "-" ||
      undefined
    ) {
      (
        await this.chargeService.saveAdditionalCharge(
          this.additionalTaxData.data,
          event
        )
      ).subscribe(
        () => {
          this.isSuccessful = true;
          this.getAdditionalTaxDiesel();
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }
    return this.isSuccessful;
  }

  async saveRebates(event: any) {
    if (
      (this.rebatesData.data[0].rebateAmount && this.rebatesData.data[0].rebateName) !==
        "-" ||
      undefined
    ) {
      (
        await this.rebatesDieselService.saveDieselRebate(
          this.rebatesData.data,
          event
        )
      ).subscribe(
        () => {
          this.isSuccessful = true;
          this.getRebatesDiesel();
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }
    return this.isSuccessful;
  }

  async saveSpecialTax(event: any) {
    if (
      (this.specialTaxData.data[0].powerRating &&
        this.specialTaxData.data[0].minPaymentFee) !== "-" ||
      undefined
    ) {
      (
        await this.specialTaxService.saveSpecialTax(this.specialTaxData.data, event)
      ).subscribe(
        () => {
          this.isSuccessful = true;
          this.getSpecialTax();
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || "Failed to submit to the server"
          )
      );
    }

    return this.isSuccessful;
  }

  openConfirmationPopup(displayedRows: Row[][]) {
    const uiData: UiData = {
      icon: "error",
      header: "Submit your VALUES for review?",
      subheader: "You will not be able to make amendments to your submission",
      displayedRows,
      buttons: [
        {
          text: "Back to Main",
          function: "close",
          class: "blue-button",
        },
        {
          text: "Approve",
          function: "submit",
          class: "blue-button",
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  getSpecialTaxChild() {
    let obj: any;
    let hasDateOverlap: boolean = false;
    let basetax: any[] = [];
    this.specialTaxService.getSpecialTaxDieselData().subscribe((res) => {
      let base = this.specialTaxService.formatValues(res);
    
      if (base.baseTax === undefined || base.baseTax.length === 0 || null || res === undefined) {
        basetax = [
          {
            powerRating: "-",
            specialTaxChargeType: "-",
            amount: "-",
            minPaymentFee: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return obj = {
          data: basetax,
          hasDateOverlap: hasDateOverlap,
        };
      } else {
        if (!base.isRejected) {
          hasDateOverlap = this.formulaCardService.hasDateOverlap(res);
          base.baseTax.forEach((data: any) => {
            data.specialTax.forEach((record: any) => {
              basetax.push(record);
            });
          });
        }
      }
      return obj = {
        data: basetax,
        hasDateOverlap: hasDateOverlap,
      };
    });
    return obj;
  }

  getRebatesChild() {
    let hasDateOverlap: boolean = false;
    let rebatestax: any;
    let obj: any;
    this.rebatesDieselService.getRebatesDieselData().subscribe((res) => {
      console.log("rebate", res);
      let rebate = this.rebatesDieselService.formatValues(
        res,
        this.lastParentIndexRebates
      );
      console.log("rebate", rebate);
      if (rebate.rebates === undefined || rebate.rebates.length === 0 || null) {
        rebatestax = [
          {
            rebateName: "-",
            rebateAmount: "-",
            rebateType: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return obj = {
          data: rebatestax,
          hasDateOverlap: hasDateOverlap,
        };
      } else {
        if (!rebate.isRejected) {
          hasDateOverlap = this.formulaCardService.hasRegDateOverlap(res);
        }
      }
      return obj = {
        data: rebate.rebates,
        hasDateOverlap: hasDateOverlap,
      };
    });
    return obj;
  }

  getAddChargeChild() {
    let charge: any;
    let hasDateOverlap = false;
    let obj: any;
    this.chargeService.getAdditionalTaxData().subscribe((res) => {
      let additionalTax = this.chargeService.formatValues(
        res,
        this.lastParentIndexAdditional
      );
      if (
        additionalTax.addCharge === undefined ||
        additionalTax.addCharge.length === 0 ||
        null
      ) {
        charge = [
          {
            addChargeName: "-",
            addChargeAmount: "-",
            addChargeType: "-",
            effectiveDate: "-",
            endDate: "-",
          },
        ];
        return obj = {
          data: charge,
          hasDateOverlap: hasDateOverlap,
        };
      } else {
        if (!additionalTax.isRejected) {
          hasDateOverlap = this.formulaCardService.hasRegDateOverlap(res);
        }
      }

      return obj = {
        data: additionalTax.addCharge,
        hasDateOverlap: hasDateOverlap,
      };
    });
    return obj;
  }
}
