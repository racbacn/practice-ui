import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DieselTabComponent } from './diesel-tab.component';

describe('DieselTabComponent', () => {
  let component: DieselTabComponent;
  let fixture: ComponentFixture<DieselTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DieselTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DieselTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
