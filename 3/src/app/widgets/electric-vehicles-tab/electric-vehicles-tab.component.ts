import { Component, OnInit } from '@angular/core';
import {
  FormulaCardService,
  RemoveRoadTaxPayload,
} from '../formula-card/formula-card.service';
import {
  ElectricVehiclesTabService,
  Fees,
} from './electric-vehicles-tab.service';
import { TableBaseRoadTaxFee } from './interfaces/table-base-road-tax-fee';
import { Row } from '../formula-card/popup/interfaces/row';
import { UiData } from '../formula-card/popup/interfaces/ui-data';
import { PopupService } from '../formula-card/services/popup.service';

type Selected = 'baseFee' | 'rebate' | 'addCharge';

interface PopupHeader {
  label: string;
  property: string;
}

interface SelectOption {
  label: string;
  value: string;
}

interface Header {
  label: string;
  property: string;
  type: string;
  width: string;
  property1?: string;
  property2?: string;
  property3?: string;
}

interface BaseFee {
  headers: Header[];
  fees: TableBaseRoadTaxFee[];
  removedDraftRows: TableBaseRoadTaxFee[];
}

@Component({
  selector: 'app-electric-vehicles-tab',
  templateUrl: './electric-vehicles-tab.component.html',
  styleUrls: ['./electric-vehicles-tab.component.scss'],
})
export class ElectricVehiclesTabComponent implements OnInit {
  settings = {
    showEcMultiplier: false,
  };
  commonHeaders: Header[] = [
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'dateField',
      width: '10%',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'dateField',
      width: '10%',
    },
  ];
  commonPopupHeaders = [
    {
      label: 'Effective Date:',
      property: 'effectiveDate',
    },
    {
      label: 'End Date:',
      property: 'endDate',
    },
  ];
  baseFee: BaseFee = {
    headers: [
      {
        label: '',
        property: 'warningTooltipButton',
        type: '',
        width: '5%',
      },
      {
        label: 'Power Rating (PR) in kW',
        property: 'engineCapacity',
        type: 'string',
        width: '15%',
      },
      {
        label: '6-Monthly Road Tax',
        property: 'sixMonthlyRoadTax',
        type: 'collapsible',
        width: '25%',
      },
      {
        label: 'New Value',
        property: 'newValue',
        property1: 'baseAmt',
        property2: 'ecMultiplier',
        property3: 'discount',
        type: 'formulaField',
        width: '30%',
      },
      ...this.commonHeaders,
      {
        label: '',
        property: 'rowAddButton',
        type: '',
        width: '5%',
      },
    ],
    fees: [],
    removedDraftRows: [],
  };
  rebate: any = {
    headers: [
      {
        label: '',
        property: 'warningTooltipButton',
        type: '',
        width: '5%',
      },
      {
        label: 'Rebate Name',
        property: 'rebateName',
        type: 'textField',
        width: '20%',
      },
      {
        label: 'Rebate Type',
        property: 'rebateType',
        type: 'selectField',
        width: '20%',
      },
      {
        label: 'For Vehicles with Registration Date(s) From',
        property: 'vehicleRegistrationFrom',
        type: 'dateField',
        width: '10%',
      },
      {
        label: 'Till',
        property: 'vehicleRegistrationTo',
        type: 'dateField',
        width: '10%',
      },
      {
        label: 'Rebate Amount',
        property: 'rebateAmount',
        type: 'numberField',
        width: '10%',
      },
      ...this.commonHeaders,
      {
        label: '',
        property: 'addButton',
        type: '',
        width: '5%',
      },
    ],
    fees: [],
    removedDraftRows: [],
  };
  addCharge = {
    headers: [
      {
        label: '',
        property: 'warningTooltipButton',
        type: '',
        width: '5%',
      },
      {
        label: 'Additional Charge Name',
        property: 'addChargeName',
        type: 'textField',
        width: '20%',
      },
      {
        label: 'Charge Type',
        property: 'addChargeType',
        type: 'selectField',
        width: '20%',
      },
      {
        label: 'For Vehicles with Registration Date(s) From',
        property: 'vehicleRegistrationFrom',
        type: 'dateField',
        width: '10%',
      },
      {
        label: 'Till',
        property: 'vehicleRegistrationTo',
        type: 'dateField',
        width: '10%',
      },
      {
        label: 'Charge Amount',
        property: 'addChargeAmount',
        type: 'numberField',
        width: '10%',
      },
      ...this.commonHeaders,
      {
        label: '',
        property: 'addButton',
        type: '',
        width: '5%',
      },
    ],
    fees: [],
    removedDraftRows: [],
  };
  selection = [
    {
      value: 'Flat amount',
    },
    {
      value: '% of total Road Tax Amount',
    },
  ];
  show = false;
  selectOptions: SelectOption[] = [
    {
      label: 'Base Road Tax Fee (Electric Cars)',
      value: 'baseFee',
    },
    {
      label: 'Road Tax Rebate(s)',
      value: 'rebate',
    },
    {
      label: 'Additional Flat Component of Road Tax',
      value: 'addCharge',
    },
  ];
  exportData = [];
  selected: Selected = 'baseFee';
  headers = [];

  constructor(
    private electricTabService: ElectricVehiclesTabService,
    private formulaCardService: FormulaCardService,
    private popupService: PopupService
  ) {}

  ngOnInit(): void {
    this.initializeElectricVehicles();
  }

  initializeElectricVehicles() {
    this.electricTabService.getAllElectricVehicles().subscribe(
      (response) => {
        this.baseFee.fees = response[0];
        this.rebate.fees = response[1];
        this.addCharge.fees = response[2];
        this.show = true;
      },
      (error: Error) => {
        const generic = 'Cannot get API data';
        this.formulaCardService.openErrorPopup(error.message || generic);
      }
    );
  }

  onSelectionChange(selected: Selected) {
    this.selected = selected;
  }

  closeAllChildRows() {
    this.baseFee.fees = this.baseFee.fees.map((fee: TableBaseRoadTaxFee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  expandParent(index: number | null) {
    if (index || index === 0) {
      const parentFee = this.baseFee.fees[index];
      parentFee.isExpanded = true;
      parentFee.hasChildren = true;
    }
  }

  expandChildren(parentIndex: number | null) {
    this.baseFee.fees = this.baseFee.fees.map((child: TableBaseRoadTaxFee) => {
      if (child.parentIndex === parentIndex) {
        child.isExpanded = true;
      }
      return child;
    });
  }

  getLastChildIndex(parentIndex: number): number {
    const children = this.baseFee.fees.filter(
      (child: TableBaseRoadTaxFee) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index;
  }

  newlyAddedCount: any = {};

  createChild(parent: TableBaseRoadTaxFee): TableBaseRoadTaxFee {
    let counter = this.newlyAddedCount[parent.index];
    if (!counter && counter !== 0) {
      counter = 0;
    }
    let lastChildIndex = this.getLastChildIndex(parent.index);
    if (!counter && lastChildIndex !== parent.index && !parent.hasChildren) {
      lastChildIndex--;
    }
    this.newlyAddedCount[parent.index] = counter + 1;
    const lastChild = this.baseFee.fees[lastChildIndex];
    const endDate = lastChild.endDate;
    return {
      ...parent,
      full: { ...parent },
      engineCapacity: '',
      newValuePopoverDisplay: parent.newValue,
      engineCapacityPopoverDisplay: parent.engineCapacity,
      minusText: parent.minusText,
      newValue: '',
      sixMonthlyRoadTax: '',
      effectiveDate: this.formulaCardService.addDaysToDate(endDate, 1),
      endDate: this.formulaCardService.addDaysToDate(endDate, 30),
      isAParent: false,
      isExpanded: true,
      parentIndex: parent.index,
      hasChildren: false,
      newlyAdded: true,
      fromApi: false,
      transactionId: NaN,
      showEcMultiplier: parent.index > 0,
    };
  }

  addChild(parent: TableBaseRoadTaxFee) {
    const lastChildIndex = this.getLastChildIndex(parent.index);
    const position = lastChildIndex + 1;
    const itemsToBeRemoved = 0;
    const lateFeeToBeAdded = this.createChild(parent);
    this.baseFee.fees.splice(position, itemsToBeRemoved, lateFeeToBeAdded);
    this.baseFee.fees = [...this.baseFee.fees];
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.baseFee.fees = this.baseFee.fees.map(
      (fee: TableBaseRoadTaxFee, index: number) => {
        fee.index = index;
        if (fee.isAParent) {
          currentParentIndex = index;
          return fee;
        }
        fee.parentIndex = currentParentIndex;
        return fee;
      }
    );
  }

  onAddNewChildClick(row: TableBaseRoadTaxFee) {
    if (!row.isExpanded) {
      this.closeAllChildRows();
    }
    this.expandParent(row.index);
    this.expandChildren(row.index);
    this.addChild(row);
    this.updateParentIndex();
  }

  getFees(type: Selected) {
    if (type === 'baseFee') {
      return this.baseFee;
    } else if (type === 'rebate') {
      return this.rebate;
    }
    return this.addCharge;
  }

  onAddParentRow(type: Selected) {
    const fees = this.getFees(type);
    const length = fees.fees.length;
    const sharedProperties = {
      transactionId: NaN,
      tooltip: '',
      fromApi: false,
      newlyAdded: true,
      amendmentStatus: '',
      active: false,
      rebateTypeKey:201,
      addChargeTypeKey: 201,
    };
    if (!length) {
      fees.fees.push({
        addChargeAmount: '',
        addChargeType: '',
        addChargeName: '',
        rebateType: '',
        effectiveDate: new Date(),
        endDate: new Date(),
        vehicleRegistrationFrom: new Date(),
        vehicleRegistrationTo: new Date(),
        ...sharedProperties,
      });
      fees.fees = [...fees.fees];
      return;
    }
    const lastRowIndex = length - 1;
    const lastRow: any = fees.fees[lastRowIndex];
    const endDate = lastRow.endDate;
    const vehicleRegistrationTo = lastRow.vehicleRegistrationTo;
    const newRow: any = {
      ...lastRow,
      effectiveDate: this.formulaCardService.addDaysToDate(endDate, 1),
      endDate: this.formulaCardService.addDaysToDate(endDate, 30),
      vehicleRegistrationFrom: this.formulaCardService.addDaysToDate(
        vehicleRegistrationTo,
        1
      ),
      vehicleRegistrationTo: this.formulaCardService.addDaysToDate(
        vehicleRegistrationTo,
        30
      ),
      ...sharedProperties,
    };
    fees.fees.push(newRow);
    fees.fees = [...fees.fees];
  }

  setParent(parentIndex: number) {
    const children = this.baseFee.fees.filter(
      (child: TableBaseRoadTaxFee) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.baseFee.fees[parentIndex];
      parent.hasChildren = false;
    }
  }

  async submitDeletedLateFee(type: Selected) {
    const fees = this.getFees(type);
    const removed = fees.removedDraftRows;
    try {
      const request = await this.electricTabService.callDeleteApi(removed);
      request.subscribe(
        () => {
          const message = 'The late fee draft row has been deleted.';
          this.formulaCardService.openSuccessPopup(message);
        },
        (error: Error) => {
          const message = 'late fee save API error';
          this.formulaCardService.openSuccessPopup(error.message || message);
        }
      );
    } catch (error) {
      this.formulaCardService.openErrorPopup('Cannot delete late fee.');
    }
  }

  async onRemoveBaseFeeRow(baseFee: TableBaseRoadTaxFee) {
    if (this.formulaCardService.isADraft(baseFee.amendmentStatus)) {
      this.baseFee.removedDraftRows.push({ ...baseFee, isRemoved: true });
    }
    const indexToBeRemoved = baseFee.index;
    this.baseFee.fees.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = baseFee.parentIndex ?? 0;
    this.setParent(parentIndex);
    await this.submitDeletedLateFee('baseFee');
  }

  async onRemoveParentRow(row: any, type: Selected) {
    let fees = this.getFees(type);
    if (this.formulaCardService.isADraft(row.amendmentStatus)) {
      fees.removedDraftRows.push({ ...row, isRemoved: true });
    }
    const indexToBeRemoved = row.index;
    fees.fees.splice(indexToBeRemoved, 1);
    fees.fees = fees.fees.map((fee: any, index: number) => {
      fee.index = index;
      return fee;
    });
    await this.submitDeletedLateFee(type);
  }

  onFieldSelectionChange(row: any, type: Selected) {
    const fees = this.getFees(type);
    const changed = fees.fees[row.index];
    const { rebateType, addChargeType } = row;
    changed.rebateType = rebateType;
    changed.addChargeType = addChargeType;
    changed.rebateTypeKey = this.formulaCardService.getTypeKey(rebateType);
    changed.addChargeTypeKey =
      this.formulaCardService.getTypeKey(addChargeType);
    changed.newlyAdded = true;
    fees.fees = [...fees.fees];
  }

  onInputChange(update: any, property: Selected) {
    const fees = this.getFees(property);
    const toUpdate = fees.fees[update.index];
    const { baseAmt, ecMultiplier, discount } = update;
    toUpdate.baseAmt = update.baseAmt;
    toUpdate.discount = update.discount;
    toUpdate.ecMultiplier = update.ecMultiplier;
    toUpdate.rebateName = update.rebateName;
    toUpdate.rebateAmount = update.rebateAmount;
    toUpdate.addChargeName = update.addChargeName;
    toUpdate.addChargeAmount = update.addChargeAmount;
    const computation = `[$${baseAmt} + $${ecMultiplier} (EC - 1,000)] x ${discount}`;
    toUpdate.newValuePopoverDisplay = computation || toUpdate.newValue;
    toUpdate.newlyAdded = true;
    fees.fees = [...fees.fees];
  }

  onDatepickerChange(value: any, property: Selected) {
    const fees = this.getFees(property);
    const index = value.index;
    const changed: any = fees.fees[index];
    changed.vehicleRegistrationFrom = value.vehicleRegistrationFrom;
    changed.vehicleRegistrationTo = value.vehicleRegistrationTo;
    changed.effectiveDate = value.effectiveDate;
    changed.endDate = value.endDate;
    changed.newlyAdded = true;
    fees.fees = [...fees.fees];
  }

  closeAllRows() {
    this.baseFee.fees = this.baseFee.fees.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  onToggleCollapse(row: any) {
    const { collapsedFee, currentFees } = row;
    this.baseFee.fees = currentFees;
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.baseFee.fees = this.baseFee.fees.map((fee, index) => {
      const isChild = fee.parentIndex === collapsedFee.index;
      const isItself = index === collapsedFee.index;
      if (isChild || isItself) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  checkForDateOverlaps() {
    const base = this.baseFee.fees;
    const baseDate = this.formulaCardService.hasDateOverlap(base);
    const rebate = this.rebate.fees;
    const rebateDate = this.formulaCardService.hasRegDateOverlap(rebate);
    const charge = this.addCharge.fees;
    const chargeDate = this.formulaCardService.hasRegDateOverlap(charge);
    return baseDate || rebateDate || chargeDate;
  }

  setPopupRows(rowData: { fees: any; headers: PopupHeader[] }) {
    const { fees, headers } = rowData;
    const rows: Row[][] = [];
    fees.forEach((fee: any) => {
      const row: Row[] = [];
      Object.values(headers).forEach((header: PopupHeader) => {
        let value = fee[header.property];
        if (header.property.includes('Date')) {
          value = this.formulaCardService.toString(value);
          value = this.formulaCardService.formatDate(value);
        }
        row.push({
          label: header.label,
          value,
          subvalue: '',
          remarks: fee.isRemoved ? 'This is removed' : '',
        });
      });
      rows.push(row);
    });
    return rows;
  }

  getPopoverBaseFeeRows(fees: any[]) {
    const headers = [
      {
        label: 'Power Rating (PR) in kW:',
        property: 'engineCapacityPopoverDisplay',
      },
      {
        label: '6-Monthly Road Tax:',
        property: 'newValuePopoverDisplay',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getPopoverRebateRows(fees: any[]) {
    const headers = [
      {
        label: 'Rebate Name:',
        property: 'rebateName',
      },
      {
        label: 'Rebate Amount:',
        property: 'rebateAmount',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getPopoverAddChargeRows(fees: any[]) {
    const headers = [
      {
        label: 'Additional Charge Name::',
        property: 'addChargeName',
      },
      {
        label: 'Rebate Amount:',
        property: 'addChargeAmount',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getAmendedFees(fees: any) {
    return fees.filter((fee: any) => fee?.newlyAdded);
  }

  getRemovedFees(fees: any) {
    return fees.filter((fee: any) => fee?.removed);
  }

  openConfirmationPopup(displayedRows: Row[][]) {
    const uiData: UiData = {
      icon: 'error',
      header: 'Submit your VALUES for review?',
      subheader: 'You will not be able to make amendments to your submission',
      displayedRows,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Submit',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  async submitRoadTaxFees(
    fees: Fees,
    removed: RemoveRoadTaxPayload[],
    saveAsDraft = false,
    message = 'Amended Formula value(s) successfully submitted for review'
  ) {
    const response = saveAsDraft
      ? await this.electricTabService.saveAsDraft(fees)
      : await this.electricTabService.submitRoadTaxFees(fees);
    response.subscribe(
      () => {
        this.formulaCardService.openSuccessPopup(message);
        this.initializeElectricVehicles();
      },
      (error: Error) => {
        const message = error.message || 'Cannot save data';
        this.formulaCardService.openErrorPopup(message);
      }
    );
  }

  getNewlyAddedRows() {
    const baseFee = this.getAmendedFees(this.baseFee.fees);
    const baseFeePopup = this.getPopoverBaseFeeRows(baseFee);
    const rebate = this.getAmendedFees(this.rebate.fees);
    const rebatePopup = this.getPopoverRebateRows(rebate);
    const addCharges = this.getAmendedFees(this.addCharge.fees);
    const addChargePopup = this.getPopoverAddChargeRows(addCharges);
    const removedBaseFee = this.baseFee.removedDraftRows;
    const removedBaseFeePopup = this.getPopoverBaseFeeRows(removedBaseFee);
    const removedRebate = this.rebate.removedDraftRows;
    const removedRebatePopup = this.getPopoverRebateRows(removedRebate);
    const removedAddCharge = this.addCharge.removedDraftRows;
    const removedAddChargePopup =
      this.getPopoverAddChargeRows(removedAddCharge);
    return {
      popupRows: [
        ...baseFeePopup,
        ...removedBaseFeePopup,
        ...rebatePopup,
        ...removedRebatePopup,
        ...addChargePopup,
        ...removedAddChargePopup,
      ],
      amended: { baseFee, rebate, addCharges },
      removedDraftRows: [
        ...removedBaseFee,
        ...removedRebate,
        ...removedAddCharge,
      ],
      allRemovedRows: [removedBaseFee, removedRebate, removedAddCharge],
      hasAddedRows: [...baseFee, ...rebate, ...addCharges].find(
        (row: any) =>
          !this.formulaCardService.isADraft(row.amendmentStatus) &&
          row.newlyAdded
      ),
    };
  }

  async onSaveAsDraftClick() {
    const newlyAdded = this.getNewlyAddedRows();
    if (!newlyAdded.hasAddedRows) {
      const errorMessage = 'Add rows first.';
      return this.formulaCardService.openErrorPopup(errorMessage);
    }
    const hasOverlap = this.checkForDateOverlaps();
    if (hasOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    const successMessage = 'Your values have been saved.';
    const amended = newlyAdded.amended;
    const removed = newlyAdded.removedDraftRows;
    await this.submitRoadTaxFees(amended, removed, true, successMessage);
  }

  onApproveButtonClick() {
    const newlyAdded = this.getNewlyAddedRows();
    if (!newlyAdded.popupRows.length && !newlyAdded.allRemovedRows.length) {
      const errorMessage = 'Add rows or remove draft rows first.';
      return this.formulaCardService.openErrorPopup(errorMessage);
    }
    const hasOverlap = this.checkForDateOverlaps();
    if (hasOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    const confirmationPopup = this.openConfirmationPopup(newlyAdded.popupRows);
    confirmationPopup.afterClosed().subscribe(async (data) => {
      if (data) {
        const amended = newlyAdded.amended;
        const removed = newlyAdded.removedDraftRows;
        await this.submitRoadTaxFees(amended, removed);
      }
    });
  }
}
