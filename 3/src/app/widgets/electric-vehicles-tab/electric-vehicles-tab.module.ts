import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElectricVehiclesTabComponent } from './electric-vehicles-tab.component';
import { FormulaCardContextualModule } from '../formula-card/formula-card-contextual/formula-card-contextual.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TitleHeaderModule } from '../approve-amended-road-tax/widgets/title-header/title-header.module';
import { SubtitleTextgroupModule } from '../approve-amended-road-tax/widgets/subtitle-textgroup/subtitle-textgroup.module';
import { PetrolVehiclesRoutingModule } from '../approve-amended-road-tax/petrol-vehicles/petrol-vehicles-routing.module';
import { FormulaCardSelectModule } from '../formula-card/formula-card-select/formula-card-select.module';
import { FormulaCardExportModule } from '../formula-card/formula-card-export/formula-card-export.module';
import { FormulaCardLegendModule } from '../formula-card/formula-card-legend/formula-card-legend.module';
import { HeaderTableDividerModule } from '../formula-card/header-table-divider/header-table-divider.module';
import { ContextualTableModule } from '../formula-card/contextual-table/contextual-table.module';

@NgModule({
  declarations: [ElectricVehiclesTabComponent],
  imports: [
    CommonModule,
    FormulaCardContextualModule,
    FlexLayoutModule,
    TitleHeaderModule,
    SubtitleTextgroupModule,
    PetrolVehiclesRoutingModule,
    FormulaCardSelectModule,
    FormulaCardExportModule,
    FormulaCardLegendModule,
    HeaderTableDividerModule,
    ContextualTableModule,
  ],
  exports: [ElectricVehiclesTabComponent],
})
export class ElectricVehiclesTabModule {}
