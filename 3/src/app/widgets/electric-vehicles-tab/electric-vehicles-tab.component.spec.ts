import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricVehiclesTabComponent } from './electric-vehicles-tab.component';

describe('ElectricVehiclesTabComponent', () => {
  let component: ElectricVehiclesTabComponent;
  let fixture: ComponentFixture<ElectricVehiclesTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricVehiclesTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricVehiclesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
