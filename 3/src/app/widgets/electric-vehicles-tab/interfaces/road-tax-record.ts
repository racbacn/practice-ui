export interface RoadTaxRecord {
  active: boolean;
  amendmentStatus: string;
  baseAmt: number;
  discount: number;
  ecMultiplier: number;
  effectiveDate: string;
  endDate: string;
  transactionId: number;
}
