import { RoadTaxRecord } from './road-tax-record';

export interface TableBaseRoadTaxFee {
  engineCapacity: string;
  roadTaxAmtCatKey: number;
  active: boolean;
  amendmentStatus: string;
  newValue: string;
  baseAmt: string;
  discount: number;
  ecMultiplier: number;
  effectiveDate: Date;
  endDate: Date;
  transactionId: number;
  isAParent: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  isExpanded: boolean;
  newlyAdded: boolean;
  readOnly: boolean;
  fromApi: boolean;
  index: number;
  hasBackground: boolean;
  backgroundColor: string;
  tooltip: string;
  isRemoved: boolean;
  full: any;
  newValuePopoverDisplay?: string;
  engineCapacityPopoverDisplay?: string;
  forResubmission: boolean;
  sixMonthlyRoadTax: string;
  minusText: string;
  showEcMultiplier: boolean;
  isOfficer: boolean;
  parentNotEditable?: boolean;
}
