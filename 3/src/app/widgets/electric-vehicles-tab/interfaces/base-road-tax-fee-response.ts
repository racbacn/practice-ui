import { BaseRoadTaxFee } from './base-road-tax-fee';

export interface BaseRoadTaxFeeResponse {
  BaseRoadTaxFee: BaseRoadTaxFee[];
}
