import { EvMinusValues } from '../../formula-card/formula-card.service';
import { RoadTaxRecord } from './road-tax-record';

export interface BaseRoadTaxFee {
  engineCapacity: EvMinusValues;
  roadTaxAmtCatKey: number;
  roadTaxRecords: RoadTaxRecord[];
}
