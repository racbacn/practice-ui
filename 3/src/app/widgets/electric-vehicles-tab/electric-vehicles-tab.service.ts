import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {
  FormulaCardService,
  RemoveRoadTaxPayload,
} from '../formula-card/formula-card.service';
import { BaseRoadTaxFee } from './interfaces/base-road-tax-fee';
import { BaseRoadTaxFeeResponse } from './interfaces/base-road-tax-fee-response';
import { RoadTaxRecord } from './interfaces/road-tax-record';
import { TableBaseRoadTaxFee } from './interfaces/table-base-road-tax-fee';

export interface Fees {
  baseFee: any;
  addCharges: any;
  rebate: any;
}

type EndpointType = 'baseFee' | 'rebate' | 'addCharge';

@Injectable({
  providedIn: 'root',
})
export class ElectricVehiclesTabService {
  private SERVER_URL = environment.serverUrl;
  private API_URL = this.SERVER_URL + '/manage-roadtax-service/electric';
  private SUBMIT_TYPE = 'save';
  private endpoints = {
    baseFee: {
      save: '/saveBaseFee',
      draft: '/baseFee/draft',
      delete: '/baseFee/delete',
    },
    rebate: {
      save: '/saveRebate',
      draft: '/rebate/draft',
      delete: '/rebate/delete',
    },
    addCharge: {
      save: '/saveAdditionalCharge',
      draft: '/additionalCharge/draft',
      delete: '/additionalCharge/delete',
    },
  };

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService
  ) {}

  setTooltipMessage(record: any) {
    const { amendmentStatus, rejectComment } = record;
    const isRejected = this.formulaCardService.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  getActiveRoadTax(
    activeBaseRoadTaxFee: RoadTaxRecord,
    baseRoadTaxFee: BaseRoadTaxFee
  ) {
    const { discount, ecMultiplier, baseAmt, active } = activeBaseRoadTaxFee;
    const { effectiveDate, endDate, transactionId } = activeBaseRoadTaxFee;
    const { roadTaxAmtCatKey, engineCapacity } = baseRoadTaxFee;
    const full = { engineCapacity, roadTaxAmtCatKey, ...activeBaseRoadTaxFee };
    const formula = this.formulaCardService.getEvFormula(
      baseAmt,
      ecMultiplier,
      discount,
      engineCapacity
    );
    return {
      roadTaxAmtCatKey,
      baseAmt: baseAmt.toString(),
      discount,
      ecMultiplier,
      transactionId,
      engineCapacity,
      engineCapacityPopoverDisplay: engineCapacity,
      active: active || false,
      sixMonthlyRoadTax: formula,
      minusText: this.formulaCardService.getPrMinusValue(engineCapacity),
      newValue: formula,
      newValuePopoverDisplay: formula,
      effectiveDate: this.formulaCardService.formatDate(effectiveDate),
      endDate: this.formulaCardService.formatDate(endDate),
      full,
    };
  }

  getInactiveRoadTaxChild(
    activeBaseRoadTaxFee: RoadTaxRecord,
    baseRoadTaxFee: BaseRoadTaxFee,
    child: RoadTaxRecord
  ) {
    const { roadTaxAmtCatKey, engineCapacity } = baseRoadTaxFee;
    const { effectiveDate, endDate, baseAmt, ecMultiplier, discount } = child;
    const formula = this.formulaCardService.getEvFormula(
      baseAmt,
      ecMultiplier,
      discount,
      engineCapacity
    );
    return {
      roadTaxAmtCatKey,
      baseAmt: baseAmt.toString(),
      discount,
      ecMultiplier,
      transactionId: child.transactionId,
      engineCapacity: '',
      engineCapacityPopoverDisplay: engineCapacity,
      active: child?.active || false,
      sixMonthlyRoadTax: '',
      minusText: this.formulaCardService.getPrMinusValue(engineCapacity),
      newValue: formula,
      newValuePopoverDisplay: formula,
      effectiveDate: this.formulaCardService.formatDate(effectiveDate),
      endDate: this.formulaCardService.formatDate(endDate),
      full: { engineCapacity, roadTaxAmtCatKey, ...child },
    };
  }

  getParentRowProperties(
    record: RoadTaxRecord,
    index: number,
    inactive?: RoadTaxRecord[]
  ) {
    return {
      isAParent: true,
      isExpanded: false,
      parentIndex: null,
      hasChildren: inactive?.length ? !!inactive.length : false,
      newlyAdded:
        this.formulaCardService.isRejected(record.amendmentStatus) ||
        this.formulaCardService.isADraft(record.amendmentStatus),
      readOnly: true,
      fromApi: true,
      index,
      hasBackground: false,
      backgroundColor: '',
      isAResponse: true,
      tooltip: this.setTooltipMessage(record),
      isRemoved: false,
      full: record,
      amendmentStatus: record.amendmentStatus,
      showEcMultiplier: index > 0,
    };
  }

  getChildRowProperties(
    record: RoadTaxRecord,
    index: number,
    parentIndex: number
  ) {
    return {
      isAParent: false,
      isExpanded: false,
      parentIndex,
      hasChildren: false,
      newlyAdded:
        this.formulaCardService.isRejected(record.amendmentStatus) ||
        this.formulaCardService.isADraft(record.amendmentStatus),
      readOnly: false,
      fromApi: true,
      index,
      hasBackground: false,
      backgroundColor: '',
      isAResponse: true,
      tooltip: this.setTooltipMessage(record),
      isRemoved: false,
      full: record,
      amendmentStatus: record.amendmentStatus,
      showEcMultiplier: parentIndex > 0,
    };
  }

  formatBaseRoadTaxFees(data: BaseRoadTaxFeeResponse) {
    const tableFees: TableBaseRoadTaxFee[] = [];
    let index = -1;
    let parentIndex = -1;
    let forResubmission = false;
    let noActiveFee = false;
    data.BaseRoadTaxFee.forEach((baseFee: BaseRoadTaxFee) => {
      baseFee.roadTaxRecords.find((record: RoadTaxRecord) => {
        if (record.amendmentStatus.toLowerCase() === 'manager rejected') {
          forResubmission = true;
          noActiveFee = false;
          return true;
        }
        return false;
      });
    });
    data.BaseRoadTaxFee.forEach((fee: BaseRoadTaxFee) => {
      index++;
      parentIndex = index;
      let activeFee!: RoadTaxRecord;
      const inactive: RoadTaxRecord[] = [];
      noActiveFee = false;
      fee.roadTaxRecords.forEach((record: RoadTaxRecord) => {
        if (record.active) {
          activeFee = record;
          return;
        }
        inactive.push(record);
      });
      if (!activeFee) {
        if (!fee.roadTaxRecords[0]) {
          return;
        }
        activeFee = fee.roadTaxRecords[0];
        noActiveFee = true;
      }
      const parentBaseRoadTax: TableBaseRoadTaxFee = {
        ...this.getActiveRoadTax(activeFee, fee),
        ...this.getParentRowProperties(activeFee, index, inactive),
        hasChildren: fee.roadTaxRecords.length > 1,
        forResubmission,
        isOfficer: true,
        parentNotEditable: true,
      };
      tableFees.push(parentBaseRoadTax);
      if (!inactive.length) {
        return;
      }
      inactive.forEach((child: RoadTaxRecord, childIndex: number) => {
        if (noActiveFee && !childIndex) {
          return;
        }
        index++;
        const chilBaseRoadTaxFee: TableBaseRoadTaxFee = {
          ...this.getInactiveRoadTaxChild(activeFee, fee, child),
          ...this.getChildRowProperties(child, index, parentIndex),
          forResubmission,
          isOfficer: true,
        };
        tableFees.push(chilBaseRoadTaxFee);
      });
    });
    return tableFees;
  }

  formatNoChildFee(data: any, property: string) {
    let forResubmission = false;
    data[property].find((record: any) => {
      if (record.amendmentStatus.toLowerCase() === 'manager rejected') {
        forResubmission = true;
        return true;
      }
      return false;
    });
    const tableFees = data[property].map((record: any, index: number) => {
      const {
        vehicleRegistrationFrom,
        vehicleRegistrationTo,
        effectiveDate,
        endDate,
      } = record;
      return {
        ...record,
        ...this.getParentRowProperties(record, index),
        vehicleRegistrationFrom: this.formulaCardService.formatDate(
          vehicleRegistrationFrom
        ),
        vehicleRegistrationTo: this.formulaCardService.formatDate(
          vehicleRegistrationTo
        ),
        effectiveDate: this.formulaCardService.formatDate(effectiveDate),
        endDate: this.formulaCardService.formatDate(endDate),
        forResubmission,
        isOfficer: true,
        roadTaxAmtCatKey: record.roadTaxAmtCatKey,
      };
    });
    return tableFees;
  }

  formatResponse(data: any, property: string): any[] {
    let formattedFees: any[] = [];
    if (property === 'BaseRoadTaxFee') {
      formattedFees = this.formatBaseRoadTaxFees(data);
    } else if (property === 'RebateList') {
      formattedFees = this.formatNoChildFee(data, property);
    } else if (property === 'AdditionalChargeList') {
      formattedFees = this.formatNoChildFee(data, property);
    }
    return formattedFees;
  }

  formatPayload(valueCategory: string, property: string): Observable<any> {
    const endpoint = '/manage-roadtax-service/get/RoadTaxValues';
    const electricTabActive = 3;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', electricTabActive);
    params = params.append('userId', userId);
    params = params.append('valueCategory', valueCategory);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    params = params.append('userTypeKey', userTypeKey);
    return this.http.get<any>(this.SERVER_URL + endpoint, { params }).pipe(
      map((response: any) => {
        const hasLength = response[property].length;
        return hasLength ? this.formatResponse(response, property) : [];
      })
    );
  }

  getAllElectricVehicles() {
    const baseFee = this.formatPayload('baseFee', 'BaseRoadTaxFee');
    const rebate = this.formatPayload('rebate', 'RebateList');
    const addCharge = this.formatPayload('addCharge', 'AdditionalChargeList');
    return forkJoin([baseFee, rebate, addCharge]);
  }

  setBaseFeePayload(requestHeader: any, baseFee: any) {
    if (!baseFee.length) {
      return false;
    }
    const baseRoadTaxFee = baseFee.map((fee: any) => {
      const formatted: any = {
        roadTaxAmtCatKey: fee.roadTaxAmtCatKey,
        baseAmt: +fee.baseAmt,
        discount: +fee.discount,
        effectiveDate: this.formulaCardService.toString(fee.effectiveDate),
        endDate: this.formulaCardService.toString(fee.endDate),
        ecMultiplier: +fee.ecMultiplier,
      };
      if (fee.transactionId) {
        formatted.transactionId = fee.transactionId;
      }
      return formatted;
    });
    const apiUrl = this.setApiUrl('baseFee');
    const payload = { requestHeader, baseRoadTaxFee };
    return this.http.post<any>(apiUrl, payload);
  }

  setRebatePayload(requestHeader: any, rebate: any) {
    if (!rebate.length) {
      return false;
    }
    const rebateValue = rebate.map((fee: any) => {
      const formatted: any = {
        chargeTypeKey: fee.rebateTypeKey,
        rebateName: fee.rebateName,
        rebateAmount: fee.rebateAmount,
        vehicleRegistrationFrom: this.formulaCardService.toString(
          fee.vehicleRegistrationFrom
        ),
        vehicleRegistrationTo: this.formulaCardService.toString(
          fee.vehicleRegistrationTo
        ),
        effectiveDate: this.formulaCardService.toString(fee.effectiveDate),
        endDate: this.formulaCardService.toString(fee.endDate),
      };
      if (fee.transactionId) {
        formatted.transactionId = fee.transactionId;
      }
      return formatted;
    });
    const apiUrl = this.setApiUrl('rebate');
    const payload = { requestHeader, rebateValue };
    return this.http.post<any>(apiUrl, payload);
  }

  setAddChargePayload(requestHeader: any, addCharge: any) {
    if (!addCharge.length) {
      return false;
    }
    const additionalCharges = addCharge.map((fee: any) => {
      const formatted: any = {
        addChargeTypeKey: fee.addChargeTypeKey,
        addChargeName: fee.addChargeName,
        addChargeValue: +fee.addChargeAmount,
        vehicleRegistrationFrom: this.formulaCardService.toString(
          fee.vehicleRegistrationFrom
        ),
        vehicleRegistrationTo: this.formulaCardService.toString(
          fee.vehicleRegistrationTo
        ),
        effectiveDate: this.formulaCardService.toString(fee.effectiveDate),
        endDate: this.formulaCardService.toString(fee.endDate),
      };
      if (fee.transactionId) {
        formatted.transactionId = fee.transactionId;
      }
      return formatted;
    });
    const apiUrl = this.setApiUrl('addCharge');
    const payload = { requestHeader, additionalCharges };
    return this.http.post<any>(apiUrl, payload);
  }

  setApiUrl(endpointType: EndpointType) {
    const type = this.endpoints[endpointType];
    const endpoint = this.SUBMIT_TYPE === 'save' ? type.save : type.draft;
    return this.API_URL + endpoint;
  }

  async callSaveSubscription(fees: Fees): Promise<Observable<any>> {
    const header = await this.formulaCardService.getOfficerSaveRequestHeader();
    const subscriptions: Observable<any>[] = [];
    const { baseFee, rebate, addCharges } = fees;
    const baseFeeRequest = this.setBaseFeePayload(header, baseFee);
    if (baseFeeRequest) {
      subscriptions.push(baseFeeRequest);
    }
    const rebateRequest = this.setRebatePayload(header, rebate);
    if (rebateRequest) {
      subscriptions.push(rebateRequest);
    }
    const addChargeRequest = this.setAddChargePayload(header, addCharges);
    if (addChargeRequest) {
      subscriptions.push(addChargeRequest);
    }
    return forkJoin(subscriptions);
  }

  async callDeleteApi(fees: RemoveRoadTaxPayload[]) {
    return await this.formulaCardService.deleteRoadTaxValue(fees);
  }

  async submitRoadTaxFees(fees: Fees): Promise<Observable<any>> {
    this.SUBMIT_TYPE = 'save';
    const subscriptions: Observable<any>[] = [];
    const saveSubscription = await this.callSaveSubscription(fees);
    subscriptions.push(saveSubscription);
    return forkJoin(subscriptions);
  }

  async saveAsDraft(fees: Fees): Promise<Observable<any>> {
    this.SUBMIT_TYPE = 'draft';
    const subscriptions: Observable<any>[] = [];
    const saveSubscription = await this.callSaveSubscription(fees);
    subscriptions.push(saveSubscription);
    return forkJoin(subscriptions);
  }
}
