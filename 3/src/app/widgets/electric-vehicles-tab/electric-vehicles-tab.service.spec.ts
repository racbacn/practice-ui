import { TestBed } from '@angular/core/testing';

import { ElectricVehiclesTabService } from './electric-vehicles-tab.service';

describe('ElectricVehiclesTabService', () => {
  let service: ElectricVehiclesTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElectricVehiclesTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
