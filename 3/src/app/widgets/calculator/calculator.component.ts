import { Component, OnInit } from "@angular/core";
import { CalculatorApiService } from "../../services/calculator-api.service";
import { RoadTaxPayment } from "../../interface/road-tax-payment";
import { CalculatorInputs } from "../../interface/calculator-inputs";
import { BaseRoadTaxAmount } from "../../interface/base-road-tax-amount";
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from "@angular/material/core";
import { MomentDateAdapter } from "@angular/material-moment-adapter";
import * as moment from "moment";

export const MY_FORMATS = {
  parse: {
    dateInput: "DD-MM-YYYY",
  },
  display: {
    dateInput: "DD-MM-YYYY",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "DD-MM-YYYY",
    monthYearA11yLabel: "DD-MM-YYYY",
  },
};

@Component({
  selector: "app-calculator",
  templateUrl: "./calculator.component.html",
  styleUrls: ["./calculator.component.scss"],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class CalculatorComponent implements OnInit {
  selectedPropellantType!: number;
  selectedEmissionStandard!: number;
  engineCapacity!: string;
  powerRating!: string;
  roadTaxStartDate!: string;
  roadTaxEndDate!: string;
  vehicleRegistrationDate!: string;
  resultsDisplay: boolean = false;

  propellantTypeList: Array<any> = [
    {
      label: "Petrol",
      value: 1,
    },
    {
      label: "Diesel",
      value: 2,
    },
    {
      label: "Electric",
      value: 3,
    },
    {
      label: "Petrol-Electric",
      value: 4,
    },
  ];

  EmissionStandardList: Array<any> = [];
  nonDieselEmissionStandardList: Array<any> = [
    {
      label: "N/A",
      value: 0,
    },
  ];

  dieselEmissionStandardList: Array<any> = [
    {
      label: "EURO I-III",
      value: 106,
    },
    {
      label: "EURO IV",
      value: 107,
    },
    {
      label: "EURO V",
      value: 108,
    },
    {
      label: "JPN2009",
      value: 109,
    },
  ];

  onSelectPropellantType(event: any) {
    this.selectedPropellantType = event;
    switch (event) {
      case 1:
        this.EmissionStandardList = this.nonDieselEmissionStandardList;
        break;
      case 2:
        this.EmissionStandardList = this.dieselEmissionStandardList;
        break;
      case 3:
        this.EmissionStandardList = this.nonDieselEmissionStandardList;
        break;
      case 4:
        this.EmissionStandardList = this.nonDieselEmissionStandardList;
        break;
    }
    this.engineCapacity = "";
    this.powerRating = "";
  }

  onSelectEmissionStandard(event: any) {
    this.selectedEmissionStandard = event;
  }

  onSelectedVehicleRegistrationDate(event: any) {
    this.vehicleRegistrationDate = event;
  }

  roadTaxPayments: Array<RoadTaxPayment> = [];
  baseRoadTaxAmountBreakdown: Array<BaseRoadTaxAmount> = [];
  constructor(private calculatorAPIService: CalculatorApiService) {}

  ngOnInit(): void {}

  onCalculateRoadTax() {
    let calculatorInputs: CalculatorInputs = {
      vehicleRefNum: "sampleVEH",
      vehiclePropellantTypeKey: this.selectedPropellantType,
      engineCapacity: this.engineCapacity,
      powerRating: this.powerRating,
      emissionStandardCategoryKey: this.selectedEmissionStandard,
      vehicleAge: this.calculateVehicleAge(),
      licensingStartDate: moment(this.roadTaxStartDate).format("DD-MM-YYYY"),
      licensingEndDate: moment(this.roadTaxEndDate).format("DD-MM-YYYY"),
      vehicleRegistrationDate: moment(this.vehicleRegistrationDate).format(
        "DD-MM-YYYY"
      ),
    };

    this.calculatorAPIService
      .calculateRoadTax(calculatorInputs)
      .subscribe((calculatorOutputs) => {
        console.log(calculatorOutputs);
        if (calculatorOutputs) {
          this.roadTaxPayments = calculatorOutputs.roadTaxPayments;
          this.getBaseRoadTaxAmountBreakDown();
          this.resultsDisplay = true;
        }
      });
  }

  calculateVehicleAge(
    dateToday = moment(new Date()).format("DD-MM-YYYY"),
    regDate = moment(this.vehicleRegistrationDate).format("DD-MM-YYYY")
  ) {
    let dateTodayArray = dateToday.split("-");
    let RegDateArray = regDate.split("-");
    let dateTodayDay = parseInt(dateTodayArray[0]);
    let dateTodayMonth = parseInt(dateTodayArray[1]);
    let dateTodayYear = parseInt(dateTodayArray[2]);
    let RegDateDay = parseInt(RegDateArray[0]);
    let RegDateMonth = parseInt(RegDateArray[1]);
    let RegDateYear = parseInt(RegDateArray[2]);
    let vehicleAge = dateTodayYear - RegDateYear;
    if (
      dateTodayMonth < RegDateMonth ||
      (dateTodayMonth == RegDateMonth && dateTodayDay < RegDateDay)
    ) {
      vehicleAge = vehicleAge - 1;
      if (vehicleAge < 0) {
        vehicleAge = 0;
      }
    }
    return vehicleAge;
  }

  getBaseRoadTaxAmountBreakDown() {
    let sixMonthRoadTaxAmountBreakdown: Array<BaseRoadTaxAmount> = [];
    let nonSixMonthRoadTaxAmountBreakdown: Array<BaseRoadTaxAmount> = [];

    this.roadTaxPayments[0].baseRoadTaxAmountBreakdown.forEach((data) => {
      if (data.Name === "6-Monthly Road Tax Amount") {
        sixMonthRoadTaxAmountBreakdown.push(data);
      } else {
        nonSixMonthRoadTaxAmountBreakdown.push(data);
      }
    });
    let maxSixMonthRoadTaxAmountBreakdown: BaseRoadTaxAmount =
      sixMonthRoadTaxAmountBreakdown.reduce((max, data) =>
        parseInt(max.Amount) > parseInt(data.Amount) ? max : data
      );
    this.baseRoadTaxAmountBreakdown = [
      maxSixMonthRoadTaxAmountBreakdown,
      ...nonSixMonthRoadTaxAmountBreakdown,
    ];
  }
}
