import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Output() pageMove: EventEmitter<any> = new EventEmitter<any>();
  @Input() pageNo = 0;

  constructor() {}

  ngOnInit(): void {}

  onNextPageClick(): void {
    const pageNumber = this.pageNo + 1;
    this.onPageMove(pageNumber);
  }

  onPreviousPageClick(): void {
    const pageNumber = this.pageNo - 1;
    this.onPageMove(pageNumber);
  }

  onPageMove(pageNumber: number): void {
    this.pageMove.next(pageNumber);
  }
}
