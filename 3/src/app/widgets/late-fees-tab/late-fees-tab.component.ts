import { Component, OnInit } from '@angular/core';
import { FormulaCardService } from '../formula-card/formula-card.service';
import { UiData } from '../formula-card/popup/interfaces/ui-data';
import { PopupService } from '../formula-card/services/popup.service';
import { LateFee } from './interfaces/late-fee';
import { LateFeeRecord } from './interfaces/late-fee-record';
import { LateFeeValue } from './interfaces/late-fee-value';
import { TableLateFee } from './interfaces/table-late-fee';
import { LateFeesTabService } from './late-fees-tab.service';
import { Row } from '../formula-card/popup/interfaces/row';

@Component({
  selector: 'app-late-fees-tab',
  templateUrl: './late-fees-tab.component.html',
  styleUrls: ['./late-fees-tab.component.scss'],
})
export class LateFeesTabComponent implements OnInit {
  settings = {
    submitButtonText: 'Proceed',
    hasSelectOptions: false,
    headerText: 'Late renewal fees for Cars',
    hasPreviewButton: false,
    hasSaveAsDraftButton: true,
  };
  spreadsheetHeaders: any[] = [
    {
      label: 'Engine Capacity',
      property: 'engineCapacity',
      type: 'collapsible',
      width: '10%',
    },
    {
      label: 'Within 1 month of expiry date',
      property: 'lateFeePeriod1',
      type: 'numberField',
      width: '10%',
    },
    {
      label: 'Between 1 and 2.5 months',
      property: 'lateFeePeriod2',
      type: 'numberField',
      width: '10%',
    },
    {
      label: 'More than 2.5 months',
      property: 'lateFeePeriod3',
      type: 'numberField',
      width: '10%',
    },
    {
      label: 'More than 3 months',
      property: 'lateFeePeriod4',
      type: 'numberField',
      width: '10%',
    },
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'dateField',
      width: '20%',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'dateField',
      width: '20%',
    },
  ];
  uiTableHeaders: any[] = [
    {
      label: '',
      property: 'warningTooltipButton',
      type: '',
      width: '5%',
    },
    ...this.spreadsheetHeaders,
    {
      label: '',
      property: 'rowAddButton',
      type: '',
      width: '5%',
    },
  ];
  lateFees: TableLateFee[] = [];
  exportData = {
    headers: this.spreadsheetHeaders,
    rows: this.lateFees,
    filename: 'late-fee.xlsx',
  };
  addRowCount = 0;
  removedDraftRows: TableLateFee[] = [];

  constructor(
    private lateFeesTabService: LateFeesTabService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService
  ) {}

  ngOnInit(): void {
    this.initializeLateFees();
  }

  private initializeLateFees(): void {
    this.addRowCount = 0;
    this.lateFeesTabService.getLateFee().subscribe(
      (response: TableLateFee[]) => {
        this.lateFees = response;
      },
      (error: Error) => {
        const generic = 'Cannot get API data';
        this.formulaCardService.openErrorPopup(error.message || generic);
      }
    );
  }

  async submitLateFeePayload(
    lateFees: any,
    saveAsDraft = false,
    message = 'Amended Formula value(s) successfully submitted for review'
  ) {
    const values = this.createLateFeeValues(lateFees);
    let response = saveAsDraft
      ? await this.lateFeesTabService.saveAsDraft(values)
      : await this.lateFeesTabService.submit(values);
    response.subscribe(
      () => {
        this.formulaCardService.openSuccessPopup(message);
        this.initializeLateFees();
      },
      (error: Error) =>
        this.formulaCardService.openErrorPopup(
          error.message || 'Failed to submit to the server'
        )
    );
  }

  openConfirmationPopup(lateFees: LateFee[]) {
    const dialogHeaders = [
      {
        label: 'Engine Capacity',
        property: 'engineCapacity',
      },
      {
        label: 'Within 1 month of expiry date',
        property: 'lateFeePeriod1',
      },
      {
        label: 'Between 1 and 2.5 months',
        property: 'lateFeePeriod2',
      },
      {
        label: 'More than 2.5 months',
        property: 'lateFeePeriod3',
      },
      {
        label: 'More than 3 months',
        property: 'lateFeePeriod4',
      },
      {
        label: 'Effective Date',
        property: 'effectiveDate',
      },
      {
        label: 'End Date',
        property: 'endDate',
      },
    ];
    const displayedRows: any[] = [];
    lateFees.forEach((lateFee: LateFee) => {
      lateFee.lateFeeRecords.forEach((record: any) => {
        const engineCapacity = lateFee.engineCapacity;
        record = { ...record, engineCapacity };
        const row: Row[] = [];
        dialogHeaders.forEach((header) => {
          const value = record[header.property];
          row.push({ label: header.label, value, subvalue: '' });
        });
        displayedRows.push(row);
      });
    });
    if (this.removedDraftRows.length) {
      this.removedDraftRows.forEach((removed: any) => {
        const engineCapacity = removed.full.engineCapacity;
        removed = { ...removed, engineCapacity };
        const row: Row[] = [];
        dialogHeaders.forEach((header) => {
          const value = removed[header.property];
          const label = header.label;
          const remarks = 'This is removed';
          row.push({ label, value, subvalue: '', remarks });
        });
        displayedRows.push(row);
      });
    }
    const uiData: UiData = {
      icon: 'error',
      header: 'Submit your VALUES for review?',
      subheader: 'You will not be able to make amendments to your submission',
      apiPayload: lateFees,
      displayedRows,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Submit',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    const confirmationDialog = this.popupService.openConfirmation(uiData);
    confirmationDialog.afterClosed().subscribe(async (data) => {
      if (data) {
        await this.submitLateFeePayload(lateFees);
      }
    });
  }

  createLateFeeValues(lateFees: LateFee[]): LateFeeValue[] {
    const lateFeeValues: LateFeeValue[] = [];
    lateFees.forEach((lateFee: LateFee) => {
      lateFee.lateFeeRecords.forEach((record: LateFeeRecord) => {
        const newLateFee = {
          roadTaxAmtCatKey: record.roadTaxAmtCatKey.toString(),
          lateFeePeriod1: record.lateFeePeriod1,
          lateFeePeriod2: record.lateFeePeriod2,
          lateFeePeriod3: record.lateFeePeriod3,
          lateFeePeriod4: record.lateFeePeriod4,
          transactionId: record.transactionId || NaN,
          effectiveDate: record.effectiveDate,
          endDate: record.endDate,
        };
        lateFeeValues.push(newLateFee);
      });
    });
    return lateFeeValues;
  }

  setExportData() {
    this.exportData.rows = this.lateFees;
  }

  setParent(parentIndex: number) {
    const children = this.lateFees.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.lateFees[parentIndex];
      parent.hasChildren = false;
    }
  }

  async submitDeletedLateFee() {
    try {
      const removed = this.removedDraftRows;
      const request = await this.lateFeesTabService.callDeleteApi(removed);
      request.subscribe(
        () => {
          const message = 'The late fee draft row has been deleted.';
          this.formulaCardService.openSuccessPopup(message);
          this.removedDraftRows = [];
        },
        (error: Error) => {
          const message = 'late fee save API error';
          this.formulaCardService.openSuccessPopup(error.message || message);
        }
      );
    } catch (error) {
      this.formulaCardService.openErrorPopup('Cannot delete late fee.');
    }
  }

  async onRemoveRow(lateFee: TableLateFee) {
    if (this.formulaCardService.isADraft(lateFee.amendmentStatus)) {
      this.removedDraftRows.push({ ...lateFee, isRemoved: true });
    }
    const indexToBeRemoved = lateFee?.index;
    this.lateFees.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = lateFee.parentIndex ?? 0;
    this.setParent(parentIndex);
    await this.submitDeletedLateFee();
  }

  getNewlyAdded(lateFees: any) {
    const newlyAdded: LateFee[] = [];
    lateFees.forEach((lateFee: TableLateFee, parentIndex: number) => {
      if (lateFee.isAParent) {
        const parent = lateFee;
        const hasNewlyAdded = lateFees.find(
          (child: TableLateFee) =>
            child.parentIndex === parentIndex && child.newlyAdded
        );
        const parentStatus = parent.amendmentStatus;
        const parentIsADraft = this.formulaCardService.isADraft(parentStatus);
        if (hasNewlyAdded || parent.newlyAdded || parentIsADraft) {
          const newLateFeeChildren = lateFees.filter(
            (child: TableLateFee) =>
              child.parentIndex === parentIndex && child.newlyAdded
          );
          if (parent.newlyAdded || parentIsADraft) {
            newLateFeeChildren.push(parent);
          }
          const newLateFee: LateFee = {
            engineCapacity: parent.engineCapacity,
            roadTaxAmtCatKey: parent.roadTaxAmtCatKey,
            lateFeeRecords: newLateFeeChildren.map((child: TableLateFee) => {
              return {
                engineCapacity: child.engineCapacity,
                roadTaxAmtCatKey: parent.roadTaxAmtCatKey,
                lateFeePeriod1: child.lateFeePeriod1,
                lateFeePeriod2: child.lateFeePeriod2,
                lateFeePeriod3: child.lateFeePeriod3,
                lateFeePeriod4: child.lateFeePeriod4,
                transactionId: child.transactionId || NaN,
                effectiveDate: this.formulaCardService.toString(
                  child.effectiveDate
                ),
                endDate: this.formulaCardService.toString(child.endDate),
                active: child.active,
              };
            }),
          };
          newlyAdded.push(newLateFee);
        }
      }
    });
    return newlyAdded;
  }

  get newlyAddedRows() {
    const newlyAdded = this.lateFees.find(
      (lateFee: any) =>
        !this.formulaCardService.isADraft(lateFee.amendmentStatus) &&
        lateFee.newlyAdded
    );
    return newlyAdded;
  }

  async onSaveAsDraftClick(lateFees: any[]) {
    if (!this.newlyAddedRows) {
      const message = 'Add rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const hasDateOverlap = this.formulaCardService.hasDateOverlap(lateFees);
    if (hasDateOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    const newlyAdded = this.getNewlyAdded(lateFees);
    const successMessage = 'Your values have been saved.';
    await this.submitLateFeePayload(newlyAdded, true, successMessage);
  }

  onProceedButtonClick(lateFees: any) {
    const newlyAdded = this.getNewlyAdded(lateFees);
    if (!newlyAdded.length && !this.removedDraftRows.length) {
      const message = 'Add rows or remove draft rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const hasDateOverlap = this.formulaCardService.hasDateOverlap(lateFees);
    if (hasDateOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    this.openConfirmationPopup(newlyAdded);
  }

  updateLateFees(currentFees: any) {
    this.lateFees = currentFees;
  }

  onToggleCollapse(data: any) {
    const { collapsedFee, currentFees } = data;
    this.updateLateFees(currentFees);
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.lateFees = this.lateFees.map((fee, index) => {
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.lateFees = this.lateFees.map((fee, index) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  closeAllRows() {
    this.lateFees = this.lateFees.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  expandParent(index: number | null) {
    if (index || index === 0) {
      const parentFee = this.lateFees[index];
      parentFee.isExpanded = true;
      parentFee.hasChildren = true;
    }
  }

  expandChildren(parentLateFeeIndex: number | null) {
    this.lateFees = this.lateFees.map((child) => {
      if (child.parentIndex === parentLateFeeIndex) {
        child.isExpanded = true;
      }
      return child;
    });
  }

  newlyAddedCount: any = {};

  createChildLateFee(parent: TableLateFee): TableLateFee {
    let counter = this.newlyAddedCount[parent.index];
    if (!counter && counter !== 0) {
      counter = 0;
    }
    let lastChildIndex = this.getLastChildIndex(parent.index);
    if (!counter && lastChildIndex !== parent.index && !parent.hasChildren) {
      lastChildIndex--;
    }
    this.newlyAddedCount[parent.index] = counter + 1;
    const { endDate } = { ...this.lateFees[lastChildIndex] };
    return {
      ...parent,
      effectiveDate: this.formulaCardService.addDaysToDate(endDate, 1),
      endDate: this.formulaCardService.addDaysToDate(endDate, 30),
      isAParent: false,
      isExpanded: true,
      parentIndex: parent?.index || null,
      hasChildren: false,
      newlyAdded: true,
      fromApi: false,
      transactionId: NaN,
    };
  }

  addChildToLateFees(parentLateFee: TableLateFee) {
    if (parentLateFee.index || parentLateFee.index === 0) {
      const lastChildIndex = this.getLastChildIndex(parentLateFee.index);
      const position = lastChildIndex + 1;
      const itemsToBeRemoved = 0;
      const lateFeeToBeAdded = this.createChildLateFee(parentLateFee);
      this.lateFees.splice(position, itemsToBeRemoved, lateFeeToBeAdded);
      this.lateFees = [...this.lateFees];
    }
    this.addRowCount++;
  }

  onAddNewChildLateFeeClick(parentLateFee: any) {
    if (!parentLateFee.isExpanded) {
      this.closeAllRows();
    }
    const parentLateFeeIndex = parentLateFee?.index ?? null;
    this.expandParent(parentLateFeeIndex);
    this.expandChildren(parentLateFeeIndex);
    if (parentLateFeeIndex || parentLateFeeIndex === 0) {
      this.addChildToLateFees(parentLateFee);
      this.updateParentIndex();
    }
  }

  onDatepickerChange(changed: TableLateFee) {
    const index = changed.index ?? 0;
    const changedLateFee = this.lateFees[index];
    const effectiveDateChanged =
      changedLateFee.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedLateFee.effectiveDate = changed.effectiveDate;
      changedLateFee.newlyAdded = true;
    }
    const endDateChanged =
      changedLateFee.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedLateFee.endDate = changed.endDate;
      changedLateFee.newlyAdded = true;
    }
    this.lateFees = [...this.lateFees];
  }

  onInputChange(changed: TableLateFee) {
    const changedLateFee = this.lateFees[changed.index];
    changedLateFee.engineCapacity = changed.engineCapacity;
    changedLateFee.lateFeePeriod1 = changed.lateFeePeriod1;
    changedLateFee.lateFeePeriod2 = changed.lateFeePeriod2;
    changedLateFee.lateFeePeriod3 = changed.lateFeePeriod3;
    changedLateFee.lateFeePeriod4 = changed.lateFeePeriod4;
    changedLateFee.newlyAdded = true;
    this.lateFees = [...this.lateFees];
  }

  getLastChildIndex(parentIndex: number): number {
    const children = this.lateFees.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index;
  }
}
