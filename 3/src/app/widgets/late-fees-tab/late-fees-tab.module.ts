import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LateFeesTabComponent } from './late-fees-tab.component';
import { FormulaCardContextualModule } from '../formula-card/formula-card-contextual/formula-card-contextual.module';
import { PopupModule } from '../formula-card/popup/popup.module';

@NgModule({
  declarations: [LateFeesTabComponent],
  imports: [CommonModule, FormulaCardContextualModule, PopupModule],
  exports: [LateFeesTabComponent],
})
export class LateFeesTabModule {}
