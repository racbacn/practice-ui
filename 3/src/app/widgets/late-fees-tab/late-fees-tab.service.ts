import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormulaCardService } from '../formula-card/formula-card.service';
import { AddedOptions } from './interfaces/added-options';
import { LateFee } from './interfaces/late-fee';
import { LateFeeRecord } from './interfaces/late-fee-record';
import { LateFeeResponse } from './interfaces/late-fee-response';
import { LateFeeSavePayload } from './interfaces/late-fee-save-payload';
import { LateFeeValue } from './interfaces/late-fee-value';
import { TableLateFee } from './interfaces/table-late-fee';

@Injectable({
  providedIn: 'root',
})
export class LateFeesTabService {
  private SERVER_URL = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService
  ) {}

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }

  isADraft(status: string): boolean {
    return this.formulaCardService.isADraft(status);
  }

  setTooltipMessage(surcharge: LateFeeRecord) {
    const { amendmentStatus, rejectComment } = surcharge;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  private get lateFeeRecordModel(): LateFeeRecord {
    return {
      active: false,
      effectiveDate: '',
      endDate: '',
      lateFeePeriod1: 0,
      lateFeePeriod2: 0,
      lateFeePeriod3: 0,
      lateFeePeriod4: 0,
      transactionId: 0,
      roadTaxAmtCatKey: 0,
      amendmentStatus: '',
      rejectComment: '',
    };
  }

  private createRow(
    lateFeeRecord: LateFeeRecord,
    addedOptions: AddedOptions,
    full: LateFee
  ): TableLateFee {
    const { active, effectiveDate, endDate } = lateFeeRecord;
    const { transactionId, roadTaxAmtCatKey, amendmentStatus } = lateFeeRecord;
    const { lateFeePeriod1, lateFeePeriod2, lateFeePeriod3, lateFeePeriod4 } =
      lateFeeRecord;
    const { hasChildren, engineCapacity, hasBackground } = addedOptions;
    const { index, parentIndex, forResubmission, noActiveLateFee } =
      addedOptions;
    return {
      engineCapacity,
      active: lateFeeRecord?.active || false,
      lateFeePeriod1,
      lateFeePeriod2,
      lateFeePeriod3,
      lateFeePeriod4,
      transactionId,
      roadTaxAmtCatKey,
      effectiveDate: new Date(
        this.formulaCardService.formatDate(effectiveDate)
      ),
      endDate: new Date(this.formulaCardService.formatDate(endDate)),
      isAParent: active || noActiveLateFee || false,
      isExpanded: false,
      parentIndex: parentIndex,
      hasChildren: hasChildren,
      index: index,
      newlyAdded:
        this.isRejected(lateFeeRecord.amendmentStatus) ||
        this.isADraft(lateFeeRecord.amendmentStatus),
      fromApi: true,
      hasBackground,
      backgroundColor: '',
      isAResponse: true,
      tooltip: this.setTooltipMessage(lateFeeRecord),
      isRemoved: false,
      full,
      amendmentStatus,
      forResubmission,
      isOfficer: true,
    };
  }

  private formatLateFees(data: LateFeeResponse): TableLateFee[] {
    const lateFees: TableLateFee[] = [];
    let index = -1;
    let parentIndex = -1;
    let forResubmission = false;
    let noActiveLateFee = false;
    data.RoadTaxLateFee.forEach((lateFee: LateFee) => {
      lateFee.lateFeeRecords.find((record: LateFeeRecord) => {
        if (record.amendmentStatus.toLowerCase() === 'manager rejected') {
          forResubmission = true;
          noActiveLateFee = false;
          return true;
        }
        return false;
      });
    });
    data.RoadTaxLateFee.forEach((lateFee: LateFee) => {
      index++;
      parentIndex = index;
      const { engineCapacity, lateFeeRecords, roadTaxAmtCatKey } = lateFee;
      let activeLateFee!: LateFeeRecord;
      const inactiveLateFees: LateFeeRecord[] = [];
      noActiveLateFee = false;
      lateFeeRecords.forEach((lateFeeRecord: LateFeeRecord) => {
        if (lateFeeRecord.active) {
          activeLateFee = lateFeeRecord;
          return;
        }
        inactiveLateFees.push(lateFeeRecord);
      });
      if (!activeLateFee) {
        if (!lateFeeRecords[0]) {
          return;
        }
        activeLateFee = lateFeeRecords[0];
        noActiveLateFee = true;
      }
      activeLateFee.roadTaxAmtCatKey = roadTaxAmtCatKey;
      let addedOptions: AddedOptions = {
        hasChildren: lateFeeRecords.length > 1,
        index,
        engineCapacity,
        hasBackground: true,
        backgroundColor: '',
        parentIndex: NaN,
        forResubmission,
        noActiveLateFee,
      };
      const active = this.createRow(activeLateFee, addedOptions, lateFee);
      lateFees.push({ ...active, parentNotEditable: true });
      if (!inactiveLateFees.length) {
        return;
      }
      inactiveLateFees.forEach(
        (inactiveRecord: LateFeeRecord, childIndex: number) => {
          if (noActiveLateFee && !childIndex) {
            return;
          }
          index++;
          addedOptions = {
            hasChildren: false,
            index,
            engineCapacity: '',
            hasBackground: false,
            backgroundColor: '',
            parentIndex,
            forResubmission,
            noActiveLateFee,
          };
          inactiveRecord.roadTaxAmtCatKey = roadTaxAmtCatKey;
          const inactive = this.createRow(
            inactiveRecord,
            addedOptions,
            lateFee
          );
          lateFees.push(inactive);
        }
      );
    });
    return lateFees;
  }

  getLateFeeResponse(): Observable<LateFeeResponse> {
    const apiEndpoint = '/manage-roadtax-service/get/RoadTaxValues';
    const apiUrl = this.SERVER_URL + apiEndpoint;
    const lateFeeTab = 5;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', lateFeeTab);
    params = params.append('userId', userId);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    params = params.append('userTypeKey', userTypeKey);
    return this.http.get<LateFeeResponse>(apiUrl, { params });
  }

  getLateFee(): Observable<TableLateFee[]> {
    return this.getLateFeeResponse().pipe(
      map((response: LateFeeResponse) => {
        const hasLateFees = response.RoadTaxLateFee.length;
        return hasLateFees ? this.formatLateFees(response) : [];
      })
    );
  }

  async callSaveApi(endpoint: string, lateFeeValues: LateFeeValue[]) {
    const apiUrl = this.SERVER_URL + endpoint;
    const requestHeader =
      await this.formulaCardService.getOfficerSaveRequestHeader();
    const payload: LateFeeSavePayload = { requestHeader, lateFeeValues };
    return this.http.post<any>(apiUrl, payload);
  }

  async callDeleteApi(values: TableLateFee[]) {
    return await this.formulaCardService.deleteRoadTaxValue(values);
  }

  async saveAsDraft(lateFeeValues: LateFeeValue[]): Promise<Observable<any>> {
    const endpoint = '/manage-roadtax-service/lateFee/draft';
    const subscriptions: Observable<any>[] = [];
    if (lateFeeValues.length) {
      const saveValues = await this.callSaveApi(endpoint, lateFeeValues);
      subscriptions.push(saveValues);
    }
    return forkJoin(subscriptions);
  }

  async submit(lateFeeValues: LateFeeValue[]): Promise<Observable<any>> {
    const endpoint = '/manage-roadtax-service/saveLateFee';
    const subscriptions: Observable<any>[] = [];
    if (lateFeeValues.length) {
      const saveValues = await this.callSaveApi(endpoint, lateFeeValues);
      subscriptions.push(saveValues);
    }
    return forkJoin(subscriptions);
  }
}
