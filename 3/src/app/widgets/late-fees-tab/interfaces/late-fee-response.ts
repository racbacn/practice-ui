import { LateFee } from './late-fee';

export interface LateFeeResponse {
  RoadTaxLateFee: LateFee[];
}
