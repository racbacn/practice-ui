import { LateFeeValue } from './late-fee-value';

export interface LateFeeSavePayload {
  requestHeader: {
    ltaTeamTypeKey: number;
    userTypeKey: number;
    managerId: number;
    userId: number;
  };
  lateFeeValues: LateFeeValue[];
}
