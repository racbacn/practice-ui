export interface TableLateFee {
  engineCapacity: string;
  active: boolean;
  lateFeePeriod1: number;
  lateFeePeriod2: number;
  lateFeePeriod3: number;
  lateFeePeriod4: number;
  transactionId: number;
  roadTaxAmtCatKey: number;
  effectiveDate: Date;
  endDate: Date;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  hasBackground: boolean;
  backgroundColor: string;
  isAResponse: boolean;
  tooltip: string;
  isRemoved: boolean;
  full: any;
  amendmentStatus: string;
  forResubmission: boolean;
  isOfficer: boolean;
  parentNotEditable?: boolean;
}
