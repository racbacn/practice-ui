export interface LateFeeRecord {
  active: boolean;
  effectiveDate: string;
  endDate: string;
  lateFeePeriod1: number;
  lateFeePeriod2: number;
  lateFeePeriod3: number;
  lateFeePeriod4: number;
  transactionId: number;
  roadTaxAmtCatKey: number;
  amendmentStatus: string;
  rejectComment: string;
}
