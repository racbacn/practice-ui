export interface LateFeeValue {
  roadTaxAmtCatKey: string;
  lateFeePeriod1: number;
  lateFeePeriod2: number;
  lateFeePeriod3: number;
  lateFeePeriod4: number;
  effectiveDate: string;
  endDate: string;
}
