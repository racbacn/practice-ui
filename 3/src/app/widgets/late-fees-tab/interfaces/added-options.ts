export interface AddedOptions {
  engineCapacity: string;
  hasChildren: boolean;
  index: number;
  hasBackground: boolean;
  backgroundColor: string;
  parentIndex: number;
  forResubmission: boolean;
  noActiveLateFee: boolean;
}
