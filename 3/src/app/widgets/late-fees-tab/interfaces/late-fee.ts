import { LateFeeRecord } from './late-fee-record';

export interface LateFee {
  engineCapacity: string;
  lateFeeRecords: LateFeeRecord[];
  roadTaxAmtCatKey: number;
}
