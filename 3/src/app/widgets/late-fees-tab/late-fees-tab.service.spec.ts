import { TestBed } from '@angular/core/testing';

import { LateFeesTabService } from './late-fees-tab.service';

describe('LateFeesTabService', () => {
  let service: LateFeesTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LateFeesTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
