import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LateFeesTabComponent } from './late-fees-tab.component';

describe('LateFeesTabComponent', () => {
  let component: LateFeesTabComponent;
  let fixture: ComponentFixture<LateFeesTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LateFeesTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LateFeesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
