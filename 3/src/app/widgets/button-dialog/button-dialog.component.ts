import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button-dialog',
  templateUrl: './button-dialog.component.html',
  styleUrls: ['./button-dialog.component.scss']
})
export class ButtonDialogComponent implements OnInit {
  @Input() btnConfig: any;

  constructor() { }

  ngOnInit(): void {
  }

}
