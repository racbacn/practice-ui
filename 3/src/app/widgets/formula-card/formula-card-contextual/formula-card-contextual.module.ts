import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormulaCardSelectModule } from '../formula-card-select/formula-card-select.module';
import { FormulaCardContextualComponent } from './formula-card-contextual.component';
import { FormulaCardLegendModule } from '../formula-card-legend/formula-card-legend.module';
import { FormulaCardExportModule } from '../formula-card-export/formula-card-export.module';
import { HeaderTableDividerModule } from '../header-table-divider/header-table-divider.module';
import { ContextualTableModule } from '../contextual-table/contextual-table.module';
import { HeaderTextModule } from '../header-text/header-text.module';

@NgModule({
  declarations: [FormulaCardContextualComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormulaCardSelectModule,
    FormulaCardExportModule,
    FormulaCardLegendModule,
    HeaderTableDividerModule,
    ContextualTableModule,
    HeaderTextModule,
  ],
  exports: [FormulaCardContextualComponent],
})
export class FormulaCardContextualModule {}
