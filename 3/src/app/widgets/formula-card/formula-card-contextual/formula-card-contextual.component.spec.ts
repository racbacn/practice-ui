import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaCardContextualComponent } from './formula-card-contextual.component';

describe('FormulaCardContextualComponent', () => {
  let component: FormulaCardContextualComponent;
  let fixture: ComponentFixture<FormulaCardContextualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaCardContextualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaCardContextualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
