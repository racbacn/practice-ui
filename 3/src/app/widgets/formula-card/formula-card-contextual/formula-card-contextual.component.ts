import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TableHeader } from 'src/app/interface/table-header';

@Component({
  selector: 'app-formula-card-contextual',
  templateUrl: './formula-card-contextual.component.html',
  styleUrls: ['./formula-card-contextual.component.scss'],
})
export class FormulaCardContextualComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() exportData: any = { headers: [], rows: [], filename: 'test.xlsx' };
  @Input() headers: TableHeader[] = [];
  @Input() fees: any[] = [];
  @Input() settings: any = {};
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() addRow: EventEmitter<null> = new EventEmitter<null>();
  @Output() removeRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() datepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() remarksFieldChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() approveCheckboxChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveAsDraft: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onAddRow() {
    this.addRow.emit();
  }

  onRemoveRow(row: any) {
    this.removeRow.emit(row);
  }

  onSelectionChange(selected: any) {
    this.selectionChange.emit(selected);
  }

  onProceedButtonClick(fees: any) {
    this.proceedButtonClick.emit(fees);
  }

  onToggleCollapse(fee: any) {
    this.toggleCollapse.emit(fee);
  }

  onAddNewValueClick(fee: any) {
    this.addNewValueClick.emit(fee);
  }

  onDatepickerChange(fees: any) {
    this.datepickerChange.emit(fees);
  }

  onInputChange(fee: any) {
    this.inputChange.emit(fee);
  }

  onRemarksFieldChange(changed: any) {
    this.remarksFieldChange.emit(changed);
  }

  onApproveCheckboxChange(changed: any) {
    this.approveCheckboxChange.emit(changed);
  }

  onSaveAsDraft(fees: any) {
    this.saveAsDraft.emit(fees);
  }
}
