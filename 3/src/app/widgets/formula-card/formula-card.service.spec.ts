import { TestBed } from '@angular/core/testing';

import { FormulaCardService } from './formula-card.service';

describe('FormulaCardService', () => {
  let service: FormulaCardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormulaCardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
