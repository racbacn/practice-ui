import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-new-field',
  templateUrl: './add-new-field.component.html',
  styleUrls: ['./add-new-field.component.scss']
})
export class AddNewFieldComponent implements OnInit {

  @Input() transactions: any;
  @Input() add: any;
  roadTaxForm!: FormGroup;
  arrayForm!: FormArray;
  startDate!: Date;
  showDelete!: boolean;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
    console.log("add", this.add)
    this.addItem(this.add);
  }

  createForm() {

    this.roadTaxForm = this.formBuilder.group({

      arrayForm: this.formBuilder.array([])
    });
    return this.roadTaxForm;
  }



  get roadTaxFormGroup() {
    return this.roadTaxForm.get('arrayForm') as FormArray;
  }

  addItem(val: any) {
    console.log
    for (let data of this.transactions) {
      if (this.add === data.engineCapacity) {
        const active = data.monthlyAmt.filter((amount: any) => {
          return amount.active
        })[0]
    
        let value = this.formBuilder.group({
          baseAmt: [active.baseAmt],
          ecMultiplier: [active.ecMultiplier],
          discount: [active.discount],
          effectiveDate: [new Date(active.effectiveDate)],
          endDate: [new Date(active.endDate)],

        });
        const form = this.roadTaxForm.get('arrayForm') as FormArray
        form.push(value);
        let dateToday = new Date(active.endDate);
        this.startDate = new Date(dateToday.setDate(dateToday.getDate() + 1));


      }
    }
    console.log("form", this.roadTaxForm.value)
    // console.log("data", this.dataSource)

    if ((this.roadTaxForm.get('arrayForm') as FormArray).length >= 1) {
      this.showDelete = true;

    } else if ((this.roadTaxForm.get('arrayForm') as FormArray).length === 0) {
      this.showDelete = false;
    }
  }

  deleteItem(index: any) {
    (this.roadTaxForm.controls['arrayForm'] as FormArray).removeAt(index)
  }



}
