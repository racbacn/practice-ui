import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-contextual-table',
  templateUrl: './contextual-table.component.html',
  styleUrls: ['./contextual-table.component.scss'],
})
export class ContextualTableComponent implements OnInit {
  @Input() headers: any[] = [];
  @Input() fees: any[] = [];
  @Input() submitButtonText: string = 'Proceed';
  @Input() hasPreviewButton = false;
  @Input() hasSaveAsDraftButton = false;
  @Input() selection: any[] = [];
  @Input() placeholder!: string;
  @Input() settings: any = {
    showEcMultiplier: true,
  };
  @Output() addRow: EventEmitter<null> = new EventEmitter<null>();
  @Output() removeRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<any> = new EventEmitter<any>();
  @Output() datepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() remarksFieldChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() approveCheckboxChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() rangeDatepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() rangeDatepickerChange2: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveAsDraft: EventEmitter<any> = new EventEmitter<any>();

  feesForm!: FormGroup;
  displayedColumns: string[] = [];
  show = false;
  selected: any;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
  }

  initializeFees() {
 
    this.show = false;
    let fees: any[] = [];
    if (this.fees.length) {
      fees = this.fees.map((fee) => this.createFee(fee));
    }
    this.feesForm = this.formBuilder.group({
      fees: this.formBuilder.array([]),
    });
    this.feesForm = this.formBuilder.group({
      fees: this.formBuilder.array(fees),
    });
    this.show = true;
  }

  ngOnChanges() {
    this.initializeFees();
    this.initializeHeaders();
  }

  initializeHeaders() {
    if (this.headers.length) {
      this.displayedColumns = this.headers.map((header) => header.property);
    }
  }

  get feesFormArray() {
    const fees = this.feesForm.get('fees') as FormArray;
    return fees.controls;
  }

  createFee(fee: any) {
    const feeGroup: any = {};
    Object.keys(fee).forEach((feeKey) => {
      const value = fee[feeKey];
      feeGroup[feeKey] = value;
    });
    return this.formBuilder.group(feeGroup);
  }

  onAddRow() {
    const fees = this.feesFormValue;
    console.log("fees", fees)
    this.addRow.emit(fees);
  }

  onRemoveClick(row: any) {
    this.removeRow.emit(row);
  }

  addIndexToElement(element: any, index: number) {
    return { ...element, index };
  }

  get feesFormValue() {
    const { fees } = this.feesForm.value;
    return fees;
  }

  onProceedButtonClick() {
    const fees = this.feesFormValue;
    this.proceedButtonClick.emit(fees);
  }

  onAddNewValueClick(fee: any) {
    console.log("click", fee)
    this.addNewValueClick.emit(fee);
  }

  hasNoBorderRight(header: any) {
    return (
      header.type &&
      header.type !== 'dateField' &&
      header.property !== 'newValue' &&
      header.property !== 'ageOfVehicle'
    );
  }

  onToggleCollapse(fee: number) {
    const fees = this.feesFormValue;
    this.toggleCollapse.emit({ collapsedFee: fee, currentFees: fees });
  }

  hideRow(row: any) {
    return !row.isAParent && !row.isExpanded;
  }

  preview() {}

  onDatepickerChange(index: number) {
    const fees = this.feesFormValue;
    const fee = this.addIndexToElement(fees[index], index);
    this.initializeFees();
    console.log("here", fee)
    this.datepickerChange.emit(fee);
  }

  onDatepickerRangeChange(index: number){
    const fees = this.feesFormValue;
    const fee = this.addIndexToElement(fees[index], index);
    this.initializeFees();
    this.rangeDatepickerChange.emit(fee);
  }
  onDatepickerRangeChange2(index: number){
    const fees = this.feesFormValue;
    const fee = this.addIndexToElement(fees[index], index);
    this.initializeFees();
    this.rangeDatepickerChange2.emit(fee);
  }

  onInputChange(index: number, hasMultipleValues?: boolean) {
    let fee
    const fees = this.feesFormValue;
    fee = this.addIndexToElement(fees[index], index);
    this.inputChange.emit(fee);
  }

  onSelectChange(index: number) {
    const  selected  = this.feesFormValue;
    let fees = this.addIndexToElement(selected[index], index);
    this.selectionChange.next(fees);
  }

  getFormControl(control: string, index: number): FormControl {
    const fees = this.feesForm.get('fees') as FormArray;
    const fee = fees.controls[index] as FormGroup;
    return fee.get(control) as FormControl;
  }

  disableRemarksField(index: number) {
    const approveCheckbox = this.getFormControl('approveCheckbox', index);
    const approveCheckboxValue = approveCheckbox.value;
    const remarksControl = this.getFormControl('remarksFieldValue', index);
    approveCheckboxValue ? remarksControl.disable() : remarksControl.enable();
    return {
      remarks: remarksControl.value,
      approveCheckbox: approveCheckboxValue,
    };
  }

  disableApproveCheckbox(index: number) {
    const remarksControl = this.getFormControl('remarksFieldValue', index);
    const remarksControlValue = remarksControl.value;
    const approveCheckbox = this.getFormControl('approveCheckbox', index);
    remarksControlValue ? approveCheckbox.disable() : approveCheckbox.enable();
    return {
      remarks: remarksControlValue,
      approveCheckbox: approveCheckbox.value,
    };
  }

  onRemarksFieldChange(index: number) {
    const changed = this.disableApproveCheckbox(index);
    this.remarksFieldChange.emit({
      index,
      remarksFieldValue: changed.remarks,
      approveCheckbox: changed.approveCheckbox,
    });
  }

  onCheckboxChange(index: number) {
    this.disableRemarksField(index);
    const changed = this.disableRemarksField(index);
    this.approveCheckboxChange.emit({
      index,
      remarksFieldValue: changed.remarks,
      approveCheckbox: changed.approveCheckbox,
    });
  }



  setTdClass(element: any, header: any) {
    const headerIsRemarksField = header.property === 'remarksFieldValue';
    const headerIsApproveCheckbox = header.property === 'approveCheckbox';
    const isApprovingCell = headerIsRemarksField || headerIsApproveCheckbox;
    const forApproval = element.amendmentStatus === 'Pending Manager Review';
    return {
      'approving-cell': isApprovingCell && forApproval,
      'approving-cell-circled':  headerIsApproveCheckbox && forApproval,
    };
  }

  adjustWidth(header: any) {
    if (header?.width) {
      return { width: header.width };
    }
    return {};
  }

  onSaveAsDraft() {
    this.saveAsDraft.emit(this.feesFormValue);
  }
}
