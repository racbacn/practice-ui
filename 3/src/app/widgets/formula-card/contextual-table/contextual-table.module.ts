import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContextualTableComponent } from './contextual-table.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CellModule } from '../cell/cell.module';
import { PreviewButtonModule } from '../preview-button/preview-button.module';
import { SubmitButtonModule } from '../submit-button/submit-button.module';
import { SaveAsDraftButtonModule } from '../save-as-draft-button/save-as-draft-button.module';

@NgModule({
  declarations: [ContextualTableComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    CellModule,
    PreviewButtonModule,
    SubmitButtonModule,
    SaveAsDraftButtonModule
  ],
  exports: [ContextualTableComponent],
})
export class ContextualTableModule {}
