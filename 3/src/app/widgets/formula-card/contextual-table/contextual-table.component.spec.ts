import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualTableComponent } from './contextual-table.component';

describe('ContextualTableComponent', () => {
  let component: ContextualTableComponent;
  let fixture: ComponentFixture<ContextualTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContextualTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
