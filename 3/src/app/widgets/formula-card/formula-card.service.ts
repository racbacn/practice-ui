import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PopupService } from './services/popup.service';

export interface ManagerApiResponse {
  responseHeader: {
    responseAppId: string;
    responseDateTime: Date;
  };
  managerUserId: number;
}

interface OverpaymentKeys {
  amendmentStatus: string;
  roadTaxAmtCatKey: number;
  transactionId: number;
}

export type EvMinusValues =
  | 'PR <= 7.5'
  | '7.5 < PR <= 30 '
  | '30 < PR <= 230 '
  | 'PR > 230 ';

export type PetrolMinusValues =
  | 'EC <= 600'
  | '600 < EC <= 1000'
  | '1000 < EC <= 1600'
  | '1600 < EC <= 3000'
  | 'EC > 3000';

export interface RemoveRoadTaxPayload {
  transactionId: number;
  roadTaxAmtCatKey: number;
}

@Injectable({
  providedIn: 'root',
})
export class FormulaCardService {
  private SERVER_URL = environment.serverUrl;
  private petrolPrMinusValues = {
    'EC <= 600': 0,
    '600 < EC <= 1000': 600,
    '1000 < EC <= 1600': 1000,
    '1600 < EC <= 3000': 1600,
    'EC > 3000': 3000,
  };
  private evPrMinusValues = {
    'PR <= 7.5': 0,
    '7.5 < PR <= 30 ': 7.5,
    '30 < PR <= 230 ': 30,
    'PR > 230 ': 230,
  };

  constructor(private http: HttpClient, private popupService: PopupService) {}

  formatDate(date: string): Date {
    if (!date) {
      return new Date();
    }
    const parts = date.split('-');
    const year = +parts[2];
    const month = +parts[1] - 1;
    const day = +parts[0];
    return new Date(year, month, day);
  }

  toString(date: Date): string {
    return moment(date).format('DD-MM-yyyy');
  }

  toDateToString(date: string): string {
    const formatted = this.formatDate(date);
    return this.toString(formatted);
  }

  getManagerId(userId: number): Promise<ManagerApiResponse> {
    const apiEndpoint = `/manage-user-service/user/managerOfOfficer/${userId}`;
    const apiUrl = this.SERVER_URL + apiEndpoint;
    return new Promise((resolve, reject) =>
      this.http.get<ManagerApiResponse>(apiUrl).subscribe(
        (response: ManagerApiResponse) => resolve(response),
        (error) => reject(error)
      )
    );
  }

  getUserId(): number {
    const USER = JSON.parse(sessionStorage.USER);
    const { userId } = USER;
    return +userId;
  }

  getLtaTeamTypeKey(): number {
    const USER = JSON.parse(sessionStorage.USER);
    const { ltaTeamTypeKey } = USER;
    return ltaTeamTypeKey;
  }

  getUserTypeKey(): number {
    const USER = JSON.parse(sessionStorage.USER);
    const { userTypeKey } = USER;
    return userTypeKey;
  }

  async getUserAndManagerId(): Promise<{ userId: number; managerId: number }> {
    const userId = this.getUserId();
    const data = await this.getManagerId(userId);
    const managerId = data.managerUserId;
    return { userId, managerId };
  }

  async getOfficerSaveRequestHeader() {
    const ltaTeamTypeKey = this.getLtaTeamTypeKey();
    const userTypeKey = this.getUserTypeKey();
    const userId = this.getUserId();
    const response = await this.getManagerId(userId);
    const managerId = response.managerUserId;
    return { ltaTeamTypeKey, managerId, userId, userTypeKey };
  }

  getManagerSaveRequestHeader() {
    const ltaTeamTypeKey = this.getLtaTeamTypeKey();
    const userTypeKey = this.getUserTypeKey();
    const userId = this.getUserId();
    const managerId = userId;
    return { ltaTeamTypeKey, managerId, userId, userTypeKey };
  }

  get getRequestHeader() {
    return {
      ltaTeamTypeKey: this.getLtaTeamTypeKey(),
      userId: this.getUserId(),
      userTypeKey: this.getUserTypeKey(),
    };
  }

  openSuccessPopup(
    message: string = 'Amended Formula value(s) successfully submitted for review'
  ) {
    const uiData = {
      icon: 'check_circle',
      message,
      buttonText: 'Back to Main',
    };
    this.popupService.openSuccess(uiData);
  }

  openErrorPopup(message: string = 'Cannot connect to API') {
    const uiData = {
      icon: 'cancel',
      message,
      buttonText: 'Back to Main',
    };
    this.popupService.openSuccess(uiData);
  }

  approveRoadTaxFees(amendedRoadTaxFees: any) {
    const subscriptions: Observable<any>[] = [];
    amendedRoadTaxFees.forEach((roadTaxFee: any) => {
      const { transactionId, remarks } = roadTaxFee;
      if (roadTaxFee.approved) {
        const approved = this.approveFee(transactionId);
        subscriptions.push(approved);
        return;
      }
      const rejected = this.rejectFee(transactionId, remarks);
      subscriptions.push(rejected);
    });
    return forkJoin(subscriptions);
  }

  approveFee(transactionId: number) {
    const apiEndpoint = '/manage-roadtax-service/approveRoadTaxAmendment';
    const apiUrl = this.SERVER_URL + apiEndpoint;
    const requestHeader = this.getManagerSaveRequestHeader();
    const payload = { requestHeader, transactionId };
    return this.http.put<any>(apiUrl, payload);
  }

  rejectFee(transactionId: number, comment: string) {
    const apiEndpoint = '/manage-roadtax-service/rejectRoadTaxAmendment';
    const apiUrl = this.SERVER_URL + apiEndpoint;
    const requestHeader = this.getManagerSaveRequestHeader();
    const payload = { requestHeader, transactionId, comment };
    return this.http.put<any>(apiUrl, payload);
  }

  getLastChildIndex(fees: any, parentIndex: number): number {
    const children = fees.filter(
      (child: any) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index;
  }

  hasDateOverlap(fees: any[]): boolean {
    let hasAtLeastAnOverlap = false;
    fees.find((fee: any, index: number) => {
      if (!fee.newlyAdded) {
        return false;
      }
      fees.find((current, currentIndex) => {
        const isItself = index === currentIndex;
        const notTheParent = fee.parentIndex !== currentIndex;
        const notASibling = fee.parentIndex !== current.parentIndex;
        const comparedToAParent = current.isAParent && fee.isAParent;
        if (isItself || (notTheParent && notASibling) || comparedToAParent) {
          return false;
        }
        const feeEffectiveDate = fee.effectiveDate.getTime();
        const feeEndDate = fee.endDate.getTime();
        const currentEffectiveDate = current.effectiveDate.getTime();
        const currentEndDate = current.endDate.getTime();
        const effectiveDateOverlap =
          feeEffectiveDate >= currentEffectiveDate &&
          feeEffectiveDate <= currentEndDate;
        const endDateOverlap =
          feeEndDate >= currentEffectiveDate && feeEndDate <= currentEndDate;
        if (effectiveDateOverlap || endDateOverlap) {
          hasAtLeastAnOverlap = true;
          return true;
        }
        return false;
      });
      return hasAtLeastAnOverlap;
    });
    return hasAtLeastAnOverlap;
  }

  hasRegDateOverlap(fees: any[]): boolean {
    let hasAtLeastAnOverlap = false;
    fees.find((current: any, currentIndex: number) => {
      if (!current.newlyAdded) {
        return false;
      }
      fees.find((row, rowIndex) => {
        const isItself = currentIndex === rowIndex;
        if (isItself) {
          return false;
        }
        const currentRegFrom = current.vehicleRegistrationFrom.getTime();
        const currentRegTo = current.vehicleRegistrationTo.getTime();
        const rowRegFrom = row.vehicleRegistrationFrom.getTime();
        const rowRegTo = row.vehicleRegistrationTo.getTime();
        const regFromOverlap =
          currentRegFrom >= rowRegFrom && currentRegFrom <= rowRegTo;
        const regToOverlap =
          currentRegTo >= rowRegFrom && currentRegTo <= rowRegTo;
        const regDateOverlap = regFromOverlap || regToOverlap;
        const regDateRangeError = currentRegFrom > currentRegTo;
        const currentEffective = current.effectiveDate.getTime();
        const rowEffective = row.effectiveDate.getTime();
        const currentEnd = current.endDate.getTime();
        const rowEnd = row.endDate.getTime();
        const effectiveOverlap =
          currentEffective >= rowEffective && currentEffective <= rowEnd;
        const endOverlap = currentEnd >= rowEffective && currentEnd <= rowEnd;
        const effectiveEndOverlap = effectiveOverlap || endOverlap;
        const dateRangeError = currentEffective > currentEnd;
        const rangeError = regDateRangeError || dateRangeError;
        if ((regDateOverlap && effectiveEndOverlap) || rangeError) {
          hasAtLeastAnOverlap = true;
          return true;
        }
        return false;
      });
      return hasAtLeastAnOverlap;
    });
    return hasAtLeastAnOverlap;
  }

  addDaysToDate(date: Date, numberOfDays: number): Date {
    const addDayToThisDate = new Date(date);
    let addedDays = numberOfDays;
    if (numberOfDays >= 30) {
      const year = date.getFullYear();
      const month = date.getMonth();
      addedDays = new Date(year, month, 0).getDate() + 2;
    }
    addDayToThisDate.setDate(addDayToThisDate.getDate() + addedDays);
    return addDayToThisDate;
  }

  isRejected(status: string) {
    if (!status) {
      return false;
    }
    return status.toLowerCase() === 'manager rejected';
  }

  getPrMinusValue(engineCapacity: EvMinusValues) {
    const minus = this.evPrMinusValues[engineCapacity];
    return `(PR - ${minus})`;
  }

  getECMinusValue(engineCapacity: PetrolMinusValues) {
    const minus = this.petrolPrMinusValues[engineCapacity];
    return `(EC - ${minus})`;
  }

  getEvFormula(
    base: number = 0,
    multiplier: number = 0,
    discount: number = 0,
    engineCapacity: EvMinusValues
  ) {
    const minus = this.getPrMinusValue(engineCapacity);
    return `[$${base} + $${multiplier}${minus}] x ${discount}`;
  }

  getPetrolFormula(
    base: number = 0,
    multiplier: number = 0,
    discount: number = 0,
    engineCapacity: PetrolMinusValues
  ) {
    const minus = this.petrolPrMinusValues[engineCapacity];
    if (engineCapacity === 'EC <= 600') {
      return `${base} x ${discount}`;
    }
    return `[$${base} + $${multiplier}(EC - ${minus})] x ${discount}`;
  }

  isADraft(amendmentStatus: string): boolean {
    return amendmentStatus.toLowerCase() === 'draft';
  }

  isPending(amendmentStatus: string): boolean {
    return amendmentStatus.toLowerCase() === 'pending manager review';
  }

  getOverpaymentParameters(fees: OverpaymentKeys[]) {
    const levels: number[] = [];
    const ids: number[] = [];
    fees.forEach((fee: OverpaymentKeys, index: number) => {
      if (this.isPending(fee.amendmentStatus)) {
        levels.push(fee.roadTaxAmtCatKey);
        ids.push(fee.transactionId);
      }
    });
    return {
      levels: levels.join(),
      ids: ids.join(),
    };
  }

  getTypeKey(type: string) {
    if (type === 'Flat Amount') {
      return 201;
    } else if (type === '% of total Road Tax Amount') {
      return 202;
    }
    return NaN;
  }

  getOverpaymentReportUrlParams(
    levelKey: string,
    levels: string,
    ids: string
  ): HttpParams {
    const userId = this.getUserId();
    const userTypeKey = this.getUserTypeKey();
    let params = new HttpParams();
    params = params.append(levelKey, levels);
    params = params.append('transactionIds', ids);
    params = params.append('userId', userId);
    params = params.append('userTypeKey', userTypeKey);
    return params;
  }

  formatRemovedValuesPayload(values: RemoveRoadTaxPayload[]) {
    return values.map((value: RemoveRoadTaxPayload) => ({
      transactionId: value.transactionId,
      roadTaxAmtCatKey: value.roadTaxAmtCatKey,
    }));
  }

  async deleteRoadTaxValue(values: RemoveRoadTaxPayload[]) {
    const apiUrl =
      this.SERVER_URL + '/manage-roadtax-service/removeRoadTaxDraft';
    const roadTaxValues = this.formatRemovedValuesPayload(values);
    return this.http.put<any>(apiUrl, { roadTaxValues });
  }
}
