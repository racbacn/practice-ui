import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmComponent } from './confirm/confirm.component';
import { SuccessComponent } from './success/success.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  declarations: [ConfirmComponent, SuccessComponent],
  imports: [CommonModule, FlexLayoutModule, MaterialModule],
  exports: [ConfirmComponent, SuccessComponent],
})
export class PopupModule {}
