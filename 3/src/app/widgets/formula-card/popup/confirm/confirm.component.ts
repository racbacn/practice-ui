import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {
  widths = ['', '300px', '400px', '500px', '600px', '700px', '800px'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ConfirmComponent>
  ) {
    console.log(this.data);
  }

  ngOnInit(): void {
    console.log(this.data);
  }

  isADate(value: any) {
    return value instanceof Date && !isNaN(+value);
  }

  close() {
    this.dialogRef.close();
  }

  submit() {
    this.dialogRef.close({ data: this.data });
  }

  action(buttonFunction: string) {
    if (buttonFunction === 'submit') {
      this.submit();
    } else if (buttonFunction === 'close') {
      this.close();
    }
  }

  isApproved(row: any) {
    return (
      row[row.length - 1].value === 'No' ||
      row[row.length - 1]?.remarks === 'This is removed'
    );
  }

  getRemarks(row: any) {
    return row[row.length - 1]?.remarks;
  }

  getWidth() {
    let longest = 0;
    this.data.displayedRows.forEach((row: any) => {
      if (row.length > longest) {
        longest = row.length;
      }
    });
    return { width: longest > 6 ? '90%' : this.widths[longest] };
  }
}
