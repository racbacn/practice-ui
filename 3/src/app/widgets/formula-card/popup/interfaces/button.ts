export interface Button {
  text: string;
  function: string | 'close' | 'submit';
  class: string;
}
