export interface Row {
  label: string;
  value: string;
  subvalue: string;
  remarks?: string;
}
