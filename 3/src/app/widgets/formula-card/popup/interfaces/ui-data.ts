import { Button } from './button';
import { Row } from './row';

export interface UiData {
  icon: string;
  header: string;
  subheader: string;
  displayedRows: Row[][];
  buttons: Button[];
  apiPayload?: any[];
}
