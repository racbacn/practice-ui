import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTableDividerComponent } from './header-table-divider.component';

describe('HeaderTableDividerComponent', () => {
  let component: HeaderTableDividerComponent;
  let fixture: ComponentFixture<HeaderTableDividerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderTableDividerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderTableDividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
