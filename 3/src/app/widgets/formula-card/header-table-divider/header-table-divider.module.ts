import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { HeaderTableDividerComponent } from './header-table-divider.component';

@NgModule({
  declarations: [HeaderTableDividerComponent],
  imports: [CommonModule, MaterialModule],
  exports: [HeaderTableDividerComponent],
})
export class HeaderTableDividerModule {}
