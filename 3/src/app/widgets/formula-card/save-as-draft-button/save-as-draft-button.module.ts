import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaveAsDraftButtonComponent } from './save-as-draft-button.component';

@NgModule({
  declarations: [SaveAsDraftButtonComponent],
  imports: [CommonModule],
  exports: [SaveAsDraftButtonComponent],
})
export class SaveAsDraftButtonModule {}
