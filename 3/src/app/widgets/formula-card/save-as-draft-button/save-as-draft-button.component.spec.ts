import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveAsDraftButtonComponent } from './save-as-draft-button.component';

describe('SaveAsDraftButtonComponent', () => {
  let component: SaveAsDraftButtonComponent;
  let fixture: ComponentFixture<SaveAsDraftButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveAsDraftButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveAsDraftButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
