import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-save-as-draft-button',
  templateUrl: './save-as-draft-button.component.html',
  styleUrls: ['./save-as-draft-button.component.scss'],
})
export class SaveAsDraftButtonComponent implements OnInit {
  @Output() saveAsDraft: EventEmitter<null> = new EventEmitter<null>();

  constructor() {}

  ngOnInit(): void {}

  onSaveAsDraft() {
    this.saveAsDraft.emit();
  }
}
