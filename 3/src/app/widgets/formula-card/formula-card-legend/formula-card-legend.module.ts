import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormulaCardLegendComponent } from './formula-card-legend.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [FormulaCardLegendComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [FormulaCardLegendComponent],
})
export class FormulaCardLegendModule {}
