import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaCardLegendComponent } from './formula-card-legend.component';

describe('FormulaCardLegendComponent', () => {
  let component: FormulaCardLegendComponent;
  let fixture: ComponentFixture<FormulaCardLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaCardLegendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaCardLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
