import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaCardSelectComponent } from './formula-card-select.component';

describe('FormulaCardSelectComponent', () => {
  let component: FormulaCardSelectComponent;
  let fixture: ComponentFixture<FormulaCardSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaCardSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaCardSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
