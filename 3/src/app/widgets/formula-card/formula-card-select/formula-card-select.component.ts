import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: "app-formula-card-select",
  templateUrl: "./formula-card-select.component.html",
  styleUrls: ["./formula-card-select.component.scss"],
})
export class FormulaCardSelectComponent implements OnInit {
  @Input() options: any[] = [];
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();

  selectForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.selectForm = this.formBuilder.group({
      selected: [this.options[0].value],
    });
  }

  onSelectChange() {
    const { selected } = this.selectForm.value;
    console.log(selected)
    this.selectionChange.next(selected);
  }
}
