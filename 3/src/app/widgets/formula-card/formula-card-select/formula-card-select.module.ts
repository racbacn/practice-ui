import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormulaCardSelectComponent } from './formula-card-select.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [FormulaCardSelectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [FormulaCardSelectComponent],
})
export class FormulaCardSelectModule {}
