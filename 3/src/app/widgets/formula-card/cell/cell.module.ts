import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CellComponent } from './cell.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [CellComponent],
  imports: [CommonModule, MaterialModule, FlexLayoutModule],
  exports: [CellComponent],
})
export class CellModule {}
