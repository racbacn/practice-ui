import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.scss'],
})
export class CellComponent implements OnInit {
  @Input() element: any = {};
  @Input() header: any = {};
  @Input() display = '';
  @Input() type = 'string';
  @Output() removeClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  onRemoveClick() {
    this.removeClick.emit(this.element);
  }

  onAddNewValueClick() {
    this.addNewValueClick.emit(this.element);
  }

  onToggleCollapse() {
    this.toggleCollapse.emit(this.element);
  }

  get isHighlighted() {
    return (
      this.header.property === 'newValue' ||
      this.header.property === 'effectiveDate' ||
      this.header.property === 'endDate'
    );
  }

  endDateIsPastCurrentDate() {
    return new Date(this.element.endDate).getTime() <= new Date().getTime();
  }

  isInactive() {
    return (
      this.isHighlighted &&
      !this.element.isAParent &&
      this.element.fromApi &&
      !this.element.active &&
      this.endDateIsPastCurrentDate()
    );
  }

  inactiveHistory() {
    return (
      this.isHighlighted &&
      !this.element.isAParent &&
      this.element.fromApi &&
      !this.element.active
    );
  }

  get hasNoTdContent() {
    return (
      (this.element.isAParent ||
        (!this.element.isAParent && this.element.fromApi)) &&
      (this.header.property === 'remarksFieldValue' ||
        this.header.property === 'approveCheckbox')
    );
  }

  get isEditable() {
    const amendmentStatus = this.element?.amendmentStatus?.toLowerCase();
    const isRejected = amendmentStatus === 'manager rejected';
    const isADraft = amendmentStatus === 'draft';
    const fieldNotEndDateEditable =
      !this.element?.parentNotEditable && !this.element?.isEditedEndDateOnly;
    return (
      (isRejected && fieldNotEndDateEditable) ||
      (isADraft && fieldNotEndDateEditable)
    );
  }

  get parentEndDateEditable() {
    return this.header.property === 'endDate';
  }
}
