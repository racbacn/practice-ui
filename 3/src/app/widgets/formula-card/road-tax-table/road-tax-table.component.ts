import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormArray, FormGroup } from "@angular/forms";
import { TableHeader } from "src/app/interface/table-header";
import { RoadTaxTable } from "./interface/road-tax-table";
import { RoadTaxService } from "src/app/services/road-tax.service";
import { PopupService } from "../services/popup.service";
import { FormulaCardService } from "../formula-card.service";
import { RoadTaxRebatesService } from "src/app/services/road-tax-rebates.service";
import { AMENDMENT_STATUS } from "src/app/shared/constants/application.constants";

@Component({
  selector: "app-road-tax-table",
  templateUrl: "./road-tax-table.component.html",
  styleUrls: ["./road-tax-table.component.scss"],
})
export class RoadTaxTableComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() exportData: any = { headers: [], rows: [], filename: "test.xlsx" };
  @Input() headers: TableHeader[] = [];
  @Input() roadTax: any[] = [];
  @Input() editedData: any[] = [];
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() addRow: EventEmitter<null> = new EventEmitter<null>();
  @Output() removeRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() datepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() changes: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();

  @Input() transactions: any;
  panelOpenState = false;
  displayedColumns!: string[];
  dataSource: any;
  expandedElement!: any;
  roadTaxForm!: FormGroup;
  arrayForm!: FormArray;
  recipes: any;
  roadTaxList: any[] = [];
  arrayNum!: number;
  showDelete!: boolean;
  baseAmount!: string;
  engineCapacity!: any;
  isActive!: boolean;
  startDate!: Date;
  historyData: any[] = [];
  dateArray: any;
  showError: boolean | undefined;
  expandedRows: { [key: number]: boolean } = {};
  showAdd!: boolean;
  isTableExpanded = false;
  add: any;
  open: boolean = false;
  @Input() roadTaxRebates: RoadTaxTable[] = [];
  spreadsheetHeaders: any[] = [
    {
      label: "Engine Capacity (EC) in cc",
      property: "engineCapacity",
      type: "string",
    },
    {
      label: "6-Monthly Road Tax",
      property: "monthlyTax",
      type: "collapsible",
    },
    {
      label: "New Value",
      property: "newValue",
      property1: "baseAmt",
      property2: "ecMultiplier",
      property3: "discount",
      type: "formulaField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];
  uiTableHeaders: any[] = [
    {
      label: "",
      property: "warningTooltipButton",
      type: "",
    },

    ...this.spreadsheetHeaders,
    {
      label: "",
      property: "rowAddButton",
      type: "",
    },
  ];
  settings = {
    submitButtonText: "Proceed",
    hasSelectOptions: false,
    headerText: "",
    hasPreviewButton: false,
  };
  addRowCount = 0;
  validateDates: any[] = [];
  baseRoadTax: any;
  rebates: any;
  additionalTax: any;
  lastParentIndex: any;
  constructor(
    private roadTaxService: RoadTaxService, private formulaCardService: FormulaCardService
  ) {}

  ngOnInit() {
    this.roadTaxService.getRoadTax().subscribe((res) => {
      console.log("baseFeesroadtaaxs", res);
      this.roadTaxRebates = res;
    });
  }



  onDatepickerChange(changed: RoadTaxTable) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.roadTaxRebates[index];
    const effectiveDateChanged =
      changedSurcharge.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedSurcharge.effectiveDate = changed.effectiveDate;
      changedSurcharge.newlyAdded = true;
    }
    const endDateChanged =
      changedSurcharge.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedSurcharge.endDate = changed.endDate;
      changedSurcharge.newlyAdded = true;
    }
    this.roadTaxRebates = [...this.roadTaxRebates];
    this.roadTaxService.setBaseRoadTax(this.roadTaxRebates);
  }

  onInputChange(changed: RoadTaxTable) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.roadTaxRebates[index];
    changedSurcharge.baseAmt = changed.baseAmt;
    changedSurcharge.discount = changed.discount;
    changedSurcharge.ecMultiplier = changed.ecMultiplier;
    changedSurcharge.newlyAdded = true;
    this.roadTaxRebates = [...this.roadTaxRebates];

    this.roadTaxService.setBaseRoadTax(this.roadTaxRebates);
  }

  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.roadTaxRebates.filter(
      (child: RoadTaxTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  createChild(roadTax: RoadTaxTable): RoadTaxTable {
    const lastChildIndex = this.getLastChildIndex(roadTax.index);
    const newChildrenLength = this.getNewlyAddedChildrenLength(roadTax.index);
    let base = lastChildIndex;
    if (!newChildrenLength) {
      base = roadTax.index;
    }
    let {
      engineCapacity,
      endDate,
      effectiveDate,
      baseAmt,
      discount,
      ecMultiplier,
      showEcMultiplier,
    } = {
      ...this.roadTaxRebates[base],
    };
    let isDraft: boolean = false;
    if (engineCapacity === "EC <= 600") {
      ecMultiplier = 0;
      showEcMultiplier = false;
    }
    return {
      ...roadTax,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      monthlyTax: "",
      baseAmt: baseAmt,
      discount: discount,
      ecMultiplier: ecMultiplier,
      engineCapacity: "",
      isAParent: false,
      isExpanded: true,
      parentIndex: roadTax?.index || null,
      hasChildren: false,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true,
      minusText: roadTax.minusText,
      showEcMultiplier: showEcMultiplier,
      isDraft: isDraft
    };
  }

  addChildToRoadTax(parentSurcharge: RoadTaxTable) {
    if (parentSurcharge.index || parentSurcharge.index === 0) {
      const lastChildIndex = this.getLastChildIndex(parentSurcharge.index);
      const hasNoChildren =
        parentSurcharge.index === lastChildIndex || this.addRowCount;
      const position = lastChildIndex + (hasNoChildren ? 1 : 0);
      const itemsToBeRemoved = 0;
      const surchargeToBeAdded = this.createChild(parentSurcharge);
      this.roadTaxRebates.splice(
        position,
        itemsToBeRemoved,
        surchargeToBeAdded
      );
      this.roadTaxRebates = [...this.roadTaxRebates];
      this.roadTaxService.setBaseRoadTax(this.roadTaxRebates);
    }
    this.addRowCount++;
  }

  getLastChildIndex(parentIndex: number): number {
    const children = this.roadTaxRebates.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index || 0;
  }

  onAddNewChildSurchargeClick(parentRoadTax: any) {
    if (!parentRoadTax.isExpanded) {
      this.closeAllRows();
    }
    const parentIndex = parentRoadTax?.index ?? null;
    this.expandParent(parentIndex);
    this.expandChildren(parentIndex);
    if (parentIndex || parentIndex === 0) {
      this.addChildToRoadTax(parentRoadTax);
      this.updateParentIndex();
    }
  }

  expandParent(index: number | null) {
    if (index || index === 0) {
      const parentFee = this.roadTaxRebates[index];
      parentFee.isExpanded = true;
      parentFee.hasChildren = true;
    }
  }

  expandChildren(parentSurchargeIndex: number | null) {
    this.roadTaxRebates = this.roadTaxRebates.map((child) => {
      if (child.parentIndex === parentSurchargeIndex) {
        child.isExpanded = true;
      }
      return child;
    });
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.roadTaxRebates = this.roadTaxRebates.map((fee, index) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  onToggleCollapse(data: any) {
    const { collapsedFee, currentFees } = data;
    this.updateRoadTax(currentFees);
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }

    this.roadTaxRebates = this.roadTaxRebates.map((fee, index) => {
      console.log(collapsedFee.index);
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  updateRoadTax(currentFees: any) {
    this.roadTaxRebates = currentFees;
  }

  closeAllRows() {
    this.roadTaxRebates = this.roadTaxRebates.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }
  onRemoveRow(roadTax: RoadTaxTable) {
    let draftsParams: any[] = []
    draftsParams.push(roadTax)
    if(roadTax.amendmentStatus === AMENDMENT_STATUS.draft){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = roadTax?.index;
    this.roadTaxRebates.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = roadTax.parentIndex ?? 0;
    this.setParent(parentIndex);
    this.roadTaxService.setBaseRoadTax(this.roadTaxRebates);
  }

  setParent(parentIndex: number) {
    const children = this.roadTaxRebates.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.roadTaxRebates[parentIndex];
      parent.hasChildren = false;
    }
  }

  submit(event: any) {
    this.proceedButtonClick.emit();
  }

  saveAsDraft(event: any) {
    let isDraft = true;
    this.saveDraft.emit(isDraft);
  }

  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }
}
