import { PetrolMinusValues } from '../../formula-card.service';
import { RoadTaxRecord } from './road-tax-record';

export interface RoadTax {
  engineCapacity: PetrolMinusValues;
  roadTaxAmtCatKey: number;
  roadTaxRecords: RoadTaxRecord[];
}

export interface RoadTaxList {
  engineCapacity: string;
  roadTaxAmtCatKey: number;
  roadTaxRecords: RoadTaxRecord[];
}

export interface RoadTaxResponse {
  BaseRoadTaxFee: RoadTax[];
}

