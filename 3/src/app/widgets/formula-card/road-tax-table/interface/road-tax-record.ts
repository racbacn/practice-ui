export interface RoadTaxRecord {
  baseAmt: number;
  discount: number;
  effectiveDate: string;
  endDate: string;
  active?: boolean;
  ecMultiplier?: number | undefined;
  amendmentStatus: string;
  rejectComment: string;
  transactionId: number;
}