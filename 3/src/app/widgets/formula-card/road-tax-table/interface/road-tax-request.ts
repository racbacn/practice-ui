export interface RoadTaxRequest {
  roadTaxAmtCatKey: number;
  baseAmt: number;
  discount: number;
  effectiveDate: string;
  endDate: string;
  ecMultiplier?: number | undefined | string;
  transactionId?: number;
}


export interface RemoveDraftRequest {
  roadTaxValues: RemoveDraftParams[];
}

export interface RemoveDraftParams {
  transactionId: number;
  roadTaxAmtCatKey: number
}

