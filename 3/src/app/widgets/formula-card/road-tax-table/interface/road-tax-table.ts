import { PetrolMinusValues } from "../../formula-card.service";

export interface RoadTaxTable {
  engineCapacity: string;
  baseAmt: number;
  effectiveDate: Date;
  monthlyTax: string;
  newValue: string;
  endDate: Date;
  active?: boolean | undefined;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  isNewRow: boolean;
  roadTaxAmtCatKey: number;
  ecMultiplier?: number;
  discount: number;
  isBaseRoadTax: boolean;
  amendmentStatus: string;
  rejectComment: string;
  transactionId: number;
  forResubmission: boolean;
  minusText: string;
  showEcMultiplier: boolean;
  isDraft: boolean;
}
