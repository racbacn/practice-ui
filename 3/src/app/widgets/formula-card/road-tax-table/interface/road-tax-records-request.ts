import { RoadTaxRequest } from './road-tax-request';

export interface BaseRoadTaxRequest {
  requestHeader: {
    userId: number;
    managerId: any;
    ltaTeamTypeKey: number,
    userTypeKey: number
}
baseRoadTaxFee: RoadTaxRequest[];
}


