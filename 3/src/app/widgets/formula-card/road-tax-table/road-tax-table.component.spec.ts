import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadTaxTableComponent } from './road-tax-table.component';

describe('RoadTaxTableComponent', () => {
  let component: RoadTaxTableComponent;
  let fixture: ComponentFixture<RoadTaxTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadTaxTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadTaxTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
