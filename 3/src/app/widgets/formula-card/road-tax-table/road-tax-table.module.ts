import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoadTaxTableComponent } from './road-tax-table.component';
import { MaterialModule } from 'src/app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SubmitButtonModule } from '../submit-button/submit-button.module';
import { ContextualTableModule } from '../contextual-table/contextual-table.module';

@NgModule({
  declarations: [RoadTaxTableComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SharedModule,
    SubmitButtonModule,
    ContextualTableModule,
    SubmitButtonModule
  ],
  exports: [RoadTaxTableComponent],
})
export class RoadTaxTableModule {}
