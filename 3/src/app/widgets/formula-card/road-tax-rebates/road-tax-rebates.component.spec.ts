import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadTaxRebatesComponent } from './road-tax-rebates.component';

describe('RoadTaxRebatesComponent', () => {
  let component: RoadTaxRebatesComponent;
  let fixture: ComponentFixture<RoadTaxRebatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadTaxRebatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadTaxRebatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
