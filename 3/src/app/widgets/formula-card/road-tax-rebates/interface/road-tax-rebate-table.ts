export interface RoadTaxRebatesTable {
  rebateName: string;
  rebateAmount: number | undefined;
  effectiveDate: Date;
  rebateTypeKey: number | undefined;
  rebateType: string;
  endDate: Date;
  active?: boolean | undefined;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | undefined;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  isNewRow: boolean;
  transactionId: number | undefined;
  amendmentStatus: string;
  rejectComment: string;
  forResubmission: boolean;
  vehicleRegistrationTo: Date;
  vehicleRegistrationFrom: Date;
}
