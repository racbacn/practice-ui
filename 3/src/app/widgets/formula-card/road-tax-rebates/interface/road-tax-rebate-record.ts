export interface RoadTaxRebatesRecord {
  rebateName: string;
  rebateTypeKey: number | undefined;
  rebateType: string;
  rebateAmount: number | undefined;
  effectiveDate: string;
  endDate: string;
  vehicleRegistrationTo: string;
  vehicleRegistrationFrom: string;
  active?: boolean | undefined;
  transactionId: number;
  amendmentStatus: string;
  rejectComment: string;
  roadTaxAmtCatKey: number;
}