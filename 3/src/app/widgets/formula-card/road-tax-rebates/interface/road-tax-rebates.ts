import { RoadTaxRebatesRecord } from './road-tax-rebate-record';

export interface RoadTaxRebates {
  roadTaxRebatesRecords: RoadTaxRebatesRecord[];
}

export interface RoadTaxRebatesResponse {
  RebateList: RoadTaxRebatesRecord[];
}

export interface RebateRequest {
  chargeTypeKey: number | undefined;
  rebateName: string;
  rebateAmount: number | undefined;
  effectiveDate: string;
  endDate: string;
  vehicleRegistrationTo: string;
  vehicleRegistrationFrom: string;
  transactionId?: number | undefined;

}

export interface TaxRebatesRequest {
  requestHeader: {
    userId: number;
    managerId: any;
    ltaTeamTypeKey: number,
    userTypeKey: number
}
rebateValue: RebateRequest[];
}

