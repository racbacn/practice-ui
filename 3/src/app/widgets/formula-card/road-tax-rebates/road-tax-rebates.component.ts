import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { DateOverlapValidation } from "src/app/core/util/date-overlap-validation.service";
import { RoadTaxService } from "src/app/services/road-tax.service";import { AMENDMENT_STATUS } from "src/app/shared/constants/application.constants";
import { FormulaCardService } from "../formula-card.service";
;
import { RoadTaxRebatesTable } from "./interface/road-tax-rebate-table";


@Component({
  selector: "app-road-tax-rebates",
  templateUrl: "./road-tax-rebates.component.html",
  styleUrls: ["./road-tax-rebates.component.scss"],
})
export class RoadTaxRebatesComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() exportData: any = { headers: [], rows: [], filename: "test.xlsx" };

  @Input() fees: any[] = [];
  @Input() settings: any = {};
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() addRow: EventEmitter<null> = new EventEmitter<null>();
  @Output() removeRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() datepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();

  placeHolder = "Select Rebate Type"
  columnHeaders: any[] = [
    {
      label: "Rebate Name",
      property: "rebateName",
      type: "textField",
    },
    {
      label: "Rebate Type",
      property: "rebateType",
      type: "selectField",
    },
    {
      label: "For Vehicles with Registration Date(s)",
      property: "vehicleRegistrationFrom",
      property2: "vehicleRegistrationTo",
      type: "rangeField",
    },
    {
      label: "Rebate Amount",
      property: "rebateAmount",
      type: "numberField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];

  headers: any;
  rebates: any;

  addRowCount = 0;
  selection = [
    {
      value: "Flat Amount",
    },
    {
      value: "% of total Road Tax Amount",
    },
  ];
  showError: boolean | undefined;
  lastParentIndex!: any;
  baseRoadTax: any;
  additionalTax: any; 
  constructor(private roadTaxService: RoadTaxService, private formulaCardService: FormulaCardService) {}

  ngOnInit() {
    this.roadTaxService.getRebates().subscribe((res) => {
      this.rebates = res;
      this.lastParentIndex = this.rebates[this.rebates.length - 1];
      this.rebates.forEach((data: RoadTaxRebatesTable) => {
        if(!data.forResubmission){
          let headers =  [{
            label: "",
            property: "warningTooltipButton",
            type: "",
          },
          ...this.columnHeaders,
          {
            label: "",
            property: "addButton",
            type: "",
          }]

          this.headers = headers
        }else {
          
          let headers =  [{
            label: "",
            property: "warningTooltipButton",
            type: "",
          },
          ...this.columnHeaders,
          {
            label: "",
            property: "addButtonResubmission",
            type: "",
          }]
          this.headers = headers;
        }
  
      });
    });
  }

  onAddRow(row: any) {
    console.log("click add", row);
    this.addRow.emit(row);
  }

  onSelectionChange(selected: any) {
    this.selectionChange.emit(selected);
  }

  onAddNewValueClick(fee: any) {
    console.log(fee);
    this.addNewValueClick.emit(fee);
  }

  onInputChange(changed: RoadTaxRebatesTable) {
    const index = changed.index ?? 0;
    const changedFees = this.rebates[index];
    changedFees.rebateName = changed.rebateName;
    changedFees.rebateAmount = changed.rebateAmount;
    changedFees.newlyAdded = true;
    this.rebates = [...this.rebates];
    this.roadTaxService.setRebates(this.rebates);
  }

  selectChange(select: RoadTaxRebatesTable) {
    const index = select.index ?? 0;
    const changedFees = this.rebates[index];
    changedFees.rebateType = select.rebateType;
    changedFees.newlyAdded = true;
    changedFees.rebateTypeKey = this.formulaCardService.getTypeKey(
      changedFees.rebateType
    );
    console.log(changedFees);
    this.rebates = [...this.rebates];
    this.roadTaxService.setRebates(this.rebates);
  }

  addRebatesRow(data: any) {
    if (data.index || data.index !== 0) {
      const lastChildIndex = data[data.length - 1];
      const rebates = this.createChild(lastChildIndex);
      this.rebates.push({
        active: false,
        effectiveDate: rebates.effectiveDate,
        endDate: rebates.endDate,
        vehicleRegistrationFrom: rebates.vehicleRegistrationFrom,
        vehicleRegistrationTo: rebates.vehicleRegistrationTo,
        index: data?.index || null,
        isAParent: true,
        isExpanded: false,
        newlyAdded: true,
        parentIndex: data?.index || null,
        rebateAmount: rebates.rebateAmount,
        rebateName: rebates.rebateName,
        rebateType: rebates.rebateType,
        rebateTypeKey: rebates.rebateTypeKey,
        amendmendmentStatus: rebates.amendmentStatus,
        fromApi: false,
        isNewRow: true,
        transactionId: rebates.transactionId
      });
      this.rebates = [...this.rebates];
      this.roadTaxService.setRebates(this.rebates);
    }

    this.addRowCount++;
  }
  getLastParentIndex(data: any): number {
    const lastChild = data[data.length - 1];
    return lastChild.index;
  }

  createChild(roadTax: RoadTaxRebatesTable): RoadTaxRebatesTable {
        const { effectiveDate, endDate, rebateAmount, rebateName, rebateTypeKey, rebateType, amendmentStatus, transactionId, vehicleRegistrationTo,vehicleRegistrationFrom} = {
      ...roadTax,
    };
    return {
      ...roadTax,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      vehicleRegistrationTo: this.addDaysToDate(vehicleRegistrationTo, 2),
      vehicleRegistrationFrom: this.addDaysToDate(vehicleRegistrationTo, 1),
      rebateAmount: rebateAmount,
      rebateName: rebateName,
      rebateTypeKey: rebateTypeKey,
      rebateType: rebateType,
      isAParent: true,
      isExpanded: false,
      parentIndex: roadTax?.index || undefined,
      amendmentStatus: amendmentStatus,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true,
      transactionId: transactionId

    };
  }

  onRangeDatepickerChange(changed: RoadTaxRebatesTable) {
    const index = changed.index ?? 0;
    const changedDates = this.rebates[index];
    const effectiveDateChanged =
    changedDates.vehicleRegistrationFrom.getTime() !==
      changed.vehicleRegistrationFrom.getTime();
     
    if (effectiveDateChanged) {
      changedDates.vehicleRegistrationFrom = changed.vehicleRegistrationFrom;
      changedDates.newlyAdded = true;
    }
 
    const endDateChanged =
    changedDates.vehicleRegistrationTo.getTime() !== changed.vehicleRegistrationTo.getTime();
    if (endDateChanged) {
      changedDates.vehicleRegistrationTo = changed.vehicleRegistrationTo;
      changedDates.newlyAdded = true;
    }

    this.rebates = [...this.rebates];
    this.roadTaxService.setRebates(this.rebates);
  }


  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.rebates.filter(
      (child: RoadTaxRebatesTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  setParent(parentIndex: number) {
    const children = this.rebates.filter(
      (data: any) => data.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.rebates[parentIndex];
      parent.hasChildren = false;
    }
  }

  onProceedButtonClick(data: any) {
    const hasOverlappedDates = DateOverlapValidation.checkOverlapDates(
      this.rebates
    );
    console.log(hasOverlappedDates);
    if (hasOverlappedDates) {
      return alert("Invalid Dates");
    }
  }

  onDatepickerChange(changed: RoadTaxRebatesTable) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.rebates[index];
    const effectiveDateChanged =
      changedSurcharge.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedSurcharge.effectiveDate = changed.effectiveDate;
      changedSurcharge.newlyAdded = true;
    }
    const endDateChanged =
      changedSurcharge.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedSurcharge.endDate = changed.endDate;
      changedSurcharge.newlyAdded = true;
    }
    this.rebates = [...this.rebates];
    this.roadTaxService.setRebates(this.rebates);
  }

  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }

  onRemoveRow(rebates: RoadTaxRebatesTable) {
    let draftsParams: any[] = []
    draftsParams.push(rebates)
    if(rebates.amendmentStatus === AMENDMENT_STATUS.draft){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = rebates?.index;
    this.rebates.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = rebates.parentIndex ?? 0;
    this.roadTaxService.setRebates(this.rebates);
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.rebates = this.rebates.map((fee: any, index: any) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  submit(event: any) {
    this.proceedButtonClick.emit();
  }
  
  saveAsDraft(event: any){
    let isDraft = true;
    this.saveDraft.emit(isDraft);
}

}
