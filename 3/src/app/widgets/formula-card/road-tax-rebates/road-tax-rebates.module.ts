import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoadTaxRebatesComponent } from './road-tax-rebates.component';
import { ContextualTableModule } from '../contextual-table/contextual-table.module';
import { MaterialModule } from 'src/app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from 'src/app/shared/shared.module';
import { SubmitButtonModule } from '../submit-button/submit-button.module';



@NgModule({
  declarations: [
    RoadTaxRebatesComponent
  ],
  imports: [
    CommonModule,
    ContextualTableModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SharedModule,
    SubmitButtonModule
  ],
  exports: [RoadTaxRebatesComponent]
})
export class RoadTaxRebatesModule { }
