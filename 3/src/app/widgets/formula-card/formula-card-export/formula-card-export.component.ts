import { Component, Input, OnInit } from '@angular/core';
import { TableHeader } from 'src/app/interface/table-header';
import { ExportXlsService } from 'src/app/services/export-xls.service';

export interface SpreadsheetData {
  headers: TableHeader[];
  rows: any[];
  filename: string;
}

@Component({
  selector: 'app-formula-card-export',
  templateUrl: './formula-card-export.component.html',
  styleUrls: ['./formula-card-export.component.scss'],
})
export class FormulaCardExportComponent implements OnInit {
  @Input() data: any = { headers: [], rows: [], filename: 'test.xlsx' };

  constructor(private exportXlsService: ExportXlsService) {}

  ngOnInit(): void {}

  exportData() {
    const { headers, rows, filename } = this.data;
    this.exportXlsService.export(headers, rows, filename);
  }
}
