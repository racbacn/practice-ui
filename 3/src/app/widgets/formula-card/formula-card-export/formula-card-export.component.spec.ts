import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaCardExportComponent } from './formula-card-export.component';

describe('FormulaCardExportComponent', () => {
  let component: FormulaCardExportComponent;
  let fixture: ComponentFixture<FormulaCardExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaCardExportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaCardExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
