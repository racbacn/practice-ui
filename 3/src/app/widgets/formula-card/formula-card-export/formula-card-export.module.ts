import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormulaCardExportComponent } from './formula-card-export.component';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  declarations: [FormulaCardExportComponent],
  imports: [CommonModule, MaterialModule],
  exports: [FormulaCardExportComponent],
})
export class FormulaCardExportModule {}
