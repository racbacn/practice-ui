import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewButtonComponent } from './preview-button.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [PreviewButtonComponent],
  imports: [CommonModule, FlexLayoutModule, MaterialModule],
  exports: [PreviewButtonComponent],
})
export class PreviewButtonModule {}
