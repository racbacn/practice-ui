import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmComponent } from '../popup/confirm/confirm.component';
import { UiData } from '../popup/interfaces/ui-data';
import { SuccessComponent } from '../popup/success/success.component';

@Injectable({
  providedIn: 'root',
})
export class PopupService {
  constructor(private dialog: MatDialog) {}

  openConfirmation(data: UiData): MatDialogRef<ConfirmComponent> {
    const confiramationDialog = this.dialog.open(ConfirmComponent, {
      data,
      width: '1041px',
      height: '530px',
      disableClose: true,
      autoFocus: false,
      panelClass: 'popup-dialog',
    });
    console.log("confiramationDialog", confiramationDialog)
    return confiramationDialog;
  }

  openSuccess(data: any) {
    this.dialog.open(SuccessComponent, {
      data,
      width: '1041px',
      height: '530px',
      disableClose: true,
      autoFocus: false,
    });
  }
}
