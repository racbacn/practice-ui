import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderTextComponent } from './header-text.component';

@NgModule({
  declarations: [HeaderTextComponent],
  imports: [CommonModule],
  exports: [HeaderTextComponent],
})
export class HeaderTextModule {}
