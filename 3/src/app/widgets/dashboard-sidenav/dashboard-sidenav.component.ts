import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-sidenav',
  templateUrl: './dashboard-sidenav.component.html',
  styleUrls: ['./dashboard-sidenav.component.scss'],
})
export class DashboardSidenavComponent implements OnInit {
  @Input() userdetails: any;
  @Input() sidenavdetails: any;
  greeting: string | undefined;
  username: string = "Peter";
 dateToday: Date = new Date();

  @ViewChild(MatSidenav) drawer!: MatSidenav;

  //userdetails: Array<any> = [{name: 'James Tan', id: 'EID12345'}];
  isboolean: boolean = true;
  public isMenuOpen = true;
  
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  get isHandset(): boolean {
    return this.breakpointObserver.isMatched(Breakpoints.Handset);
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
    this.router.events.subscribe((val) => {
      // Filter the event type
      if (val instanceof NavigationStart) {
        if (this.isHandset) {
          console.log('mobile');
          this.drawer.close();
        } else {
          console.log('mobilewrsdv');
          this.isMenuOpen = true;
        }
      }
    });
  }

  ngOnInit(): void {
    this.checkMobileCloseMenu();
  }


  checkMobileCloseMenu() {
    if (this.isHandset) {
      this.isMenuOpen = false;
    } else {
      this.isMenuOpen = true;
    }
  }
}
