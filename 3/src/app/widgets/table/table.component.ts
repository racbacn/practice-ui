import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild,} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {SharedService} from 'src/app/services/shared.service';
import {ExportXlsService} from 'src/app/services/export-xls.service';
import {TableHeaders} from 'src/app/interface/table-headers';
import {TableHeader} from 'src/app/interface/table-header';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';

export interface PeriodicElement {
    fileno: string;
    status: string;
    assignedto: string;
    approval: number;
    duedate: Date;
    actions: boolean;
    timeline: string;
}

type TableHeadersColumns =
    | 'select'
    | 'actions'
    | 'transactionNo'
    | 'transactionType'
    | 'status'
    | 'assignedto'
    | 'approval'
    | 'duedate'
    | 'transactionDate'
    | 'registrationNo'
    | 'ownerId'
    | 'dateReg'
    | 'timeReg'
    | 'vehicleType'
    | 'regMarkId'
    | 'transactionId'
    | 'transactionRefNum'
    | 'refundMethodDescription'
    | 'createdBy'
    | 'gstAmount'
    | 'createdDateTime'
    | 'certifyStatusTypeDescription';

/**
 * @title Table with sorting
 */
@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],

})

export class TableComponent implements AfterViewInit {
    @Input() transactionDetails!: any;
    @Input() link!: any;
    @Input() userType!: string;
    @Input() approvalList!: string;
    @Output() routeTo: EventEmitter<any> = new EventEmitter<any>();
    @Output() searchData: EventEmitter<any> = new EventEmitter<any>();
    @Output() filterVal: EventEmitter<any> = new EventEmitter<any>();
    @Output() filter: EventEmitter<any> = new EventEmitter<any>();
    @Output() sort: EventEmitter<any> = new EventEmitter<any>();
    @Output() approve: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sortCol!: MatSort;

    selectForm!: FormGroup;

    headers: TableHeaders = {
        transactionNo: {
            label: 'Transaction No.',
            property: 'transactionRefNum'
        },
        transactionType: {
            label: 'Transaction Type',
            property: 'transactionType'
        },
        status: {
            label: 'Status',
            property: 'transactionStatus'
        },
        assignedto: {
            label: 'Assigned To',
            property: 'assignedUserId'
        },
        approval: {
            label: 'Likelihood of approval',
            property: 'likelihoodOfApproval'
        },
        duedate: {
            label: 'Due Date',
            property: 'dueDate'
        },
        transactionDate: {
            label: 'Transacation Date',
            property: 'dueDate'
        },
        registrationNo: {
            label: 'Registration No.',
            property: 'registrationNo'
        },
        ownerId: {
            label: 'Owner ID',
            property: 'ownerId'
        },
        dateReg: {
            label: 'Date Registered',
            property: 'dateRegistered'
        },
        timeReg: {
            label: 'Time Registered',
            property: 'timeRegistered'
        },
        vehicleType: {
            label: 'Vehicle Type',
            property: 'vehicleType'
        },
        regMarkId: {
            label: 'Registration Mark ID',
            property: 'registrationMarkId'
        },
        select: {
            label: 'Registration Mark ID',
            property: 'dueDate'
        },
        actions: {
            label: 'Registration Mark ID',
            property: 'dueDate'
        },
        transactionId: {
            label: 'Transaction ID',
            property: 'transactionId'
        },
        transactionRefNum: {
            label: 'Refund Reference Number',
            property: 'transactionRefNum'
        },
        refundMethodDescription: {
            label: 'Refund Method',
            property: 'refundMethodDescription'
        },
        createdBy: {
            label: 'Created By',
            property: 'createdBy'
        },
        createdDateTime: {
            label: 'Creation Date/Time',
            property: 'createdDateTime'
        },
        certifyStatusTypeDescription: {
            label: 'Status',
            property: 'certifyStatusTypeDescription'
        },
        gstAmount: {
            label: 'Amount',
            property: 'gstAmount'
        }
    };

    displayedColumns: TableHeadersColumns[] = [];
    dataSource: any;


    selection!: any;
    totalTransactions: any;
    numSelected: any;
    isManager!: boolean;
    isAll!: boolean;
    isVehicleReg!: boolean;
    isApprovalList!: boolean;
    showFilter!: boolean;
    regNum!: boolean;
    isCertifyRefund!: boolean;
    transactionNo!: boolean;
    selected = '10';
    isVlc!: boolean;
    isVqr!: boolean;

    constructor(
        private router: Router,
        private sharedService: SharedService,
        private exportXlsService: ExportXlsService,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnChanges() {
        console.log(this.transactionDetails)

        this.dataSource = new MatTableDataSource(this.transactionDetails);
        this.totalTransactions = this.dataSource.data.length;
        // this.dataSource.sort = this.sortCol;
        // this.dataSource.paginator = this.paginator;

    }

    ngOnInit() {
        console.log("transactionDetails >>>", this.searchData);
        this.dataSource = new MatTableDataSource(this.transactionDetails);
        this.selection = new SelectionModel<any>(true, this.dataSource);

        console.log(this.userType)

        switch (this.userType) {
            case  "manager":
            case "vlc-manager": {
                this.isApprovalList = false;
                this.isManager = true;
                this.isAll = true;
                this.showFilter = true;
                this.transactionNo = true;
                this.isCertifyRefund = false;
                this.displayedColumns = [
                    'select',
                    'transactionNo',
                    'transactionType',
                    'status',
                    'assignedto',
                    'approval',
                    'duedate',
                    'actions',
                ];
            }
                break;
            case "officer":
            case "vlc-officer": {
                this.isAll = true;
                this.showFilter = true;
                this.transactionNo = true;
                this.isManager = false;
                this.isCertifyRefund = false
                this.displayedColumns = [
                    'select',
                    'transactionNo',
                    'transactionType',
                    'status',
                    'approval',
                    'duedate',
                    'actions'
                ];

            }
                break;
            case "approvalList": {
                this.isApprovalList = true
                this.isAll = false;
                this.isCertifyRefund = false
                this.displayedColumns = [
                    'select',
                    'transactionNo',
                    'transactionType',
                    'status',
                    'approval',
                    'transactionDate',
                    'duedate',
                    'actions'

                ];
                this.regNum = false;
                this.isManager = false;
                this.showFilter = false;
            }
                break;
            case "regVehicles":
                this.displayedColumns = [
                    'select',
                    'registrationNo',
                    'status',
                    'ownerId',
                    'dateReg',
                    'timeReg',
                    'vehicleType',
                    'assignedto',
                    'actions'
                ]
                this.regNum = true;
                this.isManager = false;
                this.showFilter = false;
                this.isAll = true;
                this.isCertifyRefund = false
                break;
            case "pmd":
                this.displayedColumns = [
                    'select',
                    'registrationNo',
                    'status',
                    'ownerId',
                    'dateReg',
                    'timeReg',
                    'regMarkId',
                    'assignedto',
                    'actions'
                ]
                this.regNum = true;
                this.isManager = false;
                this.showFilter = false;
                this.isAll = true;
                this.isCertifyRefund = false
                break;
            case "reviewRefund":
                this.displayedColumns = [
                    'select',
                    'transactionId',
                    'transactionRefNum',
                    'refundMethodDescription',
                    'createdBy',
                    'gstAmount',
                    'createdDateTime',
                    'certifyStatusTypeDescription',
                    'actions'
                ]
                this.regNum = false;
                this.isManager = false;
                this.showFilter = false;
                this.isAll = true;
                this.isCertifyRefund = true;
                break;
            default:
        }
        if (this.userType.includes('vlc')) {
            this.isVlc = true;
        } else {
            this.isVqr = true;
        }
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sortCol;
        this.dataSource.paginator = this.paginator;
    }


    createForm() {
        this.selectForm = this.formBuilder.group({
            filter: new FormControl(),

        });
    }


    onChange(event: any) {
        console.log("filter val", event.value)

    }

    filterList(event: any) {
        let filtername = event.value;
        this.filter.emit(filtername)
    }

    sortList(event: any) {
        console.log("sort", event.value)
        let sortName = event.value;
        this.sort.emit(sortName);
    }

    message = 'Click on a button';

    textBtnConfig = {
        styles: {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: '#ffd11a',
            color: '#000',
            fontFamily: 'sans-serif',
            fontSize: '10px',
            borderRadius: '20px',
            padding: '5px 10px',
        },
        text: 'Reassign',
        iconName: 'assignment_ind',
    };

    textBtnConfigView = {
        styles: {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: '#626ede',
            color: '#fff',
            fontFamily: 'sans-serif',
            fontSize: '10px',
            borderRadius: '20px',
            padding: '5px 10px',
        },
        text: 'View',
        iconName: 'visibility',
    };

    textBtnConfigApprove = {
        styles: {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: '#626ede',
            color: '#fff',
            fontFamily: 'sans-serif',
            fontSize: '10px',
            borderRadius: '20px',
            padding: '5px 10px',
        },
        text: 'Approve',
        iconName: 'check_circle',
    };

    onClickEventReceived(event: string) {
        this.message = event;
        alert(this.message);
    }

    viewDetails(event: string) {
        this.routeTo.emit(event);
    }

    approveTransactions(event: any) {
        this.approve.emit(event);
    }

    onRowClicked(row: any) {
        console.log('Row clicked: here ');
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    masterToggle() {
        this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach((row: any) => this.selection.select(row));
    }

    export() {
        if (this.dataSource.data[0]?.hasOwnProperty('assignedToId')) {
            this.headers.assignedto.property = 'assignedToId';
        }
        const columns: TableHeadersColumns[] = [...this.displayedColumns].filter(
            (column) => column !== 'select' && column !== 'actions'
        );
        const headers: TableHeader[] = columns.map(
            (column: TableHeadersColumns) => this.headers[column]!
        );
        const selectedRows = [...this.selection.selected];
        const filename = 'transactions.xlsx';
        this.exportXlsService.export(headers, selectedRows, filename);
    }
}


