import {Component, OnInit, Inject} from '@angular/core';
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";

@Component({
    selector: 'app-dashboard-greeting',
    templateUrl: './dashboard-greeting.component.html',
    styleUrls: ['./dashboard-greeting.component.scss'],
})
export class DashboardGreetingComponent implements OnInit {
    dateToday: Date = new Date();
    greeting: string | undefined;
    username!: string;

    constructor() {
    }

    ngOnInit(): void {
        this.greetingTime();
    }

    greetingTime() {
        let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER));

        if (userResponse) {
            switch (userResponse.userId.toString()) {
                case "3": {
                    this.username = "Peter"
                    break;
                }
                case "4":{
                    this.username = "James"
                    break;
                }
                case "5":{
                    this.username = "David"
                    break;
                }
                case "6":{
                    this.username = "Joana"
                    break;
                }
            }

        }

        var myDate = new Date();
        var hrs = myDate.getHours();
        console.log(hrs);
        if (hrs < 12) {
            this.greeting = 'Good Morning';
        } else if (hrs >= 12 && hrs <= 17) {
            this.greeting = 'Good Afternoon';
        } else if (hrs >= 17 && hrs <= 24) {
            this.greeting = 'Good Evening';
        }
    }
}
