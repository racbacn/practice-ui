import { AdditionalRoadTaxRecords } from "./additional-road-tax-records";

export interface AdditionalRoadTax {
}

export interface AdditionalRoadTaxResponse {
    AdditionalChargeList: AdditionalRoadTaxRecords[];
}


export interface AdditionalRoadTaxRequest {
    addChargeTypeKey: number | null;
    addChargeName: string;
    addChargeValue: number | null;
    effectiveDate: string;
    endDate: string;
    vehicleRegistrationFrom: string;
    vehicleRegistrationTo: string;
    transactionId: number | null;
  
  }
  
  export interface AdditionalRoadRequest {
    requestHeader: {
      userId: number;
      managerId: any;
      ltaTeamTypeKey: number,
      userTypeKey: number
  }
  additionalCharges: AdditionalRoadTaxRequest[];
  }
