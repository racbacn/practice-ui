export interface AdditionalRoadTaxRecords {
    addChargeName: string;
    addChargeTypeKey: number | null;
    addChargeType: string;
    addChargeAmount: number | null;
    effectiveDate: string;
    endDate: string;
    vehicleRegistrationTo: string;
    vehicleRegistrationFrom: string;
    active?: boolean | undefined;
    transactionId: number;
    amendmentStatus: string;
    rejectComment: string;
    roadTaxAmtCatKey: number;
}



