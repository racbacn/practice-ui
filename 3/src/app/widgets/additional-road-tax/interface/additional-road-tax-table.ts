export interface AdditionalRoadTaxTable {
    addChargeName: string;
    addChargeTypeKey: number | null;
    addChargeType: string;
    addChargeAmount: number | null;
    effectiveDate: Date;
    endDate: Date;
    active?: boolean | undefined;
    transactionId: number;
    isAParent: boolean;
    isExpanded: boolean;
    parentIndex: number | null;
    index: number;
    newlyAdded: boolean;
    fromApi: boolean;
    isNewRow: boolean;
    vehicleRegistrationFrom: Date;
    vehicleRegistrationTo: Date;
    amendmentStatus: string;
    rejectComment: string;
    forResubmission: boolean;
}
