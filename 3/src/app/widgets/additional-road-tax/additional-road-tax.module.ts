import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdditionalRoadTaxComponent } from './additional-road-tax.component';
import { ContextualTableModule } from '../formula-card/contextual-table/contextual-table.module';
import { MaterialModule } from 'src/app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from 'src/app/shared/shared.module';
import { SubmitButtonModule } from '../formula-card/submit-button/submit-button.module';



@NgModule({
  declarations: [
    AdditionalRoadTaxComponent
  ],
  imports: [
    CommonModule,
    ContextualTableModule,
    MaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SharedModule,
    SubmitButtonModule
  ],
  exports: [AdditionalRoadTaxComponent]
})
export class AdditionalRoadTaxModule { }
