import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalRoadTaxComponent } from './additional-road-tax.component';

describe('AdditionalRoadTaxComponent', () => {
  let component: AdditionalRoadTaxComponent;
  let fixture: ComponentFixture<AdditionalRoadTaxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalRoadTaxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalRoadTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
