import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RoadTaxService } from 'src/app/services/road-tax.service';
import { AMENDMENT_STATUS } from 'src/app/shared/constants/application.constants';
import { FormulaCardService } from '../formula-card/formula-card.service';
import { AdditionalRoadTaxTable } from './interface/additional-road-tax-table';

@Component({
  selector: 'app-additional-road-tax',
  templateUrl: './additional-road-tax.component.html',
  styleUrls: ['./additional-road-tax.component.scss']
})
export class AdditionalRoadTaxComponent implements OnInit {
  
  @Input() options: any[] = [];
  @Input() exportData: any = { headers: [], rows: [], filename: "test.xlsx" };

  @Input() fees: any[] = [];
  @Input() settings: any = {};
  @Output() selectionChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() addRow: EventEmitter<null> = new EventEmitter<null>();
  @Output() removeRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() proceedButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggleCollapse: EventEmitter<any> = new EventEmitter<any>();
  @Output() addNewValueClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() datepickerChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() inputChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() saveDraft: EventEmitter<any> = new EventEmitter<any>();

  placeholder = "Select Aditional Charge Type";
  columnHeaders: any[] = [
    {
      label: "Additional Charge Name",
      property: "addChargeName",
      type: "textField",
    },
    {
      label: "Charge Type",
      property: "addChargeType",
      type: "selectField",
    },
    {
      label: "For Vehicles with Registration Date(s)",
      property: "vehicleRegistrationFrom",
      property2: "vehicleRegistrationTo",
      type: "rangeField",
    },
    {
      label: "Charge Amount",
      property: "addChargeAmount",
      type: "numberField",
    },
    {
      label: "Effective Date",
      property: "effectiveDate",
      type: "dateField",
    },
    {
      label: "End Date",
      property: "endDate",
      type: "dateField",
    },
  ];

  headers: any[] = [
    {
      label: "",
      property: "warningTooltipButton",
      type: "",
    },
    ...this.columnHeaders,
    {
      label: "",
      property: "addButton",
      type: "",
    },
  ];
  additionalRoadTax: any;

  addRowCount = 0;
  selection = [
    {
      value: "Flat Amount",
    },
    {
      value: "% of total Road Tax Amount",
    },
  ];
  showError: boolean | undefined;
  lastParentIndex: any;

  constructor(private roadTaxService: RoadTaxService, private formulaCardService: FormulaCardService) { }

  ngOnInit() {
    this.getDetails();
  }
  getDetails() {
    this.roadTaxService.getAdditionalTax().subscribe((res: any) => {
      this.additionalRoadTax =  res;
      this.lastParentIndex = this.additionalRoadTax[this.additionalRoadTax.length - 1];
      this.additionalRoadTax.forEach((data: AdditionalRoadTaxTable) => {
        if(!data.forResubmission){
          let headers =  [{
            label: "",
            property: "warningTooltipButton",
            type: "",
          },
          ...this.columnHeaders,
          {
            label: "",
            property: "addButton",
            type: "",
          }]
          this.headers = headers
        }else {
          
          let headers =  [{
            label: "",
            property: "warningTooltipButton",
            type: "",
          },
          ...this.columnHeaders,
          {
            label: "",
            property: "addButtonResubmission",
            type: "",
          }]
          this.headers = headers;
        }
  
      });
    });
  }
  onAddRow(row: any) {
    this.addRow.emit(row);
  }

  onSelectionChange(selected: any) {
    this.selectionChange.emit(selected);
  }

  onAddNewValueClick(fee: any) {
    this.addNewValueClick.emit(fee);
  }

  addRebatesRow(data: any) {

    if (data.index || data.index !== 0) {
      const lastChildIndex = data[data.length - 1];
      const add = this.createChild(lastChildIndex);
      this.additionalRoadTax.push({
        active: false,
        effectiveDate: this.addDaysToDate(add.endDate, 1),
        endDate: this.addDaysToDate(add.endDate, 2),
        vehicleRegistrationFrom: this.addDaysToDate(add.vehicleRegistrationTo, 1),
        vehicleRegistrationTo: this.addDaysToDate(add.vehicleRegistrationTo, 2),
        index: 0,
        isAParent: true,
        isExpanded: false,
        newlyAdded: true,
        parentIndex: data?.index || null,
        addChargeAmount: add.addChargeAmount,
        addChargeName: add.addChargeName,
        addChargeTypeKey: add.addChargeTypeKey,
        addChargeType: add.addChargeType,
        fromApi: false,
        isNewRow: true
      });
      this.additionalRoadTax = [...this.additionalRoadTax]
      this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
    }

    this.addRowCount++;
 
  }
  getLastChildIndex(data: any): number {
    const lastChild = data[data.length - 1];
    return lastChild.index || 0;
  }

  createChild(parentSurcharge: AdditionalRoadTaxTable): AdditionalRoadTaxTable {
    const { effectiveDate, endDate, addChargeAmount, addChargeName, addChargeTypeKey, addChargeType, vehicleRegistrationTo, vehicleRegistrationFrom } = {
      ...parentSurcharge,
    };
    return {
      ...parentSurcharge,
      effectiveDate: this.addDaysToDate(endDate, 1),
      endDate: this.addDaysToDate(endDate, 2),
      vehicleRegistrationFrom: this.addDaysToDate(vehicleRegistrationTo, 1),
      vehicleRegistrationTo: this.addDaysToDate(vehicleRegistrationTo, 2),
      addChargeAmount: addChargeAmount,
      addChargeName: addChargeName,
      addChargeTypeKey: addChargeTypeKey,
      addChargeType: addChargeType,
      isAParent: true,
      isExpanded: false,
      parentIndex: parentSurcharge?.index || null,
      newlyAdded: true,
      fromApi: false,
      isNewRow: true
    };
  }

  getNewlyAddedChildrenLength(parentIndex: number) {
    const newlyAddedChildren = this.additionalRoadTax.filter(
      (child: AdditionalRoadTaxTable) =>
        child.parentIndex === parentIndex && child.newlyAdded
    );
    return newlyAddedChildren.length;
  }

  setParent(parentIndex: number) {
    const children = this.additionalRoadTax.filter(
      (data: any) => data.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.additionalRoadTax[parentIndex];
      parent.hasChildren = false;
    }
  }

  onDatepickerChange(changed: AdditionalRoadTaxTable) {
  
    const index = changed.index ?? 0;
    const changedDates = this.additionalRoadTax[index];
    const effectiveDateChanged =
    changedDates.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedDates.effectiveDate = changed.effectiveDate;
      changedDates.newlyAdded = true;
    }
 
    const endDateChanged =
    changedDates.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedDates.endDate = changed.endDate;
      changedDates.newlyAdded = true;
    }
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
  }

  onRangeDatepickerChange(changed: AdditionalRoadTaxTable) {
    const index = changed.index ?? 0;
    const changedDates = this.additionalRoadTax[index];
    const effectiveDateChanged =
    changedDates.vehicleRegistrationFrom.getTime() !==
      changed.vehicleRegistrationFrom.getTime();
     
    if (effectiveDateChanged) {
      changedDates.vehicleRegistrationFrom = changed.vehicleRegistrationFrom;
      changedDates.newlyAdded = true;
    }
 
    const endDateChanged =
    changedDates.vehicleRegistrationTo.getTime() !== changed.vehicleRegistrationTo.getTime();
    if (endDateChanged) {
      changedDates.vehicleRegistrationTo = changed.vehicleRegistrationTo;
      changedDates.newlyAdded = true;
    }

    this.additionalRoadTax = [...this.additionalRoadTax];
    this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
  }

  onInputChange(changed: AdditionalRoadTaxTable) {
    const index = changed.index ?? 0;
    const changedFees = this.additionalRoadTax[index];
    changedFees.addChargeName = changed.addChargeName;
    changedFees.addChargeAmount = changed.addChargeAmount;
    changedFees.newlyAdded = true;
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
  }


  selectChange(select: AdditionalRoadTaxTable){
    const index = select.index ?? 0;
    const changedFees = this.additionalRoadTax[index];
    changedFees.addChargeTypeKey = this.formulaCardService.getTypeKey(
      select.addChargeType
    );
    changedFees.addChargeType = select.addChargeType;
    changedFees.newlyAdded = true;
    this.additionalRoadTax = [...this.additionalRoadTax];
      this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
  }

  onDatepickerChange2(changed: AdditionalRoadTaxTable) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.additionalRoadTax[index];
    const effectiveDateChanged =
      changedSurcharge.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedSurcharge.effectiveDate = changed.effectiveDate;
      changedSurcharge.newlyAdded = true;
    }
    const endDateChanged =
      changedSurcharge.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedSurcharge.endDate = changed.endDate;
      changedSurcharge.newlyAdded = true;
    }
    this.additionalRoadTax = [...this.additionalRoadTax];
    this.roadTaxService.setAdditionalTax(this.additionalRoadTax);
  }


  addDaysToDate(date: Date, numberOfDays: number) {
    const addDayToThisDate = new Date(date);
    addDayToThisDate.setDate(addDayToThisDate.getDate() + numberOfDays);
    return addDayToThisDate;
  }

  onRemoveRow(charge: AdditionalRoadTaxTable) {
    let draftsParams: any[] = []
    draftsParams.push(charge)
    if(charge.amendmentStatus === AMENDMENT_STATUS.draft){
      this.roadTaxService.removeDraft(draftsParams).subscribe(res => {
        const message = 'The draft row has been deleted.';
        this.formulaCardService.openSuccessPopup(message);
      });
    }
    const indexToBeRemoved = charge?.index;
    this.additionalRoadTax.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = charge.parentIndex ?? 0;
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.additionalRoadTax = this.additionalRoadTax.map((fee: any, index: any) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  submit(event: any) {
     this.proceedButtonClick.emit();
  }
  saveAsDraft(event: any){
    let isDraft = true;
    this.saveDraft.emit(isDraft);
}
}
