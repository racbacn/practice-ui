import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router'
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";

@Component({
  selector: 'app-approve-reject-dialog',
  templateUrl: './approve-reject-dialog.component.html',
  styleUrls: ['./approve-reject-dialog.component.scss'],
})
export class ApproveRejectDialogComponent implements OnInit {
  vqrManager: string = 'defaultUserVQRManager';
  vqrOfficer: string = 'defaultUserVQROfficer';

  dashboardButtonConfig = {
    buttonText: 'Back to Dashboard',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonClassName: 'dashboardButton',
    buttonFont: 'button-font-16',
  };

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      icon: string;
      tick: string;
      title: string;
      details: string;
    }, public dialog: MatDialogRef<ApproveRejectDialogComponent>, private router: Router
  ) { dialog.disableClose = true; }

  ngOnInit(): void { }

  onClose() {
    this.dialog.close();
  }

  back() {
    this.dialog.close();
    let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))
    let username = userResponse.username;

    if (username == this.vqrManager) {
      this.router.navigate(['vqr-manager/dashboard-vqrmanager'])

    } else if (username == this.vqrOfficer) {

      this.router.navigate(['vqr-officer/dashboard-vqrofficer'])
    }
  }
}
