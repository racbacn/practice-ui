import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { ApproveRejectDialogComponent } from '../approve-reject-dialog/approve-reject-dialog.component';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  remarksControl = new FormControl();

  proceedButtonConfig = {
    buttonText: 'Proceed',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonClassName: 'proceedButton',
    buttonFont: 'button-font-16'
  };

  constructor(public dialog: MatDialogRef<DialogComponent>, public rejectDialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) {
    dialog.disableClose = true;
    console.log(this.data["details"])
  }
  openRejectDialog() {
    this.rejectDialog.open(ApproveRejectDialogComponent, {
      minHeight: '480px',
      width: '80%',
      data: {
        icon:"../../../assets/images/reject-icon.svg",
        tick:"../../../assets/images/cross-icon.svg",
        title: "APPLICATION HAS BEEN REJECTED",
        details: this.data["details"],
      }
    });
  }
  ngOnInit(): void {}

  onClose() {
    //Return the remarks
    this.dialog.close(this.remarksControl.value);
  }

}
