import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SearchInput} from "../../interface/search-input";


@Component({
    selector: 'app-search-form',
    templateUrl: './search-form.component.html',
    styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
    searchForm!: FormGroup;
    @Input() userType!: string;
    @Input() regNo!: string;
    @Input() ownerId!: string;
    @Input() vehicleType!: string;
    @Input() dateFrom!: string;
    @Input() dateTo!: string;
    @Output() searchParams: EventEmitter<any> = new EventEmitter<any>();
    panelOpenState = false;
    @Input() searchData!: any;
    isPmd!: boolean;
    fieldList!: Array<any>;
    initFormValue!: any;

    textBtnConfigApprove = {
        styles: {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: '#626ede',
            color: '#fff',
            fontFamily: 'sans-serif',
            fontSize: '10px',
            borderRadius: '20px',
            padding: '5px 10px',
        },
        text: 'Approve',
        iconName: 'check_circle',
    };
    isVehicle!: boolean;


    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.createForm();
        this.formValueChanges();
    }

    createForm() {
        this.searchForm = this.formBuilder.group({});

        if (this.userType) {
            switch (this.userType) {
                case "reviewRefund" : {
                    this.fieldList = this.getFieldListReviewRefund();
                    break;
                }
                case "pmd" : {
                    this.fieldList = this.getFieldListPmd();
                    break;
                }
                case "regVehicles" : {
                    this.fieldList = this.getFieldListRegVehicle();
                    break;
                }
                default:
                    break;
            }
        }

        if (this.fieldList.length > 0) {
            this.fieldList.forEach(field => {
                this.searchForm.addControl(field.formControlName, new FormControl(''))
            })

            this.initFormValue = this.searchForm.value;
            this.searchData = this.initFormValue;
        }
    }

    formValueChanges() {
        this.searchForm.valueChanges.subscribe(data => {
            this.searchData = data;
        })
    }

    search() {
        this.searchParams.emit(this.searchData);
    }

    clear() {
        this.searchForm.reset(this.initFormValue);
    }

    private getFieldListReviewRefund(): Array<SearchInput> {
        return [
            {
                formControlName: 'transactionRefNum',
                label: 'Refund Reference No.',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'creationDateTimeFrom',
                label: 'Action Date/Time From',
                type: 'date',
                selectInputList: []
            },
            {
                formControlName: 'creationDateTimeTo',
                label: 'Action Date/Time To',
                type: 'date',
                selectInputList: []
            },
            {
                formControlName: 'certifyStatusTypeKey',
                label: 'Status',
                type: 'select',
                selectInputList: [
                    {
                        name: 'PENDING_APPROVAL',
                        value: "1"
                    },
                    {
                        name: 'CERTIFIED',
                        value: "2"
                    }
                    , {
                        name: 'REFUND_REJECTED',
                        value: "1"
                    }
                ]
            }
        ]
    }

    private getFieldListPmd(): Array<SearchInput> {
        return [
            {
                formControlName: 'transactionRefNum',
                label: 'Registration No.',
                type: 'text',
                selectInputList: []

            },
            {
                formControlName: 'fromUserId',
                label: 'Owner ID',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'regMarkId',
                label: 'Registration Mark Id',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'dateFrom',
                label: 'Action Date/Time From',
                type: 'date',
                selectInputList: []
            },
            {
                formControlName: 'dateTo',
                label: 'Action Date/Time To',
                type: 'date',
                selectInputList: []
            }
        ]
    }

    private getFieldListRegVehicle(): Array<SearchInput> {
        return [
            {
                formControlName: 'transactionRefNum',
                label: 'Registration No.',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'fromUserId',
                label: 'Owner ID',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'vehicleType',
                label: 'Vehicle Type',
                type: 'text',
                selectInputList: []
            },
            {
                formControlName: 'actionDateTimeFrom',
                label: 'Action Date/Time From',
                type: 'date',
                selectInputList: []
            },
            {
                formControlName: 'actionDateTimeTo',
                label: 'Action Date/Time To',
                type: 'date',
                selectInputList: []
            }
        ]
    }

}
