import {Component, OnInit, OnDestroy} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogComponent} from 'src/app/widgets/dialog/dialog.component';
import {DialogPmdRejectComponent} from 'src/app/widgets/dialog-pmd-reject/dialog-pmd-reject.component';
import {ApproveRejectDialogComponent} from 'src/app/widgets/approve-reject-dialog/approve-reject-dialog.component';
import {SharedService} from 'src/app/services/shared.service';
import {HttpServicesService} from 'src/app/services/http-services.service';
import {Subscription} from 'rxjs';
import {rejectRegistration} from 'src/app/interface/rejectRegistration';
import {DatePipe} from '@angular/common';
import {serverUrl} from 'src/app/constants/sharedValue'
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component'
import {UserResponse} from "../../interface/user-response";
import {SessionUtil} from "../../core/util/sessionUtil";
import {RoadTaxCertifyRefundService} from "../../services/road-tax-certify-refund.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit, OnDestroy {
    clickApproveRejectsubscription: Subscription = new Subscription;
    confirmSubscription: Subscription = new Subscription;
    datePipe: DatePipe = new DatePipe("en-US");
    apiURL: string = serverUrl

    constructor(public dialog: MatDialog, private sharedService: SharedService, private httpServices: HttpServicesService, private router: Router) {
        this.clickApproveRejectsubscription = this.sharedService.getApproveClick().subscribe((dataIn) => {
            console.log(dataIn)
            this.openApproveDialog("APPLICATION HAS BEEN APPROVED", dataIn["transactionId"]);
        })

        this.confirmSubscription = this.sharedService.getConfirmApproveClick().subscribe((dataIn) => {
            this.openConfrimApproveDialog(dataIn["transactionId"]);
        })
        const rejectSubscription = this.sharedService.getRejectClick().subscribe((dataIn) => {
            console.log(dataIn)
            //vehicleType is PMD or VEHICLE
            if (dataIn["vehicleType"] == "PMD") {
                this.openPMDDialog(dataIn);
            } else {
                this.openDialog(dataIn["transactionId"]);
            }
        })
        this.clickApproveRejectsubscription.add(rejectSubscription);

        const approveCertifyRefundSubscription = this.sharedService.getApproveCertifyRefundResponse().subscribe(data => {
            this.openApproveDialog("REFUND APPLICATION HAVE BEEN CERTIFIED", data.transactionId);
        });
        this.clickApproveRejectsubscription.add(approveCertifyRefundSubscription);

        const rejectCertifyRefundSubscription = this.sharedService.getRejectCertifyRefundResponse().subscribe(data => {
            this.openRejectDialog("REFUND APPLICATION HAVE BEEN REJJECTED", data.transactionId)
        });
        this.clickApproveRejectsubscription.add(rejectCertifyRefundSubscription);
    }

    openPMDDialog(dataIn: any) {
        let dialogReject = this.dialog.open(DialogPmdRejectComponent, {
            minHeight: '600px',
            width: '80%',
            data: {
                details: "Transaction No.: " + dataIn["transactionId"],
                userType: dataIn["userType"],

            }
        });
        dialogReject.afterClosed().subscribe(res => {
            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

            //Prevent press the closed X button and send the api
            if (res !== null) {
                let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
                if (timeNow) {

                    console.log(timeNow)
                    let rejectContent = {
                        "requestHeader": {
                            "requestAppId": "VRLS_PMD_Rejection",
                            "requestDateTime": timeNow,
                            "userId": userResponse.userId,
                            "userTypeKey": userResponse.userTypeKey
                        },
                        transactionId: dataIn["transactionId"],
                        comment: res.comments,
                        // userType: res.userType    //set to 3 for vqr-officer, 4 for vqr-manager

                    };
                    console.log(rejectContent)

                    this.sendRejectPMDAPI(rejectContent)
                }
            }
        })
    }

    openDialog(transID: string) {
        let dialogReject = this.dialog.open(DialogComponent, {
            minHeight: '600px',
            width: '80%',
            data: {
                details: "Transaction No.: " + transID,

            }
        });
        dialogReject.afterClosed().subscribe(res => {
            let userResponse: UserResponse = JSON.parse(<string>SessionUtil.getSessionStorage(SessionUtil.USER))

            //Prevent press the closed X button and send the api
            if (res !== null) {
                let timeNow = this.datePipe.transform(Date(), "YYYY-MM-ddTHH:MM:ss");
                console.log(timeNow)
                if (timeNow) {
                    let rejectContent = {
                        "requestHeader": {
                            "requestAppId": "VRLS_Reject_Vehicle_Registration",
                            "requestDateTime": timeNow,
                            "userId": userResponse.userId,
                            "userTypeKey": userResponse.userTypeKey
                        },
                        transactionId: transID,
                        comment: res,
                    };
                    console.log(rejectContent)

                    this.sendRejectAPI(rejectContent)
                }
            }
        })
    }

    openApproveDialog(title: string, transID: string) {
        let approveDialog = this.dialog.open(ApproveRejectDialogComponent, {
            minHeight: '480px',
            width: '80%',
            data: {
                icon: "../../../assets/images/approve-icon.svg",
                tick: "../../../assets/images/tick-icon.svg",
                title: title,
                details: "Transaction No.: " + transID,
            }
        });

        approveDialog.afterClosed().subscribe(() => {
            window.location.reload();
        })
    }


    openConfrimApproveDialog(transID: string) {
        this.dialog.open(ConfirmDialogComponent, {
            minHeight: '480px',
            width: '80%',
            data: {
                icon: "../../../assets/images/approve-icon.svg",
                tick: "../../../assets/images/tick-icon.svg",
                title: "APPLICATION HAS BEEN APPROVED",
                details: "Transaction No.: " + transID,
            }
        });
    }

    openRejectDialog(title: string, transID: string) {
        let rejectDialog = this.dialog.open(ApproveRejectDialogComponent, {
            minHeight: '480px',
            width: '80%',
            data: {
                icon: "../../../assets/images/reject-icon.svg",
                tick: "../../../assets/images/x_reject.svg",
                title: title,
                details: "Transaction No.: " + transID,
            }
        });

        rejectDialog.afterClosed().subscribe(() => {
            window.location.reload();
        })
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        this.clickApproveRejectsubscription.unsubscribe();
    }

    sendRejectAPI(dataPut: rejectRegistration) {
        this.httpServices.httpPut(this.apiURL + "/manage-vehicle-service/rejectVehicleRegistration", dataPut).subscribe(res => {
            console.log(res)
            this.openRejectDialog("APPLICATION HAS BEEN REJECTED", dataPut.transactionId)
        });
    }

    sendRejectPMDAPI(dataPut: rejectRegistration) {
        this.httpServices.httpPut(this.apiURL + "/manage-pmd-service/rejectPMDRegistration", dataPut).subscribe(res => {
            console.log(res)
            this.openRejectDialog("APPLICATION HAS BEEN REJECTED", dataPut.transactionId)
        });
    }
}
