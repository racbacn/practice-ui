import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPmdRejectComponent } from './dialog-pmd-reject.component';

describe('DialogPmdRejectComponent', () => {
  let component: DialogPmdRejectComponent;
  let fixture: ComponentFixture<DialogPmdRejectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogPmdRejectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPmdRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
