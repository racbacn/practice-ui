import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { ApproveRejectDialogComponent } from '../approve-reject-dialog/approve-reject-dialog.component';

@Component({
  selector: 'app-dialog-pmd-reject',
  templateUrl: './dialog-pmd-reject.component.html',
  styleUrls: ['./dialog-pmd-reject.component.scss']
})
export class DialogPmdRejectComponent implements OnInit {
  remarksControl = new FormControl();

  proceedButtonConfig = {
    buttonText: 'Proceed',
    hasIcon: true,
    iconName: 'arrow_right_alt',
    buttonClassName: 'proceedButton',
    buttonFont: 'button-font-16'
  };

  constructor(public dialog: MatDialogRef<DialogPmdRejectComponent>, public rejectDialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) {
    dialog.disableClose = true;
    console.log(this.data["details"])
  }
  openRejectDialog() {
    this.rejectDialog.open(ApproveRejectDialogComponent, {
      minHeight: '480px',
      width: '80%',
      data: {
        icon:"../../../assets/images/reject-icon.svg",
        tick:"../../../assets/images/cross-icon.svg",
        title: "APPLICATION HAS BEEN REJECTED",
        details: this.data["details"],
      }
    });
  }
  ngOnInit(): void {}

  onClose() {
    let retClosedObj = {
      comments: this.remarksControl.value,
      userType: this.data["userType"]
    };
    //Return the remarks
    this.dialog.close(retClosedObj);
  }

  onCloseExit() {
    this.dialog.close();
  }
}

