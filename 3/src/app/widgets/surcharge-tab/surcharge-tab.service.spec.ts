import { TestBed } from '@angular/core/testing';

import { SurchargeTabService } from './surcharge-tab.service';

describe('SurchargeTabService', () => {
  let service: SurchargeTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SurchargeTabService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
