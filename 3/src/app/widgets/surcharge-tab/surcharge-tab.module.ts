import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurchargeTabComponent } from './surcharge-tab.component';
import { FormulaCardContextualModule } from '../formula-card/formula-card-contextual/formula-card-contextual.module';
import { PopupModule } from '../formula-card/popup/popup.module';

@NgModule({
  declarations: [SurchargeTabComponent],
  imports: [CommonModule, FormulaCardContextualModule, PopupModule],
  exports: [SurchargeTabComponent],
})
export class SurchargeTabModule {}
