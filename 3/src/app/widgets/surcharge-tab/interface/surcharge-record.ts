export interface SurchargeRecord {
  transactionId: number;
  annualSurcharge: number;
  effectiveDate: string;
  endDate: string;
  active: boolean;
  rejectComment: string;
  amendmentStatus: string;
}
