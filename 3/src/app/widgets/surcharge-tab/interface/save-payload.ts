import { SavePayloadSurcharge } from './save-payload-surcharge';

export interface SavePayload {
  requestHeader: {
    ltaTeamTypeKey: number;
    userTypeKey: number;
    managerId: number;
    userId: number;
  };
  surchargeValues: SavePayloadSurcharge[];
}
