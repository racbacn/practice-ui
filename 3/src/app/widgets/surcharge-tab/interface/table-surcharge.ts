export interface TableSurcharge {
  vehicleAge: string;
  roadTaxAmtCatKey: number;
  newValue: number | null;
  annualSurcharge: number | null;
  effectiveDate: Date;
  endDate: Date;
  active: boolean;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  isAResponse: boolean;
  amendmentStatus: string;
  isRemoved: boolean;
  transactionId: number;
}
