export interface SavePayloadSurcharge {
  transactionId?: number;
  roadTaxAmtCatKey: string;
  surchargeValue: number;
  effectiveDate: string;
  endDate: string;
}
