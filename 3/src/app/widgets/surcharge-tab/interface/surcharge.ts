import { SurchargeRecord } from './surcharge-record';

export interface Surcharge {
  vehicleAge: string;
  roadTaxAmtCatKey: number;
  surchargeRecords: SurchargeRecord[];
}
