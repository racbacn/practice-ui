import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurchargeTabComponent } from './surcharge-tab.component';

describe('SurchargeTabComponent', () => {
  let component: SurchargeTabComponent;
  let fixture: ComponentFixture<SurchargeTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurchargeTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurchargeTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
