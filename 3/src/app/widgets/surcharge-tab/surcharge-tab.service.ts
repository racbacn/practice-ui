import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormulaCardService } from '../formula-card/formula-card.service';
import { SavePayload } from './interface/save-payload';
import { SavePayloadSurcharge } from './interface/save-payload-surcharge';
import { Surcharge } from './interface/surcharge';
import { SurchargeRecord } from './interface/surcharge-record';
import { TableSurcharge } from './interface/table-surcharge';

interface SurchargeResponse {
  RoadTaxSurcharge: Surcharge[];
}

@Injectable({
  providedIn: 'root',
})
export class SurchargeTabService {
  private SERVER_URL = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService
  ) {}

  isRejected(status: string) {
    return this.formulaCardService.isRejected(status);
  }

  isADraft(status: string): boolean {
    return this.formulaCardService.isADraft(status);
  }

  setTooltipMessage(surcharge: SurchargeRecord) {
    const { amendmentStatus, rejectComment } = surcharge;
    const isRejected = this.isRejected(amendmentStatus);
    if (isRejected) {
      return rejectComment || amendmentStatus;
    }
    return '';
  }

  formatSurcharge(data: SurchargeResponse): TableSurcharge[] {
    const surcharges: TableSurcharge[] = [];
    let index = -1;
    let parentIndex = -1;
    let forResubmission = false;
    let noActiveSurcharge = false;
    data.RoadTaxSurcharge.forEach((surcharge: Surcharge) => {
      surcharge.surchargeRecords.find((record: SurchargeRecord) => {
        if (record.amendmentStatus.toLowerCase() === 'manager rejected') {
          forResubmission = true;
          noActiveSurcharge = false;
          return true;
        }
        return false;
      });
    });
    data.RoadTaxSurcharge.forEach((surcharge: Surcharge) => {
      const { vehicleAge, roadTaxAmtCatKey, surchargeRecords } = surcharge;
      noActiveSurcharge = false;
      let activeSurcharge = surchargeRecords.filter(
        (surcharge: SurchargeRecord) => surcharge.active
      )[0];
      if (!activeSurcharge) {
        if (!surchargeRecords[0]) {
          return;
        }
        activeSurcharge = surchargeRecords[0];
        noActiveSurcharge = true;
      }
      const { effectiveDate, endDate, annualSurcharge } = activeSurcharge;
      index++;
      parentIndex = index;
      const parentSurcharge = {
        vehicleAge,
        roadTaxAmtCatKey,
        ...activeSurcharge,
        transactionId: activeSurcharge.transactionId,
        newValue: annualSurcharge,
        effectiveDate: this.formulaCardService.formatDate(effectiveDate),
        endDate: this.formulaCardService.formatDate(endDate),
        isAParent: true,
        isExpanded: false,
        parentIndex: null,
        hasChildren: surchargeRecords.length > 1,
        newlyAdded:
          this.isRejected(activeSurcharge.amendmentStatus) ||
          this.isADraft(activeSurcharge.amendmentStatus),
        fromApi: true,
        index,
        isAResponse: true,
        tooltip: this.setTooltipMessage(activeSurcharge),
        isRemoved: false,
        full: surcharge,
        forResubmission,
        isOfficer: true,
        parentNotEditable: true,
      };
      surcharges.push(parentSurcharge);
      const inactiveSurcharges = surcharge.surchargeRecords.filter(
        (surcharge: SurchargeRecord) => !surcharge.active
      );
      if (!inactiveSurcharges.length) {
        return;
      }
      inactiveSurcharges.forEach((child: any, childIndex: number) => {
        if (noActiveSurcharge && !childIndex) {
          return;
        }
        index++;
        const chilSurcharge = {
          transactionId: child.transactionId,
          vehicleAge: '',
          roadTaxAmtCatKey,
          ...child,
          newValue: child.annualSurcharge,
          annualSurcharge: '',
          effectiveDate: this.formulaCardService.formatDate(
            child.effectiveDate
          ),
          endDate: this.formulaCardService.formatDate(child.endDate),
          isAParent: false,
          isExpanded: false,
          parentIndex: parentIndex,
          hasChildren: false,
          newlyAdded:
            this.isRejected(child.amendmentStatus) ||
            this.isADraft(child.amendmentStatus),
          fromApi: true,
          index: index,
          isAResponse: true,
          tooltip: this.setTooltipMessage(child),
          isRemoved: false,
          full: { ...surcharge, activeSurcharge: activeSurcharge || null },
          forResubmission,
          isOfficer: true,
        };
        surcharges.push(chilSurcharge);
      });
    });
    return surcharges;
  }

  getSurchargeResponse(): Observable<SurchargeResponse> {
    const apiEndpoint = '/manage-roadtax-service/get/RoadTaxValues';
    const apiUrl = this.SERVER_URL + apiEndpoint;
    const surchargeTab = 4;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', surchargeTab);
    params = params.append('userId', userId);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    params = params.append('userTypeKey', userTypeKey);
    return this.http.get<SurchargeResponse>(apiUrl, { params });
  }

  getSurcharge(): Observable<TableSurcharge[]> {
    return this.getSurchargeResponse().pipe(
      map((response: SurchargeResponse) => {
        if (response.RoadTaxSurcharge.length) {
          return this.formatSurcharge(response);
        }
        return [];
      })
    );
  }

  async callSaveApi(endpoint: string, surchargeValues: SavePayloadSurcharge[]) {
    const apiUrl = this.SERVER_URL + endpoint;
    const header = await this.formulaCardService.getOfficerSaveRequestHeader();
    const payload: SavePayload = { requestHeader: header, surchargeValues };
    return this.http.post<any>(apiUrl, payload);
  }

  async callDeleteApi(values: TableSurcharge[]) {
    return await this.formulaCardService.deleteRoadTaxValue(values);
  }

  async submitSurcharges(
    values: SavePayloadSurcharge[]
  ): Promise<Observable<any>> {
    const endpoint = '/manage-roadtax-service/saveSurcharge';
    const subscriptions: Observable<any>[] = [];
    if (values.length) {
      const saveValues = await this.callSaveApi(endpoint, values);
      subscriptions.push(saveValues);
    }
    return forkJoin(subscriptions);
  }

  async saveSurchargeAsDraft(
    values: SavePayloadSurcharge[]
  ): Promise<Observable<any>> {
    const endpoint = '/manage-roadtax-service/surcharge/draft';
    const subscriptions: Observable<any>[] = [];
    if (values.length) {
      const saveValues = await this.callSaveApi(endpoint, values);
      subscriptions.push(saveValues);
    }
    return forkJoin(subscriptions);
  }
}
