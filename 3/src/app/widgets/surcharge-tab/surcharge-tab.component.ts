import { Component, OnInit } from '@angular/core';
import { PopupService } from '../formula-card/services/popup.service';
import { TableSurcharge } from './interface/table-surcharge';
import { Surcharge } from './interface/surcharge';
import { SurchargeTabService } from './surcharge-tab.service';
import { UiData } from '../formula-card/popup/interfaces/ui-data';
import { SurchargeRecord } from './interface/surcharge-record';
import { SavePayloadSurcharge } from './interface/save-payload-surcharge';
import { FormulaCardService } from '../formula-card/formula-card.service';
import { Row } from '../formula-card/popup/interfaces/row';

@Component({
  selector: 'app-surcharge-tab',
  templateUrl: './surcharge-tab.component.html',
  styleUrls: ['./surcharge-tab.component.scss'],
})
export class SurchargeTabComponent implements OnInit {
  selectOptions = [
    {
      label: 'Base1 Road Tax Fee ( Petrol, CNG or Diesel Cars)',
      value: 'BRTF',
    },

    {
      label: 'Road Tax Rebate(s)',
      value: 'RTR',
    },
  ];
  settings = {
    submitButtonText: 'Proceed',
    hasSelectOptions: false,
    headerText: 'Road tax surcharge for vehicles over 10 years',
    hasPreviewButton: false,
    hasSaveAsDraftButton: true,
  };
  spreadsheetHeaders: any[] = [
    {
      label: 'Age of vehicle',
      property: 'vehicleAge',
      type: 'string',
    },
    {
      label: 'Annual road tax surcharge',
      property: 'annualSurcharge',
      type: 'collapsible',
    },
    {
      label: 'New Value',
      property: 'newValue',
      type: 'numberField',
    },
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'dateField',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'dateField',
    },
  ];
  uiTableHeaders: any[] = [
    {
      label: '',
      property: 'warningTooltipButton',
      type: '',
    },
    ...this.spreadsheetHeaders,
    {
      label: '',
      property: 'rowAddButton',
      type: '',
    },
  ];
  surcharges: TableSurcharge[] = [];
  exportData = {
    headers: this.spreadsheetHeaders,
    rows: this.surcharges,
    filename: 'surcharge.xlsx',
  };
  show = false;
  addRowCount = 0;
  removedDraftRows: TableSurcharge[] = [];

  constructor(
    private surchargeTabService: SurchargeTabService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService
  ) {}

  ngOnInit(): void {
    this.initializeSurcharges();
  }

  initializeSurcharges() {
    this.addRowCount = 0;
    this.surchargeTabService.getSurcharge().subscribe(
      (response) => {
        this.surcharges = response;
        this.setExportData();
        this.show = true;
      },
      (error: Error) => {
        const generic = 'Cannot get surcharge from API';
        this.formulaCardService.openErrorPopup(error.message || generic);
      }
    );
  }

  openConfirmationPopup(surcharges: Surcharge[]) {
    const dialogHeaders = [
      {
        label: 'Surcharge',
        property: 'annualSurcharge',
      },
      {
        label: 'Effective Date',
        property: 'effectiveDate',
      },
      {
        label: 'End Date',
        property: 'endDate',
      },
    ];
    const displayedRows: any[] = [];
    const firstLabel = this.spreadsheetHeaders[0].label;
    surcharges.forEach((surcharge) => {
      surcharge.surchargeRecords.forEach((record: any, index) => {
        const row = [
          { label: firstLabel, value: surcharge.vehicleAge, subvalue: '' },
        ];
        dialogHeaders.forEach((header) => {
          const value = record[header.property];
          row.push({ label: header.label, value, subvalue: '' });
        });
        displayedRows.push(row);
      });
    });
    if (this.removedDraftRows.length) {
      this.removedDraftRows.forEach((removed: any) => {
        const vehicleAge = removed.full.vehicleAge;
        const annualSurcharge = removed.newValue;
        removed = { ...removed, vehicleAge, annualSurcharge };
        const row: Row[] = [
          { label: firstLabel, value: removed.vehicleAge, subvalue: '' },
        ];
        dialogHeaders.forEach((header) => {
          const value = removed[header.property];
          row.push({
            label: header.label,
            value,
            subvalue: '',
            remarks: 'This is removed',
          });
        });
        displayedRows.push(row);
      });
    }
    const uiData: UiData = {
      icon: 'error',
      header: 'Submit your VALUES for review?',
      subheader: 'You will not be able to make amendments to your submission',
      apiPayload: surcharges,
      displayedRows,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Submit',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    const confirmationDialog = this.popupService.openConfirmation(uiData);
    confirmationDialog
      .afterClosed()
      .subscribe(async (data: { data: UiData }) => {
        if (data) {
          await this.submitSurchargesPayload(surcharges);
        }
      });
  }

  async submitSurchargesPayload(
    surcharges: Surcharge[],
    saveAsDraft = false,
    message = 'Amended Formula value(s) successfully submitted for review'
  ) {
    const payload = this.setSurchargeSavePayload(surcharges);
    try {
      let response = saveAsDraft
        ? await this.surchargeTabService.saveSurchargeAsDraft(payload)
        : await this.surchargeTabService.submitSurcharges(payload);
      response.subscribe(
        () => {
          this.formulaCardService.openSuccessPopup(message);
          this.initializeSurcharges();
        },
        (error: Error) =>
          this.formulaCardService.openErrorPopup(
            error.message || 'Failed to submit to the server'
          )
      );
    } catch (error) {
      this.formulaCardService.openErrorPopup('Cannot save surcharge');
    }
  }

  setSurchargeSavePayload(surcharges: Surcharge[]) {
    const values: SavePayloadSurcharge[] = [];
    surcharges.forEach((surcharge: Surcharge) => {
      surcharge.surchargeRecords.forEach((record: SurchargeRecord) => {
        const { effectiveDate, endDate } = record;
        const effectiveDateAsDate = new Date(effectiveDate);
        const endDateAsDate = new Date(endDate);
        const newSurcharge: SavePayloadSurcharge = {
          roadTaxAmtCatKey: surcharge.roadTaxAmtCatKey.toString(),
          surchargeValue: record.annualSurcharge || 0,
          effectiveDate: this.formulaCardService.toString(effectiveDateAsDate),
          endDate: this.formulaCardService.toString(endDateAsDate),
        };
        if (record.transactionId) {
          newSurcharge.transactionId = record.transactionId;
        }
        values.push(newSurcharge);
      });
    });
    return values;
  }

  setExportData() {
    this.exportData.rows = this.surcharges;
  }

  setParent(parentIndex: number) {
    const children = this.surcharges.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      const parent = this.surcharges[parentIndex];
      parent.hasChildren = false;
    }
  }

  async submitDeletedSurcharges() {
    try {
      const removed = this.removedDraftRows;
      const request = await this.surchargeTabService.callDeleteApi(removed);
      request.subscribe(
        () => {
          const message = 'The surcharge draft row has been deleted.';
          this.formulaCardService.openSuccessPopup(message);
          this.removedDraftRows = [];
        },
        (error: Error) => {
          const message = 'surcharge save API error';
          this.formulaCardService.openSuccessPopup(error.message || message);
        }
      );
    } catch (error) {
      this.formulaCardService.openErrorPopup('Cannot delete surcharge.');
    }
  }

  async onRemoveRow(surcharge: TableSurcharge) {
    if (this.formulaCardService.isADraft(surcharge.amendmentStatus)) {
      this.removedDraftRows.push({ ...surcharge, isRemoved: true });
    }
    const indexToBeRemoved = surcharge?.index;
    this.surcharges.splice(indexToBeRemoved, 1);
    this.updateParentIndex();
    const parentIndex = surcharge.parentIndex ?? 0;
    this.setParent(parentIndex);
    await this.submitDeletedSurcharges();
  }

  getNewlyAdded(surcharges: TableSurcharge[]): Surcharge[] {
    const newlyAdded: Surcharge[] = [];
    surcharges.forEach((surcharge: TableSurcharge, parentIndex: number) => {
      if (surcharge.isAParent) {
        const parent = surcharge;
        const hasNewlyAdded = surcharges.find(
          (child: TableSurcharge) =>
            child.parentIndex === parentIndex && child.newlyAdded
        );
        const parentStatus = parent.amendmentStatus;
        const parentIsADraft = this.formulaCardService.isADraft(parentStatus);
        if (hasNewlyAdded || parent.newlyAdded || parentIsADraft) {
          const newSurchargeChildren: any = surcharges.filter(
            (child: TableSurcharge) =>
              child.parentIndex === parentIndex && child.newlyAdded
          );
          if (parent.newlyAdded || parentIsADraft) {
            newSurchargeChildren.push(parent);
          }
          const newSurcharge: Surcharge = {
            vehicleAge: parent.vehicleAge,
            roadTaxAmtCatKey: parent.roadTaxAmtCatKey,
            surchargeRecords: newSurchargeChildren.map(
              (child: TableSurcharge) => {
                const { newValue, effectiveDate, endDate, active } = child;
                const annualSurcharge = newValue;
                const transactionId = child.transactionId;
                return {
                  annualSurcharge,
                  effectiveDate,
                  endDate,
                  active,
                  transactionId,
                };
              }
            ),
          };
          newlyAdded.push(newSurcharge);
        }
      }
    });
    return newlyAdded;
  }

  get newlyAddedRows() {
    const newlyAdded = this.surcharges.find(
      (surcharge: TableSurcharge) =>
        !this.formulaCardService.isADraft(surcharge.amendmentStatus) &&
        surcharge.newlyAdded
    );
    return newlyAdded;
  }

  onProceedButtonClick(surcharges: any) {
    const newlyAdded: Surcharge[] = this.getNewlyAdded(surcharges);
    if (!newlyAdded.length && !this.removedDraftRows.length) {
      const message = 'Add rows or remove draft rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const hasDateOverlap = this.formulaCardService.hasDateOverlap(surcharges);
    if (hasDateOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    this.openConfirmationPopup(newlyAdded);
  }

  onSaveAsDraftClick(surcharges: TableSurcharge[]) {
    if (!this.newlyAddedRows) {
      const message = 'Add rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const hasDateOverlap = this.formulaCardService.hasDateOverlap(surcharges);
    if (hasDateOverlap) {
      return this.formulaCardService.openErrorPopup('Invalid Dates');
    }
    const newlyAdded: Surcharge[] = this.getNewlyAdded(surcharges);
    const successMessage = 'Your values have been saved.';
    this.submitSurchargesPayload(newlyAdded, true, successMessage);
  }

  updateSurcharges(currentFees: any) {
    this.surcharges = currentFees;
  }

  onToggleCollapse(data: any) {
    const { collapsedFee, currentFees } = data;
    this.updateSurcharges(currentFees);
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.surcharges = this.surcharges.map((fee, index) => {
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  updateParentIndex() {
    let currentParentIndex = -1;
    this.surcharges = this.surcharges.map((fee, index) => {
      fee.index = index;
      if (fee.isAParent) {
        currentParentIndex = index;
        return fee;
      }
      fee.parentIndex = currentParentIndex;
      return fee;
    });
  }

  closeAllRows() {
    this.surcharges = this.surcharges.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  expandParent(index: number | null) {
    if (index || index === 0) {
      const parentFee = this.surcharges[index];
      parentFee.isExpanded = true;
      parentFee.hasChildren = true;
    }
  }

  expandChildren(parentSurchargeIndex: number | null) {
    this.surcharges = this.surcharges.map((child) => {
      if (child.parentIndex === parentSurchargeIndex) {
        child.isExpanded = true;
      }
      return child;
    });
  }

  newlyAddedCount: any = {};

  createChildSurcharge(parent: TableSurcharge): TableSurcharge {
    let counter = this.newlyAddedCount[parent.index];
    if (!counter && counter !== 0) {
      counter = 0;
    }
    let lastChildIndex = this.getLastChildIndex(parent.index);
    if (!counter && lastChildIndex !== parent.index && !parent.hasChildren) {
      lastChildIndex--;
    }
    this.newlyAddedCount[parent.index] = counter + 1;
    const { endDate } = { ...this.surcharges[lastChildIndex] };
    return {
      ...parent,
      effectiveDate: this.formulaCardService.addDaysToDate(endDate, 1),
      endDate: this.formulaCardService.addDaysToDate(endDate, 30),
      newValue: parent.annualSurcharge,
      vehicleAge: '',
      annualSurcharge: null,
      isAParent: false,
      isExpanded: true,
      parentIndex: parent?.index || null,
      hasChildren: false,
      newlyAdded: true,
      fromApi: false,
      transactionId: NaN,
    };
  }

  addChildToSurcharges(parentSurcharge: TableSurcharge) {
    if (parentSurcharge.index || parentSurcharge.index === 0) {
      const lastChildIndex = this.getLastChildIndex(parentSurcharge.index);
      const position = lastChildIndex + 1;
      const itemsToBeRemoved = 0;
      const surchargeToBeAdded = this.createChildSurcharge(parentSurcharge);
      this.surcharges.splice(position, itemsToBeRemoved, surchargeToBeAdded);
      this.surcharges = [...this.surcharges];
    }

    this.addRowCount++;
  }

  onAddNewChildSurchargeClick(parentSurcharge: any) {
    if (!parentSurcharge.isExpanded) {
      this.closeAllRows();
    }
    const parentSurchargeIndex = parentSurcharge?.index ?? null;
    this.expandParent(parentSurchargeIndex);
    this.expandChildren(parentSurchargeIndex);
    if (parentSurchargeIndex || parentSurchargeIndex === 0) {
      this.addChildToSurcharges(parentSurcharge);
      this.updateParentIndex();
    }
  }

  onDatepickerChange(changed: TableSurcharge) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.surcharges[index];
    const effectiveDateChanged =
      changedSurcharge.effectiveDate.getTime() !==
      changed.effectiveDate.getTime();
    if (effectiveDateChanged) {
      changedSurcharge.effectiveDate = changed.effectiveDate;
      changedSurcharge.newlyAdded = true;
    }
    const endDateChanged =
      changedSurcharge.endDate.getTime() !== changed.endDate.getTime();
    if (endDateChanged) {
      changedSurcharge.endDate = changed.endDate;
      changedSurcharge.newlyAdded = true;
    }
    this.surcharges = [...this.surcharges];
  }

  onInputChange(changed: TableSurcharge) {
    const index = changed.index ?? 0;
    const changedSurcharge = this.surcharges[index];
    if (changedSurcharge.newValue !== changed.newValue) {
      changedSurcharge.newValue = changed.newValue;
      changedSurcharge.newlyAdded = true;
      this.surcharges = [...this.surcharges];
    }
  }

  getLastChildIndex(parentIndex: number): number {
    const children = this.surcharges.filter(
      (child) => child.parentIndex === parentIndex
    );
    if (!children.length) {
      return parentIndex;
    }
    const lastChild = children[children.length - 1];
    return lastChild.index;
  }
}
