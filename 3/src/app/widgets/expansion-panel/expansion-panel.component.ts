import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss']
})
export class ExpansionPanelComponent implements OnInit {

  panelOpenState = false;

  panels = [
    {
    title: 'panel 1',
    content: 'content 1'
  },
  {
    title: 'panel 2',
    content: 'content 2'
  },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
