import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { Row } from '../../formula-card/popup/interfaces/row';
import { UiData } from '../../formula-card/popup/interfaces/ui-data';
import { PopupService } from '../../formula-card/services/popup.service';
import { LateFeesService } from './late-fees.service';

@Component({
  selector: 'app-late-fees',
  templateUrl: './late-fees.component.html',
  styleUrls: ['./late-fees.component.scss'],
})
export class LateFeesComponent implements OnInit {
  settings = {
    submitButtonText: 'Approve',
    hasSelectOptions: false,
    headerText: 'Late renewal fees for Cars',
    hasPreviewButton: false,
  };
  spreadsheetHeaders: any[] = [
    {
      label: 'Engine Capacity',
      property: 'engineCapacity',
      type: 'collapsible',
      width: '12%',
    },
    {
      label: 'Within 1 month of expiry date',
      property: 'lateFeePeriod1',
      type: 'string',
      width: '10%',
    },
    {
      label: 'Between 1 and 2.5 months',
      property: 'lateFeePeriod2',
      type: 'string',
      width: '10%',
    },
    {
      label: 'More than 2.5 months',
      property: 'lateFeePeriod3',
      type: 'string',
      width: '10%',
    },
    {
      label: 'More than 3 months',
      property: 'lateFeePeriod4',
      type: 'string',
      width: '8%',
    },
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'string',
      width: '10%',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'string',
      width: '10%',
    },
    {
      label: 'For Approving Officer',
      property: 'remarksFieldValue',
      type: 'remarksFieldAndApproveButton',
      width: '20%',
    },
    {
      label: '',
      property: 'approveCheckbox',
      type: 'checkboxFieldAndApproveButton',
      width: '10%',
    },
  ];
  lateFees: any[] = [];
  exportData = {
    headers: this.spreadsheetHeaders,
    rows: this.lateFees,
    filename: 'late-fees.xlsx',
  };
  titleHeader = '';
  subtitleTextGroup: { label: string; value: string }[] = [];
  headers!: any;
  referenceNumber = '';

  constructor(
    private lateFeesService: LateFeesService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.subscribeToRouteParams();
  }

  subscribeToRouteParams() {
    this.route.params.subscribe((params: Params) => {
      this.referenceNumber = params.transactionRefNum;
      this.initializeLateFees();
    });
  }

  initializeLateFees() {
    this.lateFeesService.getRoadTaxLateFee().subscribe(
      (response) => {
        this.lateFees = response.values;
        this.headers = response.headers;
        this.setTitleHeader();
        this.setSubtitleTextGroup();
      },
      (error: Error) => {
        const message = 'Cannot get late fee';
        this.formulaCardService.openErrorPopup(error.message || message);
      }
    );
  }

  setTitleHeader() {
    this.titleHeader = 'Review Amended Road Tax Formula Value(s) REF NO. ';
    this.titleHeader = this.titleHeader + this.referenceNumber;
  }

  setSubtitleTextGroup() {
    this.subtitleTextGroup = [];
    const { requestedBy, submittedOn, propellant } = this.headers;
    this.subtitleTextGroup.push({
      label: 'Amendment Requested by:',
      value: requestedBy,
    });
    this.subtitleTextGroup.push({
      label: 'Submitted on:',
      value: submittedOn,
    });
    this.subtitleTextGroup.push({
      label: 'Propellant / Component Type:',
      value: propellant,
    });
  }

  onSelectionChange(selected: string) {}

  closeAllRows() {
    this.lateFees = this.lateFees.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  openConfirmationPopup(lateFees: any[]) {
    const dialogHeaders = [
      {
        label: 'Within 1 month of expiry date',
        property: 'lateFeePeriod1',
      },
      {
        label: 'Between 1 and 2.5 months',
        property: 'lateFeePeriod2',
      },
      {
        label: 'More than 2.5 months',
        property: 'lateFeePeriod3',
      },
      {
        label: 'More than 3 months',
        property: 'lateFeePeriod4',
      },
      {
        label: 'Effective Date:',
        property: 'effectiveDate',
      },
      {
        label: 'End Date:',
        property: 'endDate',
      },
    ];
    const displayedRows: Row[][] = [];
    lateFees.forEach((lateFee: any) => {
      const { engineCapacity, approved, remarks } = lateFee;
      const row: Row[] = [
        {
          label: 'Engine Capacity (EC):',
          value: engineCapacity,
          subvalue: '',
        },
      ];
      Object.values(dialogHeaders).forEach((header) => {
        let value = lateFee[header.property];
        if (header.property.includes('Date')) {
          value = this.formulaCardService.formatDate(value);
        }
        row.push({ label: header.label, value, subvalue: '' });
      });
      const mark = approved ? 'Yes' : 'No';
      row.push({ label: 'Approved:', value: mark, subvalue: '', remarks });
      displayedRows.push(row);
    });
    const uiData: UiData = {
      icon: 'error',
      header: 'PROCEED TO APPROVE VALUES?',
      subheader:
        'Road Tax Values will proceed to take effect during the effective period once approved',
      displayedRows,
      apiPayload: lateFees,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Approve',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  onApproveButtonClick(lateFees: any) {
    const amendedlateFees = lateFees
      .filter(
        (lateFee: any) => lateFee?.approveCheckbox || lateFee?.remarksFieldValue
      )
      .map((lateFee: any) => ({
        ...lateFee.fullBaseRoadTax,
        newValue: lateFee.newValue,
        approved: !!lateFee?.approveCheckbox,
        remarks: lateFee?.remarksFieldValue || '',
      }));
    if (!amendedlateFees.length) {
      return this.formulaCardService.openErrorPopup('Approve or reject first.');
    }
    const confirmationPopup = this.openConfirmationPopup(amendedlateFees);
    confirmationPopup.afterClosed().subscribe(async (data) => {
      if (data) {
        this.formulaCardService.approveRoadTaxFees(amendedlateFees).subscribe(
          () => {
            this.formulaCardService.openSuccessPopup(
              'FEEDBACK AND APPROVAL(S) HAVE BEEN SUBMITTED SUCCESSFULLY'
            );
            this.initializeLateFees();
          },
          (error: Error) =>
            this.formulaCardService.openErrorPopup(error.message)
        );
      }
    });
  }

  onToggleCollapse(data: any) {
    const { collapsedFee } = data;
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.lateFees = this.lateFees.map((fee, index) => {
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  updateRemarksFieldAndApproveButton(
    index: number,
    remarksFieldValue: string,
    approveCheckbox: boolean
  ) {
    const changedlateFee = this.lateFees[index];
    changedlateFee.remarksFieldValue = remarksFieldValue;
    changedlateFee.approveCheckbox = approveCheckbox;
  }

  onRemarksFieldChange(changed: any) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.updateRemarksFieldAndApproveButton(
      index,
      remarksFieldValue,
      approveCheckbox
    );
  }

  onApproveCheckboxChange(changed: any) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.updateRemarksFieldAndApproveButton(
      index,
      remarksFieldValue,
      approveCheckbox
    );
  }
}
