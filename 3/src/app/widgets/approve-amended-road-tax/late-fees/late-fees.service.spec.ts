import { TestBed } from '@angular/core/testing';

import { LateFeesService } from './late-fees.service';

describe('LateFeesService', () => {
  let service: LateFeesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LateFeesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
