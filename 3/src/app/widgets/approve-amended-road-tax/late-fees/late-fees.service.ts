import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { LateFeesTabService } from '../../late-fees-tab/late-fees-tab.service';

interface TableLateFee {
  engineCapacity: string;
  newValue: string | null;
  effectiveDate: string;
  endDate: string;
  active: boolean;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index?: number;
  newlyAdded: boolean;
  fullBaseRoadTax: any;
}

interface LateFeeRecord {
  transactionId: number;
  lateFeePeriod1: number;
  lateFeePeriod2: number;
  lateFeePeriod3: number;
  lateFeePeriod4: number;
  effectiveDate: string;
  endDate: string;
  active: boolean;
}

interface LateFee {
  roadTaxAmtCatKey: number;
  engineCapacity: string;
  lateFeeRecords: LateFeeRecord[];
}

interface LateFeeResponse {
  headers: any;
  RoadTaxLateFee: LateFee[];
}

@Injectable({
  providedIn: 'root',
})
export class LateFeesService {
  constructor(
    private formulaCardService: FormulaCardService,
    private lateFeeTabService: LateFeesTabService
  ) {}

  formatLateFee(data: LateFeeResponse): TableLateFee[] {
    const baseRoadTaxFees: TableLateFee[] = [];
    let indexCounter = 0;
    let parentIndexCounter = 0;
    let noActiveLateFee = false;
    data.RoadTaxLateFee.forEach((lateFee: LateFee) => {
      const { roadTaxAmtCatKey, engineCapacity, lateFeeRecords } = lateFee;
      noActiveLateFee = false;
      let activeLateFee = lateFeeRecords.filter(
        (lateFeeRecord: LateFeeRecord) => lateFeeRecord.active
      )[0];
      if (!activeLateFee) {
        if (!lateFeeRecords[0]) {
          return;
        }
        activeLateFee = lateFeeRecords[0];
        noActiveLateFee = true;
      }
      const { effectiveDate, endDate } = activeLateFee;
      indexCounter++;
      parentIndexCounter = indexCounter;
      const parentBaseRoadTax = {
        engineCapacity,
        ...activeLateFee,
        newValue: ``,
        effectiveDate: this.formulaCardService.toString(
          this.formulaCardService.formatDate(effectiveDate)
        ),
        endDate: this.formulaCardService.toString(
          this.formulaCardService.formatDate(endDate)
        ),
        remarksFieldValue: '',
        approveCheckbox: false,
        isAParent: true,
        isExpanded: false,
        parentIndex: null,
        hasChildren: lateFeeRecords.length > 1,
        newlyAdded: false,
        readOnly: true,
        fromApi: true,
        index: indexCounter,
        fullBaseRoadTax: {
          engineCapacity,
          roadTaxAmtCatKey,
          ...activeLateFee,
        },
      };
      baseRoadTaxFees.push(parentBaseRoadTax);
      const inactiveLateFee = lateFeeRecords.filter(
        (lateFeeRecord: LateFeeRecord) => !lateFeeRecord.active
      );
      if (!inactiveLateFee.length) {
        return;
      }
      inactiveLateFee.forEach((child: LateFeeRecord, childIndex: number) => {
        if (noActiveLateFee && !childIndex) {
          return;
        }
        indexCounter++;
        const formattedEffectiveDate = this.formulaCardService.toString(
          this.formulaCardService.formatDate(child.effectiveDate)
        );
        const formattedEndDate = this.formulaCardService.toString(
          this.formulaCardService.formatDate(child.endDate)
        );
        const chilBaseRoadTaxFee = {
          ...child,
          engineCapacity: '',
          active: child.active || false,
          baseAmt: '',
          newValue: ``,
          effectiveDate: formattedEffectiveDate,
          endDate: formattedEndDate,
          remarksFieldValue: '',
          approveCheckbox: false,
          isAParent: false,
          isExpanded: false,
          parentIndex: parentIndexCounter - 1,
          hasChildren: false,
          newlyAdded: false,
          readOnly: true,
          fromApi: true,
          index: indexCounter,
          fullBaseRoadTax: {
            engineCapacity,
            roadTaxAmtCatKey,
            ...child,
          },
        };
        baseRoadTaxFees.push(chilBaseRoadTaxFee);
      });
    });
    return baseRoadTaxFees;
  }

  getRoadTaxLateFee(): Observable<any> {
    const lateFeeResponse: Observable<any> =
      this.lateFeeTabService.getLateFeeResponse();
    return lateFeeResponse.pipe(
      map((response: LateFeeResponse) => {
        const hasLateFee = response.RoadTaxLateFee.length;
        const lateFee = {
          headers: {
            referenceNumber: '20190808100445732637',
            requestedBy: 'John Lim',
            submittedOn: '7 Oct 2021 19:18',
            propellant: 'Late Fee',
          },
          values: this.formatLateFee(response),
        };
        return hasLateFee ? lateFee : [];
      })
    );
  }
}
