import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LateFeesComponent } from './late-fees.component';

const routes: Routes = [
  {
    path: '',
    component: LateFeesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LateFeesRoutingModule {}
