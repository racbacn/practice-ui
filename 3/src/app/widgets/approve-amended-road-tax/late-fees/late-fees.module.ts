import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LateFeesComponent } from './late-fees.component';
import { FormulaCardContextualModule } from '../../formula-card/formula-card-contextual/formula-card-contextual.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TitleHeaderModule } from '../widgets/title-header/title-header.module';
import { SubtitleTextgroupModule } from '../widgets/subtitle-textgroup/subtitle-textgroup.module';
import { LateFeesRoutingModule } from './late-fees-routing.module';

@NgModule({
  declarations: [LateFeesComponent],
  imports: [
    CommonModule,
    FormulaCardContextualModule,
    FlexLayoutModule,
    TitleHeaderModule,
    SubtitleTextgroupModule,
    LateFeesRoutingModule,
  ],
})
export class LateFeesModule {}
