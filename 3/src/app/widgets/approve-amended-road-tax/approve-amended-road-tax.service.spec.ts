import { TestBed } from '@angular/core/testing';

import { ApproveAmendedRoadTaxService } from './approve-amended-road-tax.service';

describe('ApproveAmendedRoadTaxService', () => {
  let service: ApproveAmendedRoadTaxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApproveAmendedRoadTaxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
