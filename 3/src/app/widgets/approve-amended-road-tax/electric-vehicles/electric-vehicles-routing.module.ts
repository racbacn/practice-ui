import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ElectricVehiclesComponent } from './electric-vehicles.component';

const routes: Routes = [{ path: '', component: ElectricVehiclesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElectricVehiclesRoutingModule {}
