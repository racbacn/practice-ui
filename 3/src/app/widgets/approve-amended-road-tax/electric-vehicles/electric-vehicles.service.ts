import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { baseRoadTax } from 'src/app/shared/constants/application.constants';
import { environment } from 'src/environments/environment';
import {
  EvMinusValues,
  FormulaCardService,
} from '../../formula-card/formula-card.service';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';

interface RoadTaxRecord {
  transactionId: number;
  baseAmt: number;
  ecMultiplier: number;
  discount: number;
  effectiveDate: string;
  endDate: string;
  active: boolean;
  amendmentStatus: string;
}

export interface Headers {
  requestedBy: string;
  submittedOn: string;
  propellant: string;
}

interface BaseRoadTaxFee {
  roadTaxAmtCatKey: number;
  engineCapacity: EvMinusValues;
  roadTaxRecords: RoadTaxRecord[];
}

interface BaseFeeResponse {
  BaseRoadTaxFee: BaseRoadTaxFee[];
}

interface Rebate {
  effectiveDate: string;
  vehicleRegistrationFrom: string;
  vehicleRegistrationTo: string;
  transactionId: number;
  rebateName: string;
  rebateTypeKey: number;
  rebateType: string;
  rebateAmount: number;
  endDate: string;
  active: boolean;
  amendmentStatus: string;
}

interface RebateResponse {
  RebateList: Rebate[];
}

interface AdditionalCharge {
  effectiveDate: string;
  vehicleRegistrationFrom: string;
  vehicleRegistrationTo: string;
  transactionId: number;
  addChargeName: string;
  addChargeTypeKey: number;
  addChargeType: string;
  addChargeAmount: number;
  endDate: string;
  active: boolean;
  amendmentStatus: string;
}

interface AddChargeResponse {
  AdditionalChargeList: AdditionalCharge[];
}

export type Category = 'baseFee' | 'rebate' | 'addCharge';

@Injectable({
  providedIn: 'root',
})
export class ElectricVehiclesService {
  private SERVER_URL = environment.serverUrl;
  private headers: Headers = {
    requestedBy: 'John Lim',
    submittedOn: '7 Oct 2021 19:18',
    propellant: 'Electric',
  };
  BASE_FEE: Category = 'baseFee';
  REBATE: Category = 'rebate';
  ADD_CHARGE: Category = 'addCharge';

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  setParams(valueCategory: Category): HttpParams {
    const tabActive = 3;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', tabActive);
    params = params.append('userId', userId);
    params = params.append('valueCategory', valueCategory);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    params = params.append('userTypeKey', userTypeKey);
    return params;
  }

  get emptyResponse() {
    return {
      headers: { requestedBy: '', submittedOn: '', propellant: '' },
      values: [],
    };
  }

  formatDate(date: string) {
    if (!date) {
      return '';
    }
    return this.formulaCardService.toDateToString(date);
  }

  getActiveRoadTax(activeRecord: RoadTaxRecord, baseFee: BaseRoadTaxFee) {
    const { engineCapacity, roadTaxAmtCatKey } = baseFee;
    const { baseAmt, ecMultiplier, discount } = activeRecord;
    const full = { engineCapacity, roadTaxAmtCatKey, ...activeRecord };
    const formula = this.formulaCardService.getEvFormula(
      baseAmt,
      ecMultiplier,
      discount,
      engineCapacity
    );
    return {
      ...activeRecord,
      engineCapacityDisplay: engineCapacity,
      engineCapacity,
      roadTaxAmtCatKey,
      active: !!activeRecord.active,
      baseAmt: `$${activeRecord.baseAmt} x ${activeRecord.discount}`,
      newValue: formula,
      sixMonthlyRoadTax: formula,
      effectiveDate: this.formatDate(activeRecord.effectiveDate),
      endDate: this.formatDate(activeRecord.endDate),
      full,
    };
  }

  getInactiveRoadTaxChild(
    activeRecord: RoadTaxRecord,
    baseFee: BaseRoadTaxFee,
    child: RoadTaxRecord
  ) {
    const { engineCapacity, roadTaxAmtCatKey } = baseFee;
    const { effectiveDate, endDate, baseAmt, ecMultiplier, discount } = child;
    const full = { engineCapacity, roadTaxAmtCatKey, activeRecord, ...child };
    const formula = this.formulaCardService.getEvFormula(
      baseAmt,
      ecMultiplier,
      discount,
      engineCapacity
    );
    return {
      ...child,
      engineCapacityDisplay: engineCapacity,
      engineCapacity: '',
      roadTaxAmtCatKey,
      active: !!child.active,
      baseAmt: '',
      newValue: formula,
      sixMonthlyRoadTax: '',
      effectiveDate: this.formatDate(effectiveDate),
      endDate: this.formatDate(endDate),
      full,
    };
  }

  getTableProperties(
    isAParent: boolean,
    index: number,
    parentIndex: number | null = null,
    hasChildren: boolean = false
  ) {
    return {
      isAParent,
      hasChildren,
      index,
      parentIndex,
      remarksFieldValue: '',
      approveCheckbox: false,
      isExpanded: false,
      newlyAdded: false,
      readOnly: true,
      fromApi: true,
    };
  }

  setBaseFee(data: BaseRoadTaxFee[]): any[] {
    const tableFees: any[] = [];
    let index = -1;
    let parentIndex = -1;
    let noActiveFee = false;
    data.forEach((fee: BaseRoadTaxFee) => {
      let activeFee!: RoadTaxRecord;
      const inactiveFees: RoadTaxRecord[] = [];
      noActiveFee = false;
      fee.roadTaxRecords.forEach((record: RoadTaxRecord) => {
        if (record.active && !activeFee) {
          activeFee = record;
          return;
        }
        inactiveFees.push(record);
      });
      if (!activeFee) {
        if (!fee.roadTaxRecords[0]) {
          return;
        }
        activeFee = fee.roadTaxRecords[0];
        noActiveFee = true;
      }
      index++;
      parentIndex = index;
      const hasChildren = fee.roadTaxRecords.length > 1;
      const parentTableFee = {
        ...this.getActiveRoadTax(activeFee, fee),
        ...this.getTableProperties(true, index, null, hasChildren),
        hasChildren: fee.roadTaxRecords.length > 1,
      };
      tableFees.push(parentTableFee);
      if (!inactiveFees.length) {
        return;
      }
      inactiveFees.forEach((child: RoadTaxRecord, childIndex: number) => {
        if (noActiveFee && !childIndex) {
          return;
        }
        index++;
        const chilBaseRoadTaxFee = {
          ...this.getInactiveRoadTaxChild(activeFee, fee, child),
          ...this.getTableProperties(false, index, parentIndex),
        };
        tableFees.push(chilBaseRoadTaxFee);
      });
    });
    return tableFees;
  }

  setNoChild(data: Rebate[] | AdditionalCharge[]): any[] {
    return data.map((record: any, index: number) => {
      const { vehicleRegistrationFrom, vehicleRegistrationTo } = record;
      const { effectiveDate, endDate } = record;
      return {
        ...record,
        ...this.getTableProperties(true, index),
        effectiveDate: this.formatDate(effectiveDate),
        endDate: this.formatDate(endDate),
        vehicleRegistrationFrom: this.formatDate(vehicleRegistrationFrom),
        vehicleRegistrationTo: this.formatDate(vehicleRegistrationTo),
        amendmentStatus: record.amendmentStatus,
      };
    });
  }

  getAllElectricVehicles(): Observable<any[]> {
    const apiUrl = this.SERVER_URL + baseRoadTax;
    const headers = this.headers;
    const baseFee = this.http
      .get<BaseFeeResponse>(apiUrl, { params: this.setParams(this.BASE_FEE) })
      .pipe(
        map((response: BaseFeeResponse) =>
          response.BaseRoadTaxFee.length
            ? { headers, values: this.setBaseFee(response.BaseRoadTaxFee) }
            : this.emptyResponse
        )
      );
    const rebate = this.http
      .get<RebateResponse>(apiUrl, { params: this.setParams(this.REBATE) })
      .pipe(
        map((response: RebateResponse) =>
          response.RebateList.length
            ? { headers, values: this.setNoChild(response.RebateList) }
            : this.emptyResponse
        )
      );
    const addCharge = this.http
      .get<AddChargeResponse>(apiUrl, {
        params: this.setParams(this.ADD_CHARGE),
      })
      .pipe(
        map((response: AddChargeResponse) =>
          response.AdditionalChargeList.length
            ? {
                headers,
                values: this.setNoChild(response.AdditionalChargeList),
              }
            : this.emptyResponse
        )
      );
    return forkJoin([baseFee, rebate, addCharge]);
  }

  getOverpaymentReport(levels: string, ids: string) {
    const levelKey = 'affectedPowerRatingLevels';
    const params = this.formulaCardService.getOverpaymentReportUrlParams(
      levelKey,
      levels,
      ids
    );
    const endpoint = 'electric';
    return this.approveAmendedRoadTaxService.getOverpaymentReport(
      params,
      endpoint
    );
  }
}
