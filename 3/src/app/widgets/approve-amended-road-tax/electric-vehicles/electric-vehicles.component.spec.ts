import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricVehiclesComponent } from './electric-vehicles.component';

describe('ElectricVehiclesComponent', () => {
  let component: ElectricVehiclesComponent;
  let fixture: ComponentFixture<ElectricVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
