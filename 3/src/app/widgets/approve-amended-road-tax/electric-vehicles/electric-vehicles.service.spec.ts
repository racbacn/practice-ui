import { TestBed } from '@angular/core/testing';

import { ElectricVehiclesService } from './electric-vehicles.service';

describe('ElectricVehiclesService', () => {
  let service: ElectricVehiclesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElectricVehiclesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
