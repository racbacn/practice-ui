import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DieselVehiclesComponent } from './diesel-vehicles.component';

describe('DieselVehiclesComponent', () => {
  let component: DieselVehiclesComponent;
  let fixture: ComponentFixture<DieselVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DieselVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DieselVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
