import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DieselVehiclesComponent } from './diesel-vehicles.component';

const routes: Routes = [{ component: DieselVehiclesComponent, path: '' }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DieselVehiclesRoutingModule {}
