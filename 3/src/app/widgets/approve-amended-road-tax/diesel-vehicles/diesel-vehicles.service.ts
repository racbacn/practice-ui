import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';
import { TableDieselVehicle } from './interfaces/table-diesel-vehicle';

interface SpecialTax {
  active: boolean;
  effectiveDate: string;
  endDate: string;
  amendmentStatus: string;
  amount: number;
  minPaymentFee: number;
  transactionId: number;
  full: any;
  powerRating: string;
  roadTaxAmtCatKey: number;
  specialTaxChargeType: string;
}

interface RoadTaxSpecialTax {
  powerRating: string;
  roadTaxAmtCatKey: number;
  specialTax: SpecialTax[];
}

interface SpecialTaxResponse {
  RoadTaxSpecialTax: RoadTaxSpecialTax[];
}

interface Rebate {
  active: boolean;
  amendmentStatus: string;
  effectiveDate: string;
  endDate: string;
  rebateAmount: number;
  rebateName: string;
  rebateType: string;
  rebateTypeKey: number;
  transactionId: number;
  vehicleRegistrationFrom: string;
  vehicleRegistrationTo: string;
}

interface AdditionalCharge {
  active: boolean;
  addChargeAmount: number;
  addChargeName: string;
  addChargeType: string;
  addChargeTypeKey: number;
  amendmentStatus: string;
  effectiveDate: string;
  endDate: string;
  transactionId: number;
  vehicleRegistrationFrom: string;
  vehicleRegistrationTo: string;
}

@Injectable({
  providedIn: 'root',
})
export class DieselVehiclesService {
  private SERVER_URL = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  getParentProperties(childrenLength: number, index: number) {
    return {
      remarksFieldValue: '',
      approveCheckbox: false,
      isAParent: true,
      isExpanded: false,
      parentIndex: null,
      hasChildren: childrenLength > 0,
      newlyAdded: false,
      readOnly: true,
      fromApi: true,
      index,
    };
  }

  private getActiveFee(tax: SpecialTax) {
    const { active, effectiveDate, endDate, transactionId } = tax;
    const { amendmentStatus, amount, minPaymentFee, specialTaxChargeType } =
      tax;
    return {
      amendmentStatus,
      specialTaxChargeType,
      amount,
      transactionId,
      minPaymentFee,
      active,
      effectiveDate: this.formulaCardService.toDateToString(effectiveDate),
      endDate: this.formulaCardService.toDateToString(endDate),
      full: tax,
    };
  }

  private getInactiveFee(activeFee: SpecialTax, child: SpecialTax) {
    const { amendmentStatus, amount, minPaymentFee, transactionId } = child;
    const { specialTaxChargeType, effectiveDate, endDate } = child;
    return {
      amendmentStatus,
      specialTaxChargeType,
      amount,
      transactionId,
      minPaymentFee,
      active: !!child.active,
      effectiveDate: this.formulaCardService.toDateToString(effectiveDate),
      endDate: this.formulaCardService.toDateToString(endDate),
      full: { activeFee, ...child },
    };
  }

  getChildProperties(
    formattedEndDate: string,
    parentIndex: number,
    index: number
  ) {
    return {
      remarksFieldValue: '',
      approveCheckbox: false,
      isAParent: false,
      isExpanded: false,
      parentIndex,
      hasChildren: false,
      newlyAdded: false,
      readOnly: true,
      fromApi: true,
      index,
    };
  }

  private formatSpecialTax(data: SpecialTaxResponse): TableDieselVehicle[] {
    const tableFees: TableDieselVehicle[] = [];
    let index = -1;
    let parentIndex = -1;
    let noActiveFee = false;
    data.RoadTaxSpecialTax.forEach((dieselVehicles: RoadTaxSpecialTax) => {
      index++;
      parentIndex = index;
      const { powerRating, roadTaxAmtCatKey, specialTax } = dieselVehicles;
      let activeFee!: SpecialTax;
      const inactiveFee: SpecialTax[] = [];
      noActiveFee = false;
      specialTax.forEach((tax: SpecialTax) => {
        if (tax.hasOwnProperty('active') && tax?.active && !activeFee) {
          activeFee = tax;
          return;
        }
        inactiveFee.push(tax);
      });
      if (!activeFee) {
        if (!specialTax[0]) {
          return;
        }
        activeFee = specialTax[0];
        noActiveFee = true;
      }
      const active = {
        ...this.getActiveFee(activeFee),
        ...this.getParentProperties(inactiveFee.length, index),
        powerRating,
        roadTaxAmtCatKey,
        hasChildren: dieselVehicles.specialTax.length > 1,
      };
      tableFees.push(active);
      if (!inactiveFee.length) {
        return;
      }
      inactiveFee.forEach((child: SpecialTax, childIndex: number) => {
        if (noActiveFee && !childIndex) {
          return;
        }
        index++;
        const endDate = this.formulaCardService.toDateToString(child.endDate);
        const inactiveFee = {
          ...this.getInactiveFee(activeFee, child),
          ...this.getChildProperties(endDate, parentIndex, index),
          powerRatingDisplay: powerRating,
          roadTaxAmtCatKey,
        };
        tableFees.push(inactiveFee);
      });
    });
    return tableFees;
  }

  private formatNoChildFees(data: AdditionalCharge[] | Rebate[]) {
    const tableFees: any[] = [];
    data.forEach((record: any, index: number) => {
      const tableFee = {
        ...record,
        ...this.getParentProperties(0, index),
        vehicleRegistrationFrom: this.formulaCardService.toDateToString(
          record.vehicleRegistrationFrom
        ),
        vehicleRegistrationTo: this.formulaCardService.toDateToString(
          record.vehicleRegistrationTo
        ),
        effectiveDate: this.formulaCardService.toDateToString(
          record.effectiveDate
        ),
        endDate: this.formulaCardService.toDateToString(record.endDate),
        amendmentStatus: record.amendmentStatus,
      };
      tableFees.push(tableFee);
    });
    return tableFees;
  }

  getSubscription(valueCategory: string): Observable<any> {
    const endpoint = '/manage-roadtax-service/get/RoadTaxValues';
    const apiUrl = this.SERVER_URL + endpoint;
    const dieselVehicleTab = 2;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', dieselVehicleTab);
    params = params.append('userId', userId);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    params = params.append('userTypeKey', userTypeKey);
    params = params.append('valueCategory', valueCategory);
    return this.http.get<any>(apiUrl, { params });
  }

  formatFee(response: any, property: string): any[] {
    let formattedFees: any[] = [];
    if (property === 'RoadTaxSpecialTax') {
      formattedFees = this.formatSpecialTax(response);
    } else if (property === 'RebateList') {
      formattedFees = this.formatNoChildFees(response.RebateList);
    } else if (property === 'AdditionalChargeList') {
      formattedFees = this.formatNoChildFees(response.AdditionalChargeList);
    }
    return formattedFees;
  }

  getValues(category: string, property: string): Observable<any> {
    const subscription: Observable<any> = this.getSubscription(category);
    return subscription.pipe(
      map((response: any) => {
        const hasLength = response[property]?.length;
        if (hasLength) {
          return {
            headers: {
              requestedBy: 'John Lim',
              submittedOn: '7 Oct 2021 19:18',
              propellant: 'Diesel',
            },
            values: this.formatFee(response, property),
          };
        }
        return {
          headers: { requestedBy: '', submittedOn: '', propellant: '' },
          values: [],
        };
      })
    );
  }

  getAllDieselVehicles() {
    const baseFee = this.getValues('specialTax', 'RoadTaxSpecialTax');
    const rebates = this.getValues('rebate', 'RebateList');
    const addCharge = this.getValues('addCharge', 'AdditionalChargeList');
    const subscriptions = [baseFee, rebates, addCharge];
    return forkJoin(subscriptions);
  }

  getOverpaymentReport(levels: string, ids: string) {
    const levelKey = 'affectedEmissionStandardLevel';
    const params = this.formulaCardService.getOverpaymentReportUrlParams(
      levelKey,
      levels,
      ids
    );
    const endpoint = 'diesel';
    return this.approveAmendedRoadTaxService.getOverpaymentReport(
      params,
      endpoint
    );
  }
}
