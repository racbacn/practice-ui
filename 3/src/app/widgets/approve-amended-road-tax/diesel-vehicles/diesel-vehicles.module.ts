import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DieselVehiclesRoutingModule } from './diesel-vehicles-routing.module';
import { DieselVehiclesComponent } from './diesel-vehicles.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TitleHeaderModule } from '../widgets/title-header/title-header.module';
import { SubtitleTextgroupModule } from '../widgets/subtitle-textgroup/subtitle-textgroup.module';
import { FormulaCardSelectModule } from '../../formula-card/formula-card-select/formula-card-select.module';
import { FormulaCardLegendModule } from '../../formula-card/formula-card-legend/formula-card-legend.module';
import { HeaderTableDividerModule } from '../../formula-card/header-table-divider/header-table-divider.module';
import { ContextualTableModule } from '../../formula-card/contextual-table/contextual-table.module';
import { ViewButtonModule } from '../../overpayment-report/view-button/view-button.module';
import { PopupModule } from '../../overpayment-report/popup/popup.module';

@NgModule({
  declarations: [DieselVehiclesComponent],
  imports: [
    CommonModule,
    DieselVehiclesRoutingModule,
    CommonModule,
    FlexLayoutModule,
    TitleHeaderModule,
    SubtitleTextgroupModule,
    FormulaCardSelectModule,
    FormulaCardLegendModule,
    HeaderTableDividerModule,
    ContextualTableModule,
    ViewButtonModule,
    PopupModule,
  ],
  exports: [DieselVehiclesComponent],
})
export class DieselVehiclesModule {}
