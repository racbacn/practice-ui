export interface FormattedResponseHeaders {
  requestedBy: string;
  submittedOn: string;
  propellant: string;
}
