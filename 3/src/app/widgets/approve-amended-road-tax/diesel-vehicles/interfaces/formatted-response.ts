import { FormattedResponseHeaders } from './formatted-response-headers';
import { TableDieselVehicle } from './table-diesel-vehicle';

export interface FormattedResponse {
  headers: FormattedResponseHeaders;
  values: TableDieselVehicle[];
}
