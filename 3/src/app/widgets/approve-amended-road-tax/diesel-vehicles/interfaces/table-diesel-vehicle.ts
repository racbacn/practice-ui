export interface TableDieselVehicle {
  amendmentStatus: string;
  amount: number;
  minPaymentFee: number;
  transactionId: number;
  effectiveDate: string;
  endDate: string;
  active: boolean;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index: number;
  newlyAdded: boolean;
  fromApi: boolean;
  full: any;
  remarksFieldValue: string;
  approveCheckbox: boolean;
  approved?: boolean;
  remarks?: string;
}
