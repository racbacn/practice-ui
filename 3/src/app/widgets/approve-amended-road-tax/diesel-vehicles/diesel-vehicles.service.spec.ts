import { TestBed } from '@angular/core/testing';

import { DieselVehiclesService } from './diesel-vehicles.service';

describe('DieselVehiclesService', () => {
  let service: DieselVehiclesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DieselVehiclesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
