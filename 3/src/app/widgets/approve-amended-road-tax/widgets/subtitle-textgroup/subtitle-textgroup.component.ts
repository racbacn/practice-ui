import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-subtitle-textgroup',
  templateUrl: './subtitle-textgroup.component.html',
  styleUrls: ['./subtitle-textgroup.component.scss'],
})
export class SubtitleTextgroupComponent implements OnInit {
  @Input() subtitleTextgroup: { label: string; value: string }[] = [];

  constructor() {}

  ngOnInit(): void {}
}
