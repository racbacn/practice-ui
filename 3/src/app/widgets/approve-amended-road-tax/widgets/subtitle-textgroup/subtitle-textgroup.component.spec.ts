import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtitleTextgroupComponent } from './subtitle-textgroup.component';

describe('SubtitleTextgroupComponent', () => {
  let component: SubtitleTextgroupComponent;
  let fixture: ComponentFixture<SubtitleTextgroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubtitleTextgroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtitleTextgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
