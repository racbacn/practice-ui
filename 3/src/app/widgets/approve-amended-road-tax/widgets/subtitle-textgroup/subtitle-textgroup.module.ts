import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubtitleTextgroupComponent } from './subtitle-textgroup.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [SubtitleTextgroupComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [SubtitleTextgroupComponent],
})
export class SubtitleTextgroupModule {}
