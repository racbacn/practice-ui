import { TestBed } from '@angular/core/testing';

import { PetrolVehiclesService } from './petrol-vehicles.service';

describe('PetrolVehiclesService', () => {
  let service: PetrolVehiclesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PetrolVehiclesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
