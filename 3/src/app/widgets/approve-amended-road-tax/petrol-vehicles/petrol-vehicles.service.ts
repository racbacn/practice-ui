import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {
  FormulaCardService,
  PetrolMinusValues,
} from '../../formula-card/formula-card.service';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';

interface TableBaseRoadTaxFee {
  engineCapacity: string;
  newValue: string | null;
  effectiveDate: string;
  endDate: string;
  active: boolean;
  isAParent: boolean;
  isExpanded: boolean;
  parentIndex: number | null;
  hasChildren: boolean;
  index?: number;
  newlyAdded: boolean;
  fullBaseRoadTax: any;
}

interface RoadTaxRecord {
  transactionId: number;
  baseAmt: number;
  discount: number;
  effectiveDate: string;
  endDate: string;
  active?: boolean;
  ecMultiplier?: number;
  amendmentStatus: string;
}

interface BaseRoadTaxFee {
  roadTaxAmtCatKey: number;
  engineCapacity: PetrolMinusValues;
  roadTaxRecords: RoadTaxRecord[];
}

interface RoadTaxFeeResponse {
  headers: any;
  BaseRoadTaxFee: BaseRoadTaxFee[];
}

@Injectable({
  providedIn: 'root',
})
export class PetrolVehiclesService {
  private SERVER_URL = environment.serverUrl;

  constructor(
    private http: HttpClient,
    private formulaCardService: FormulaCardService,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  getActiveRoadTax(
    activeBaseRoadTaxFee: RoadTaxRecord,
    baseRoadTaxFee: BaseRoadTaxFee
  ) {
    const { discount, ecMultiplier, baseAmt, active } = activeBaseRoadTaxFee;
    const { effectiveDate, endDate, transactionId, amendmentStatus } =
      activeBaseRoadTaxFee;
    const { roadTaxAmtCatKey, engineCapacity } = baseRoadTaxFee;
    const full = { engineCapacity, roadTaxAmtCatKey, ...activeBaseRoadTaxFee };
    const formula = this.formulaCardService.getPetrolFormula(
      baseAmt,
      ecMultiplier,
      discount,
      engineCapacity
    );
    return {
      amendmentStatus,
      roadTaxAmtCatKey,
      transactionId,
      engineCapacity,
      active: active || false,
      baseAmt: `$${baseAmt} x ${discount}`,
      newValue: formula,
      sixMonthlyRoadTax: formula,
      effectiveDate: this.formulaCardService.toDateToString(effectiveDate),
      endDate: this.formulaCardService.toDateToString(endDate),
      fullBaseRoadTax: full,
    };
  }

  getInactiveRoadTaxChild(
    activeBaseRoadTaxFee: RoadTaxRecord,
    baseRoadTaxFee: BaseRoadTaxFee,
    child: RoadTaxRecord
  ) {
    const { roadTaxAmtCatKey, engineCapacity } = baseRoadTaxFee;
    const { effectiveDate, endDate, baseAmt, ecMultiplier, discount } = child;
    const { amendmentStatus } = child;
    return {
      amendmentStatus,
      roadTaxAmtCatKey,
      transactionId: child.transactionId,
      engineCapacity: '',
      active: child?.active || false,
      baseAmt: '',
      newValue: this.formulaCardService.getPetrolFormula(
        baseAmt,
        ecMultiplier,
        discount,
        engineCapacity
      ),
      sixMonthlyRoadTax: '',
      effectiveDate: this.formulaCardService.toDateToString(effectiveDate),
      endDate: this.formulaCardService.toDateToString(endDate),
      fullBaseRoadTax: { engineCapacity, roadTaxAmtCatKey, ...child },
    };
  }

  getParentProperties(childrenLength: number, index: number) {
    return {
      remarksFieldValue: '',
      approveCheckbox: false,
      isAParent: true,
      isExpanded: false,
      parentIndex: null,
      hasChildren: childrenLength > 0,
      newlyAdded: false,
      readOnly: true,
      fromApi: true,
      index,
    };
  }

  getChildProperties(
    formattedEndDate: string,
    parentIndex: number,
    index: number
  ) {
    return {
      remarksFieldValue: '',
      approveCheckbox: false,
      isAParent: false,
      isExpanded: false,
      parentIndex,
      hasChildren: false,
      newlyAdded: false,
      readOnly: true,
      fromApi: true,
      index,
    };
  }

  formatBaseRoadTaxFees(data: RoadTaxFeeResponse): TableBaseRoadTaxFee[] {
    const tableFees: TableBaseRoadTaxFee[] = [];
    let index = -1;
    let parentIndex = -1;
    let noActiveFee = false;
    data.BaseRoadTaxFee.forEach((fee: BaseRoadTaxFee) => {
      index++;
      parentIndex = index;
      let activeFee!: RoadTaxRecord;
      const inactive: RoadTaxRecord[] = [];
      noActiveFee = false;
      fee.roadTaxRecords.forEach((record: RoadTaxRecord) => {
        if (record?.active && !activeFee) {
          activeFee = record;
          return;
        }
        inactive.push(record);
      });
      if (!activeFee) {
        if (!fee.roadTaxRecords[0]) {
          return;
        }
        activeFee = fee.roadTaxRecords[0];
        noActiveFee = true;
      }
      const parentBaseRoadTax = {
        ...this.getActiveRoadTax(activeFee, fee),
        ...this.getParentProperties(inactive.length, index),
        hasChildren: fee.roadTaxRecords.length > 1,
      };
      tableFees.push(parentBaseRoadTax);
      if (!inactive.length) {
        return;
      }
      inactive.forEach((child: RoadTaxRecord, childIndex: number) => {
        if (noActiveFee && !childIndex) {
          return;
        }
        index++;
        const endDate = this.formulaCardService.toDateToString(child.endDate);
        const chilBaseRoadTaxFee = {
          ...this.getInactiveRoadTaxChild(activeFee, fee, child),
          ...this.getChildProperties(endDate, parentIndex, index),
        };
        tableFees.push(chilBaseRoadTaxFee);
      });
    });
    return tableFees;
  }

  formatNoChildList(data: any, property: string): any[] {
    const tableFees: any[] = [];
    data[property].forEach((record: any, index: number) => {
      let vehicleRegistrationFrom = record.vehicleRegistrationFrom || '';
      vehicleRegistrationFrom = vehicleRegistrationFrom
        ? this.formulaCardService.toDateToString(vehicleRegistrationFrom)
        : '';
      let vehicleRegistrationTo = record.vehicleRegistrationTo || '';
      vehicleRegistrationTo = vehicleRegistrationTo
        ? this.formulaCardService.toDateToString(vehicleRegistrationTo)
        : '';
      const tableFee = {
        ...record,
        ...this.getParentProperties(0, index),
        effectiveDate: this.formulaCardService.toDateToString(
          record.effectiveDate
        ),
        endDate: this.formulaCardService.toDateToString(record.endDate),
        vehicleRegistrationFrom,
        vehicleRegistrationTo,
        amendmentStatus: record.amendmentStatus,
      };
      tableFees.push(tableFee);
    });
    return tableFees;
  }

  formatFee(data: any, property: string): any[] {
    let formattedFees: any[] = [];
    if (property === 'BaseRoadTaxFee') {
      formattedFees = this.formatBaseRoadTaxFees(data);
    } else if (property === 'RebateList') {
      formattedFees = this.formatNoChildList(data, property);
    } else if (property === 'AdditionalChargeList') {
      formattedFees = this.formatNoChildList(data, property);
    }
    return formattedFees;
  }

  getValues(value: string, property: string): Observable<any> {
    const apiEndpoint = '/manage-roadtax-service/get/RoadTaxValues';
    const apiUrl = this.SERVER_URL + apiEndpoint;
    const tabActive = 1;
    const userId = this.formulaCardService.getUserId();
    const ltaTeamTypeKey = this.formulaCardService.getLtaTeamTypeKey();
    const userTypeKey = this.formulaCardService.getUserTypeKey();
    let params = new HttpParams();
    params = params.append('tabActive', tabActive);
    params = params.append('userId', userId);
    params = params.append('valueCategory', value);
    params = params.append('userTypeKey', userTypeKey);
    params = params.append('ltaTeamTypeKey', ltaTeamTypeKey);
    return this.http.get<any>(apiUrl, { params }).pipe(
      map((response: any) => {
        const hasRoadTaxValues = response[property].length;
        const roadTax = {
          headers: {
            requestedBy: 'John Lim',
            submittedOn: '7 Oct 2021 19:18',
            propellant: 'Petrol',
          },
          values: this.formatFee(response, property),
        };
        return hasRoadTaxValues ? roadTax : [];
      })
    );
  }

  getAllPetrolVehicles() {
    const baseRoadTaxFees = this.getValues('baseFee', 'BaseRoadTaxFee');
    const rebates = this.getValues('rebate', 'RebateList');
    const additionalCharges = this.getValues(
      'addCharge',
      'AdditionalChargeList'
    );
    const subscriptions = [baseRoadTaxFees, rebates, additionalCharges];
    return forkJoin(subscriptions);
  }

  getOverpaymentReport(levels: string, ids: string) {
    const levelKey = 'affectedEngineCapacityLevels';
    const params = this.formulaCardService.getOverpaymentReportUrlParams(
      levelKey,
      levels,
      ids
    );
    const endpoint = 'petrol';
    return this.approveAmendedRoadTaxService.getOverpaymentReport(
      params,
      endpoint
    );
  }
}
