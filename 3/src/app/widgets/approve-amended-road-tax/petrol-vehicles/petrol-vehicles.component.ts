import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { Row } from '../../formula-card/popup/interfaces/row';
import { UiData } from '../../formula-card/popup/interfaces/ui-data';
import { PopupService } from '../../formula-card/services/popup.service';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';
import { PetrolVehiclesService } from './petrol-vehicles.service';

type Selected = 'baseFee' | 'rebate' | 'addCharge';

interface SelectOption {
  label: string;
  value: string;
}

interface Header {
  label: string;
  property: string;
}

@Component({
  selector: 'app-petrol-vehicles',
  templateUrl: './petrol-vehicles.component.html',
  styleUrls: ['./petrol-vehicles.component.scss'],
})
export class PetrolVehiclesComponent implements OnInit {
  selectOptions: SelectOption[] = [
    {
      label: 'Base Road Tax Fee ( Petrol, CNG or Diesel Cars)',
      value: 'baseFee',
    },
    {
      label: 'Road Tax Rebate(s)',
      value: 'rebate',
    },
    {
      label: 'Additional Road Tax Charge(s)',
      value: 'addCharge',
    },
  ];
  commonHeaders = [
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'string',
      width: '10%',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'string',
      width: '10%',
    },
    {
      label: 'For Approving Officer',
      property: 'remarksFieldValue',
      type: 'remarksFieldAndApproveButton',
      width: '15%',
    },
    {
      label: '',
      property: 'approveCheckbox',
      type: 'checkboxFieldAndApproveButton',
      width: '10%',
    },
  ];
  commonPopupHeaders = [
    {
      label: 'Effective Date:',
      property: 'effectiveDate',
    },
    {
      label: 'End Date:',
      property: 'endDate',
    },
  ];
  baseFee: any = {
    headers: [
      {
        label: 'Engine Capacity (EC) in cc',
        property: 'engineCapacity',
        type: 'string',
        width: '15%',
      },
      {
        label: '6-Monthly Road Tax',
        property: 'sixMonthlyRoadTax',
        type: 'collapsible',
        width: '15%',
      },
      {
        label: 'New Value',
        property: 'newValue',
        type: 'string',
        width: '25%',
      },
      ...this.commonHeaders,
    ],
    fees: [],
  };
  rebate = {
    headers: [
      {
        label: 'Rebate Name',
        property: 'rebateName',
        type: 'string',
        width: '15%',
      },
      {
        label: 'Rebate Type',
        property: 'rebateType',
        type: 'string',
        width: '10%',
      },
      {
        label: 'For Vehicles with Registration Date(s) From',
        property: 'vehicleRegistrationFrom',
        type: 'string',
        width: '10%',
      },
      {
        label: 'Till',
        property: 'vehicleRegistrationTo',
        type: 'string',
        width: '10%',
      },
      {
        label: 'Rebate Amount',
        property: 'rebateAmount',
        type: 'string',
        width: '10%',
      },
      ...this.commonHeaders,
    ],
    fees: [],
  };
  addCharge = {
    headers: [
      {
        label: 'Additional Charge Name',
        property: 'addChargeName',
        type: 'string',
        width: '15%',
      },
      {
        label: 'Charge Type',
        property: 'addChargeType',
        type: 'string',
        width: '10%',
      },
      {
        label: 'For Vehicles with Registration Date(s) From',
        property: 'vehicleRegistrationFrom',
        type: 'string',
        width: '10%',
      },
      {
        label: 'Till',
        property: 'vehicleRegistrationTo',
        type: 'string',
        width: '10%',
      },
      {
        label: 'Charge Amount',
        property: 'addChargeAmount',
        type: 'string',
        width: '10%',
      },
      ...this.commonHeaders,
    ],
    fees: [],
  };
  exportData = { headers: [], rows: [], filename: 'petrol-vehicles.xlsx' };
  titleHeader = '';
  subtitleTextGroup: { label: string; value: string }[] = [];
  selected: Selected = 'baseFee';
  show = false;
  referenceNumber = '';

  constructor(
    private petrolVehicleService: PetrolVehiclesService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService,
    private route: ActivatedRoute,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  ngOnInit(): void {
    this.subscribeToRouteParams();
  }

  subscribeToRouteParams() {
    this.route.params.subscribe((params: Params) => {
      this.referenceNumber = params.transactionRefNum;
      this.initializeRoadTaxFees();
    });
  }

  initializeRoadTaxFees() {
    this.petrolVehicleService.getAllPetrolVehicles().subscribe(
      (response: any) => {
        this.baseFee.fees = response[0].values;
        this.rebate.fees = response[1].values;
        this.addCharge.fees = response[2].values;
        this.setTitleHeader(response[0].headers);
        this.setSubtitleTextGroup(response[0].headers);
        this.show = true;
      },
      (error: Error) => {
        this.formulaCardService.openErrorPopup(error.message);
      }
    );
  }

  setTitleHeader(headers: any) {
    this.titleHeader = 'Review Amended Road Tax Formula Value(s) REF NO. ';
    this.titleHeader = this.titleHeader + this.referenceNumber;
  }

  setSubtitleTextGroup(headers: any) {
    this.subtitleTextGroup = [];
    const { requestedBy, submittedOn, propellant } = headers;
    this.subtitleTextGroup.push({
      label: 'Amendment Requested by:',
      value: requestedBy,
    });
    this.subtitleTextGroup.push({
      label: 'Submitted on:',
      value: submittedOn,
    });
    this.subtitleTextGroup.push({
      label: 'Propellant / Component Type:',
      value: propellant,
    });
  }

  onSelectionChange(selectedCategory: Selected) {
    this.selected = selectedCategory;
  }

  update(index: number, remarks: string, approve: boolean, table: Selected) {
    let changeRow = this.baseFee.fees[index];
    if (table === 'rebate') {
      changeRow = this.rebate.fees[index];
    } else if (table === 'addCharge') {
      changeRow = this.addCharge.fees[index];
    }
    changeRow.remarksFieldValue = remarks;
    changeRow.approveCheckbox = approve;
  }

  onRemarksFieldChange(changed: any, table: Selected) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.update(index, remarksFieldValue, approveCheckbox, table);
  }

  onApproveCheckboxChange(changed: any, table: Selected) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.update(index, remarksFieldValue, approveCheckbox, table);
  }

  closeAllRows() {
    this.baseFee.fees = this.baseFee.fees.map((fee: any) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  onToggleCollapse(data: any) {
    const { collapsedFee } = data;
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.baseFee.fees = this.baseFee.fees.map((fee: any, index: number) => {
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  setPopupRows(rowData: { fees: any; headers: Header[] }) {
    const { fees, headers } = rowData;
    const rows: Row[][] = [];
    fees.forEach((fee: any) => {
      const { approved, remarks } = fee;
      const row: Row[] = [];
      Object.values(headers).forEach((header: Header) => {
        let value = fee[header.property];
        if (header.property.includes('Date')) {
          value = this.formulaCardService.formatDate(value);
        }
        row.push({ label: header.label, value, subvalue: '' });
      });
      const mark = approved ? 'Yes' : 'No';
      row.push({ label: 'Approved:', value: mark, subvalue: '', remarks });
      rows.push(row);
    });
    return rows;
  }

  openConfirmationPopup(displayedRows: Row[][]) {
    const uiData: UiData = {
      icon: 'error',
      header: 'PROCEED TO APPROVE VALUES?',
      subheader:
        'Road Tax Values will proceed to take effect during the effective period once approved',
      displayedRows,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Approve',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  getAmendedFees(fees: any) {
    return fees
      .filter((fee: any) => fee?.approveCheckbox || fee?.remarksFieldValue)
      .map((fee: any) => ({
        ...fee,
        newValue: fee.newValue,
        approved: !!fee?.approveCheckbox,
        remarks: fee?.remarksFieldValue || '',
      }));
  }

  getPopoverBaseFeeRows(fees: any[]) {
    const headers = [
      {
        label: 'Engine Capacity (EC):',
        property: 'engineCapacity',
      },
      {
        label: '6-month Base Fee:',
        property: 'newValue',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getAmendedRebateRows(fees: any[]) {
    const headers = [
      {
        label: 'Rebate Name:',
        property: 'rebateName',
      },
      {
        label: 'Rebate Amount:',
        property: 'rebateAmount',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  getAmendedAddChargeRows(fees: any[]) {
    const headers = [
      {
        label: 'Additional Charge Name:',
        property: 'addChargeName',
      },
      {
        label: 'Charge Amount:',
        property: 'addChargeAmount',
      },
      ...this.commonPopupHeaders,
    ];
    const rowData = { fees, headers };
    return this.setPopupRows(rowData);
  }

  onApproveButtonClick(roadTaxFees: any) {
    const baseFees = this.getAmendedFees(this.baseFee.fees);
    const baseFeePopoverRows = this.getPopoverBaseFeeRows(baseFees);
    const rebates = this.getAmendedFees(this.rebate.fees);
    const rebatePopoverRows = this.getAmendedRebateRows(rebates);
    const addCharges = this.getAmendedFees(this.addCharge.fees);
    const addChargePopoverRows = this.getAmendedAddChargeRows(addCharges);
    const popoverRows = [
      ...baseFeePopoverRows,
      ...rebatePopoverRows,
      ...addChargePopoverRows,
    ];
    if (!popoverRows.length) {
      const message = 'Approve or reject rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const confirmationPopup = this.openConfirmationPopup(popoverRows);
    confirmationPopup.afterClosed().subscribe(async (data) => {
      if (data) {
        const allAmendedFees = [...baseFees, ...rebates, ...addCharges];
        this.approveRoadTaxFees(allAmendedFees);
      }
    });
  }

  approveRoadTaxFees(allAmendedFees: any[]) {
    this.formulaCardService.approveRoadTaxFees(allAmendedFees).subscribe(
      (response) => {
        this.formulaCardService.openSuccessPopup(
          'FEEDBACK AND APPROVAL(S) HAVE BEEN SUBMITTED SUCCESSFULLY'
        );
        this.initializeRoadTaxFees();
      },
      (error: Error) => this.formulaCardService.openErrorPopup(error.message)
    );
  }

  onViewOverPaymentReportClick() {
    const fees = this.baseFee.fees;
    const parameters = this.formulaCardService.getOverpaymentParameters(fees);
    const { levels, ids } = parameters;
    if (!levels || !ids) {
      return this.formulaCardService.openErrorPopup(
        'No existing rows for approval'
      );
    }
    this.petrolVehicleService.getOverpaymentReport(levels, ids).subscribe(
      (response) => {
        if (!response.header) {
          return this.formulaCardService.openErrorPopup('Empty rows');
        }
        this.approveAmendedRoadTaxService.openOverpaymentReportPopup(response);
      },
      (error: Error) => {
        const message = error.message || 'Cannot get overpayment report data';
        this.formulaCardService.openErrorPopup(message);
      }
    );
  }
}
