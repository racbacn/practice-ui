import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PetrolVehiclesComponent } from './petrol-vehicles.component';

const routes: Routes = [
  {
    path: '',
    component: PetrolVehiclesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PetrolVehiclesRoutingModule {}
