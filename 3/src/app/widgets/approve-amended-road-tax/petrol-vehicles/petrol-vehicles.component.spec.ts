import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetrolVehiclesComponent } from './petrol-vehicles.component';

describe('PetrolVehiclesComponent', () => {
  let component: PetrolVehiclesComponent;
  let fixture: ComponentFixture<PetrolVehiclesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetrolVehiclesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetrolVehiclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
