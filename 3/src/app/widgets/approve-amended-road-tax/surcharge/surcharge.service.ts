import { Injectable } from '@angular/core';
import { Surcharge } from '../../surcharge-tab/interface/surcharge';
import { TableSurcharge } from '../../surcharge-tab/interface/table-surcharge';
import { SurchargeRecord } from '../../surcharge-tab/interface/surcharge-record';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SurchargeTabService } from '../../surcharge-tab/surcharge-tab.service';
import { HttpParams } from '@angular/common/http';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';

interface SurchargeResponse {
  headers: any;
  RoadTaxSurcharge: Surcharge[];
}

@Injectable({
  providedIn: 'root',
})
export class SurchargeService {
  constructor(
    private formulaCardService: FormulaCardService,
    private surchargeTabService: SurchargeTabService,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  formatSurcharge(data: SurchargeResponse): TableSurcharge[] {
    const surcharges: any[] = [];
    let indexCounter = 0;
    let parentIndexCounter = 0;
    let noActiveSurcharge = false;
    data.RoadTaxSurcharge.forEach((surcharge: Surcharge) => {
      const { vehicleAge, surchargeRecords } = surcharge;
      let activeSurcharge = surcharge.surchargeRecords.filter(
        (surcharge: SurchargeRecord) => {
          if (surcharge.active) {
            noActiveSurcharge = false;
            return true;
          }
          return false;
        }
      )[0];
      if (!activeSurcharge) {
        if (!surchargeRecords[0]) {
          return;
        }
        activeSurcharge = surchargeRecords[0];
        noActiveSurcharge = true;
      }
      const { effectiveDate, endDate, active, annualSurcharge } =
        activeSurcharge;
      indexCounter++;
      parentIndexCounter = indexCounter;
      const parentSurcharge = {
        vehicleAge,
        ...activeSurcharge,
        active: active || false,
        newValue: annualSurcharge,
        effectiveDate: this.formulaCardService.toString(
          this.formulaCardService.formatDate(effectiveDate)
        ),
        endDate: this.formulaCardService.toString(
          this.formulaCardService.formatDate(endDate)
        ),
        remarksFieldValue: '',
        approveCheckbox: false,
        isAParent: true,
        isExpanded: false,
        parentIndex: null,
        hasChildren: surchargeRecords.length > 1,
        newlyAdded: false,
        readOnly: true,
        fromApi: true,
        index: indexCounter,
        fullSurcharge: {
          vehicleAge,
          ...activeSurcharge,
        },
        roadTaxAmtCatKey: surcharge.roadTaxAmtCatKey,
        amendmentStatus: activeSurcharge.amendmentStatus,
      };
      surcharges.push(parentSurcharge);
      const inactiveSurcharge = surcharge.surchargeRecords.filter(
        (surcharge: SurchargeRecord) => !surcharge.active
      );
      if (!inactiveSurcharge.length) {
        return;
      }
      inactiveSurcharge.forEach(
        (child: SurchargeRecord, childIndex: number) => {
          if (noActiveSurcharge && !childIndex) {
            return;
          }
          indexCounter++;
          const formattedEndDate = this.formulaCardService.toString(
            this.formulaCardService.formatDate(child.endDate)
          );
          const chilsurcharge = {
            ...child,
            engineCapacity: '',
            active: child.active || false,
            baseAmt: '',
            newValue: child.annualSurcharge,
            effectiveDate: this.formulaCardService.toString(
              this.formulaCardService.formatDate(child.effectiveDate)
            ),
            endDate: formattedEndDate,
            remarksFieldValue: '',
            approveCheckbox: false,
            isAParent: false,
            isExpanded: false,
            parentIndex: parentIndexCounter - 1,
            hasChildren: false,
            newlyAdded: false,
            readOnly: true,
            fromApi: true,
            index: indexCounter,
            fullSurcharge: {
              vehicleAge,
              ...child,
            },
            roadTaxAmtCatKey: surcharge.roadTaxAmtCatKey,
            amendmentStatus: child.amendmentStatus,
          };
          surcharges.push(chilsurcharge);
        }
      );
    });
    return surcharges;
  }

  getSurcharge(): Observable<any> {
    const surchargeResponse: Observable<any> =
      this.surchargeTabService.getSurchargeResponse();
    return surchargeResponse.pipe(
      map((response: SurchargeResponse) => {
        const hasSurcharge = response.RoadTaxSurcharge.length;
        const surcharge = {
          headers: {
            referenceNumber: '20190808100445732637',
            requestedBy: 'John Lim',
            submittedOn: '7 Oct 2021 19:18',
            propellant: 'Surcharge',
          },
          values: this.formatSurcharge(response),
        };
        return hasSurcharge ? surcharge : [];
      })
    );
  }

  getOverpaymentReport(levels: string, ids: string) {
    const levelKey = 'affectedVehicleAgeForSurcharge';
    const params = this.formulaCardService.getOverpaymentReportUrlParams(
      levelKey,
      levels,
      ids
    );
    const endpoint = 'surcharge';
    return this.approveAmendedRoadTaxService.getOverpaymentReport(
      params,
      endpoint
    );
  }
}
