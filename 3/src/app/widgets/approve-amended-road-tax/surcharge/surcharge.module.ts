import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SurchargeComponent } from './surcharge.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TitleHeaderModule } from '../widgets/title-header/title-header.module';
import { SubtitleTextgroupModule } from '../widgets/subtitle-textgroup/subtitle-textgroup.module';
import { SurchargeRoutingModule } from './surcharge-routing.module';
import { ViewButtonModule } from '../../overpayment-report/view-button/view-button.module';
import { PopupModule } from '../../overpayment-report/popup/popup.module';
import { FormulaCardLegendModule } from '../../formula-card/formula-card-legend/formula-card-legend.module';
import { HeaderTableDividerModule } from '../../formula-card/header-table-divider/header-table-divider.module';
import { ContextualTableModule } from '../../formula-card/contextual-table/contextual-table.module';

@NgModule({
  declarations: [SurchargeComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    TitleHeaderModule,
    SubtitleTextgroupModule,
    SurchargeRoutingModule,
    FormulaCardLegendModule,
    HeaderTableDividerModule,
    ContextualTableModule,
    ViewButtonModule,
    PopupModule,
  ],
})
export class SurchargeModule {}
