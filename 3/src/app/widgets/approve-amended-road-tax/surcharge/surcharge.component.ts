import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormulaCardService } from '../../formula-card/formula-card.service';
import { Row } from '../../formula-card/popup/interfaces/row';
import { UiData } from '../../formula-card/popup/interfaces/ui-data';
import { PopupService } from '../../formula-card/services/popup.service';
import { ApproveAmendedRoadTaxService } from '../approve-amended-road-tax.service';
import { SurchargeService } from './surcharge.service';

@Component({
  selector: 'app-surcharge',
  templateUrl: './surcharge.component.html',
  styleUrls: ['./surcharge.component.scss'],
})
export class SurchargeComponent implements OnInit {
  settings = {
    submitButtonText: 'Approve',
    hasSelectOptions: false,
    headerText: 'Road tax surcharge for vehicles over 10 years',
    hasPreviewButton: false,
  };
  spreadsheetHeaders: any[] = [
    {
      label: 'Age of vehicle ',
      property: 'vehicleAge',
      type: 'string',
    },
    {
      label: 'Annual road tax surcharge',
      property: 'annualSurcharge',
      type: 'collapsible',
    },
    {
      label: 'New Value',
      property: 'newValue',
      type: 'string',
    },
    {
      label: 'Effective Date',
      property: 'effectiveDate',
      type: 'string',
    },
    {
      label: 'End Date',
      property: 'endDate',
      type: 'string',
    },
    {
      label: 'For Approving Officer',
      property: 'remarksFieldValue',
      type: 'remarksFieldAndApproveButton',
    },
    {
      label: '',
      property: 'approveCheckbox',
      type: 'checkboxFieldAndApproveButton',
    },
  ];
  surcharges: any[] = [];
  exportData = {
    headers: this.spreadsheetHeaders,
    rows: this.surcharges,
    filename: 'surcharge.xlsx',
  };
  titleHeader = '';
  subtitleTextGroup: { label: string; value: string }[] = [];
  headers!: any;
  referenceNumber = '';

  constructor(
    private surchargeService: SurchargeService,
    private popupService: PopupService,
    private formulaCardService: FormulaCardService,
    private route: ActivatedRoute,
    private approveAmendedRoadTaxService: ApproveAmendedRoadTaxService
  ) {}

  ngOnInit(): void {
    this.subscribeToRouteParams();
  }

  subscribeToRouteParams() {
    this.route.params.subscribe((params: Params) => {
      this.referenceNumber = params.transactionRefNum;
      this.initializeSurcharges();
    });
  }

  initializeSurcharges() {
    this.surchargeService.getSurcharge().subscribe(
      (response) => {
        this.surcharges = response.values;
        this.headers = response.headers;
        this.setTitleHeader();
        this.setSubtitleTextGroup();
      },
      (error: Error) =>
        this.formulaCardService.openErrorPopup(
          error.message || 'Cannot get surcharge'
        )
    );
  }

  setTitleHeader() {
    this.titleHeader = 'Review Amended Road Tax Formula Value(s) REF NO. ';
    this.titleHeader = this.titleHeader + this.referenceNumber;
  }

  setSubtitleTextGroup() {
    this.subtitleTextGroup = [];
    const { requestedBy, submittedOn, propellant } = this.headers;
    this.subtitleTextGroup.push({
      label: 'Amendment Requested by:',
      value: requestedBy,
    });
    this.subtitleTextGroup.push({
      label: 'Submitted on:',
      value: submittedOn,
    });
    this.subtitleTextGroup.push({
      label: 'Propellant / Component Type:',
      value: propellant,
    });
  }

  closeAllRows() {
    this.surcharges = this.surcharges.map((fee) => {
      fee.isExpanded = false;
      return fee;
    });
  }

  openConfirmationPopup(surcharges: any[]) {
    const dialogHeaders = [
      {
        label: 'Surcharge',
        property: 'annualSurcharge',
      },
      {
        label: 'Effective Date:',
        property: 'effectiveDate',
      },
      {
        label: 'End Date:',
        property: 'endDate',
      },
    ];
    const displayedRows: Row[][] = [];
    surcharges.forEach((surcharge: any) => {
      const { vehicleAge, approved, remarks } = surcharge;
      const row: Row[] = [
        {
          label: 'Age of Vehicle',
          value: vehicleAge,
          subvalue: '',
        },
      ];
      Object.values(dialogHeaders).forEach((header) => {
        let value = surcharge[header.property];
        if (header.property.includes('Date')) {
          value = this.formulaCardService.formatDate(value);
        }
        if (header.property.includes('annualSurcharge')) {
          value = value + '% of Road Tax';
        }
        row.push({ label: header.label, value, subvalue: '' });
      });
      const mark = approved ? 'Yes' : 'No';
      row.push({ label: 'Approved:', value: mark, subvalue: '', remarks });
      displayedRows.push(row);
    });
    const uiData: UiData = {
      icon: 'error',
      header: 'PROCEED TO APPROVE VALUES?',
      subheader:
        'Road Tax Values will proceed to take effect during the effective period once approved',
      displayedRows,
      apiPayload: surcharges,
      buttons: [
        {
          text: 'Back to Main',
          function: 'close',
          class: 'blue-button',
        },
        {
          text: 'Approve',
          function: 'submit',
          class: 'blue-button',
        },
      ],
    };
    return this.popupService.openConfirmation(uiData);
  }

  openSuccessPopup() {
    const uiData = {
      icon: 'check_circle',
      message: 'FEEDBACK AND APPROVAL(S) HAVE BEEN SUBMITTED SUCCESSFULLY',
      buttonText: 'Back to Main',
    };
    this.popupService.openSuccess(uiData);
  }

  onApproveButtonClick(surcharges: any) {
    const amendedSurcharges = surcharges
      .filter(
        (surcharge: any) =>
          surcharge?.approveCheckbox || surcharge?.remarksFieldValue
      )
      .map((surcharge: any) => ({
        ...surcharge.fullSurcharge,
        newValue: surcharge.newValue,
        approved: !!surcharge?.approveCheckbox,
        remarks: surcharge?.remarksFieldValue || '',
      }));
    if (!amendedSurcharges.length) {
      const message = 'Approve or reject rows first.';
      return this.formulaCardService.openErrorPopup(message);
    }
    const confirmationPopup = this.openConfirmationPopup(amendedSurcharges);
    confirmationPopup.afterClosed().subscribe((data) => {
      if (data) {
        this.formulaCardService.approveRoadTaxFees(amendedSurcharges).subscribe(
          () => {
            const message =
              'FEEDBACK AND APPROVAL(S) HAVE BEEN SUBMITTED SUCCESSFULLY';
            this.formulaCardService.openSuccessPopup(message);
            this.initializeSurcharges();
          },
          (error: Error) =>
            this.formulaCardService.openErrorPopup(error.message)
        );
      }
    });
  }

  onToggleCollapse(data: any) {
    const { collapsedFee } = data;
    if (!collapsedFee.isExpanded) {
      this.closeAllRows();
    }
    this.surcharges = this.surcharges.map((fee, index) => {
      if (
        fee.parentIndex === collapsedFee.index ||
        collapsedFee.index === index
      ) {
        fee.isExpanded = !fee.isExpanded;
      }
      return fee;
    });
  }

  updateRemarksFieldAndApproveButton(
    index: number,
    remarksFieldValue: string,
    approveCheckbox: boolean
  ) {
    const changedSurcharge = this.surcharges[index];
    changedSurcharge.remarksFieldValue = remarksFieldValue;
    changedSurcharge.approveCheckbox = approveCheckbox;
  }

  onRemarksFieldChange(changed: any) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.updateRemarksFieldAndApproveButton(
      index,
      remarksFieldValue,
      approveCheckbox
    );
  }

  onApproveCheckboxChange(changed: any) {
    const { index, remarksFieldValue, approveCheckbox } = changed;
    this.updateRemarksFieldAndApproveButton(
      index,
      remarksFieldValue,
      approveCheckbox
    );
  }

  onViewOverPaymentReportClick() {
    const fees = this.surcharges;
    const parameters = this.formulaCardService.getOverpaymentParameters(fees);
    const { levels, ids } = parameters;
    if (!levels || !ids) {
      return this.formulaCardService.openErrorPopup(
        'No existing rows for approval'
      );
    }
    this.surchargeService.getOverpaymentReport(levels, ids).subscribe(
      (response) => {
        if (!response.header) {
          return this.formulaCardService.openErrorPopup('Empty rows');
        }
        this.approveAmendedRoadTaxService.openOverpaymentReportPopup(response);
      },
      (error: Error) => {
        const message = error.message || 'Cannot get overpayment report data';
        this.formulaCardService.openErrorPopup(message);
      }
    );
  }
}
