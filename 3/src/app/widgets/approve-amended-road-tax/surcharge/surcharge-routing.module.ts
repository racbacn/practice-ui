import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SurchargeComponent } from './surcharge.component';

const routes: Routes = [
  {
    path: '',
    component: SurchargeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SurchargeRoutingModule {}
