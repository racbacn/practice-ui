import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SpreadsheetData } from '../formula-card/formula-card-export/formula-card-export.component';
import { PopupComponent } from '../overpayment-report/popup/popup.component';

export interface Header {
  label: string;
  property: string;
  width: string;
}

interface CellTooltip {
  header: string;
  subheader: string;
}

export interface TableCell {
  label: string;
  tooltip?: CellTooltip;
}

export interface TableRow {
  [key: string]: TableCell;
}

interface TableData {
  headers: Header[];
  rows: TableRow[];
}

export interface PopupData {
  header: string;
  subheader: string;
  table: TableData;
  spreadsheet: SpreadsheetData;
}

interface ResponseHeader {
  responseAppId: string;
  responseDateTime: string;
}

interface RoadTaxPayment {
  vehicleNumber: string;
  engineCapacity: string;
  powerRating: string;
  roadTaxPaymentDate: string;
  licensingStartDate: string;
  licensingEndDate: string;
  roadTaxPaymentAmount: string;
  newRoadTaxPaymentAmount: string;
  sixMonthBaseFormulaUsedForNewRoadTax: string[];
  overPaymentAmount: string;
}

type OverpaymentEndpoint = 'petrol' | 'diesel' | 'electric' | 'surcharge';

export interface OverpaymentReportResponse {
  responseHeader: ResponseHeader;
  roadTaxPayments: RoadTaxPayment[];
}

@Injectable({
  providedIn: 'root',
})
export class ApproveAmendedRoadTaxService {
  private SERVER_URL = environment.serverUrl;

  OVERPAYMENT_REPORT = {
    header: 'Vehicles Affected by Overpayment',
    subheader:
      'The following Vehicles will be affected by the proposed road tax value change(s)',
    tableHeaders: [
      { label: 'Vehicle Number', property: 'vehicleNumber', width: '15%' },
      {
        label: 'Engine Capacity / Power Rating',
        property: 'engineCapacityPowerRating',
        width: '15%',
      },
      {
        label: 'Road Tax Payment Date',
        property: 'roadTaxPaymentDate',
        width: '12%',
      },
      {
        label: 'Road Tax Validity Period',
        property: 'roadTaxValidityPeriod',
        width: '20%',
      },
      {
        label: 'Road Tax Payment Amt. (A)',
        property: 'roadTaxPaymentAmount',
        width: '13%',
      },
      {
        label: 'NEW Road Tax Payment Amt. (B)',
        property: 'newRoadTaxPaymentAmount',
        width: '15%',
      },
      {
        label: 'Overpayment Amt. (A - B)',
        property: 'overPaymentAmount',
        width: '10%',
      },
    ],
    baseEndpoint: '/manage-roadtax-service/generateOverpayment',
    endpoints: {
      petrol: '/petrol',
      diesel: '/diesel',
      electric: '/electric',
      surcharge: '/surcharge',
    },
  };

  constructor(private dialog: MatDialog, private http: HttpClient) {}

  setSpreadsheetRows(rows: any[]) {
    return rows.map((row) => {
      const { vehicleNumber, engineCapacityPowerRating } = row;
      const { roadTaxPaymentDate, roadTaxValidityPeriod } = row;
      const { roadTaxPaymentAmount, newRoadTaxPaymentAmount } = row;
      const { overPaymentAmount } = row;
      const { header, subheader } = newRoadTaxPaymentAmount.tooltip;
      const tooltip = header + subheader;
      return {
        vehicleNumber: vehicleNumber.label,
        engineCapacityPowerRating: engineCapacityPowerRating.label,
        roadTaxPaymentDate: roadTaxPaymentDate.label,
        roadTaxValidityPeriod: roadTaxValidityPeriod.label,
        roadTaxPaymentAmount: roadTaxPaymentAmount.label,
        newRoadTaxPaymentAmount:
          newRoadTaxPaymentAmount.label + ` (${tooltip})`,
        overPaymentAmount: overPaymentAmount.label,
      };
    });
  }

  openOverpaymentReportPopup(data: PopupData) {
    this.dialog.open(PopupComponent, {
      data,
      height: '790px',
      width: '1767px',
      disableClose: true,
      autoFocus: false,
    });
  }

  setOverpaymentReportRows(roadTaxPayments: RoadTaxPayment[]): TableRow[] {
    return roadTaxPayments.map((payment) => {
      const { vehicleNumber, engineCapacity, powerRating } = payment;
      const { roadTaxPaymentDate, roadTaxPaymentAmount } = payment;
      const { licensingStartDate, licensingEndDate } = payment;
      const { newRoadTaxPaymentAmount, overPaymentAmount } = payment;
      const { sixMonthBaseFormulaUsedForNewRoadTax } = payment;
      const power = powerRating === '0.0' ? engineCapacity : powerRating;
      const roadTaxValidityPeriod = `${licensingStartDate} - ${licensingEndDate}`;
      const label = newRoadTaxPaymentAmount;
      const header = 'Formula Applied: ';
      const subheader = sixMonthBaseFormulaUsedForNewRoadTax.join(', ');
      return {
        vehicleNumber: { label: vehicleNumber },
        engineCapacityPowerRating: { label: power },
        roadTaxPaymentDate: { label: roadTaxPaymentDate },
        roadTaxValidityPeriod: { label: roadTaxValidityPeriod },
        roadTaxPaymentAmount: { label: roadTaxPaymentAmount },
        newRoadTaxPaymentAmount: { label, tooltip: { header, subheader } },
        overPaymentAmount: { label: overPaymentAmount },
      };
    });
  }

  getApiUrl(endpoint: OverpaymentEndpoint) {
    const base = this.OVERPAYMENT_REPORT.baseEndpoint;
    const type = this.OVERPAYMENT_REPORT.endpoints[endpoint];
    return this.SERVER_URL + base + type;
  }

  getOverpaymentReport(
    params: HttpParams,
    endpoint: OverpaymentEndpoint
  ): Observable<PopupData> {
    const apiUrl = this.getApiUrl(endpoint);
    return this.http.get<OverpaymentReportResponse>(apiUrl, { params }).pipe(
      map((response) => {
        if (!response.roadTaxPayments.length) {
          return {
            header: '',
            subheader: '',
            table: { headers: [], rows: [] },
            spreadsheet: { headers: [], rows: [], filename: '' },
          };
        }
        const headers = this.OVERPAYMENT_REPORT.tableHeaders;
        const rows = this.setOverpaymentReportRows(response.roadTaxPayments);
        const spreadsheetRows = this.setSpreadsheetRows(rows);
        const filename = `${endpoint}-overpayment-report.xlsx`;
        return {
          header: this.OVERPAYMENT_REPORT.header,
          subheader: this.OVERPAYMENT_REPORT.subheader,
          table: { headers, rows },
          spreadsheet: { headers, rows: spreadsheetRows, filename },
        };
      })
    );
  }
}
