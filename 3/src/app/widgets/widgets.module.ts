import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../app/material.module';
import { CardComponent } from './card/card.component';
import { TableComponent } from './table/table.component';
import { ButtonComponent } from './button/button.component';
import { DashboardSidenavComponent } from './dashboard-sidenav/dashboard-sidenav.component';
import { RouterModule, Routes } from '@angular/router';
import { ExpansionPanelComponent } from './expansion-panel/expansion-panel.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { TextFieldComponent } from './text-field/text-field.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ApproveRejectDialogComponent } from './approve-reject-dialog/approve-reject-dialog.component';
import { DialogComponent } from './dialog/dialog.component';
import { SharedModule } from '../shared/shared.module';
import { ButtonDialogComponent } from './button-dialog/button-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { DashboardGreetingComponent } from './dashboard-greeting/dashboard-greeting.component';
import { ErrorComponent } from './error/error.component';
import { SearchFormComponent } from  './search-form/search-form.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { DialogPmdRejectComponent } from './dialog-pmd-reject/dialog-pmd-reject.component';
import { ExportXlsDialogComponent } from './export-xls-dialog/export-xls-dialog.component';
import { PaginationComponent } from './pagination/pagination.component';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatExpansionModule
  ],
  declarations: [
    ButtonComponent,
    CardComponent,
    TableComponent,
    DashboardSidenavComponent,
    ExpansionPanelComponent,
    DropdownComponent,
    TextFieldComponent,
    ModalComponent,
    ApproveRejectDialogComponent,
    DialogComponent,
    ButtonDialogComponent,
    ConfirmDialogComponent,
    ErrorComponent,
    SearchFormComponent,
    DashboardGreetingComponent,
    DialogPmdRejectComponent,
    ExportXlsDialogComponent,
    PaginationComponent
  ],
  exports: [
    ButtonComponent,
    CardComponent,
    TableComponent,
    DashboardSidenavComponent,
    ExpansionPanelComponent,
    DropdownComponent,
    TextFieldComponent,
    ModalComponent,
    FlexLayoutModule,
    MaterialModule,
    DialogComponent,
    ConfirmDialogComponent,
    ErrorComponent,
    SearchFormComponent,
    DashboardGreetingComponent,
    PaginationComponent
  ],
})
export class WidgetsModule {}
