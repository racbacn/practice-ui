import { Component, OnInit, Inject, Output, EventEmitter, Optional } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  remarksControl = new FormControl();
  @Output() approveBTN: EventEmitter<any> = new EventEmitter<any>();

  approveButtonConfig = {
    buttonText: 'Approve',
    hasIcon: true,
    iconName: 'check_circle',
    buttonClassName: 'proceedButton',
    buttonFont: 'button-font-16'
  };

  cancelButtonConfig = {
    buttonText: 'Cancel',
    hasIcon: true,
    iconName: 'cancel',
    buttonClassName: 'dashboardButton',
    buttonFont: 'button-font-16',
  };

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {
      icon:string;
      tick:string;
      title: string;
      details: string;
    }, public dialog: MatDialogRef<ConfirmDialogComponent>, private sharedService: SharedService, @Optional()  @Inject(MAT_DIALOG_DATA) public label: any) 
    {
      dialog.disableClose = true;
      this.label = label.pageValue}

  ngOnInit(): void {}

  onClose() {
    this.dialog.close();
}

approve(){
  let obj = {
    transactionId: this.data.details
  }
  this.dialog.close("approve");
}
}



