import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExportXlsService } from 'src/app/services/export-xls.service';

interface DialogData {
  message: string;
  showIcon: boolean;
  showDownloadButton: boolean;
}

@Component({
  selector: 'app-export-xls-dialog',
  templateUrl: './export-xls-dialog.component.html',
  styleUrls: ['./export-xls-dialog.component.scss'],
})
export class ExportXlsDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private exportXlsService: ExportXlsService
  ) {}

  ngOnInit(): void {}

  download() {
    this.exportXlsService.downloadXls();
  }
}
