import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportXlsDialogComponent } from './export-xls-dialog.component';

describe('ExportXlsDialogComponent', () => {
  let component: ExportXlsDialogComponent;
  let fixture: ComponentFixture<ExportXlsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportXlsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportXlsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
