import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupComponent } from './popup.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormulaCardExportModule } from '../../formula-card/formula-card-export/formula-card-export.module';

@NgModule({
  declarations: [PopupComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormulaCardExportModule,
  ],
})
export class PopupModule {}
