import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Header,
  PopupData,
  TableRow,
} from '../../approve-amended-road-tax/approve-amended-road-tax.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})
export class PopupComponent implements OnInit {
  displayedColumns: string[] = [];
  show = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: PopupData) {}

  ngOnInit(): void {
    this.initializeHeaders();
    this.show = true;
  }

  initializeHeaders() {
    const headers = this.data.table.headers;
    if (headers.length) {
      this.displayedColumns = headers.map((header: Header) => header.property);
    }
  }

  getLabel(element: TableRow, header: Header) {
    return element[header.property].label;
  }

  getTooltip(element: TableRow, header: Header) {
    return element[header.property].tooltip;
  }

  getTooltipText(element: TableRow, header: Header) {
    const tooltip = element[header.property].tooltip;
    if (tooltip) {
      return tooltip?.header + tooltip?.subheader;
    }
    return '';
  }
}
