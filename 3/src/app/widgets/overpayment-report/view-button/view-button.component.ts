import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-view-button',
  templateUrl: './view-button.component.html',
  styleUrls: ['./view-button.component.scss'],
})
export class ViewButtonComponent implements OnInit {
  @Output() viewOverPaymentReportClick: EventEmitter<null> =
    new EventEmitter<null>();

  constructor() {}

  ngOnInit(): void {}

  viewOverpaymentReport() {
    this.viewOverPaymentReportClick.emit();
  }
}
