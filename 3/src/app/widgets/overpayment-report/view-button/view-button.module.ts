import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewButtonComponent } from './view-button.component';
import { MaterialModule } from 'src/app/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ViewButtonComponent],
  imports: [CommonModule, MaterialModule, FlexLayoutModule],
  exports: [ViewButtonComponent],
})
export class ViewButtonModule {}
