export interface VehicleTransaction {
    transactionRefNum: string;
    vehicleNumber: string;
    transactionType: number;
    transactionTypeDesc: string;
    transactionAmt: number;
    logDateTime: string;
    ownerId: string;
    ownerName: string;
    transactionStatusType: number;
    transactionStatusTypeDesc: string;
    closestRecord: boolean;
    likelihoodOfApproval: number;
}
