import {OwnerDetails} from "./owner-details";

export interface CertifyRefundResponse {
    transactionRefNum: string;
    transactionId: number;
    vehicleNumber: string;
    refundRemarks: string;
    refundMethodTypeKey: number;
    refundMethodTypeDesc: string;
    assignedOfficerId: number;
    createdBy: string;
    createdDateTime: string;
    refundTypeKey: number;
    refundTypeDesc: string;
    refundAmountBeforeGST: number;
    refundAmountAfterGST: number;
    refundGSTAmount: number;
    certifyStatusTypeKey: number;
    certifyStatusTypeDescription: string;
    ownerDetails: OwnerDetails;
}
