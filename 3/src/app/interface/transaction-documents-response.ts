import {ResponseHeader} from "./response-header";
import {TransactionDocument} from "./transaction-document";

export interface TransactionDocumentsResponse {
    responseHeader: ResponseHeader;
    transactionId: number;
    transactionDocuments: Array<TransactionDocument>;
}
