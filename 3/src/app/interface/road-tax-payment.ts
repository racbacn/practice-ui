import {BaseRoadTaxAmount} from './base-road-tax-amount';


export interface RoadTaxPayment {
    vehicleRefNum: string,
    finalRoadTaxAmount: string,
    baseRoadTaxAmount: string,
    surcharge: string,
    surchargeRate:string,
    lateFee: string,
    baseRoadTaxAmountBreakdown:Array<BaseRoadTaxAmount>
}




