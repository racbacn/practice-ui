import {TableHeader} from "./table-header";

export interface TableHeaders {
    transactionNo: TableHeader;
    transactionType: TableHeader;
    status: TableHeader;
    assignedto: TableHeader;
    approval: TableHeader;
    duedate: TableHeader;
    transactionDate: TableHeader;
    registrationNo: TableHeader;
    ownerId: TableHeader;
    dateReg: TableHeader;
    timeReg: TableHeader;
    vehicleType: TableHeader;
    regMarkId: TableHeader;
    transactionId: TableHeader;
    transactionRefNum: TableHeader;
    refundMethodDescription: TableHeader;
    createdBy: TableHeader;
    gstAmount: TableHeader;
    createdDateTime: TableHeader;
    certifyStatusTypeDescription: TableHeader;
    select?: TableHeader;
    actions?: TableHeader;
}
