import {CertifyRefundRecord} from "./certify-refund-record";

export interface CertifyRoadTaxRefundTable {
    totalRecords: number;
    totalPages: number;
    pageNo: number;
    pageSize: number
    certifyRefundRecords: Array<CertifyRefundRecord>;
}
