export interface CertifyRefundRecord {
    transactionId: number;
    refundMethodKey: number;
    refundMethodDescription: string;
    transactionRefNum: string;
    createdBy: string;
    createdDateTime: string;
    amountBeforeGST: number;
    gstAmount: number;
    amountAfterGST: number;
    certifyStatusTypeKey: number;
    certifyStatusTypeDescription: string;
}
