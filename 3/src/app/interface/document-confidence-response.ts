import {DocumentConfidence} from "./document-confidence";
import {DocumentError} from "./document-error";
import {ResponseHeader} from "./response-header";

export interface DocumentConfidenceResponse {
    responseHeader: ResponseHeader;
    documentsInError: Array<DocumentError>
    documentConfidenceList: Array<DocumentConfidence>;
}
