import {ValueConfidence} from "./value-confidence";

export interface DocumentConfidence {
    documentId: number,
    documentRefNum: string,
    documentConfidenceAvg: number,
    documentTypeKey: number,
    fieldValueConfidence: Array<ValueConfidence>
}
