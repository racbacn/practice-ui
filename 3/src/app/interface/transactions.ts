export interface Transactions {
    transactionId: string;
    assignedUserId: string;
    sourceTransactionId: string;
    transactionRefNum: string;
    likelihoodOfApproval: string;
    dueDate: string;
    transactionDate: string;
    transactionType: string;
    transactionStatus: string;
}

export interface RegisteredPMD {
    registrationNo: string,
    transactionStatus: string,
    ownerId: number,
    sourceTransactionId: string,
    dateRegistered: string,
    timeRegistered: string,
    vehicleType: string,
    registrationMarkId: string,
    assignedToId: number
}

export interface RegisteredVehicles {
    registrationNo: string,
    transactionStatus: string,
    ownerId: number,
    sourceTransactionId: string,
    dateRegistered: string,
    timeRegistered: string,
    vehicleType: string,
    registrationMarkId: string,
    assignedToId: number
}