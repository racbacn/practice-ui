export interface DocumentAttributeConfidence {
    dacId: number;
    resultFlagKey: number;
}
