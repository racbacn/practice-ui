import {CertifyRefundRequestHeader} from "./certify-refund-request-header";

export interface ApproveCertifyRefundRequest {
    requestHeader: CertifyRefundRequestHeader;
    transactionId: number;
}
