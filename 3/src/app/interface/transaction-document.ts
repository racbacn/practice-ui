export interface TransactionDocument {
    documentId: number;
    documentRefNum: string;
    fileName: string;
}
