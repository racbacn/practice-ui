import {SelectInput} from "./select-input";

export interface SearchInput {
    formControlName: string;
    label: string;
    type: string;
    selectInputList: Array<SelectInput>;
}
