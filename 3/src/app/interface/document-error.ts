export interface DocumentError {
    documentId: number;
    documentErrorCode: number;
    documentErrorMessage: string;
}
