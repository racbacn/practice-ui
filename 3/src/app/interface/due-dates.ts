export interface TransactionDueDates {
    dueDateToday: number;
    dueDateTomorrow: number;
    dueDateWeek: number;
    dueDateMonth: number;
}