export interface  CalculatorInputs {
    vehicleRefNum: string,
    vehiclePropellantTypeKey:number,
    engineCapacity: string,
    powerRating: string,
    emissionStandardCategoryKey:number,
    vehicleAge: number,
    licensingStartDate: string,
    licensingEndDate:string,
    vehicleRegistrationDate:string
   }
   