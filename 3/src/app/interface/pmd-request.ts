import {RequestHeader} from "./request-header";
import {PmdDetails} from "./pmd-details";
import {OwnerDetails} from "./owner-details";
import { TransactionDocument } from "./transaction-document";

export interface PMDRequest {
    requestHeader: RequestHeader;
    ownerDetails: OwnerDetails;
    PmdDetails: PmdDetails;
    pmdRegistrationDocuments: TransactionDocument[];
}
