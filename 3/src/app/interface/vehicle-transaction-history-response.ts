import {VehicleTransaction} from "./vehicle-transaction";

export interface VehicleTransactionHistoryResponse {
    vehicleTransactionHistory: Array<VehicleTransaction>
}
