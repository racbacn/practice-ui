export interface rejectRegistration {
  requestHeader: {
    "requestAppId": string;
    "requestDateTime": string;
    "userId": number;
    "userTypeKey": number;
  },
  transactionId: string;
  comment: string;
}
