export interface SelectInput {
    name: string;
    value: string;
}
