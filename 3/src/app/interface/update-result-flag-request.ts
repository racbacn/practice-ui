import {DocumentAttributeConfidence} from "./document-attribute-confidence";

export interface UpdateResultFlagRequest {
    documentAttributeConfidence: Array<DocumentAttributeConfidence>;
    documentId: number;
}
