export interface ExpansionTableHeader {
    label: string;
    property: string;
}
