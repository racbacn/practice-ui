import { RequestHeader } from "./request-header";
import {Notification} from "./notification";

export interface NotificationResponse {
  responseHeader: RequestHeader,
  userid: string,
  notifications: Array <Notification>
}


