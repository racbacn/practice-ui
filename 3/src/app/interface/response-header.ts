export interface ResponseHeader {
    responseAppId: string;
    responseDateTime: string;
}
