export interface CertifyRefundRequestHeader {
    ltaTeamTypeKey: number;
    managerId: number;
    userId: number;
    userTypeKey: number
}
