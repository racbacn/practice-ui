import {ResponseHeader} from "./response-header";

export interface UserResponse {
    responseHeader: ResponseHeader;
    userId: number;
    userTypeKey: number;
    username: string;
    ltaTeamTypeKey: number;
}
