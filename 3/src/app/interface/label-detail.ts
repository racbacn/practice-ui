export interface LabelDetail {
    headerName: string;
    values: Array<string>;
    labels: Array<string>;
}
