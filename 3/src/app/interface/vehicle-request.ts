import {RequestHeader} from "./request-header";
import {VehicleDetails} from "./vehicle-details";
import {OwnerDetails} from "./owner-details";
import {RoadTaxDetails} from "./road-tax-details";

export interface VehicleRequest {
    requestHeader: RequestHeader;
    ownerDetails: OwnerDetails;
    vehicleDetails: VehicleDetails;
    roadTaxDetails: RoadTaxDetails;
}
