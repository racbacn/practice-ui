export interface TableHeader {
    label: string;
    property: string;
}
