export interface ConfidenceTable {
    key: string;
    documentField: string;
    approveCriteria: string;
    retrievedValue: string;
    confidenceScore: string;
    dacId: number;
    result: number;
}
