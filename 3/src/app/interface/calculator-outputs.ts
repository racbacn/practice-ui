import { RequestHeader } from "src/app/interface/request-header";
import { RoadTaxPayment} from "./road-tax-payment";

export interface CalculatorOutputs {
 
  requestHeader:RequestHeader;
  roadTaxPayments: Array<RoadTaxPayment>
}