export interface RequestHeader {
    requestAppId: string;
    requestDateTime: string;
    userId: string;
    userTypeKey: number;
}
