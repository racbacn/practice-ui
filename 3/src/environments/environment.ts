// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  serverUrl: 'https://istio-ingressgateway-istio-system.apps.a6067fe2802840199acec1f28c4573a1.vehsystempoc.com',
  aws: {
    accessKeyId: 'AKIA3ICF4HAUFYKYJG67',
    secretAccessKey: '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',
    s3: {
      bucket: 'textract-test-vrls',
      region: 'ap-southeast-1',
    }
  },
  pmdBucket: {
    s3: {
      bucket: 'rekognition-test-vrls',
      region: 'ap-southeast-1',
    }
  },
  auth: {
    region: 'ap-southeast-1',
    userPoolId: 'ap-southeast-1_7cm9vB3jm',
    userPoolWebClientId: 'e1mf3440bcsome3oi7c96k8fs',
    authenticationFlowType: "USER_PASSWORD_AUTH"
  },
  sso_api_username: '6hnpjddbuuhf3mo7arh66g04b3',
  sso_api_pwd: '1bpp3ndn24uq9f02psm50pf8lm69ch86midk11fnd7japuu2380s',
  loginUrl: 'https://auth-vehsystempoc.auth.ap-southeast-1.amazoncognito.com/login?' +
      'client_id=6hnpjddbuuhf3mo7arh66g04b3&response_type=code&scope=openid+profile&' +
      'redirect_uri=http://localhost:4200/callback',
  logoutUrl: 'https://auth-vehsystempoc.auth.ap-southeast-1.amazoncognito.com/' +
      'logout?client_id=6hnpjddbuuhf3mo7arh66g04b3&logout_uri=http://localhost:4200',
  cognitoTokenURL: 'https://auth-vehsystempoc.auth.ap-southeast-1.amazoncognito.com/oauth2/token',
  redirectUrl: 'http://localhost:4200/callback'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
